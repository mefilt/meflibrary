//
//  MEFRootViewConroller.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 24.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFRootViewConroller.h"

@interface MEFRootViewConroller () <MEFDrawerCardViewControllerDelegate>
@property (weak, nonatomic) IBOutlet MEFSegmentedControl *segmentedControl;

@end

@implementation MEFRootViewConroller

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self loadDrawer];

}



- (void) cleanup
{
    self.drawerViewController = nil;
}

- (void) loadDrawer
{
    [self cleanup];
    
//    [self setViewController:[[MEFCalendarViewController alloc]initCalendar] animateType:0];
    

//    [self.segmentedControl addTitle:@"День"];
//    [self.segmentedControl addTitle:@"Неделя"];
//    [self.segmentedControl addTitle:@"Месяц"];
//    [self.segmentedControl addTitle:@"Произвольная"];
//    [self.segmentedControl reloadSegments];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller
    
}


#pragma mark - - 

- (CGFloat) widthForDrawerViewController
{
    return 280;
}

@end
