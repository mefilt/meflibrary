//
//  MEFTestDrawerViewController.h
//  MEFLibrary
//
//  Created by Mefilt on 19.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <MEFLibrary/MEFContainerViewController.h>

@interface MEFTestDrawerViewController : MEFContainerViewController

@end
