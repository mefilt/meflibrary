//
//  MEFTestDrawerViewController.m
//  MEFLibrary
//
//  Created by Mefilt on 19.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFTestDrawerViewController.h"
#import "MEFTestMainViewController.h"
#import "MEFTestMenuViewController.h"
#import <MEFLibrary/MEFDrawerViewController.h>
@interface MEFTestDrawerViewController () <MEFDrawerCardViewControllerDelegate>

@end

@implementation MEFTestDrawerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    MEFTestMainViewController *main = [[self storyboard]instantiateViewControllerWithIdentifier:@"Main"];
    MEFTestMenuViewController *menu = [[self storyboard]instantiateViewControllerWithIdentifier:@"Menu"];
    
    MEFDrawerViewController *drawer = [[MEFDrawerViewController alloc]initWithDrawerViewController:menu mainController:main];
    [drawer setDelegate:self];
    __weak MEFDrawerViewController *weakDrawer = drawer;
    [main setDidTouch:^{
        [weakDrawer showDrawerViewControllerWithAnimated:true];
    }];
    
    [menu setDidTouch:^{
        [weakDrawer hideDrawerViewControllerWithAnimated:true];
    }];

    [self setViewController:drawer animateType:0];
    // Do any additional setup after loading the view.
}
- (CGFloat) widthForDrawerViewController
{
    return self.view.frame.size.width * 0.7;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
