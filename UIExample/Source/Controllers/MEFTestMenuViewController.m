//
//  MEFTestMenuViewController.m
//  MEFLibrary
//
//  Created by Mefilt on 19.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFTestMenuViewController.h"

@interface MEFTestMenuViewController ()

@end

@implementation MEFTestMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)action:(id)sender
{
    self.didTouch();
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
