//
//  MEFRootViewConroller.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 24.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFContainerViewController.h"
#import "MEFDrawerViewController.h"
#import "MEFSegmentedControl.h"
#import "MEFContainerSegue.h"
@interface MEFRootViewConroller : MEFContainerViewController
{
    
}
@property (nonatomic, readwrite, strong) MEFDrawerViewController *drawerViewController;
@end
