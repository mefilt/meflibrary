//
//  MEFMenuViewController.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 24.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFMenuViewController.h"
#import "MEFBaseTableViewController.h"
#import "MEFStoryboardIdentifiers.h"
@interface MEFMenuViewController () <MEFBaseTableViewControllerDelegate, MEFBaseTableViewControllerDatasource>

@property (nonatomic, readwrite, strong) MEFBaseTableViewController *tableViewController;
@end

@implementation MEFMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableViewController = [[self storyboard] instantiateViewControllerWithIdentifier:MEFStoryboardBaseTable];
    
    
    [self setViewController:self.tableViewController animateType:MEFContainerViewAnimateTypeNone];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - - table delegate


@end
