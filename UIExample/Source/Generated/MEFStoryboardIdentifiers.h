/* Generated document. DO NOT CHANGE */

/* Segue identifier constants */
@class NSString;

extern NSString * const MEFContainerRoot;

/* Controller identifier constants */
extern NSString * const MEFStoryboardBaseTable;
extern NSString * const MEFStoryboardDrawer;
extern NSString * const MEFStoryboardMenu;

/* Reuse identifier constants */
