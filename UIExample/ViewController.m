//
//  ViewController.m
//  UIExample
//
//  Created by Prokofev Ruslan on 25.04.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "ViewController.h"
#import <MEFLibrary/MEFBaseCollectionViewController.h>
@interface ViewController () <MEFBaseCollectionViewControllerDatasource, MEFBaseCollectionViewControllerDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    MEFBaseCollectionViewController *collection = [[self storyboard]instantiateViewControllerWithIdentifier:@"baseCollection"];
    [collection setDelegate:self];
    [collection setDatasource:self];
    [self setViewController:collection animateType:0];
    
    MEFObject *section = [MEFObject objectByType:0];
    for (int i = 0; i < 10; i++) {
        MEFObject *object = [MEFObject objectByType:0];
        object.data = [NSString stringWithFormat:@"object - %d",i];
        [section addItem:object];
    }
    [collection addSection:section];
    [collection reloadData];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - - data source

- (void) baseCollectionViewController:(MEFBaseCollectionViewController *)controller configCollectionViewCellWithObject:(MEFObject *)object cellForItemAtIndexPath:(NSIndexPath *)indexPath cell:(UICollectionViewCell *)cell
{
 
}

- (CGSize) baseCollectionViewController:(MEFBaseCollectionViewController*)controller sizeForItemAtIndexPath:(NSIndexPath *)indexPath layout:(UICollectionViewLayout  *)collectionViewLayout;
{
    return CGSizeMake(self.view.frame.size.width, 60);
}
    

- (NSString*) baseCollectionViewController:(MEFBaseCollectionViewController *)controller reuseIdentifierWithObject:(MEFObject *)object
{
    return @"test";
}
@end
