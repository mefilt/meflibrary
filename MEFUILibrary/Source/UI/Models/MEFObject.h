//
//  MEFObject.h
//  iphone_project
//
//  Created by Прокофьев Руслан on 05.08.13.
//  Copyright (c) 2013 Gevorg Petrosian. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MEFObject;
typedef uint MEFSortType;
typedef NSComparisonResult (^MEFObjectSort) (MEFObject *object1 ,MEFObject *object2);
typedef BOOL (^MEFObjectSearch) (MEFObject *object, NSString *text);
typedef MEFObject* (^MEFObjectGrouping) (MEFObject *object,MEFObject *parent);

@interface MEFObject : NSObject

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) id data;
@property (nonatomic, weak) MEFObject *parent;
@property (nonatomic, assign) int type;
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) MEFObjectSort sortBlock;
@property (nonatomic, copy) MEFObjectSearch searchBlock;

#pragma mark - - Conveince constuctor
+ (MEFObject*) objectByType:(int)type;
+ (MEFObject*) objectByType:(int)type key:(NSString*)key;
+ (MEFObject*) objectByType:(int)type key:(NSString *)key data:(id)data;


#pragma mark - - add methods
- (void) addItem:(MEFObject*) object;
- (void) addItemByIndex:(NSInteger)row object:(MEFObject*) object;
- (void) addItems:(NSArray*) objects;
- (void) insertItems:(NSArray *)objects index:(NSUInteger)index;
#pragma mark - - remove methods
- (void) removeItem:(MEFObject*) object;
- (void) removeItems;

- (void) removeSortItem:(MEFObject*) object;
- (void) restoreBeginState;

#pragma mark - - method for Composition
- (void) onEnter;
- (void) onExitParent;


#pragma mark - - get Object
- (MEFObject*) itemAtIndex:(NSInteger)index;
- (MEFObject*) itemAtKey:(NSString*)key;
- (MEFObject*) itemAtType:(int)type;

/**
    
**/
- (void) reloadObjects;

- (void) sortObjects;

#pragma mark - - search method
- (void) searchObjectsWithString:(NSString*)string;
- (void) cancelSearch;
@end




@interface MEFObject (EditMode)

- (id) copyWithoutItems;
- (NSString*) indentifierObject;


@end

@interface MEFObject (Modify)

- (void) modifyObjects:(NSMutableArray *)objects;
@property (nonatomic, readwrite, copy) void (^modifyObjects)(NSMutableArray *objects);
@end

