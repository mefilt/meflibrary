//
//  MEFObject.m
//  iphone_project
//
//  Created by Прокофьев Руслан on 05.08.13.
//  Copyright (c) 2013 Gevorg Petrosian. All rights reserved.
//

#import "MEFObject.h"
@interface MEFObject ()
@property (strong, nonatomic) NSMutableArray *unsortedItems;
@property (strong, nonatomic) NSMutableArray *findItems;
@property (strong, nonatomic) NSMutableDictionary *hashmap;
@property (assign, nonatomic) BOOL isBeginSearch;
@property (nonatomic, readwrite, copy) void (^modifyObjects)(NSMutableArray *objects);
@end

@implementation MEFObject

- (id) init
{
    if (self = [super init]) {
        self.key = @"-1";
        self.type = -1;
    }
    return self;
}
+ (MEFObject*) objectByType:(int)type
{
    MEFObject *object = [MEFObject new];
    object.type = type;
    return object;
}
+ (MEFObject*) objectByType:(int)type key:(NSString *)key data:(id)data
{
    MEFObject *object = [MEFObject new];
    object.type = type;
    object.key = key;
    object.data = data;
    return object;
}
+ (MEFObject*) objectByType:(int)type key:(NSString *)key
{
    return [MEFObject objectByType:type key:key data:nil];
}


#pragma mark - - add
- (void) addItemByIndex:(NSInteger)row object:(MEFObject*) object
{
    NSInteger count = self.unsortedItems.count;
    if (row < count) {
        [self.unsortedItems insertObject:object atIndex:row];
    } else {
        [self.unsortedItems addObject:object];
    }

    [self.hashmap setObject:object forKey:object.key];
    [object setParent:self];
    [object onEnter];
}

- (void) addItem:(MEFObject *)object
{
    [self.unsortedItems addObject:object];
    if (object.key) {
        [self.hashmap setObject:object forKey:object.key];        
    }

    [object setParent:self];
    [object onEnter];
}

- (void) addItems:(NSArray *)objects
{
    for (MEFObject *object in objects) {
        [self addItem:object];
    }
}


- (void) insertItems:(NSArray *)objects index:(NSUInteger)index
{
    NSInteger count = self.unsortedItems.count;
    if (index < count) {
        [self.unsortedItems insertObjects:objects atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(index, objects.count)]];
    } else {
        [self.unsortedItems addObjectsFromArray:objects];
    }
    
    for (MEFObject *object in objects) {
        [self.hashmap setObject:object forKey:object.key];
        [object setParent:self];
        [object onEnter];
    }

}

- (void) onEnter
{
    [self.items removeAllObjects];
}
- (void) onExitParent
{
    
}



- (void) cancelSearch
{
    [self.findItems removeAllObjects];
    self.isBeginSearch = false;
}

- (void) searchObjectsWithString:(NSString*)string
{
    self.isBeginSearch = true;
    [self.items removeAllObjects];
    [self.findItems removeAllObjects];
    NSMutableArray *sortedElements = [NSMutableArray arrayWithArray:self.unsortedItems];
    
    __weak MEFObject *weakSelf = self;
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(MEFObject *evaluatedObject, NSDictionary *bindings) {
        if (weakSelf.searchBlock){
            return weakSelf.searchBlock(evaluatedObject, string);
        }
        return true;
    }];
    NSArray *obj = [sortedElements filteredArrayUsingPredicate:predicate];
    if (obj.count) {
        [self.findItems addObjectsFromArray:obj];
    }
}


- (void) restoreBeginState;
{
    [self.items removeAllObjects];
}

- (void) removeItems
{
    NSArray *delete = [NSArray arrayWithArray:self.unsortedItems];
    for (MEFObject *object in delete) {
        [self removeItem:object];
    }
}

- (void) removeItem:(MEFObject *)object
{
    [self.unsortedItems removeObject:object];
    [self.hashmap removeObjectForKey:object.key];
    [self reloadObjects];
    [object setParent:nil];
    [object onExitParent];
}


#pragma mark - - getter methods

- (MEFObject*) itemAtType:(int)type;
{
    for (MEFObject *object in self.items) {
        if (object.type == type) {
            return object;
        }
    }
    return nil;
}


- (MEFObject*) itemAtKey:(NSString *)key
{
    return [self.hashmap objectForKey:key];
}

- (MEFObject*) itemAtIndex:(NSInteger)index
{
    if (index >= self.items.count) {
        return nil;
    }
    return [self.items objectAtIndex:index];
}


- (void) reloadObjects
{
    [self.items removeAllObjects];
    NSMutableArray *sortedElements = nil;
    if (self.isBeginSearch) {
        sortedElements = [NSMutableArray arrayWithArray:self.findItems];
    } else {
        sortedElements = [NSMutableArray arrayWithArray:self.unsortedItems];
    }
    [self.items addObjectsFromArray:sortedElements];
}

- (void) removeSortItem:(MEFObject*) object;
{
    [self.items removeObject:object];
}

- (void) sortObjects
{
    [self.items removeAllObjects];
    NSMutableArray *sortedElements = nil;
    if (self.isBeginSearch) {
        sortedElements = [NSMutableArray arrayWithArray:self.findItems];
    } else {
        sortedElements = [NSMutableArray arrayWithArray:self.unsortedItems];
    }
    
    if (self.sortBlock) {
        [sortedElements sortUsingComparator:^NSComparisonResult(MEFObject *obj1, MEFObject *obj2) {
            return self.sortBlock(obj1,obj2);
        }];
    }
    
    [self modifyObjects:sortedElements];
    
    if (self.modifyObjects) {
        self.modifyObjects(sortedElements);
    }
    
    [self.items addObjectsFromArray:sortedElements];
    
}
#pragma mark - - Properties
- (NSMutableArray*) items
{
    if (!_items) {
        _items = [[NSMutableArray alloc]init];
    }
    return _items;
}

- (NSMutableArray*) unsortedItems
{
    if (!_unsortedItems) {
        _unsortedItems = [NSMutableArray new];
    }
    return _unsortedItems;
}
- (NSMutableArray*) findItems
{
    if (!_findItems) {
        _findItems = [NSMutableArray new];
    }
    return _findItems;
}

- (NSMutableDictionary*) hashmap
{
    if (!_hashmap) {
        _hashmap = [NSMutableDictionary new];
    }
    return _hashmap;
}

- (void) modifyObjects:(NSMutableArray *)objects
{
    
}

@end

@implementation MEFObject (EditMode)

- (id) copyWithoutItems
{
    MEFObject *object = [[self class] new];
    object.type = self.type;
    object.data = self.data;
    object.key = [self.key copy];
    return object;
}

- (NSMutableString*) createIndentifierObject:(MEFObject*) object currentIndentifier:(NSString*) currentString
{
    NSMutableString *string = [NSMutableString string];
    [string appendString:[NSString stringWithFormat:@"%@", object.key]];
    [string appendString:currentString];
    if (object.parent) {
        string = [self createIndentifierObject:object.parent currentIndentifier:string];
    }
    
    return string;
}

- (NSString*) indentifierObject
{
    return [self createIndentifierObject:self currentIndentifier:@""];
}

@end