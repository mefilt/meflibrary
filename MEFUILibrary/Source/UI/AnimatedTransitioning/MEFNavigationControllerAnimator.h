//
//  BSNavigationSlitViewAnimatro.h
//  Katusha
//
//  Created by Prokofev Ruslan on 19.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface MEFNavigationControllerAnimator : NSObject <UIViewControllerAnimatedTransitioning>

@end
