//
//  MEFShadowView.m
//  MEFLibrary
//
//  Created by Mefilt on 15.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFShadowView.h"

@implementation MEFShadowView
- (id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[UIColor clearColor]];
        self.shadowBeginAlpha = 0.35;
        self.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.35];
    }
    return self;
}


- (void) drawRect:(CGRect)rect
{
    // Create a gradient from white to red
    CGFloat colors [] = {
        0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.35
    };
    [self.shadowColor getRed:&colors[4] green:&colors[5]  blue:&colors[6]  alpha:&colors[7]];
    
    
    CGColorSpaceRef baseSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(baseSpace, colors, NULL, 2);
    CGColorSpaceRelease(baseSpace), baseSpace = NULL;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(context);
    CGContextAddRect(context, rect);
    CGContextClip(context);
    
    CGPoint startPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGGradientRelease(gradient), gradient = NULL;
    
    CGContextRestoreGState(context);
    
}
@end
