//
//  MEFSegmentedControl.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 09.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFSegmentedControl.h"
#import <Masonry/Masonry.h>
#import "MEFButton.h"
const static CGFloat MEFSegmentedControlMargin = 8.0f;
@interface MEFSegmentedControl () <UIScrollViewDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, readwrite, strong) UIScrollView *scrollView;
@property (nonatomic, readwrite, strong) UIView *contentView;
@property (nonatomic, readwrite, strong) NSMutableArray *segments;
@property (nonatomic, readwrite, strong) NSMutableArray *buttons;
@property (nonatomic, readwrite, assign) NSUInteger selectedSegmentAtIndex;
@property (nonatomic, readwrite, strong) UISwipeGestureRecognizer *leftSwipeGesture;
@property (nonatomic, readwrite, strong) UISwipeGestureRecognizer *rightSwipeGesture;
@end


@implementation MEFSegmentedControl



- (id) init
{
    if (self = [super init]) {
                [self initialize];
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initialize];
    }
    return self;
}
- (id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self initialize];
    }
    return self;
}

- (void) initialize
{
//    [self.scrollView addGestureRecognizer:self.leftSwipeGesture];
//    [self.scrollView addGestureRecognizer:self.rightSwipeGesture];
//    self.tintColor = [UIColor orangeColor];
}

- (void) awakeFromNib
{
    [super awakeFromNib];
}

- (void) didMoveToSuperview
{
    [super didMoveToSuperview];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);;
    }];
    [self reloadSegments];
}

- (void) removeAll
{
    [self.buttons makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.segments removeAllObjects];
    [self.buttons removeAllObjects];
}

- (void) selectSegmentAtTitle:(NSString*)title
{
    if (!self.segments.count) {
        return;
    }
    if (![self.segments containsObject:title]) {
        return;
    }
    NSUInteger index = [self.segments indexOfObject:title];
    [self selectSegmentAtIndex:index];
}

- (void) reloadSegments
{
    
    [self.buttons makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (!self.superview) {
        return;
    }
    if (!self.buttons.count) {
        return;
    }
    NSUInteger index = 0;
    for (MEFButton *button in self.buttons) {
        [self reloadButton:button];
        [self.contentView addSubview:button];
        if (index == self.selectedSegmentAtIndex) {
            [button setSelected:true];
            [button setUserInteractionEnabled:false];
        }
        index++;
    }
    [self setNeedsLayout];
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    CGFloat x = MEFSegmentedControlMargin;
    for (MEFButton *button in self.buttons) {
        CGSize size = [button systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        [button setFrame:CGRectMake(x, MEFSegmentedControlMargin, size.width, self.frame.size.height -  MEFSegmentedControlMargin * 2)];
        x += MEFSegmentedControlMargin + size.width;
    }
    [self.contentView setFrame:CGRectMake(0, 0, x, self.frame.size.height)];
    [self.scrollView setContentSize:CGSizeMake(x, self.frame.size.height)];
}

- (void) reloadButton:(MEFButton*)button
{
    [button setBackgroundColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [button setBackgroundColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [button setBackgroundColor:self.tintColor forState:UIControlStateNormal];

    [button setTitleColor:self.tintColor forState:UIControlStateSelected];
    [button setTitleColor:self.tintColor forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

}
- (void) reloadButtons
{
    [self reloadSegments];
    
}



- (void) setTintColor:(UIColor *)tintColor
{
    _tintColor = tintColor;
    [self reloadButtons];
}

- (void) addTitle:(NSString *)title
{
    [self.segments addObject:title];
    UIButton *button = [self createButton];
    [button setTitle:title forState:UIControlStateNormal];
    [self.buttons addObject:button];
    
    
    [self reloadSegments];
}

- (UIButton*) createButton
{
    MEFButton *button = [[MEFButton alloc]init];
    button.layer.cornerRadius = 14;
    [button setContentEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 25)];
    [button.titleLabel setFont:[UIFont systemFontOfSize:13]];
    [button addTarget:self action:@selector(target:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

- (void) target:(UIButton*)sender
{
    NSUInteger segmentAtIndex = [self.buttons indexOfObject:sender];
    if (self.selectedSegmentAtIndex == segmentAtIndex) {
        return;
    }
    if (self.changedValue) {
        self.changedValue(segmentAtIndex, sender.titleLabel.text, self);
    }
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    [self selectSegmentAtIndex:segmentAtIndex];
}


- (void) selectSegmentAtIndex:(NSUInteger)index
{
    if (index == self.selectedSegmentAtIndex) {
        return;
    }
    NSUInteger oldSelectedAtIndex = self.selectedSegmentAtIndex;
    self.selectedSegmentAtIndex = index;
    
    UIButton *oldButton = self.buttons[oldSelectedAtIndex];
    UIButton *newButton = self.buttons[index];
    [oldButton setSelected:false];
    [oldButton setUserInteractionEnabled:true];
    [newButton setSelected:true];
    [newButton setUserInteractionEnabled:false];
    // Если index равен 0 и последний тогда показывать не надо.

    if (self.changedValue) {
        self.changedValue(self.selectedSegmentAtIndex,newButton.titleLabel.text, self);
    }
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    BOOL visible = false;
    CGFloat offSet = [self getContentOffSetButton:newButton indexAtButton:index visible:&visible];
    if (visible) {
        [self.scrollView setContentOffset:CGPointMake(offSet, 0) animated:true];
    }
 }




- (CGFloat) getContentOffSetForSegmentAtIndex:(NSUInteger)index visible:(BOOL*)visible
{
    UIButton *button = self.buttons[index];
    return [self getContentOffSetButton:button indexAtButton:index visible:visible];
}

- (CGFloat) getContentOffSetButton:(UIButton*)button indexAtButton:(NSUInteger)index visible:(BOOL*)visible
{
    BOOL moreSegments = (index != 0) && (index != (self.segments.count - 1));
    CGFloat contentX = self.scrollView.contentOffset.x;
    CGFloat contentEndX = self.scrollView.contentOffset.x + self.scrollView.frame.size.width;
    
    CGRect visibleFrame = CGRectMake(contentX, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    CGRect frameButton = [self boundButton:button];
    if ((*visible = CGRectIntersectsRect(visibleFrame, frameButton)) != true) {
        return 0;
    }
    CGFloat maxX =  CGRectGetMaxX(frameButton);
    CGFloat minX =  CGRectGetMinX(frameButton);

    CGRect intersectRect = CGRectIntersection(visibleFrame, frameButton);
    NSInteger direction = 1;
    if (minX < contentX && maxX > contentX) {
        direction = -1;
    } else if (minX < contentEndX && maxX > contentEndX) {
        direction = 1;
    }
    CGFloat offSet = self.scrollView.contentOffset.x + (frameButton.size.width - intersectRect.size.width) * direction;
    if (moreSegments) {
        UIButton *nextButton = self.buttons[(index + 1 * direction)];
        CGRect boundButton = [self boundButton:nextButton];
        offSet = offSet + (boundButton.size.width * 0.5) * direction;
    }
    CGFloat maxOffset = self.scrollView.contentSize.width - self.scrollView.frame.size.width;
    offSet = fmin(maxOffset, offSet);
    offSet = fmax(0, offSet);
    return offSet;
}


- (CGRect) boundButton:(UIButton*)button
{
    CGRect frameButton = [button frame];
    frameButton.size.width += MEFSegmentedControlMargin * 2;
    frameButton.origin.x -= MEFSegmentedControlMargin;
    return frameButton;
}

- (NSString*) titleForSegmentAtIndex:(NSUInteger)segment
{
    return self.segments[segment];
}

#pragma mark -- gesture


- (void) leftSwipeGesture:(UISwipeGestureRecognizer*)sender
{
    NSInteger index = self.selectedSegmentAtIndex;
    index++;
    index = fmax(0, index);
    index = fmin(self.buttons.count - 1, index);
    [self selectSegmentAtIndex:index];
}

- (void) rightSwipeGesture:(UISwipeGestureRecognizer*)sender
{
    NSInteger index = self.selectedSegmentAtIndex;
    index--;
    index = fmax(0, index);
    index = fmin(self.buttons.count - 1, index);
    [self selectSegmentAtIndex:index];
}


#pragma mark - - properties 

- (NSMutableArray*) segments
{
    if (!_segments) {
        _segments = [NSMutableArray array];
    }
    return _segments;
}
- (NSMutableArray*) buttons
{
    if (!_buttons) {
        _buttons = [NSMutableArray array];
    }
    return _buttons;
}

- (UIScrollView*) scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [_scrollView setDelegate:self];
        [_scrollView setBounces:false];
        [_scrollView setAlwaysBounceVertical:false];
        [_scrollView setShowsHorizontalScrollIndicator:false];
        [_scrollView setShowsVerticalScrollIndicator:true];
//        [_scrollView setScrollEnabled:false];
        [self addSubview:_scrollView];
    }
    return _scrollView;
}
- (UISwipeGestureRecognizer*) leftSwipeGesture
{
    if (!_leftSwipeGesture) {
        _leftSwipeGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(leftSwipeGesture:)];
        [_leftSwipeGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
//        _leftSwipeGesture.delaysTouchesBegan = true;
        [_leftSwipeGesture setDelegate:self];
    }
    return _leftSwipeGesture;
}

- (UISwipeGestureRecognizer*) rightSwipeGesture
{
    if (!_rightSwipeGesture) {
        _rightSwipeGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(rightSwipeGesture:)];
        [_rightSwipeGesture setDirection:UISwipeGestureRecognizerDirectionRight];
        [_rightSwipeGesture setDelegate:self];
    }
    return _rightSwipeGesture;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}
- (UIView*) contentView
{
    if (!_contentView) {
        _contentView = [[UIView alloc]initWithFrame:self.bounds];
        [_contentView setBackgroundColor:[UIColor clearColor]];
        [self.scrollView addSubview:_contentView];
    }
    return _contentView;
}
@end

