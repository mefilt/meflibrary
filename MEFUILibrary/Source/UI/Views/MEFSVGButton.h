//
//  MEFSVGButton.h
//  MEFLibrary
//
//  Created by Mefilt on 04.06.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEFSVGImageView.h"
@interface MEFSVGButton : UIControl
{

}
@property (nonatomic, readonly, strong) MEFSVGImageView *iconView;
@property (nonatomic, readwrite, copy) void (^didTouch)();
@property (nonatomic, readwrite, strong) IBInspectable NSString *imageIdentifier;

- (void)setSVGImageIdentifier:(NSString *)imageIdentifier forState:(UIControlState)state;
- (void)setSVGFilename:(NSString*) filename forState:(UIControlState)state modifyLayer:(void (^) (CALayer *renderLayer))modifiLayer;
- (void)setSVGFilename:(NSString*) filename forState:(UIControlState)state;
- (void)setSVGImageIdentifier:(NSString *)imageIdentifier modifyLayer:(void (^) (CALayer *renderLayer))modifiLayer forState:(UIControlState)state;
- (void)setSVGStrokeColor:(UIColor *)color forState:(UIControlState)state;
- (void)setSVGFillColor:(UIColor *)color forState:(UIControlState)state;
- (void)setSVGSize:(CGSize)size forState:(UIControlState)state;
@end
