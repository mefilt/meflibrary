//
//  MEFAlertView.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 05.11.15.
//  Copyright © 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFAlertView.h"
@interface MEFAlertView () <UIAlertViewDelegate>

@end
@implementation MEFAlertView

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.clickedButtonAtIndex) {
        self.clickedButtonAtIndex(buttonIndex);
    }
}

- (void)alertViewCancel:(UIAlertView *)alertView
{
    if (self.clickedButtonCancel) {
        self.clickedButtonCancel();
    }
}

- (void) setDelegate:(id)delegate
{
    [super setDelegate:self];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
