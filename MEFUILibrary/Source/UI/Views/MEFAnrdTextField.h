//
//  MEFAnrdTextField.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 12.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEFAnrdTextField : UIView
@property (nonatomic, readwrite, strong) UITextField *textField;
@property (nonatomic, readwrite, strong) UILabel *placeholder;

@end
