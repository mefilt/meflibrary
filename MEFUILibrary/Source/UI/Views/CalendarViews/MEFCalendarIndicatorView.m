//
//  MEFCalendarIndicatorView.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 14.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFCalendarIndicatorView.h"

@implementation MEFCalendarIndicatorView
@synthesize empty=_empty;
- (id) init
{
    if (self = [super init]) {
        [self initialize];
    }
    return self;
}

//- (id) initWithCoder:(NSCoder *)aDecoder
//{
//    if (self = [super initWithCoder:aDecoder]) {
//        [self initialize];
//    }
//    return self;
//}
//
//- (id) initWithFrame:(CGRect)frame
//{
//    if (self = [super initWithFrame:frame]) {
//        [self initialize];
//    }
//    return self;
//}

- (void) setDisable:(BOOL)disable
{
    _disable = disable;
    if (_disable) {
        [self setDateColor:[UIColor grayColor]];
    } else {
        [self setDateColor:[UIColor blackColor]];
    }
    
    self.attributesDate[NSForegroundColorAttributeName] = self.dateColor;
    self.attributesDetail[NSForegroundColorAttributeName] = self.dateColor;
}

- (void) initialize
{
    self.dateColor = [UIColor blackColor];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    self.attributesDate = [NSMutableDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans" size:15], NSFontAttributeName,self.dateColor, NSForegroundColorAttributeName, paragraphStyle, NSParagraphStyleAttributeName, nil];

    self.attributesDetail = [NSMutableDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans-SemiBold" size:9], NSFontAttributeName,[UIColor blackColor], NSForegroundColorAttributeName, paragraphStyle, NSParagraphStyleAttributeName, nil];

    
}
- (CGSize) detailSizeThatFits:(CGSize)size
{
    CGRect rect = [self.detailText boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT)
                                              options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                           attributes:self.attributesDetail
                                              context:nil];
    CGSize dateSize = rect.size;
//    [self.detailText sizeWithAttributes:self.attributesDetail];
    dateSize.width = ceilf(dateSize.width);
    dateSize.height = ceilf(dateSize.height);
    return dateSize;
}


- (CGSize) sizeThatFits:(CGSize)size
{
    CGRect rect = [self.dateText boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT)
                                              options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                           attributes:self.attributesDate
                                              context:nil];
    CGSize dateSize = rect.size;
    dateSize.width = ceilf(dateSize.width);
    dateSize.height = ceilf(dateSize.height);
    return dateSize;
}


- (void) setDate:(NSDate *)date
{
    _date = date;
}
+ (instancetype) createInctance
{
    return [[MEFCalendarIndicatorView alloc]init];
}

- (void) willReturnToPool:(MEFObjectPool *)pool
{
    self.detailText = nil;
    self.dateText = nil;
    self.date = nil;
    self.today = false;
    self.disable = false;
    self.lastDayOfMonth = false;
    self.customPosition = CGPointZero;
    self.needCenteringCustomPosition = false;
    self.nextIndicatorView = nil;
    self.prevIndicatorView = nil;
}

@end



