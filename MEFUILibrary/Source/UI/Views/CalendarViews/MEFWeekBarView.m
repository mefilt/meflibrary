//
//  MEFWeekBarView.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 13.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//
#import "MEFUIConstants.h"
#import "MEFWeekBarView.h"
#import "NSDate+MEFUtilities.h"
#import <Masonry/Masonry.h>
#import "UIColor+MEFAssistants.h"

@interface MEFWeekBarView ()

@property (nonatomic, readwrite, assign) CGFloat widthLabels;
@property (nonatomic, readwrite, assign, getter=isInitLabels) BOOL initLabels;
@end
@implementation MEFWeekBarView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (id) init
{
    if (self = [super init]) {
                [self initialize];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
                [self initialize];
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initialize];
    }
    return self;
}

- (void) initialize
{
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(deviceOrientationDidChange:) name: UIDeviceOrientationDidChangeNotification object: nil];

    self.textColor = [UIColor blackColor];
    self.weekendTextColor = [UIColor colorFromHexCode:@"C84C5B"];
    self.textFont = [UIFont fontWithName:@"OpenSans" size:9];

    
}
- (void) setLocalCalendar:(NSCalendar *)localCalendar
{
    _localCalendar = localCalendar;
    [self createLabelsForWeak];
    self.initLabels = true;
}

- (void) setWeekendTextColor:(UIColor *)weekendTextColor
{
    _weekendTextColor = weekendTextColor;
    if (self.isInitLabels) {
        [self createLabelsForWeak];
    }
}

- (void) setTextColor:(UIColor *)textColor
{
    _textColor = textColor;
    if (self.isInitLabels) {
        [self createLabelsForWeak];
    }
}
- (void) setTextFont:(UIFont *)textFont
{
    _textFont = textFont;
    if (self.isInitLabels) {
        [self createLabelsForWeak];
    }
}


- (void) settingLabel:(UILabel*)label date:(NSDate*)date
{
    if (self.textFont) {
        [label setFont:self.textFont];
    }
    if ([date isWeakday]) {
        if (self.weekendTextColor) {
            [label setTextColor:self.weekendTextColor];
        }
    } else {
        if (self.textColor) {
            [label setTextColor:self.textColor];
        }
    }

}


- (void) createLabelsForWeak
{
    if (self.labels.count) {
        [self.labels makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.labels removeAllObjects];
    }
    NSDate *now = [NSDate getFirstDayOfWeek:[NSDate date]];
    self.widthLabels = 0;
    for (int i = 0; i <  7; i++) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0)];
        NSDate *newDate = [now dateByAddingDays:i];
        NSDateComponents *components = [self.localCalendar components:NSCalendarUnitWeekday fromDate:newDate];
        NSUInteger weekdayIndex = [components weekday];
        [self settingLabel:label date:newDate];
        NSString *weekday = [self.localCalendar shortWeekdaySymbols][weekdayIndex - 1];
        [label setText:weekday];
        [self addSubview:label];
        [self.labels addObject:label];
        CGSize size = [label systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        self.widthLabels += size.width;
    }

}


- (void) updateConstraints
{
    [super updateConstraints];
}

- (void) layoutSubviews
{

    CGFloat padding = (self.frame.size.width - self.widthLabels)   / 8;
    UILabel *lastLabel = nil;
    for (UILabel *label in self.labels) {
        [label mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.mas_top);
            make.bottom.equalTo(self.mas_bottom);
            if (lastLabel) {
                make.left.equalTo(lastLabel.mas_right).offset(padding);
            } else {
                make.left.equalTo(self.mas_left).offset(padding);
            }
        }];
        lastLabel = label;
    }
    [lastLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.right.lessThanOrEqualTo(self.mas_right).offset(padding);
    }];
//    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        [super layoutSubviews];
//    }
}

- (CGPoint) centerPositionDayOfWeek:(NSDate*)date
{
    NSDateComponents *components = [self.localCalendar components:NSCalendarUnitWeekday fromDate:date];
    NSUInteger weekdayIndex = [components weekday] - 1;
    
    //TODO: fix seatch label
    //Because first day of week sunday of NSCalendar, but labels beginning first with monday
    if (weekdayIndex == 0) {
        weekdayIndex = self.labels.count - 1;
    } else {
        weekdayIndex = weekdayIndex - 1;
    }
    UILabel *label = self.labels[weekdayIndex];
    

    return CGPointMake(CGRectGetMidX(label.frame),CGRectGetMidY(label.frame));
}


- (void)deviceOrientationDidChange:(NSNotification *)notification
{
//    [self updateConstraints];
}


- (NSMutableArray*) labels
{
    if (!_labels) {
        _labels = [NSMutableArray array];
    }
    return _labels;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}
@end
