//
//  MEFCalendarContentView.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 14.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MEFCalendarContentType) {
    MEFCalendarContentTypeDays,
    MEFCalendarContentTypeMonths,
};

typedef NS_ENUM(NSUInteger, MEFCalendarContentSelectedType) {
    MEFCalendarContentSelectedTypeNone,
    MEFCalendarContentSelectedTypeAll,
    MEFCalendarContentSelectedTypeBegin,
    MEFCalendarContentSelectedTypeEnd,
    MEFCalendarContentSelectedTypeBeginEnd,
};


@class MEFCalendarContentView;
const static NSUInteger MEFCalendarContentCountDaysOfWeak = 7;

const static CGFloat MEFCalendarContentLinePadding = 14;
const static CGFloat MEFCalendarContentLinePaddingTopBottom = 17;
const static CGFloat MEFCalendarContentSelectedPadding = 8;

const static CGFloat MEFCalendarContentDetailPadding = 4;

const static CGFloat MEFCalendarContentRadiusToday = 3;
const static CGFloat MEFCalendarContentSelectedCornerRadius = 20;

//const static CGFloat MEFCalendarContentArcTopPadding = 10;
//const static CGFloat MEFCalendarContentArcBottomPadding = 10;
//const static CGFloat MEFCalendarContentArcLeftPadding = 20;
//const static CGFloat MEFCalendarContentArcRightPadding = 20;
//const static CGFloat MEFCalendarContentArcRadiusPadding = 30;
@protocol  MEFCalendarContentViewDelegate  <NSObject>

- (CGPoint) centerPositionDayOfWeek:(NSDate*)date calendarContentView:(MEFCalendarContentView*)calendarContentView;
- (void) didTouchDate:(NSDate*) date calendarContentView:(MEFCalendarContentView*)calendarContentView ;
@end

@interface MEFCalendarContentView : UIView


@property (nonatomic, readwrite, weak) id <MEFCalendarContentViewDelegate> delegate;


@property (nonatomic, readwrite, strong) NSDate *beginDate;
@property (nonatomic, readwrite, strong) NSDate *endDate;

@property (nonatomic, readonly, strong) NSCalendar *calendarUTC;
@property (nonatomic, readonly, strong) NSCalendar *calendarLocal;
@property (nonatomic, readonly, assign) MEFCalendarContentType contentType;
@property (nonatomic, readonly, strong) NSDate *selectedBeginDate;
@property (nonatomic, readonly, strong) NSDate *selectedEndDate;


@property (nonatomic, readwrite, strong) NSDate *minDate;
@property (nonatomic, readwrite, strong) NSDate *maxDate;

@property (nonatomic, readwrite, strong) UIColor *selectedColor;
@property (nonatomic, readwrite, strong) UIColor *tintTodayColor;
@property (nonatomic, readwrite, assign, getter=isDoubleBottomPadding) BOOL doubleBottomPadding;
@property (nonatomic, readwrite, assign, getter=isDisabledFutureDates) BOOL disabledFutureDates;
@property (nonatomic, readwrite, assign, getter=isVisibledToday) BOOL visibledToday;
@property (nonatomic, readonly, assign, getter=isToday) BOOL today;


//- (void) updateCalendarContentWithCalendarUTC:(NSCalendar *)calendar localCalendar:(NSCalendar*)localCalendar type:(MEFCalendarContentType)contentType begin:(NSDate*)begin end:(NSDate*)end;
- (void) updateCalendarContentWithCalendarUTC:(NSCalendar *)calendar localCalendar:(NSCalendar*)localCalendar type:(MEFCalendarContentType)contentType;
- (void) setSelectedRange:(NSDate*)begin end:(NSDate*)end;
@end
