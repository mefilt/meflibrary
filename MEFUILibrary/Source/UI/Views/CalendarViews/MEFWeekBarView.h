//
//  MEFWeekBarView.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 13.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEFWeekBarView : UIView

@property (nonatomic, readwrite, strong) NSMutableArray *labels;
@property (nonatomic, readwrite, strong) IB_DESIGNABLE UIColor *weekendTextColor;
@property (nonatomic, readwrite, strong) IB_DESIGNABLE UIColor *textColor;
@property (nonatomic, readwrite, strong) IB_DESIGNABLE UIFont *textFont;
@property (nonatomic, readwrite, strong) NSCalendar *localCalendar;

- (CGPoint) centerPositionDayOfWeek:(NSDate*)date;
@end
