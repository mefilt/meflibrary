//
//  MEFCalendarIndicatorContentView.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 14.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEFCalendarIndicatorView.h"
#import "MEFObjectPool.h"
typedef NS_ENUM(NSUInteger, MEFCalendarIndicatorContentSelectedType) {
    MEFCalendarIndicatorContentSelectedTypeNone,
    MEFCalendarIndicatorContentSelectedTypeAll,
    MEFCalendarIndicatorContentSelectedTypeBegin,
    MEFCalendarIndicatorContentSelectedTypeEnd,
    MEFCalendarIndicatorContentSelectedTypeBeginEnd,
};



@interface MEFCalendarIndicatorContentView : NSObject <MEFObjectPooling>

+ (instancetype) createInctance;
@property (nonatomic, readwrite, assign) CGRect frame;
@property (nonatomic, readwrite, assign, getter=isEmpty) BOOL empty;
@property (nonatomic, readwrite, strong) NSDate *beginDate;
@property (nonatomic, readwrite, strong) NSDate *endDate;
@property (nonatomic, readwrite, assign, getter=isToday) BOOL today;
@property (nonatomic, readwrite, strong) NSMutableArray *indicators;
@property (nonatomic, readwrite, strong) NSMutableDictionary *hashIndicators;
@property (nonatomic, readwrite, assign) MEFCalendarIndicatorContentSelectedType selectedType;
@property (nonatomic, readwrite, strong) NSDate *selectedBeginDate;
@property (nonatomic, readwrite, strong) NSDate *selectedEndDate;
@property (nonatomic, readwrite, strong) MEFCalendarIndicatorContentView *nextIndicatorContentView;
@property (nonatomic, readwrite, weak) MEFCalendarIndicatorContentView *prevIndicatorContentView;
- (void) addIndicatorView:(MEFCalendarIndicatorView*)indicatorView;

- (CGPoint) midPositionBetweenNextAndCurrentContentView:(MEFCalendarIndicatorView *)indicatorView;
- (CGPoint) midPositionBetweenPrevAndCurrentContentView:(MEFCalendarIndicatorView *)indicatorView;
- (CGSize) sizeThatFits:(CGSize)size;
@end