//
//  MEFCalendarContentView.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 14.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFCalendarContentView.h"
#import "NSCalendar+MEFCurrent.h"
#import "NSDate+MEFUtilities.h"
#import "MEFObjectPool.h"
#import "MEFCalendarIndicatorContentView.h"
#import "NSString+MEFUtilities.h"
#import "NSDateFormatter+MEFAssistants.h"

@interface MEFCalendarContentView ()

@property (nonatomic, readwrite, strong) NSMutableDictionary *hashIndicatorContainers;
@property (nonatomic, readwrite, strong) NSMutableArray *indicatorContainers;
@property (nonatomic, readwrite, strong) UITapGestureRecognizer *tapGesture;
@property (nonatomic, readwrite, strong) NSDate *selectedBeginDate;
@property (nonatomic, readwrite, strong) NSDate *selectedEndDate;
@property (nonatomic, readwrite, strong) MEFObjectPool *objectPool;
@property (nonatomic, readwrite, strong) NSCalendar *calendarUTC;
@property (nonatomic, readwrite, strong) NSCalendar *calendarLocal;
@property (nonatomic, readwrite, assign) MEFCalendarContentType contentType;
@property (nonatomic, readwrite, assign, getter=isToday) BOOL today;
@end
@implementation MEFCalendarContentView
- (id) init
{
    if (self = [super init]) {
        [self initialize];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self initialize];
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initialize];
    }
    return self;
}



- (void) initialize
{
//    [self setBackgroundColor:[UIColor randomColor]];
    self.objectPool = [MEFObjectPool new];
    [self.objectPool registerClass:[MEFCalendarIndicatorContentView class] count:6];
    [self.objectPool registerClass:[MEFCalendarIndicatorView class] count:42];
    self.tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    [self addGestureRecognizer:self.tapGesture];
    [self setBackgroundColor:[UIColor whiteColor]];
}

#pragma mark - - tapGesture
- (void) tapGesture:(UITapGestureRecognizer*)tapGesture
{
    MEFCalendarIndicatorView *indicatorView = nil;
    CGPoint globalLocation = [tapGesture locationInView:self];
    for (MEFCalendarIndicatorContentView *contentView in self.indicatorContainers) {
        if (!CGRectContainsPoint([self tapRectForContentView:contentView], globalLocation)) {
            continue;
        }
        
        for (MEFCalendarIndicatorView *view in contentView.indicators) {
            if (view.isDisable) {
                continue;
            }
            CGRect bounds = [self tapRectWithCalendarIndicatorView:view calendarIndicatorContentView:contentView];
            if (CGRectContainsPoint(bounds, globalLocation)) {
                indicatorView = view;
                break;
            }
        }
    }
    if (indicatorView) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didTouchDate:calendarContentView:)]) {
            [self.delegate didTouchDate:indicatorView.date calendarContentView:self];
        }
    }
}
- (CGRect) tapRectForContentView:(MEFCalendarIndicatorContentView*)indicatorContentView
{
    BOOL last = indicatorContentView.prevIndicatorContentView && !indicatorContentView.nextIndicatorContentView;
    BOOL first = indicatorContentView.nextIndicatorContentView && !indicatorContentView.prevIndicatorContentView;
    CGFloat x = indicatorContentView.frame.origin.x, y = 0, width = indicatorContentView.frame.size.width, height = 0;
    if (last) {
        CGFloat offSet = (CGRectGetMinY(indicatorContentView.frame) - CGRectGetMaxY(indicatorContentView.prevIndicatorContentView.frame)) * 0.5f;
        y = CGRectGetMaxY(indicatorContentView.prevIndicatorContentView.frame) + offSet;
        height = CGRectGetMaxY(indicatorContentView.frame) - y + MEFCalendarContentLinePaddingTopBottom;
    } else if (first)  {
        CGFloat offSet = (CGRectGetMinY(indicatorContentView.nextIndicatorContentView.frame) - CGRectGetMaxY(indicatorContentView.frame)) * 0.5f;
        y = indicatorContentView.frame.origin.y  - MEFCalendarContentLinePaddingTopBottom;
        height = (CGRectGetMaxY(indicatorContentView.frame) + offSet) - y;
    } else  {
        CGFloat offSetTop = (CGRectGetMinY(indicatorContentView.frame) - CGRectGetMaxY(indicatorContentView.prevIndicatorContentView.frame)) * 0.5f;
        CGFloat offSetBottom = (CGRectGetMinY(indicatorContentView.nextIndicatorContentView.frame) - CGRectGetMaxY(indicatorContentView.frame)) * 0.5f;
        y = CGRectGetMaxY(indicatorContentView.prevIndicatorContentView.frame) + offSetTop;
        height = (CGRectGetMaxY(indicatorContentView.frame) + offSetBottom) - y;
    }
    return CGRectMake(x, y, width, height);
}
- (CGRect) tapRectWithCalendarIndicatorView:(MEFCalendarIndicatorView*)calendarIndicatorView calendarIndicatorContentView:(MEFCalendarIndicatorContentView*)indicatorContentView
{
    CGRect beginIndicatorRect = calendarIndicatorView.frame;
    CGRect rectSelected = CGRectZero;
    
    CGFloat x,y,width,height;
    x = y = width = height = 0;
    if (calendarIndicatorView.prevIndicatorView) {
        CGPoint position = [indicatorContentView midPositionBetweenPrevAndCurrentContentView:calendarIndicatorView];
        width = (CGRectGetMaxX(beginIndicatorRect) + (beginIndicatorRect.origin.x - position.x)) - position.x;
        x = beginIndicatorRect.origin.x - (width - beginIndicatorRect.size.width) * 0.5;
    } else {
        CGPoint position = [indicatorContentView midPositionBetweenNextAndCurrentContentView:calendarIndicatorView];
        CGFloat xOffset = beginIndicatorRect.origin.x - (position.x - (beginIndicatorRect.origin.x + beginIndicatorRect.size.width));
        width = position.x - xOffset;
        x = beginIndicatorRect.origin.x - (width - beginIndicatorRect.size.width) * 0.5;
    }
    
    BOOL last = indicatorContentView.prevIndicatorContentView && !indicatorContentView.nextIndicatorContentView;
    BOOL first = indicatorContentView.nextIndicatorContentView && !indicatorContentView.prevIndicatorContentView;
    
    if (last) {
        CGFloat offSet = (CGRectGetMinY(indicatorContentView.frame) - CGRectGetMaxY(indicatorContentView.prevIndicatorContentView.frame)) * 0.5f;
        y = CGRectGetMaxY(indicatorContentView.prevIndicatorContentView.frame) + offSet;
        height = CGRectGetMaxY(indicatorContentView.frame) - y + MEFCalendarContentLinePaddingTopBottom;
    } else if (first)  {
        CGFloat offSet = (CGRectGetMinY(indicatorContentView.nextIndicatorContentView.frame) - CGRectGetMaxY(indicatorContentView.frame)) * 0.5f;
        y = indicatorContentView.frame.origin.y  - MEFCalendarContentLinePaddingTopBottom;
        height = (CGRectGetMaxY(indicatorContentView.frame) + offSet) - y;
    } else  {
        CGFloat offSetTop = (CGRectGetMinY(indicatorContentView.frame) - CGRectGetMaxY(indicatorContentView.prevIndicatorContentView.frame)) * 0.5f;
        CGFloat offSetBottom = (CGRectGetMinY(indicatorContentView.nextIndicatorContentView.frame) - CGRectGetMaxY(indicatorContentView.frame)) * 0.5f;
        y = CGRectGetMaxY(indicatorContentView.prevIndicatorContentView.frame) + offSetTop;
        height = (CGRectGetMaxY(indicatorContentView.frame) + offSetBottom) - y;
    }
    rectSelected = CGRectMake(x, y, width, height);
    return rectSelected;
}


#pragma mark -- other
- (NSDate*)firstDayOfWeek:(NSDate*)date
{
    NSDate *firstDayOfWeek = [NSDate getFirstDayOfWeek:date calendar:self.calendarUTC];
    return firstDayOfWeek;
}

- (void) cleanup
{
    for (MEFCalendarIndicatorContentView *contentView in self.indicatorContainers) {
        contentView.empty = true;
    }
    self.today = false;
    [self.indicatorContainers removeAllObjects];
    [self.hashIndicatorContainers removeAllObjects];
}


- (void) buildCalendarContentForDays
{
    NSDate *begin = [NSDate getFirstDayOfWeek:self.beginDate calendar:self.calendarUTC];
    NSDate *end = [NSDate getLastDayOfWeek:self.endDate calendar:self.calendarUTC];
    NSInteger days = [NSDate daysBetweenDate:begin andDate:end calendar:self.calendarUTC];
    BOOL nextWeek = true;
    NSDate *now = [self.calendarUTC nowDateWithoutTime];
    
    NSDate *currentDate = begin;
    NSDate *endDate = end;
    MEFCalendarIndicatorContentView *currentIndicatorContentView = nil;
    MEFCalendarIndicatorView  *prevCalendarIndicatorView = nil;
    
    
    NSDate *minDate = self.minDate;
    NSDate *maxDate  = self.maxDate;
    
    
    NSDateFormatter *date = [NSDateFormatter currentDateFormatter];
    NSArray *shortMonthSymbols = [date standaloneMonthSymbols];
    for (NSInteger i = 0; i < days; i++) {
        
        if (nextWeek) {
            if (currentIndicatorContentView) {
                currentIndicatorContentView.endDate = endDate;
            }
            MEFCalendarIndicatorContentView *prevIndicatorContentView = currentIndicatorContentView;
            currentIndicatorContentView = [self.objectPool emptyObjectWithClass:[MEFCalendarIndicatorContentView class]];
            [self.indicatorContainers addObject:currentIndicatorContentView];
            self.hashIndicatorContainers[currentDate] = currentIndicatorContentView;
            currentIndicatorContentView.beginDate = currentDate;
            if (prevIndicatorContentView) {
                prevIndicatorContentView.nextIndicatorContentView = currentIndicatorContentView;
                currentIndicatorContentView.prevIndicatorContentView = prevIndicatorContentView;
            }
            prevCalendarIndicatorView = nil;
        }
        
        MEFCalendarIndicatorView  *calendarIndicatorView = [self createCalendarIndicatorView:currentDate];
        calendarIndicatorView.needCenteringCustomPosition = true;
        [calendarIndicatorView setDateText:[NSString stringWithFormat:@"%ld",(long)[currentDate day]]];
//        NSLog(@"date %@ day %@",currentDate,[NSString stringWithFormat:@"%ld",(long)[currentDate day]]);
        NSDateComponents *components = [self.calendarUTC components:NSCalendarUnitDay | NSCalendarUnitWeekday fromDate:calendarIndicatorView.date];
        
        if ([components day] == 1) {
            NSString *month = shortMonthSymbols[[calendarIndicatorView.date month] - 1 ];
            [calendarIndicatorView setDetailText:[month uppercaseString]];
        }
        
//        if ([beginDate isLaterThanDate:self.beginDate] && ![beginDate isEqualToDate:self.endDate]) {
//            
//        }
        
        if ([[NSDate lastDayOfMonth:currentDate calendar:self.calendarUTC] isEqualToDate:currentDate]) {
            calendarIndicatorView.lastDayOfMonth = true;
        }
        if (maxDate && [currentDate isLaterThanDate:maxDate] && ![currentDate isEqualToDate:maxDate]) {
            calendarIndicatorView.disable = true;
        }
        
        if (minDate && [currentDate isEarlierThanDate:minDate] && ![currentDate isEqualToDate:minDate]) {
            calendarIndicatorView.disable = true;
        }

        
        
        if ([currentDate isEarlierThanDate:self.beginDate] && ![currentDate isEqualToDate:self.beginDate]) {
            calendarIndicatorView.disable = true;
        }
        if ([currentDate isLaterThanDate:self.endDate] && ![currentDate isEqualToDate:self.endDate]) {
            calendarIndicatorView.disable = true;
        }
        if ([currentDate isEqualToDate:now]) {
            self.today = true;
            calendarIndicatorView.today = true;
            currentIndicatorContentView.today = true;
        }
        if (prevCalendarIndicatorView) {
            prevCalendarIndicatorView.nextIndicatorView = calendarIndicatorView;
            calendarIndicatorView.prevIndicatorView = prevCalendarIndicatorView;
        }
        
        [currentIndicatorContentView addIndicatorView:calendarIndicatorView];
        NSUInteger weekday = [components weekday] - 1;
        nextWeek = (weekday == 0);
        endDate = currentDate;
        currentDate = [currentDate dateByAddingDays:1];
        prevCalendarIndicatorView = calendarIndicatorView;
    }
    currentIndicatorContentView.endDate = endDate;
}

- (void) buildCalendarContentForMonths
{
    NSDate *begin = self.beginDate;
    NSDate *end = self.endDate;
    NSDate *fromDate;
    NSDate *toDate;
    [self.calendarUTC rangeOfUnit:NSCalendarUnitMonth | NSCalendarUnitYear startDate:&fromDate
                 interval:NULL forDate:begin];
    [self.calendarUTC rangeOfUnit:NSCalendarUnitMonth | NSCalendarUnitYear startDate:&toDate
                 interval:NULL forDate:end];
    
    NSDateComponents *difference = [self.calendarUTC components:NSCalendarUnitMonth
                                               fromDate:fromDate toDate:toDate options:0];
    
    
    
    NSInteger months = [difference month] + 1;
    
   
    BOOL nextContent = true;
    NSDate *currentDate = begin;
    NSDate *endDate = end;
    MEFCalendarIndicatorContentView *currentIndicatorContentView = nil;
    MEFCalendarIndicatorView  *prevCalendarIndicatorView = nil;
    NSDateComponents *components = [self.calendarUTC components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:[NSDate date]];
    NSInteger monthNow = [components month];
    NSInteger yearNow = [components year];
    NSDate *minDate = self.minDate;
    NSDate *maxDate  = self.maxDate;
    
    NSDateFormatter *date = [NSDateFormatter currentDateFormatter];
    NSArray *shortMonthSymbols = [date standaloneMonthSymbols];
    for (NSInteger i = 0, p = 0; i < months; i++,p++) {
        if (nextContent) {
            p = 0;
            if (currentIndicatorContentView) {
                currentIndicatorContentView.endDate = endDate;
            }
            MEFCalendarIndicatorContentView *prevIndicatorContentView = currentIndicatorContentView;
            currentIndicatorContentView = [self.objectPool emptyObjectWithClass:[MEFCalendarIndicatorContentView class]];
            [self.indicatorContainers addObject:currentIndicatorContentView];
            self.hashIndicatorContainers[currentDate] = currentIndicatorContentView;
            currentIndicatorContentView.beginDate = currentDate;
            if (prevIndicatorContentView) {
                prevIndicatorContentView.nextIndicatorContentView = currentIndicatorContentView;
                currentIndicatorContentView.prevIndicatorContentView = prevIndicatorContentView;
            }
            prevCalendarIndicatorView = nil;
        }
        MEFCalendarIndicatorView  *calendarIndicatorView = [self createCalendarIndicatorView:currentDate];
        calendarIndicatorView.needCenteringCustomPosition = false;
        [calendarIndicatorView setDateText:shortMonthSymbols[[currentDate month] - 1]];
        
  
        if (maxDate && [currentDate isLaterThanDate:maxDate] && ![currentDate isEqualToDate:maxDate]) {
            calendarIndicatorView.disable = true;
        }
        
        if (minDate && [currentDate isEarlierThanDate:minDate] && ![currentDate isEqualToDate:minDate]) {
            calendarIndicatorView.disable = true;
        }
        
        if ([currentDate isLaterThanDate:maxDate] && ![currentDate isEqualToDate:maxDate]) {
            calendarIndicatorView.disable = true;
        }
        
        
        
        if ([currentDate isEarlierThanDate:self.beginDate] && ![currentDate isEqualToDate:self.beginDate]) {
            calendarIndicatorView.disable = true;
        }
        if ([currentDate isLaterThanDate:self.endDate] && ![currentDate isEqualToDate:self.endDate]) {
            calendarIndicatorView.disable = true;
        }
        NSDateComponents *components = [self.calendarUTC components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:currentDate];
        if (components.month == monthNow && components.year == yearNow) {
            self.today = true;
            calendarIndicatorView.today = true;
            currentIndicatorContentView.today = true;
        }
        if (prevCalendarIndicatorView) {
            prevCalendarIndicatorView.nextIndicatorView = calendarIndicatorView;
            calendarIndicatorView.prevIndicatorView = prevCalendarIndicatorView;
        }
        [currentIndicatorContentView addIndicatorView:calendarIndicatorView];
        nextContent =  (p == 3);
        endDate = currentDate;
        currentDate = [currentDate dateByAddingMonths:1 calendar:self.calendarUTC];
        prevCalendarIndicatorView = calendarIndicatorView;
    }
    currentIndicatorContentView.endDate = endDate;
}

- (void) updateCalendarContentWithCalendarUTC:(NSCalendar *)calendar localCalendar:(NSCalendar*)localCalendar type:(MEFCalendarContentType)contentType
{

    self.calendarUTC = calendar;
    self.calendarLocal = localCalendar;
    self.contentType = contentType;
    [self cleanup];
    
    switch (contentType) {
        case MEFCalendarContentTypeDays:
        {
            [self buildCalendarContentForDays];
        }   break;
        case MEFCalendarContentTypeMonths:
        {
            [self buildCalendarContentForMonths];
        }   break;
        default:
            break;
    }

    [self setNeedsLayout];
    [self setNeedsDisplay];
   
}


- (CGSize) sizeThatFits:(CGSize)size
{
    CGFloat maxHeight = MEFCalendarContentLinePaddingTopBottom;
    for (MEFCalendarIndicatorContentView *indicatorContentView in self.indicatorContainers) {
        CGSize indicatorSize = [indicatorContentView sizeThatFits:size];
        maxHeight += indicatorSize.height + MEFCalendarContentLinePadding * 2;
    }
    // mb fix?
    maxHeight -= MEFCalendarContentLinePadding * 2;
    maxHeight += MEFCalendarContentLinePaddingTopBottom * 2;
    if (self.isDoubleBottomPadding) {
        return CGSizeMake(size.width, maxHeight);
    }
    return CGSizeMake(size.width, maxHeight - MEFCalendarContentLinePaddingTopBottom);
}




- (MEFCalendarIndicatorView*) createCalendarIndicatorView:(NSDate*)date
{
    MEFCalendarIndicatorView *calendarIndicatorView = [self.objectPool emptyObjectWithClass:[MEFCalendarIndicatorView class]];
    
//    if (self.contentType == MEFCalendarContentTypeDays) {
//        [calendarIndicatorView setNeedCenteringCustomPosition:true];
//        [calendarIndicatorView setDateText:[NSString stringWithFormat:@"%ld",(long)[date day]]];
//    } else {
//        [calendarIndicatorView setNeedCenteringCustomPosition:false];
//        [calendarIndicatorView setDateText:self.calendarLocal.shortMonthSymbols[[date month] - 1]];
//    }
    calendarIndicatorView.date = date;

    return calendarIndicatorView;
}

- (MEFCalendarIndicatorContentView*) findIndicatorContentView:(NSDate*)date
{
    NSDate *firstDay = [self firstDayOfWeek:date];
    return self.hashIndicatorContainers[firstDay];
}

- (MEFCalendarContentView*) findIndicatorView:(NSDate*)date
{
    if (!date) {
        return nil;
    }
    MEFCalendarIndicatorContentView *indicatorContentView = [self findIndicatorContentView:date];
    return indicatorContentView.hashIndicators[date];
}

- (BOOL) isInValidSelectedDate:(NSDate*)selectedBeginDate selectedEndDate:(NSDate*)selectedEndDate
{
    BOOL betweenDateBegining = [self.beginDate isBetweenDate:selectedBeginDate andDate:selectedEndDate];
    BOOL equalDateBegining = [self.beginDate isEqualToDate:selectedBeginDate];
    BOOL equalDateBeginingWithSelectedEndDate = [self.beginDate isEqualToDate:selectedEndDate];
    BOOL equalDateEnding = [self.endDate isEqualToDate:selectedBeginDate];
    BOOL betweenDateEnding = [self.endDate isBetweenDate:selectedBeginDate andDate:selectedEndDate];
    
    BOOL betweenDateBeg = [selectedBeginDate isBetweenDate:self.beginDate andDate:self.endDate];
    
    return (!betweenDateBegining && !betweenDateEnding && !equalDateBegining && !equalDateEnding && !equalDateBeginingWithSelectedEndDate && !betweenDateBeg);
}



- (BOOL) isAllSelectIndicatorContentView:(MEFCalendarIndicatorContentView*) contentView
{
    if (self.contentType == MEFCalendarContentTypeDays) {
        return [self.selectedBeginDate isEarlierThanDate:contentView.beginDate]  && [contentView.endDate isEarlierThanDate:self.selectedEndDate]
        && ![self.selectedBeginDate isEqualToDate:contentView.beginDate]  && ![contentView.endDate isEqualToDate:self.selectedEndDate];
    }
    
    return [self.selectedBeginDate isEarlierThanDate:contentView.beginDate]  && [contentView.endDate isEarlierThanDate:self.selectedEndDate]
    && ![self.selectedBeginDate isEqualToDate:contentView.beginDate]  && ![contentView.endDate isEqualToDate:self.selectedEndDate];

}
- (BOOL) isBeginSelectIndicatorContentView:(MEFCalendarIndicatorContentView*) contentView
{
    return (([contentView.beginDate isEarlierThanDate:self.selectedBeginDate] || [contentView.beginDate isEqualToDate:self.selectedBeginDate])
            && ([contentView.endDate isLaterThanDate:self.selectedBeginDate] || [contentView.endDate  isEqualToDate:self.selectedBeginDate])
            && ([contentView.endDate isEarlierThanDate:self.selectedEndDate] && ![contentView.endDate isEqualToDate:self.selectedEndDate]));
    
}
- (BOOL) isBeginEndSelectIndicatorContentView:(MEFCalendarIndicatorContentView*) contentView
{
    return ([contentView.beginDate isEqualToDate:self.selectedBeginDate] || [contentView.beginDate isEarlierThanDate:self.selectedBeginDate])
            && ([contentView.endDate isLaterThanDate:self.selectedEndDate] || [contentView.endDate isEqualToDate:self.selectedEndDate]);
}
- (BOOL) isEndSelectIndicatorContentView:(MEFCalendarIndicatorContentView*) contentView
{
    return ![self.selectedBeginDate isEqualToDate:contentView.beginDate] && [self.selectedBeginDate isEarlierThanDate:contentView.beginDate] && ([self.selectedEndDate isEarlierThanDate:contentView.endDate] || [self.selectedEndDate isEqualToDate:contentView.endDate]) && ([self.selectedEndDate isLaterThanDate:contentView.beginDate] || [contentView.beginDate isEqualToDate:self.selectedEndDate]);
}


- (void) resetSelected
{
    self.selectedBeginDate = nil;
    self.selectedEndDate = nil;
    [self setNeedsDisplay];
    for (MEFCalendarIndicatorContentView *contentView in self.indicatorContainers) {
        contentView.selectedBeginDate = contentView.selectedEndDate = nil;
        contentView.selectedType = MEFCalendarContentSelectedTypeNone;
    }
}
- (void) setSelectedRange:(NSDate *)begin end:(NSDate *)end
{
    if (!self.beginDate || !self.endDate) {
        @throw [NSException exceptionWithName:@"Begin Date or End date nil" reason:nil userInfo:nil];
    }
    if (!begin || !end) {
        [self resetSelected];
        return;
    }
    
    if ([self isInValidSelectedDate:begin selectedEndDate:end]) {
        [self resetSelected];
        return;
    }

    if (self.contentType == MEFCalendarContentTypeMonths) {
        NSDate *firstDayBegin = [NSDate firstDayOfMonth:begin calendar:self.calendarUTC];
        NSDate *firstDayEnd = [NSDate firstDayOfMonth:end calendar:self.calendarUTC];
        if (![firstDayBegin isEqualToDate:begin] || ![firstDayEnd isEqualToDate:end]) {
            @throw [NSException exceptionWithName:@"Selected date should begin with first day of month. Only for type content MEFCalendarContentTypeMonths" reason:nil userInfo:nil];
        }
    }
    
    self.selectedBeginDate = begin;
    self.selectedEndDate = end;

    for (MEFCalendarIndicatorContentView *contentView in self.indicatorContainers) {
        if ([self isAllSelectIndicatorContentView:contentView]) {
            contentView.selectedBeginDate = contentView.beginDate;
            contentView.selectedEndDate = contentView.endDate;
            contentView.selectedType = MEFCalendarContentSelectedTypeAll;
        } else  if ([self isEndSelectIndicatorContentView:contentView]) {
            contentView.selectedBeginDate = contentView.beginDate;
            contentView.selectedEndDate = self.selectedEndDate;
            contentView.selectedType = MEFCalendarContentSelectedTypeEnd;
        } else  if ([self isBeginSelectIndicatorContentView:contentView]) {
            contentView.selectedBeginDate = self.selectedBeginDate;
            contentView.selectedEndDate = contentView.endDate;
            contentView.selectedType = MEFCalendarContentSelectedTypeBegin;
        } else if ([self isBeginEndSelectIndicatorContentView:contentView]) {
            contentView.selectedBeginDate = self.selectedBeginDate;
            contentView.selectedEndDate = self.selectedEndDate;
            contentView.selectedType = MEFCalendarContentSelectedTypeBeginEnd;
        } else {
            contentView.selectedBeginDate = nil;
            contentView.selectedEndDate = nil;
            contentView.selectedType = MEFCalendarContentSelectedTypeNone;
        }
    }
    
    [self setNeedsDisplay];
    
}





- (CGRect) selectedRectForMonthWithCalendarIndicatorView:(MEFCalendarIndicatorView*)calendarIndicatorView calendarIndicatorContentView:(MEFCalendarIndicatorContentView*)indicatorContentView
{
    CGRect beginIndicatorRect = calendarIndicatorView.frame;
    CGRect rectSelected = CGRectZero;
    
    CGFloat x,y,width,height;
    x = y = width = height = 0;
    if (calendarIndicatorView.prevIndicatorView) {
        CGPoint position = [indicatorContentView midPositionBetweenPrevAndCurrentContentView:calendarIndicatorView];
        width = (CGRectGetMaxX(beginIndicatorRect) + (beginIndicatorRect.origin.x - position.x)) - position.x;
        x = beginIndicatorRect.origin.x - (width - beginIndicatorRect.size.width) * 0.5;
    } else {
        CGPoint position = [indicatorContentView midPositionBetweenNextAndCurrentContentView:calendarIndicatorView];
        CGFloat xOffset = beginIndicatorRect.origin.x - (position.x - (beginIndicatorRect.origin.x + beginIndicatorRect.size.width));
        width = position.x - xOffset;
        x = beginIndicatorRect.origin.x - (width - beginIndicatorRect.size.width) * 0.5;
    }
    
    BOOL last = indicatorContentView.prevIndicatorContentView && !indicatorContentView.nextIndicatorContentView;
    BOOL first = indicatorContentView.nextIndicatorContentView && !indicatorContentView.prevIndicatorContentView;
    
    if (last) {
        CGFloat offSet = (CGRectGetMinY(indicatorContentView.frame) - CGRectGetMaxY(indicatorContentView.prevIndicatorContentView.frame)) * 0.5f;
        y = CGRectGetMaxY(indicatorContentView.prevIndicatorContentView.frame) + offSet;
        height = CGRectGetMaxY(indicatorContentView.frame) - y + (MEFCalendarContentLinePaddingTopBottom - (MEFCalendarContentLinePaddingTopBottom - MEFCalendarContentLinePadding));
    } else if (first)  {
        CGFloat offSet = (CGRectGetMinY(indicatorContentView.nextIndicatorContentView.frame) - CGRectGetMaxY(indicatorContentView.frame)) * 0.5f;
        y = indicatorContentView.frame.origin.y  - (MEFCalendarContentLinePaddingTopBottom - (MEFCalendarContentLinePaddingTopBottom - MEFCalendarContentLinePadding));
        height = (CGRectGetMaxY(indicatorContentView.frame) + offSet) - y;
    } else  {
        CGFloat offSetTop = (CGRectGetMinY(indicatorContentView.frame) - CGRectGetMaxY(indicatorContentView.prevIndicatorContentView.frame)) * 0.5f;
        CGFloat offSetBottom = (CGRectGetMinY(indicatorContentView.nextIndicatorContentView.frame) - CGRectGetMaxY(indicatorContentView.frame)) * 0.5f;
        y = CGRectGetMaxY(indicatorContentView.prevIndicatorContentView.frame) + offSetTop;
        height = (CGRectGetMaxY(indicatorContentView.frame) + offSetBottom) - y;
    }
    rectSelected = CGRectMake(x, y, width, height);
    return rectSelected;
}




- (CGRect) selectedRectForCalendarIndicatorView:(MEFCalendarIndicatorView*)calendarIndicatorView indicatorContentView:(MEFCalendarIndicatorContentView*)indicatorContentView
{
    if (self.contentType ==  MEFCalendarContentTypeMonths) {
//        return CGRectInset(calendarIndicatorView.frame, -MEFCalendarContentSelectedPadding, -MEFCalendarContentSelectedPadding);
        return [self selectedRectForMonthWithCalendarIndicatorView:calendarIndicatorView calendarIndicatorContentView:indicatorContentView];
    }
    
    CGRect rectSelected;
    CGRect beginIndicatorRect = calendarIndicatorView.frame;
    if (calendarIndicatorView.prevIndicatorView) {
        CGPoint position = [indicatorContentView midPositionBetweenPrevAndCurrentContentView:calendarIndicatorView];
        CGFloat width = (beginIndicatorRect.origin.x + beginIndicatorRect.size.width + (beginIndicatorRect.origin.x - position.x)) - position.x;
        CGFloat height = beginIndicatorRect.size.height + MEFCalendarContentSelectedPadding * 2;
        CGFloat size = fminf(width, height);
        rectSelected = CGRectMake(beginIndicatorRect.origin.x - (size - beginIndicatorRect.size.width) * 0.5, beginIndicatorRect.origin.y - (size - beginIndicatorRect.size.height) * 0.5, size, size);
    } else {
        CGPoint position = [indicatorContentView midPositionBetweenNextAndCurrentContentView:calendarIndicatorView];
        
        CGFloat x = beginIndicatorRect.origin.x - (position.x - (beginIndicatorRect.origin.x + beginIndicatorRect.size.width));
        CGFloat width = position.x - x;
        CGFloat height = beginIndicatorRect.size.height + MEFCalendarContentSelectedPadding * 2;
        CGFloat size = fminf(width, height);
        rectSelected = CGRectMake(beginIndicatorRect.origin.x - (size - beginIndicatorRect.size.width) * 0.5, beginIndicatorRect.origin.y - (size - beginIndicatorRect.size.height) * 0.5, size, size);
    }
    return rectSelected;
}


- (CGRect) rectSelectedWithIndicatorContentView:(MEFCalendarIndicatorContentView*)indicatorContentView
{
    MEFCalendarIndicatorView *beginIndicatorView = indicatorContentView.hashIndicators[indicatorContentView.selectedBeginDate];
    MEFCalendarIndicatorView *endIndicatorView = indicatorContentView.hashIndicators[indicatorContentView.selectedEndDate];
    
    CGRect beginIndicatorRect  = beginIndicatorView.frame;
    CGRect endIndicatorRect  = endIndicatorView.frame;
    
    CGRect rectSelected = CGRectMake(0, 0, 0, 0);
    switch (indicatorContentView.selectedType) {
        case MEFCalendarIndicatorContentSelectedTypeAll:
        {
            rectSelected  = CGRectMake(indicatorContentView.frame.origin.x, beginIndicatorRect.origin.y - MEFCalendarContentSelectedPadding, indicatorContentView.frame.size.width, beginIndicatorRect.size.height + MEFCalendarContentSelectedPadding * 2);
        }   break;
        case MEFCalendarIndicatorContentSelectedTypeBeginEnd:
        {
            if ([beginIndicatorView isEqual:endIndicatorView]) {
                rectSelected = [self selectedRectForCalendarIndicatorView:beginIndicatorView indicatorContentView:indicatorContentView];
            } else {
                CGPoint beginPosition = [indicatorContentView midPositionBetweenPrevAndCurrentContentView:beginIndicatorView];
                CGPoint endPosition = [indicatorContentView midPositionBetweenNextAndCurrentContentView:endIndicatorView];
                rectSelected = CGRectMake(beginPosition.x, beginIndicatorRect.origin.y - MEFCalendarContentSelectedPadding, endPosition.x - beginPosition.x, beginIndicatorRect.size.height + MEFCalendarContentSelectedPadding * 2);
            }
        }   break;
        case MEFCalendarIndicatorContentSelectedTypeBegin:
        {
            CGPoint beginPosition = [indicatorContentView midPositionBetweenPrevAndCurrentContentView:beginIndicatorView];
            rectSelected = CGRectMake(beginPosition.x, beginIndicatorRect.origin.y - MEFCalendarContentSelectedPadding, self.frame.size.width - beginPosition.x, beginIndicatorRect.size.height + MEFCalendarContentSelectedPadding * 2);
            
        }   break;
        case MEFCalendarIndicatorContentSelectedTypeEnd:
        {
            CGPoint endPosition = [indicatorContentView midPositionBetweenNextAndCurrentContentView:endIndicatorView];
            rectSelected = CGRectMake(indicatorContentView.frame.origin.x, endIndicatorRect.origin.y - MEFCalendarContentSelectedPadding,endPosition.x, endIndicatorRect.size.height + MEFCalendarContentSelectedPadding * 2);
            
        }   break;
        default:
            break;
    }
    return rectSelected;
}

- (CGMutablePathRef) createPathWithIndicatorContentView:(MEFCalendarIndicatorContentView*)indicatorContentView
{
    CGMutablePathRef path = CGPathCreateMutable();
    CGRect rectSelected = [self rectSelectedWithIndicatorContentView:indicatorContentView];
    switch (indicatorContentView.selectedType) {
        case MEFCalendarIndicatorContentSelectedTypeAll:
        {
            CGPathAddPath(path, NULL,[UIBezierPath bezierPathWithRoundedRect:rectSelected cornerRadius:0].CGPath);
        }   break;
        case MEFCalendarIndicatorContentSelectedTypeBeginEnd:
        {
            CGPathAddPath(path, NULL,[UIBezierPath bezierPathWithRoundedRect:rectSelected cornerRadius:MEFCalendarContentSelectedCornerRadius].CGPath);
        }   break;
        case MEFCalendarIndicatorContentSelectedTypeBegin:
        {
            CGPathMoveToPoint(path, NULL, CGRectGetMinX(rectSelected), CGRectGetMidY(rectSelected));
            CGPathAddArcToPoint(path, NULL, CGRectGetMinX(rectSelected), CGRectGetMinY(rectSelected), CGRectGetMidX(rectSelected), CGRectGetMinY(rectSelected), MEFCalendarContentSelectedCornerRadius);
            CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(rectSelected), CGRectGetMinY(rectSelected));
            CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(rectSelected), CGRectGetMaxY(rectSelected));
            CGPathAddLineToPoint(path, NULL, CGRectGetMidX(rectSelected), CGRectGetMaxY(rectSelected));
            CGPathMoveToPoint(path, NULL, CGRectGetMinX(rectSelected), CGRectGetMidY(rectSelected));
            CGPathAddArcToPoint(path, NULL, CGRectGetMinX(rectSelected), CGRectGetMaxY(rectSelected), CGRectGetMidX(rectSelected), CGRectGetMaxY(rectSelected), MEFCalendarContentSelectedCornerRadius);
            CGPathAddLineToPoint(path, NULL, CGRectGetMidX(rectSelected), CGRectGetMaxY(rectSelected));
            
        }   break;
        case MEFCalendarIndicatorContentSelectedTypeEnd:
        {
            CGPathMoveToPoint(path, NULL, CGRectGetMaxX(rectSelected), CGRectGetMidY(rectSelected));
            CGPathAddArcToPoint(path, NULL, CGRectGetMaxX(rectSelected), CGRectGetMinY(rectSelected), CGRectGetMidX(rectSelected), CGRectGetMinY(rectSelected), MEFCalendarContentSelectedCornerRadius);
            CGPathAddLineToPoint(path, NULL, CGRectGetMinX(rectSelected), CGRectGetMinY(rectSelected));
            CGPathAddLineToPoint(path, NULL, CGRectGetMinX(rectSelected), CGRectGetMaxY(rectSelected));
            CGPathAddLineToPoint(path, NULL, CGRectGetMidX(rectSelected), CGRectGetMaxY(rectSelected));
            CGPathMoveToPoint(path, NULL, CGRectGetMaxX(rectSelected), CGRectGetMidY(rectSelected));
            CGPathAddArcToPoint(path, NULL, CGRectGetMaxX(rectSelected), CGRectGetMaxY(rectSelected), CGRectGetMidX(rectSelected), CGRectGetMaxY(rectSelected), MEFCalendarContentSelectedCornerRadius);
            CGPathAddLineToPoint(path, NULL, CGRectGetMidX(rectSelected), CGRectGetMaxY(rectSelected));
            
        }   break;
        default:
            break;
    }
    return path;
}


#pragma mark - - date Helper
- (NSDate*) correctSelectedBeginDate:(NSDate*)date
{
    if ([self.selectedBeginDate  isEarlierThanDate:date]) {
        return date;
    }
    return self.selectedBeginDate;
}


- (NSDate*) correctSelectedEndDate:(NSDate*)date
{
    if ([self.selectedEndDate isLaterThanDate:date]) {
        return date;
    }
    return self.selectedEndDate;
}


- (NSDate*) nowDate
{
    if (self.contentType == MEFCalendarContentTypeDays) {
        return [self.calendarUTC nowDateWithoutTime];
    }
    NSDateComponents *components = [self.calendarUTC components:NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[self.calendarUTC nowDateWithoutTime]];
    return [self.calendarUTC dateFromComponents:components];
}

#pragma mark - - draw calendar
- (void) layoutSubviews
{
    [super layoutSubviews];
    CGRect rect = self.frame;
    CGFloat y = MEFCalendarContentLinePaddingTopBottom;
    for (MEFCalendarIndicatorContentView *indicatorContentView in self.indicatorContainers) {
        CGSize indicatorSize = CGSizeMake(self.frame.size.width, 0);
        CGFloat x = 0;
        BOOL needCenteringPosition = false;
        CGFloat maxWidth = 0;
        CGFloat mWidth = 0;
        for (MEFCalendarIndicatorView *calendarIndicatorView in indicatorContentView.indicators) {
            CGPoint center = CGPointZero;
            CGSize size = [calendarIndicatorView sizeThatFits:CGSizeMake(rect.size.width, 0)];
            if (calendarIndicatorView.isNeedCenteringCustomPosition) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(centerPositionDayOfWeek:calendarContentView:)]) {
                    center = [self.delegate centerPositionDayOfWeek:calendarIndicatorView.date calendarContentView:self];
                }
                calendarIndicatorView.customPosition = center;
                x = calendarIndicatorView.customPosition.x - size.width * 0.5;
            } else {
                needCenteringPosition = true;
                maxWidth += size.width;
                mWidth = fmax(mWidth, size.width);
            }
            CGRect rect = CGRectMake(x, y, size.width, size.height);
            calendarIndicatorView.frame = rect;
            indicatorSize.height = fmax(indicatorSize.height, size.height);
            
            
        }
        indicatorContentView.frame = CGRectMake(0, y, self.frame.size.width, indicatorSize.height);
        CGFloat size = indicatorContentView.frame.size.width / indicatorContentView.indicators.count;
        CGFloat xIndicator = 0;
        for (MEFCalendarIndicatorView *calendarIndicatorView in indicatorContentView.indicators) {
            if (needCenteringPosition) {
                calendarIndicatorView.frame = CGRectMake(xIndicator, calendarIndicatorView.frame.origin.y + (indicatorContentView.frame.size.height - calendarIndicatorView.frame.size.height) * 0.5, size, calendarIndicatorView.frame.size.height);
                xIndicator += calendarIndicatorView.frame.size.width;
            }
//             NSLog(@"layout date %@ day %@ size %@",calendarIndicatorView.date,[NSString stringWithFormat:@"%ld",(long)[calendarIndicatorView.date day]],NSStringFromCGSize(calendarIndicatorView.frame.size));
        }
        y += indicatorSize.height + MEFCalendarContentLinePadding * 2;
    }
}
- (void) drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    NSDate *now = [self nowDate];
    MEFCalendarIndicatorView *today = nil;
    for (MEFCalendarIndicatorContentView *indicatorContentView in self.indicatorContainers) {
        for (MEFCalendarIndicatorView *calendarIndicatorView in indicatorContentView.indicators) {
            if (calendarIndicatorView.lastDayOfMonth) {
                CGRect rect = calendarIndicatorView.frame;
                CGFloat size = 0.5;
                CGFloat offSet = 1;
                UIColor * grayColor = [UIColor colorWithRed:0 green:0.0 blue:0.0 alpha:0.125];
                CGContextSetFillColorWithColor(context, grayColor.CGColor);
                if (calendarIndicatorView.nextIndicatorView) {
                    CGFloat offSetToNext = (CGRectGetMinX(calendarIndicatorView.nextIndicatorView.frame) -  CGRectGetMaxX(calendarIndicatorView.frame)) * 0.5f;
                    CGContextFillRect(context, CGRectMake(0, CGRectGetMaxY(rect) + MEFCalendarContentLinePadding  - size, CGRectGetMaxX(rect) + offSetToNext - size, size));
                    CGFloat offSetDetail = (![calendarIndicatorView.nextIndicatorView.detailText isEmpty] && calendarIndicatorView.nextIndicatorView.detailText) ? [calendarIndicatorView.nextIndicatorView detailSizeThatFits:indicatorContentView.frame.size].height * 0.5 : 0;
                    
                    CGFloat yMin = fmax(CGRectGetMinY(rect) - MEFCalendarContentLinePadding  - offSetDetail + size, size);
                    CGFloat height = ((CGRectGetMaxY(rect) + MEFCalendarContentLinePadding  - size) - yMin);
                    
                    CGContextFillRect(context, CGRectMake(CGRectGetMaxX(rect) + offSetToNext, yMin, size, height));
                    
                    CGContextFillRect(context, CGRectMake(CGRectGetMaxX(rect) + offSetToNext + size, yMin, self.frame.size.width -  (CGRectGetMaxX(rect) + offSet), size));
                } else {
                    CGContextFillRect(context, CGRectMake(0, CGRectGetMaxY(rect) + MEFCalendarContentLinePaddingTopBottom * 0.5 - size, CGRectGetMaxX(self.frame), size));
                }
            }
        }
        if (indicatorContentView.selectedType != MEFCalendarIndicatorContentSelectedTypeNone) {
            CGMutablePathRef path = [self createPathWithIndicatorContentView:indicatorContentView];
            CGContextAddPath(context, path);
            CGPathRelease(path);
            if (self.selectedColor) {
                [self. selectedColor setFill];
            } else {
                [[UIColor redColor]setFill];
            }
            CGContextFillPath(context);
        }
        
        if (self.isVisibledToday && indicatorContentView.today) {
            today = indicatorContentView.hashIndicators[now];
        }
        for (MEFCalendarIndicatorView *calendarIndicatorView in indicatorContentView.indicators) {
            CGRect rect = calendarIndicatorView.frame;
//            CGSize sizea = [calendarIndicatorView sizeThatFits:CGSizeMake(self.frame.size.width, 0)];
            
//            if (!CGSizeEqualToSize(sizea, rect.size)) {
//                NSLog(@"draw date %@ day %@ size %@ new Size %@",calendarIndicatorView.date,[NSString stringWithFormat:@"%ld",(long)[calendarIndicatorView.date day]],NSStringFromCGSize(rect.size),NSStringFromCGSize(sizea));
//            }
//            [[UIColor orangeColor]setFill];
//            [[UIBezierPath bezierPathWithRect:rect] fill];
//
//         
//            CGContextFillPath(context);

            [calendarIndicatorView.dateText drawInRect:rect withAttributes:calendarIndicatorView.attributesDate];
            
            if (calendarIndicatorView.detailText && ![calendarIndicatorView.detailText isEmpty]) {
                CGRect rect = CGRectMake(0, 0, 0, 0);
                rect.size = [calendarIndicatorView.detailText sizeWithAttributes:calendarIndicatorView.attributesDetail];
                if (calendarIndicatorView.nextIndicatorView) {
                    rect.origin = CGPointMake(calendarIndicatorView.frame.origin.x,calendarIndicatorView.frame.origin.y - rect.size.height);
                } else {
                    CGFloat offSetToNext = (CGRectGetMinX(calendarIndicatorView.frame) -  CGRectGetMaxX(calendarIndicatorView.prevIndicatorView.frame)) * 0.5f + MEFCalendarContentDetailPadding;
                    rect.origin = CGPointMake(CGRectGetMaxX(calendarIndicatorView.prevIndicatorView.frame) + offSetToNext ,calendarIndicatorView.frame.origin.y - rect.size.height);
                }
                [calendarIndicatorView.detailText drawInRect:rect withAttributes:calendarIndicatorView.attributesDetail];
            }
        }
    }
    
    if (today) {
        CGRect currentIndicatorRect = today.frame;
        CGPoint point = CGPointMake(currentIndicatorRect.origin.x + currentIndicatorRect.size.width * 0.5, currentIndicatorRect.origin.y + currentIndicatorRect.size.height + MEFCalendarContentRadiusToday + 0.5); // ?
        CGContextAddPath(context, [UIBezierPath bezierPathWithArcCenter:point radius:MEFCalendarContentRadiusToday startAngle:0 endAngle:M_PI*2 clockwise:false].CGPath);
        if (self.tintTodayColor) {
            [self.tintTodayColor setFill];
        } else {
            [[UIColor orangeColor]setFill];
        }
     
        CGContextFillPath(context);

    }
//    CGContextAddPath(context, [UIBezierPath bezierPathWithRect:aaaa].CGPath);
//    [[UIColor redColor]setFill];
//    CGContextFillPath(context);
}


void drawLinearGradient(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef endColor)
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = @[(__bridge id) startColor, (__bridge id) endColor];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextSaveGState(context);
    CGContextAddRect(context, rect);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

#pragma mark - - properties
- (NSMutableArray*) indicatorContainers
{
    if (!_indicatorContainers) {
        _indicatorContainers = [NSMutableArray array];
    }
    return _indicatorContainers;
}
- (NSMutableDictionary*) hashIndicatorContainers
{
    if (!_hashIndicatorContainers) {
        _hashIndicatorContainers = [NSMutableDictionary dictionary];
    }
    return _hashIndicatorContainers;
}
@end
