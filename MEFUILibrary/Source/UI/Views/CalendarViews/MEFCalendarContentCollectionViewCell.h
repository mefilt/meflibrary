//
//  MEFCalendarContentCollectionViewCell.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 16.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEFCalendarContentView.h"
@interface MEFCalendarContentCollectionViewCell : UICollectionViewCell


@property (strong, nonatomic) IBOutlet MEFCalendarContentView *calendarContentView;

@end
