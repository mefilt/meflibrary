//
//  MEFCalendarIndicatorContentView.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 14.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFCalendarIndicatorContentView.h"

@implementation MEFCalendarIndicatorContentView
@synthesize empty=_empty;
- (id) init
{
    if (self = [super init]) {
        [self initialize];
    }
    return self;
}

- (void) initialize
{
    self.indicators = [NSMutableArray array];
    self.hashIndicators = [NSMutableDictionary dictionary];
}

- (CGSize) sizeThatFits:(CGSize)size
{
    CGFloat maxHeight = 0;
    for (MEFCalendarIndicatorView *indicator in self.indicators) {
        CGSize size = [indicator sizeThatFits:size];
        maxHeight = fmax(size.height, maxHeight);
    }
    return CGSizeMake(size.width, maxHeight);
}



- (void) addIndicatorView:(MEFCalendarIndicatorView*)indicatorView
{
    [self.indicators addObject:indicatorView];
    self.hashIndicators[indicatorView.date] = indicatorView;
 
}




- (CGPoint) midPositionBetweenNextAndCurrentContentView:(MEFCalendarIndicatorView *)indicatorView
{
    MEFCalendarIndicatorView *nextIndicatorView = indicatorView.nextIndicatorView;
    if (nextIndicatorView) {
        CGRect indicatorViewRect = indicatorView.frame;
        CGRect nextIndicatorViewRect = nextIndicatorView.frame;
        CGFloat offSetX = (CGRectGetMinX(nextIndicatorViewRect) - CGRectGetMaxX(indicatorViewRect)) * 0.5;
        CGFloat offSetY = (CGRectGetMidY(nextIndicatorViewRect) + CGRectGetMidY(indicatorViewRect)) * 0.5;
        return (CGPoint){CGRectGetMaxX(indicatorView.frame) + offSetX,offSetY};
    }
    return (CGPoint){CGRectGetMaxX(indicatorView.frame) + (self.frame.size.width - CGRectGetMaxX(indicatorView.frame)) * 0.5,indicatorView.frame.size.height * 0.5};
}


- (CGPoint) midPositionBetweenPrevAndCurrentContentView:(MEFCalendarIndicatorView *)indicatorView
{
    MEFCalendarIndicatorView *prevIndicatorView = indicatorView.prevIndicatorView;
    if (prevIndicatorView) {
        CGRect indicatorViewRect = indicatorView.frame;
        CGRect prevIndicatorViewRect = prevIndicatorView.frame;
        CGFloat offSetX = (CGRectGetMinX(indicatorViewRect) - CGRectGetMaxX(prevIndicatorViewRect)) * 0.5;
        CGFloat offSetY = (CGRectGetMidY(prevIndicatorViewRect) + CGRectGetMidY(indicatorViewRect)) * 0.5;
        return (CGPoint){CGRectGetMinX(indicatorView.frame) - offSetX,offSetY};
    }
    return (CGPoint){indicatorView.frame.origin.x * 0.5,indicatorView.frame.size.height * 0.5};
}


+ (instancetype) createInctance
{
    return [[MEFCalendarIndicatorContentView alloc]init];
}

- (void) willReturnToPool:(MEFObjectPool *)pool
{
    for (MEFCalendarIndicatorView *indicator in self.indicators) {
        indicator.empty = true;
    }
    [self.hashIndicators removeAllObjects];
    [self.indicators removeAllObjects];
    self.selectedType = MEFCalendarIndicatorContentSelectedTypeNone;
    self.today = false;
    self.beginDate = nil;
    self.endDate = nil;
    self.selectedBeginDate = nil;
    self.selectedEndDate = nil;
    self.nextIndicatorContentView = nil;
    self.prevIndicatorContentView = nil;
    
}
- (void) setToday:(BOOL)today
{
    _today = today;
}
@end

