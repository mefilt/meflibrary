//
//  MEFCalendarTitleView.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 17.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEFCalendarTitleView : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
