//
//  MEFAlertView.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 05.11.15.
//  Copyright © 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEFAlertView : UIAlertView
@property (nonatomic, readwrite, copy) void (^clickedButtonAtIndex) (NSInteger buttonIndex);
@property (nonatomic, readwrite, copy) void (^clickedButtonCancel) ();
@end
