//
//  MEFSVGImageView.m
//  MEFLibrary
//
//  Created by Mefilt on 26.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "MEFSVGImageView.h"
#import "MEFSVGImageSheet.h"
@interface MEFSVGImageView ()

@property (nonatomic, readwrite, strong) CALayer *renderLayer;
@end
@implementation MEFSVGImageView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
          self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void) setFlipY:(BOOL)flipY
{
    _flipY = flipY;
     [self setNeedsDisplay];
    
}
- (void) setFlipX:(BOOL)flipX
{
    _flipX = flipX;
    [self setNeedsDisplay];
}

- (void) setAngle:(CGFloat)angle
{
    _angle = angle;
    [self setNeedsDisplay];
}

-(id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if( self )
    {
        self.backgroundColor = [UIColor clearColor];

    }
    return self;
}
- (id) initWithImageIdentifier:(NSString *)imageIdentifier
{
    if (self = [super init]) {
        [self setImageIdentifier:imageIdentifier];
    }
    return self;
}
- (void) setImageSVGFile:(NSString *)SVGFilename modifyLayer:(void (^) (CALayer *renderLayer))modifiLayer
{
    self.renderLayer = [[MEFSVGImageSheet SVGImageSheet] layerWithSVGFilename:SVGFilename];
    if (modifiLayer) {
        modifiLayer(self.renderLayer);
    }
    [self setClipsToBounds:true];
    [self setNeedsDisplay];

}
- (void) setImageIdentifier:(NSString *)imageIdentifier modifyLayer:(void (^) (CALayer *renderLayer))modifiLayer
{
    self.renderLayer = [[MEFSVGImageSheet SVGImageSheet] layerWithIdentifier:imageIdentifier];
    if (modifiLayer) {
        modifiLayer(self.renderLayer);
    }
    [self setClipsToBounds:true];
    [self setNeedsDisplay];
}
- (void) setImageSVGFilename:(NSString *)imageSVGFilename
{
    _imageSVGFilename = imageSVGFilename;
    [self setImageSVGFile:imageSVGFilename modifyLayer:NULL];
}
- (void) setImageIdentifier:(NSString*)imageIdentifier
{
    _imageIdentifier = imageIdentifier;
    [self setImageIdentifier:imageIdentifier modifyLayer:nil];
}

- (void) setSVGFillColor:(UIColor *)SVGColor
{
    _SVGFillColor = SVGColor;
    if ([self.renderLayer isKindOfClass:[CAShapeLayer class]]) {
        [(CAShapeLayer*)self.renderLayer setFillColor:SVGColor.CGColor];
        [self setNeedsDisplay];
    }
}
- (void) setSVGStrokeColor:(UIColor *)SVGColor
{
    _SVGStrokeColor = SVGColor;
    if ([self.renderLayer isKindOfClass:[CAShapeLayer class]]) {
//        [(CAShapeLayer*)self.renderLayer setStrokeColor:SVGColor.CGColor];
         [(CAShapeLayer*)self.renderLayer setFillColor:SVGColor.CGColor];
        
         [self setNeedsDisplay];
    }
}
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)
- (void)drawRect:(CGRect)rect
{
    CALayer *layer = self.renderLayer;
    if (!layer && self.imageIdentifier) {
        
        layer = self.renderLayer = [[MEFSVGImageSheet SVGImageSheet] layerWithIdentifier:self.imageIdentifier];
    }    
    CGRect imageBounds = CGRectMake(0, 0, layer.frame.size.width, layer.frame.size.height);
    CGSize scaleConvertImageToView = CGSizeMake(rect.size.width / imageBounds.size.width, rect.size.height / imageBounds.size.height);
    CGFloat minScale = fminf(scaleConvertImageToView.width, scaleConvertImageToView.height);
    scaleConvertImageToView.width = scaleConvertImageToView.height = minScale;
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    
    CGSize sizeCTM = CGSizeMake((rect.size.width - imageBounds.size.width * scaleConvertImageToView.width) * 0.5, (rect.size.height - imageBounds.size.height * scaleConvertImageToView.height) * 0.5);
    
    
    CGSize imageSize = CGSizeMake(imageBounds.size.width * scaleConvertImageToView.width, imageBounds.size.height * scaleConvertImageToView.height);
    CGFloat scaleX,scaleY;
    scaleX = scaleY = minScale;
    CGSize flipSize = CGSizeMake(sizeCTM.width, sizeCTM.height);
    if (self.flipX) {
        flipSize.width = imageSize.width + sizeCTM.width;
        scaleX *= -1;
    }
    if (self.flipY) {
        flipSize.height = imageSize.height + sizeCTM.height;
        scaleY *= -1;
    }

//    CGFloat angle = self.angle;
//    CGFloat x,y;
//    x = cos(DEGREES_TO_RADIANS(angle));
//    y = sin(DEGREES_TO_RADIANS(angle));
//    CGFloat w = ((rect.size.width * 0.5) * x);
//    CGFloat h = (rect.size.height - rect.size.height * y);
//
    CGContextTranslateCTM(context,flipSize.width, flipSize.height);
    CGContextScaleCTM(context, scaleX, scaleY);

    
//    CGContextTranslateCTM(context,flipSize.width * 0.5, flipSize.height);
//    CGContextRotateCTM(context, DEGREES_TO_RADIANS(angle));
//    CGContextScaleCTM(context, scaleX, scaleY);
    
    [layer renderInContext:context];
    CGContextRestoreGState(context);

}

@end