//
//  MEFSVGImageView.h
//  MEFLibrary
//
//  Created by Mefilt on 26.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/CALayer.h>
//IB_DESIGNABLE
@interface MEFSVGImageView : UIView
@property (nonatomic, readonly, strong) CALayer *renderLayer;
@property (nonatomic, readwrite, strong) IBInspectable NSString *imageIdentifier;
@property (nonatomic, readwrite, strong) IBInspectable NSString *imageSVGFilename;
@property (nonatomic, readwrite, strong) IBInspectable UIColor *SVGStrokeColor;
@property (nonatomic, readwrite, strong) IBInspectable UIColor *SVGFillColor;
@property (nonatomic, readwrite, assign) BOOL flipY;
@property (nonatomic, readwrite, assign) BOOL flipX;
@property (nonatomic, readwrite, assign) CGFloat angle;
- (void) setImageIdentifier:(NSString *)imageIdentifier modifyLayer:(void (^) (CALayer *renderLayer))modifiLayer;
- (void) setImageSVGFile:(NSString *)SVGFilename modifyLayer:(void (^) (CALayer *renderLayer))modifiLayer;

- (id) initWithImageIdentifier:(NSString*)imageIdentifier;

@end
