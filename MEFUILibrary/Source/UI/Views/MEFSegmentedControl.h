//
//  MEFSegmentedControl.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 09.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MEFSegmentedControl : UIControl
@property (nonatomic, readwrite, strong) UIColor *tintColor;
@property (nonatomic, readonly, assign) NSUInteger selectedSegmentAtIndex;

@property (nonatomic, readwrite, copy) void (^changedValue)(NSUInteger selectedSegmentAtIndex, NSString *title, MEFSegmentedControl *control);
- (void) selectSegmentAtIndex:(NSUInteger)index;
- (void) selectSegmentAtTitle:(NSString*)title;
- (void) removeAll;
- (void) addTitle:(NSString *)title;
- (NSString *)titleForSegmentAtIndex:(NSUInteger)segment;
- (void) reloadSegments;

@end
