//
//  MEFPanGestureRecognizer.h
//  ipad_project
//
//  Created by Прокофьев Руслан on 16.10.13.
//  Copyright (c) 2013 Прокофьев Руслан. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEFPanGestureRecognizer : UIPanGestureRecognizer
@property (assign, nonatomic) CGPoint prevLocation;
@end
