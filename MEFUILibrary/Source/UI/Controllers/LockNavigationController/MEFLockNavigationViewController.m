//
//  BSLockNavigationViewController.m
//  Katusha
//
//  Created by Prokofev Ruslan on 13.04.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFLockNavigationViewController.h"

@interface MEFLockNavigationViewController ()

@end

@interface UINavigationController (BSLockNavigationViewController)

- (void)didShowViewController:(UIViewController *)viewController animated:(BOOL)animated;

@end

@interface MEFLockNavigationViewController ()


@end

@implementation MEFLockNavigationViewController

#pragma mark - Push

- (void) setViewControllers:(NSArray *)viewControllers animated:(BOOL)animated
{
    if (!self.shouldIgnorePushingViewControllers) {
        self.shouldIgnorePushingViewControllers = true;
//        [CATransaction begin];
//        [CATransaction setCompletionBlock:^{
//            self.shouldIgnorePushingViewControllers = false;
//        }];
        [super setViewControllers:viewControllers animated:animated];
    }

}
- (void) pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (!self.shouldIgnorePushingViewControllers) {
        self.shouldIgnorePushingViewControllers = YES;
        [super pushViewController:viewController animated:animated];
    }
    
}

#pragma mark - Private API

- (void) unlock
{
    self.shouldIgnorePushingViewControllers = NO;
}
// This is confirmed to be App Store safe.
// If you feel uncomfortable to use Private API, you could also use the delegate method navigationController:didShowViewController:animated:.
- (void) didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [super didShowViewController:viewController animated:animated];
    [self performSelector:@selector(unlock) withObject:nil afterDelay:0.15];

}
@end
