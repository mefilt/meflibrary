//
//  BSLockNavigationViewController.h
//  Katusha
//
//  Created by Prokofev Ruslan on 13.04.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEFLockNavigationViewController : UINavigationController
@property (nonatomic, assign) BOOL shouldIgnorePushingViewControllers;
@end
