//
//  BSNavigationSlitViewController.m
//  Katusha
//
//  Created by Prokofev Ruslan on 18.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFSplitViewController.h"
#import "MEFNavigationControllerAnimator.h"
#import "MEFLockNavigationViewController.h"
#import "MEFShadowView.h"
//#import "MEFSplitViewLeftSegue.h"
//#import "MEFSplitViewRightSegue.h"
@interface MEFSplitViewController () <UINavigationControllerDelegate>
{
    
    BOOL _isInitLeftNav;
    BOOL _isInitRightNav;
    
    
}
@property (nonatomic, readwrite, strong) UIViewController *leftViewController;
@property (nonatomic, readwrite, strong) UIViewController <MEFSplitViewViewConrollerProtocol> *rightViewController;
@property (nonatomic, readwrite, strong) UIViewController *detailViewController;
@property (nonatomic, readwrite, assign) CGFloat shiftViewControllerOffSet;
@property (nonatomic, readwrite, strong) UISwipeGestureRecognizer *swipeGesture;
@property (nonatomic, readwrite, strong) MEFShadowView *shadowView;
@property (nonatomic, readwrite, strong) MEFShadowView *shadowDetailView;
@property (nonatomic, readwrite, assign, getter=isShowDetailViewController) BOOL showDetailViewController;
@property (nonatomic, readwrite, strong) UINavigationController *detailNavigationViewController;
@property (nonatomic, readwrite, assign, getter=isCurrentSegueAnimated) BOOL currentSegueAnimated;
@end

@implementation MEFSplitViewController

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        _widthLeftViewController = -1;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.shadowView = [[MEFShadowView alloc]initWithFrame:CGRectMake(0, 0, BSSplitViewShadowWidth, 0)];
    self.shadowDetailView = [[MEFShadowView alloc]initWithFrame:CGRectMake(0, 0, BSSplitViewShadowWidth, 0)];
    [self.shadowDetailView setHidden:true];
    self.shadowDetailView.translatesAutoresizingMaskIntoConstraints = YES;
    [self.view addSubview:self.shadowDetailView];
    
    self.shadowView.translatesAutoresizingMaskIntoConstraints = YES;
    [self.view addSubview:self.shadowView];
    self.swipeGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeGesture:)];
    [self.view addGestureRecognizer:self.swipeGesture];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self layoutSubViews];
}

- (CGFloat) actualWidthLeftViewController
{
    CGFloat width = 0;
    if (_widthLeftViewController != 0) {
        width = _widthLeftViewController;
    } else {
        width = self.view.bounds.size.width * 0.5;
    }
    return width;
}

- (CGFloat) actualWidthDetailViewController
{
    CGFloat width = self.view.bounds.size.width * 0.45;
    
    return width;
}

- (CGFloat) actualWidthRightViewController
{
    CGFloat width = self.view.bounds.size.width - [self actualWidthLeftViewController];
    
    return width;
}

- (void) layoutSubViews
{
    CGRect mainBounds = self.view.bounds;
    CGRect leftFrame = self.leftViewController.view.frame;
    CGRect rightFrame = self.rightViewController.view.frame;
    CGRect detailFrame = self.detailNavigationViewController.view.frame;
    
    
    leftFrame.size = CGSizeMake([self actualWidthLeftViewController], mainBounds.size.height);
    rightFrame.size = CGSizeMake([self actualWidthRightViewController] + self.shiftViewControllerOffSet, mainBounds.size.height);
    detailFrame.size = CGSizeMake([self actualWidthDetailViewController], self.view.bounds.size.height);
    
    CGFloat y = [UIApplication sharedApplication].statusBarFrame.origin.y +     [UIApplication sharedApplication].statusBarFrame.size.height;
    
    self.leftViewController.view.frame = leftFrame;
    self.rightViewController.view.frame = rightFrame;
    self.detailNavigationViewController.view.frame = detailFrame;
    [self.shadowView  setFrame:CGRectMake(CGRectGetMinX(rightFrame) - self.shadowView.frame.size.width, y, self.shadowView.frame.size.width, self.view.frame.size.height)];
    
}

- (CGRect) visibleRightNavigationViewControllerFrame
{
    CGRect rightFrame = self.rightViewController.view.frame;
    CGRect mainBounds = self.view.bounds;
    rightFrame.size = CGSizeMake([self actualWidthRightViewController], mainBounds.size.height);
    rightFrame.origin.x = [self actualWidthLeftViewController];
    return rightFrame;
}



- (UIViewController*) setLeftViewControllerWithSegueIdentifier:(NSString *)identifier animated:(BOOL)animated
{
    self.currentSegueAnimated = animated;
    @try {
        [self performSegueWithIdentifier:identifier sender:nil];
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    return self.leftViewController;
}

- (UIViewController*) setRightViewControllerWithSegueIdentifier:(NSString *)identifier animated:(BOOL)animated
{
    self.currentSegueAnimated = animated;
    @try {
        [self performSegueWithIdentifier:identifier sender:nil];
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    return self.rightViewController;
}

#pragma mark - - left controller
- (void) setLeftViewController:(UIViewController *)leftViewController animated:(BOOL)animated
{
    //    _isInitLeftNav = true;
    if (self.leftViewController) {
        [self.leftViewController willMoveToParentViewController:nil];
        [self.leftViewController.view removeFromSuperview];
        [self.leftViewController removeFromParentViewController];
        self.leftViewController = nil;
    }
    self.leftViewController = leftViewController;
    [self addChildViewController:self.leftViewController];
    CGRect mainBounds = self.view.bounds;
    CGRect leftFrame = self.leftViewController.view.frame;
    leftFrame.size = CGSizeMake([self actualWidthLeftViewController], mainBounds.size.height);
    self.leftViewController.view.frame = leftFrame;
    [self.view addSubview:self.leftViewController.view];
    [self.leftViewController didMoveToParentViewController:self];
    [self.view bringSubviewToFront:self.shadowView];
}
#pragma mark - - right

- (void) setRightViewController:(UIViewController <MEFSplitViewViewConrollerProtocol>*)rightViewController animated:(BOOL)animated
{
    [self setRightViewController:rightViewController];
}

- (void) setRightViewController:(UIViewController<MEFSplitViewViewConrollerProtocol> *)rightViewController
{
    _isInitRightNav = true;
    [self hideDetailViewControllerWithAnimated:true];
    if (self.rightViewController) {
        [self.rightViewController willMoveToParentViewController:nil];
        [self.rightViewController.view removeFromSuperview];
        [self.rightViewController removeFromParentViewController];
        self.rightViewController = nil;
    }
    _rightViewController = rightViewController;
    [self addChildViewController:self.rightViewController];
    self.rightViewController.view.frame = [self visibleRightNavigationViewControllerFrame];
    [self.view addSubview:self.rightViewController.view];
    [self.rightViewController didMoveToParentViewController:self];
    
    
    
    //    if ([rightViewController isKindOfClass:[MEFViewController class]]) {
    //        [self addViewController:(MEFViewController*)rightViewController];
    //    }
    
    //    if (self.rightViewController) {
    //        if ([self.rightViewController respondsToSelector:@selector(shiftViewControllerOffSet)]) {
    //            [self.rightViewController removeObserver:self forKeyPath:@"shiftViewControllerOffSet"];
    //        }
    //    }
    //
    //    if ([self.rightViewController respondsToSelector:@selector(shiftViewControllerOffSet)]) {
    //        [self.rightViewController addObserver:self forKeyPath:@"shiftViewControllerOffSet" options:NSKeyValueObservingOptionNew context:nil];
    //    }
    
    [self.view bringSubviewToFront:self.shadowView];
}



- (void) showRepeatDetailViewController:(UIViewController *)detailViewController animated:(BOOL)animated
{
    _detailViewController = detailViewController;
    [self.detailNavigationViewController setViewControllers:@[_detailViewController] animated:animated];
    
}

- (void) createDetailNavigationViewController
{
    self.detailNavigationViewController = [[MEFLockNavigationViewController alloc]init];
    [self.detailNavigationViewController setDelegate:self];
    [self addChildViewController:self.detailNavigationViewController];
    [self.view addSubview:self.detailNavigationViewController.view];
    [self.detailNavigationViewController didMoveToParentViewController:self];
    [self.view bringSubviewToFront:self.rightViewController.view];
}

- (void) removeDetailNavigationViewController
{
    if (self.detailNavigationViewController) {
        [self.detailNavigationViewController willMoveToParentViewController:nil];
        [self.detailNavigationViewController.view removeFromSuperview];
        [self.detailNavigationViewController removeFromParentViewController];
        self.detailNavigationViewController = nil;
    }
}

- (void) showDetailViewController:(UIViewController *)detailViewController animated:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] postNotificationName:MEFSplitViewNotificationShowDetailView object:self];
    
    
    
    if (_detailViewController && self.detailNavigationViewController) {
        [self showRepeatDetailViewController:detailViewController animated:animated];
        return;
    }
    
    [self createDetailNavigationViewController];
    
    
    _detailViewController = detailViewController;
    [self.detailNavigationViewController setViewControllers:@[_detailViewController] animated:true];
    [self.shadowDetailView setHidden:false];
    [self.view bringSubviewToFront:self.shadowDetailView];
    CGFloat y = [UIApplication sharedApplication].statusBarFrame.origin.y +     [UIApplication sharedApplication].statusBarFrame.size.height;
    if (animated) {
        
        CGRect detailNavigationViewControllerFrameHide = CGRectMake(self.view.frame.size.width, 0, [self actualWidthDetailViewController], self.view.frame.size.height);
        self.detailNavigationViewController.view.frame = detailNavigationViewControllerFrameHide;
        [self.shadowDetailView  setFrame:CGRectMake(CGRectGetMinX(detailNavigationViewControllerFrameHide) - self.shadowDetailView.frame.size.width, y, self.shadowDetailView.frame.size.width, self.view.frame.size.height)];
        
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationCurveEaseInOut animations:^{
            CGRect rightViewControllerFrame = self.rightViewController.view.frame;
            rightViewControllerFrame.origin.x = self.view.bounds.size.width - (rightViewControllerFrame.size.width + [self actualWidthDetailViewController]);
            [self.rightViewController.view setFrame:rightViewControllerFrame];
            CGRect detailNavigationViewControllerFrameShow = CGRectMake(rightViewControllerFrame.origin.x + rightViewControllerFrame.size.width, 0, [self actualWidthDetailViewController], self.view.bounds.size.height);
            self.detailNavigationViewController.view.frame = detailNavigationViewControllerFrameShow;
            [self.shadowView  setFrame:CGRectMake(CGRectGetMinX(rightViewControllerFrame) - self.shadowView.frame.size.width, 0, self.shadowView.frame.size.width, self.view.frame.size.height)];
            [self.shadowDetailView  setFrame:CGRectMake(CGRectGetMinX(detailNavigationViewControllerFrameShow) - self.shadowDetailView.frame.size.width, y, self.shadowDetailView.frame.size.width, self.view.frame.size.height)];
        } completion:^(BOOL finished) {
        }];
    } else {
        CGRect rightViewControllerFrame = self.rightViewController.view.frame;
        rightViewControllerFrame.origin.x = self.view.bounds.size.width - (rightViewControllerFrame.size.width + [self actualWidthDetailViewController]);
        [self.rightViewController.view setFrame:rightViewControllerFrame];
        CGRect detailNavigationViewControllerFrameShow = CGRectMake(rightViewControllerFrame.origin.x + rightViewControllerFrame.size.width, 0, [self actualWidthDetailViewController], self.view.bounds.size.height);
        self.detailNavigationViewController.view.frame = detailNavigationViewControllerFrameShow;
        [self.shadowView  setFrame:CGRectMake(CGRectGetMinX(rightViewControllerFrame) - self.shadowView.frame.size.width, 0, self.shadowView.frame.size.width, self.view.frame.size.height)];
        [self.shadowDetailView  setFrame:CGRectMake(CGRectGetMinX(detailNavigationViewControllerFrameShow) - self.shadowDetailView.frame.size.width, y, self.shadowDetailView.frame.size.width, self.view.frame.size.height)];
    }
}

- (void) hideDetailViewControllerWithAnimated:(BOOL)animated
{
    [self.view bringSubviewToFront:self.shadowDetailView];
    CGFloat y = [UIApplication sharedApplication].statusBarFrame.origin.y +     [UIApplication sharedApplication].statusBarFrame.size.height;
    if (animated) {
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews animations:^{
            CGRect rightViewControllerframe = [self visibleRightNavigationViewControllerFrame];
            [self.rightViewController.view setFrame:rightViewControllerframe];
            
            CGRect detailNavigationViewControllerFrameShow =CGRectMake(rightViewControllerframe.origin.x + rightViewControllerframe.size.width, y, [self actualWidthDetailViewController], self.view.bounds.size.height);
            self.detailNavigationViewController.view.frame = detailNavigationViewControllerFrameShow;
            [self.shadowView  setFrame:CGRectMake(CGRectGetMinX(rightViewControllerframe) - self.shadowView.frame.size.width, 0, self.shadowView.frame.size.width, self.view.frame.size.height)];
            [self.shadowDetailView  setFrame:CGRectMake(CGRectGetMinX(detailNavigationViewControllerFrameShow) - self.shadowDetailView.frame.size.width, y, self.shadowDetailView.frame.size.width, self.view.frame.size.height)];
        } completion:^(BOOL finished) {
            [self.shadowDetailView setHidden:true];
            [self removeDetailNavigationViewController];
            _detailViewController = nil;
        }];
    } else {
        CGRect rightViewControllerframe = [self visibleRightNavigationViewControllerFrame];
        [self.rightViewController.view setFrame:rightViewControllerframe];
        
        CGRect detailNavigationViewControllerFrameShow =CGRectMake(rightViewControllerframe.origin.x + rightViewControllerframe.size.width, y, [self actualWidthDetailViewController], self.view.bounds.size.height);
        self.detailNavigationViewController.view.frame = detailNavigationViewControllerFrameShow;
        [self.shadowView  setFrame:CGRectMake(CGRectGetMinX(rightViewControllerframe) - self.shadowView.frame.size.width, 0, self.shadowView.frame.size.width, self.view.frame.size.height)];
        [self.shadowDetailView  setFrame:CGRectMake(CGRectGetMinX(detailNavigationViewControllerFrameShow) - self.shadowDetailView.frame.size.width, y, self.shadowDetailView.frame.size.width, self.view.frame.size.height)];
        [self.shadowDetailView setHidden:true];
        [self removeDetailNavigationViewController];
        _detailViewController = nil;
    }
    
}


#pragma mark - - Swipe gesture

- (void) swipeGesture:(UISwipeGestureRecognizer*)gesture
{
    if (!_detailViewController) {
        return;
    }
    [self hideDetailViewControllerWithAnimated:true];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (![keyPath isEqualToString:@"shiftViewControllerOffSet"]) {
        return;
    }
    NSNumber *value = [change objectForKey:NSKeyValueChangeNewKey];
    CGFloat offset = [value floatValue];
    
    self.shiftViewControllerOffSet = offset;
    CGFloat y = [UIApplication sharedApplication].statusBarFrame.origin.y +     [UIApplication sharedApplication].statusBarFrame.size.height;
    
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut animations:^{
        CGRect mainBounds = self.view.bounds;
        CGRect rightFrame = self.rightViewController.view.frame;
        
        rightFrame.size = CGSizeMake([self actualWidthRightViewController] + 0, mainBounds.size.height);
        rightFrame.origin = CGPointMake(self.shiftViewControllerOffSet , 0);
        self.rightViewController.view.bounds = rightFrame;
        
        
        [self.shadowView  setFrame:CGRectMake(CGRectGetMinX(self.rightViewController.view.frame) -self.shiftViewControllerOffSet  - self.shadowView.frame.size.width, y, self.shadowView.frame.size.width, self.view.frame.size.height)];
        
    } completion:^(BOOL finished) {
        
    }];
    
}
#pragma mark - - navigation controller;

- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                   animationControllerForOperation:(UINavigationControllerOperation)operation
                                                fromViewController:(UIViewController *)fromVC
                                                  toViewController:(UIViewController *)toVC
{
    
    
    return [MEFNavigationControllerAnimator new];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //    if ([segue isKindOfClass:[MEFSplitViewLeftSegue class]]) {
    //        [(MEFSplitViewLeftSegue*)segue setAnimated:self.isCurrentSegueAnimated];
    //    } else if ([segue isKindOfClass:[MEFSplitViewRightSegue class]]) {
    //        [(MEFSplitViewRightSegue*)segue setAnimated:self.isCurrentSegueAnimated];
    //
    //    }
}

- (void) dealloc
{
    @try {
        if (self.rightViewController  && [self.rightViewController respondsToSelector:@selector(shiftViewControllerOffSet)]) {
            [self.rightViewController removeObserver:self forKeyPath:@"shiftViewControllerOffSet"];
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
@end
