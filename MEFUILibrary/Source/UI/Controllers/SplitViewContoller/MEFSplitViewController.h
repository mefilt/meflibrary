//
//  BSNavigationSlitViewController.h
//  Katusha
//
//  Created by Prokofev Ruslan on 18.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEFAdaptiveNavigationController.h"
#import "MEFSplitViewViewConrollerProtocol.h"
static  NSString * const MEFSplitViewNotificationShowDetailView = @"BSSplitViewtNotificationShowDetailView";
static  CGFloat const BSSplitViewShadowWidth = 10.0f;
@interface MEFSplitViewController : MEFAdaptiveNavigationController
{
}
@property (nonatomic, readonly, strong) UIViewController *leftViewController;
@property (nonatomic, readonly, strong) UIViewController <MEFSplitViewViewConrollerProtocol> *rightViewController;
@property (nonatomic, readwrite, assign) CGFloat widthLeftViewController;



- (UIViewController*) setLeftViewControllerWithSegueIdentifier:(NSString*)identifier animated:(BOOL)animated;
- (UIViewController*) setRightViewControllerWithSegueIdentifier:(NSString*)identifier animated:(BOOL)animated;
//- (void) showDetailViewControllerWithSegueIdentifier:(NSString*)identifier animated:(BOOL)animated;


- (void) setLeftViewController:(UIViewController*)leftViewController animated:(BOOL)animated;
- (void) setRightViewController:(UIViewController <MEFSplitViewViewConrollerProtocol>*)rightViewController animated:(BOOL)animated;

- (void) showDetailViewController:(UIViewController *)detailViewController animated:(BOOL)animated;
- (void) hideDetailViewControllerWithAnimated:(BOOL)animated;
@end
