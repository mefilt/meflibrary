//
//  MEFContainerController.h
//  Katusha
//
//  Created by Prokofev Ruslan on 12.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEFViewController.h"

typedef NS_ENUM(NSUInteger, MEFContainerViewAnimateType) {
    MEFContainerViewAnimateTypeNone,
    MEFContainerViewAnimateTypeSlide,
    MEFContainerViewAnimateTypeFade,
};
/**
 MEFContainerViewController support segue and storyboard
 use View container and embed segue set indificator "containerRoot"
**/
static NSString * const MEFSegueContainerEmbed = @"containerRoot";
@interface MEFContainerViewController : MEFViewController
{
    UIViewController *_embedViewController;
}

@property (nonatomic, readonly, strong) UIViewController *embedViewController;
@property (nonatomic, readwrite, assign) UIEdgeInsets embedViewEdgeInserts;


- (id) setViewControllerWithSegueIdentifier:(NSString*)identifier animateType:(MEFContainerViewAnimateType)type;


- (void) setViewController:(UIViewController*)controller animateType:(MEFContainerViewAnimateType)type;

- (void) updateContentWithAnimated:(BOOL)animated;
- (void) addTopView:(UIView*)topView animated:(BOOL)animated;
- (void) removeTopView:(UIView*)topView animated:(BOOL)animated;

- (void) addBottomView:(UIView*)bottomView animated:(BOOL)animated;
- (void) removeBottomView:(UIView*)bottomView animated:(BOOL)animated;

- (void) showModalView:(UIView*)view animated:(BOOL)animated hideCallback:(void (^) ()) hideCallback;
- (void) hideModalViewWithAnimated:(BOOL)animated;
@end


@interface MEFContainerViewController (Override)
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;
@end

