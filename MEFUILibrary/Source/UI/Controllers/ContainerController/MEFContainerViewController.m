 //
//  MEFContainerController.m
//  Katusha
//
//  Created by Prokofev Ruslan on 12.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//
#import "MEFContainerViewController.h"
#import "MEFContainerSegue.h"

 @interface MEFContainerViewController ()
{

}
@property (nonatomic, readwrite, copy) void (^hideCallback) ();
@property (nonatomic, readwrite, strong) NSMutableArray *topViews;
@property (nonatomic, readwrite, strong) NSMutableArray *bottomViews;
@property (nonatomic, readwrite, strong) NSMutableArray *topRemoveViews;
@property (nonatomic, readwrite, strong) NSMutableArray *bottomRemoveViews;
@property (nonatomic, readwrite, strong) UIView *shadowView;
@property (nonatomic, readwrite, strong) UIView *modalView;
@property (nonatomic, readwrite, strong) UISwipeGestureRecognizer *gestureSwipe;
@property (nonatomic, readwrite, assign) MEFContainerViewAnimateType currentSegueAnimateType;
@end
@implementation MEFContainerViewController
- (id) init
{
    if (self = [super init]) {
      
    }
    return self;
}


- (id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
    
    }
    return self;
}
- (void) hideTopView:(UIView*)view
{
    view.frame = CGRectMake(view.frame.origin.x, -view.frame.size.height, self.view.frame.size.width, view.frame.size.height);

}

- (void) hideBottomView:(UIView*)view
{
    view.frame = CGRectMake(view.frame.origin.x, self.view.frame.size.height, self.view.frame.size.width, view.frame.size.height);

}


- (void) addBottomView:(UIView*)bottomView animated:(BOOL)animated
{
    if (!bottomView) {
        return;
    }
    [self.view addSubview:bottomView];
    [self hideBottomView:bottomView];
    [self.bottonViews addObject:bottomView];
    if (animated) {
        __weak MEFContainerViewController *weakSelf = self;
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            [weakSelf layoutSubviews];
        } completion:^(BOOL finished) {
            
        }];
    } else {
        [self layoutSubviews];
    }
}

- (void) removeBottomView:(UIView*)bottomView animated:(BOOL)animated
{
    if (!bottomView) {
        return;
    }
    [self.bottonViews removeObject:bottomView];
    [self.topRemoveViews addObject:bottomView];
    if (animated) {
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            [self layoutSubviews];
        } completion:^(BOOL finished) {
            [self.bottomRemoveViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            [self.bottomRemoveViews removeAllObjects];

        }];
    } else {
        [self layoutSubviews];
        [self.bottomRemoveViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.bottomRemoveViews removeAllObjects];

    }
}
- (void) addTopView:(UIView *)topView animated:(BOOL)animated
{
    if (!topView) {
        return;
    }
    NSAssert(self.topViews != nil, @"");
    [self hideTopView:topView];
    [self.view addSubview:topView];
    [self.view sendSubviewToBack:topView];
    [self.topViews addObject:topView];
    if (animated) {
        [UIView animateWithDuration:0.3 delay:0.0 options: UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut animations:^{
            [self layoutSubviews];
        } completion:^(BOOL finished) {
            
        }];
    } else {
        [self layoutSubviews];
    }
    
}

- (void) removeTopView:(UIView*)topView animated:(BOOL)animated
{
    if (!topView) {
        return;
    }
    [self.topViews removeObject:topView];
    [self.topRemoveViews addObject:topView];
    if (animated) {
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionLayoutSubviews animations:^{
            [self layoutSubviews];
        } completion:^(BOOL finished) {
            [self.topRemoveViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            [self.topRemoveViews removeAllObjects];
        }];
    } else {
        [self layoutSubviews];
        [self.topRemoveViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.topRemoveViews removeAllObjects];
    }
}

- (void) showModalView:(UIView *)view animated:(BOOL)animated hideCallback:(void (^)())hideCallback
{
    self.hideCallback = hideCallback;
    if (self.modalView) {
        [self.modalView removeFromSuperview];
        self.modalView = nil;
    }
    if (self.shadowView) {
        if (self.gestureSwipe) {
            [self.shadowView removeGestureRecognizer:self.gestureSwipe];
        }
        [self.shadowView removeFromSuperview];
        self.shadowView = nil;
    }
    if (self.gestureSwipe) {
        self.gestureSwipe = nil;
    }

    self.modalView = view;
    self.shadowView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - _modalView.frame.size.height)];
    [self.shadowView setAlpha:0];
    [self.shadowView setBackgroundColor:[UIColor blackColor]];
    self.gestureSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipe:)];
    [self.gestureSwipe setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.shadowView addGestureRecognizer:self.gestureSwipe];
    [self.view addSubview:self.modalView];
    [self.view addSubview:self.shadowView];
    if (animated) {
        CGRect modelViewFrame = view.frame;
        modelViewFrame.origin = CGPointMake(0, self.view.frame.size.height);
        view.frame = modelViewFrame;
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionCurveEaseInOut animations:^{
            CGRect modelViewFrame = view.frame;
            modelViewFrame.origin = CGPointMake(0, self.view.frame.size.height - modelViewFrame.size.height);
            view.frame = modelViewFrame;
            [self.shadowView setAlpha:0.3];
        } completion:^(BOOL finished) {
            
        }];
    } else {
       
    }
}
- (void) hideModalViewWithAnimated:(BOOL)animated
{
    if (animated) {
      
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionCurveEaseInOut animations:^{
            CGRect modelViewFrame = self.modalView.frame;
            modelViewFrame.origin = CGPointMake(0, self.view.frame.size.height);
            self.modalView.frame = modelViewFrame;
            CGRect shadowViewFrame = self.shadowView.frame;
            shadowViewFrame.size = CGSizeMake(shadowViewFrame.size.width, self.view.frame.size.height);
            self.shadowView.frame = shadowViewFrame;
            
            [self.shadowView setAlpha:0];
        } completion:^(BOOL finished) {
            if (self.modalView) {
                [self.modalView removeGestureRecognizer:self.gestureSwipe];
                [self.modalView removeFromSuperview];
                self.modalView = nil;
            }
            if (self.shadowView) {
                [self.shadowView removeGestureRecognizer:self.gestureSwipe];
                [self.shadowView removeFromSuperview];
                self.shadowView = nil;
            }
            if (self.gestureSwipe) {
                self.gestureSwipe = nil;
            }
            if (self.hideCallback) {
                self.hideCallback();
            }
            self.hideCallback = nil;
        }];
    } else {
        
    }
}
- (void) swipe:(id)s
{
    [self hideModalViewWithAnimated:true];
}

#pragma mark -- overide view controllers methods
- (void) viewDidLoad
{
    [super viewDidLoad];
    
    @try {
        self.currentSegueAnimateType = MEFContainerViewAnimateTypeNone;
        [self performSegueWithIdentifier:MEFSegueContainerEmbed sender:nil];
    }
    @catch (NSException *e) {
        
    }
}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self layoutSubviews];
    //support ios 7
    [self.view layoutSubviews];
}

- (id) setViewControllerWithSegueIdentifier:(NSString*)identifier animateType:(MEFContainerViewAnimateType)type
{
    self.currentSegueAnimateType = type;
    @try {
        [self performSegueWithIdentifier:identifier sender:nil];
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    return self.embedViewController;
}

- (void) updateContentWithAnimated:(BOOL)animated
{
    if (!animated) {
        [self.view setNeedsLayout];
        return;
    }
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionCurveEaseInOut animations:^{
        [self layoutSubviews];
        [self.view setNeedsLayout];
    } completion:^(BOOL finished) {
        
    }];
}

- (void) setViewController:(UIViewController *)controller animateType:(MEFContainerViewAnimateType)type
{
    switch (type) {
        case MEFContainerViewAnimateTypeNone:
        {
            [self setEmbedViewController:controller];
        } break;
        case MEFContainerViewAnimateTypeFade:
        {
            
            UIViewController *oldViewController = self.embedViewController;
            self.embedViewController = controller;
            [self addChildViewController:controller];
            [self layoutSubviews];
            [controller.view  setFrame:[self embedFrame]];
            [self.view addSubview:controller.view];
            [controller didMoveToParentViewController:self];
            [controller.view setAlpha:0];
            [UIView animateWithDuration:0.24 delay:0.0 options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionCurveEaseInOut animations:^{
                [oldViewController.view setAlpha:0];
                [controller.view setAlpha:1];
            } completion:^(BOOL finished) {
                if (oldViewController) {
                    [oldViewController willMoveToParentViewController:nil];
                    [oldViewController.view removeFromSuperview];
                    [oldViewController removeFromParentViewController];
                }
            }];

        }   break;
        case MEFContainerViewAnimateTypeSlide:
        {
            
            UIViewController *oldViewController = self.embedViewController;
            self.embedViewController = controller;
            [self addChildViewController:controller];
            [self layoutSubviews];
            CGRect frame = controller.view.frame;
            frame.origin.x = self.view.frame.size.width;
            controller.view.frame = frame;
            [self.view addSubview:controller.view];
            [controller didMoveToParentViewController:self];
            [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionCurveEaseInOut animations:^{
                [controller.view  setFrame:[self embedFrame]];
                if (oldViewController) {
                    [oldViewController.view  setFrame:CGRectMake(-self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
                }

            } completion:^(BOOL finished) {
                if (oldViewController) {
                    [oldViewController willMoveToParentViewController:nil];
                    [oldViewController.view removeFromSuperview];
                    [oldViewController removeFromParentViewController];
                }
            }];
        }   break;
        default:
            break;
    }
   
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue isKindOfClass:[MEFContainerSegue class]]) {
        [(MEFContainerSegue*)segue setAnimateType:self.currentSegueAnimateType];
    }
}


- (void) setEmbedViewController:(UIViewController*)viewController
{
    if (_embedViewController) {
        [self.embedViewController willMoveToParentViewController:nil];
        [self.embedViewController.view removeFromSuperview];
        [self.embedViewController removeFromParentViewController];
        _embedViewController = nil;
    }
    
    _embedViewController = viewController;
    if (viewController && self.embedViewController.view.superview != self.view) {
        [self addChildViewController:self.embedViewController];
        [self layoutSubviews];
        [self.embedViewController.view setNeedsDisplay];
        [self.view addSubview:self.embedViewController.view];
        [self.embedViewController didMoveToParentViewController:self];

    }
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];

}
- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}
- (CGRect) embedFrame
{
    CGFloat height = self.view.bounds.size.height;
    
    CGFloat yTopOffSet = 0;
    CGFloat yBottomOffSet = 0;
    
//    if (self.embedViewController.edgesForExtendedLayout == UIRectEdgeNone) {
//        yTopOffSet = [UIApplication sharedApplication].statusBarFrame.size.height;
//    }
    for (UIView *view in self.topViews) {
        view.frame = CGRectMake(0, yTopOffSet, self.view.bounds.size.width, view.bounds.size.height);
        yTopOffSet += view.frame.size.height;
    }
    
    for (UIView *view in self.bottonViews) {
        view.frame = CGRectMake(0, self.view.bounds.size.height - view.frame.size.height - yBottomOffSet, self.view.bounds.size.width, view.bounds.size.height);
        yBottomOffSet += view.frame.size.height;
    }
    
    
    height -= yTopOffSet;
    height -= yBottomOffSet;
    
    

    CGRect frame = CGRectZero;
    CGFloat x,y,w,h;
    x = self.embedViewEdgeInserts.left;
    y = self.embedViewEdgeInserts.top + yTopOffSet;
    w = self.view.bounds.size.width - self.embedViewEdgeInserts.right - self.embedViewEdgeInserts.left;
    h = self.view.bounds.size.height - yTopOffSet  - yBottomOffSet - self.embedViewEdgeInserts.top - self.embedViewEdgeInserts.bottom;;
    
    
    frame = CGRectMake(x, y, w, h);
    
    return frame;
}

- (void) layoutSubviews
{
    CGFloat height = self.view.bounds.size.height;
    CGFloat yTopOffSet = 0;
    CGFloat yBottomOffSet = 0;
//    if (self.embedViewController.edgesForExtendedLayout == UIRectEdgeNone) {
//        yTopOffSet = [UIApplication sharedApplication].statusBarFrame.size.height;
//    }

    for (UIView *view in self.topViews) {
        view.frame = CGRectMake(0, yTopOffSet, self.view.bounds.size.width, view.bounds.size.height);
        yTopOffSet += view.frame.size.height;
    }
    
    for (UIView *view in self.bottonViews) {
        view.frame = CGRectMake(0, self.view.bounds.size.height - view.frame.size.height - yBottomOffSet, self.view.bounds.size.width, view.bounds.size.height);
        yBottomOffSet += view.frame.size.height;
    }
    
    for (UIView *view in self.topRemoveViews) {
        [self hideTopView:view];
    }
    
    for (UIView *view in self.bottomRemoveViews) {
        [self hideBottomView:view];
    }
    
    height -= yTopOffSet;
    height -= yBottomOffSet;

    if (self.embedViewController) {
        CGRect frame = CGRectZero;
        CGFloat x,y,w,h;
        x = self.embedViewEdgeInserts.left;
        y = self.embedViewEdgeInserts.top + yTopOffSet;
        w = self.view.bounds.size.width - self.embedViewEdgeInserts.right - self.embedViewEdgeInserts.left;
        h = self.view.bounds.size.height - yTopOffSet  - yBottomOffSet - self.embedViewEdgeInserts.top - self.embedViewEdgeInserts.bottom;
        frame = CGRectMake(x, y, w, h);
        [self.embedViewController.view setFrame:frame];
    }
        
    
}

- (NSMutableArray*) topViews
{
    if (!_topViews) {
        _topViews = [NSMutableArray array];
    }
    return _topViews;
}

- (NSMutableArray*) topRemoveViews
{
    if (!_topRemoveViews) {
        _topRemoveViews = [NSMutableArray array];
    }
    return _topRemoveViews;
}

- (NSMutableArray*) bottonViews
{
    if (!_bottomViews) {
        _bottomViews = [NSMutableArray array];
    }
    return _bottomViews;
}
- (NSMutableArray*) bottomRemoveViews
{
    if (!_bottomRemoveViews) {
        _bottomRemoveViews = [NSMutableArray array];
    }
    return _bottomRemoveViews;
}



@end

