//
//  MEFMultiSelectTableViewController.h
//  ipad_project
//
//  Created by Прокофьев Руслан on 02.10.13.
//  Copyright (c) 2013 Прокофьев Руслан. All rights reserved.
//

#import "MEFBaseTableViewController.h"
#import "MEFSelectedItem.h"

//typedef NS_ENUM(NSUInteger, MEFMultiSelectTableImageType) {
//    MEFMultiSelectTableImageTypeNone,
//    MEFMultiSelectTableImageTypeSelect,
//    MEFMultiSelectTableImageTypeUnSelect,
//};

@class MEFMultiSelectTableViewController;
@protocol MEFMultiSelectTableViewControllerDelegate
@optional
- (BOOL) multiSelectContainsByObject:(MEFObject*)object controller:(MEFMultiSelectTableViewController*)controller;
//- (void) multiSelectContainsByObject:(MEFObject*)object controller:(MEFMultiSelectTableViewController*)controller;
- (void) multiSelectDidSelected:(MEFSelectedItem*)selectedItem controller:(MEFMultiSelectTableViewController*)controller;
- (void) multiSelectDidUnSelected:(MEFSelectedItem*)selectedItem controller:(MEFMultiSelectTableViewController*)controller;
@end

@protocol MEFMultiSelectTableViewControllerDataSource
- (NSString *)multiSelectUniqueIdentifierWithObject:(MEFObject*)object;
@end

@interface MEFMultiSelectTableViewController : MEFBaseTableViewController

@property (nonatomic, readwrite, assign, getter=isEnabledOnlyOne) BOOL enabledOnlyOne;
@property (nonatomic, readwrite, assign, getter=isEnabledUnselect) BOOL enabledUnselect;
@property (nonatomic, readwrite, assign, getter=isDisabledSelectionOnTop) BOOL disabledSelectionOnTop;
@property (nonatomic, readwrite, assign, getter=isHasCustomAccessory) BOOL hasCustomAccessory;
@property (nonatomic, readwrite, assign, getter=isEnabledMultiSelect) BOOL enabledMultiSelect;
@property (nonatomic, readonly, assign) int currentCountSelected;
@property (nonatomic, readonly, strong) NSMutableDictionary *selectedItemsDictionary;
@property (nonatomic, readwrite, strong) UIImage *selectImage;
@property (nonatomic, readwrite, strong) UIImage *emptySelectImage;
@property (nonatomic, readwrite, strong) UIColor *selectCircleColor;
@property (weak, nonatomic) NSObject <MEFMultiSelectTableViewControllerDelegate> *multiSelectDelegate;
@property (weak, nonatomic) NSObject <MEFMultiSelectTableViewControllerDataSource> *multiSelectDataSourse;

- (int) countItems;
- (NSMutableArray *) selectedItems;
- (MEFSelectedItem*) selectedItemByIndexPath:(NSIndexPath*)indexPath;

@end
