//
//  MEFBaseViewController.m
//  ipad_project
//
//  Created by Прокофьев Руслан on 24.09.13.
//  Copyright (c) 2013 Прокофьев Руслан. All rights reserved.
//


#import "MEFObject.h"
#import "MEFBaseTableViewController.h"

//typedef NSComparisonResult (^MEFObjectSort) (MEFObject *object1 ,MEFObject *object2);

//typedef NS_OPTIONS(NSUInteger, MEFBaseCollectionScrollFlag) {
//    MEFBaseCollectionScrollFlagNone = 1 << 0,
//    MEFBaseCollectionScrollFlagBeginUserScroll = 1 << 1,
//    MEFBaseCollectionScrollFlagVisibleBottomRefreshControlBegin = 1 << 2,
//    MEFBaseCollectionScrollFlagVisibleBottomRefreshControlEnd = 1 << 3,
//    MEFBaseCollectionScrollFlagIsCompletedBottomRefreshControl = 1 << 4,
//    MEFBaseCollectionScrollFlagEndBottomRefreshControl = 1 << 5,
//};

static CGFloat MEFBTableBTCHeight = 44;
typedef NS_OPTIONS(NSUInteger, MEFBTableOption) {
    MEFBTableOptionNine = 1 << 0,
    MEFBTableOptionBeginUserScroll = 1 << 1,
    MEFBTableOptionBeginVisibleBRCBegin = 1 << 2,
    MEFBTableOptionBeginVisibleBRCEnd = 1 << 3,
    MEFBTableOptionBeginVisibleBRCCompleted = 1 << 3,
};

@interface MEFBaseTableViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, MEFRefreshControlManagerDelegate>
{

}


@property (strong, nonatomic, readwrite) NSIndexPath *currentReorderingRow;
@property (strong, nonatomic, readwrite) NSMutableArray *unsortedSections;
@property (strong, nonatomic) NSMutableArray *sections;
@property (strong, nonatomic) NSMutableDictionary *hashmap;
@property (strong, nonatomic) NSMutableDictionary *dequeueHashmap;
@property (strong, nonatomic) NSMutableDictionary *editModeHashmap;
@property (strong, nonatomic) NSMutableArray *collaspeSections;
@property (strong, nonatomic) NSMutableArray *editedObjects;

@property (strong, nonatomic) UIView *placeholderView;
@property (strong, nonatomic) UIView *floatingHeaderView;
@property (assign, nonatomic, readwrite) BOOL invalidFloatingHeader;

@property (assign, nonatomic, readwrite) BOOL initHideSearchBar;
@property (assign, nonatomic, readwrite, getter=isAlreadyReload) BOOL alreadyReload;
@property (assign, nonatomic, readwrite, getter=isTimeoutRefreshControl) BOOL timeoutRefreshControl;
@property (assign, nonatomic, readwrite, getter=isSaveEnabledRefreshControl) BOOL saveEnabledRefreshControl;
@property (strong, nonatomic, readwrite) UIRefreshControl *saveRefreshControl;
@property (assign, nonatomic, readwrite) MEFBTableOption options;

@property (strong, nonatomic, readwrite) MEFRefreshControlManager *refreshControlManager;
@end

@implementation MEFBaseTableViewController


#pragma mark - - Search bar
- (void) setEnabledSearchBar:(BOOL)enabledSearchBar
{
    _enabledSearchBar = enabledSearchBar;
    if (_enabledSearchBar) {
        if (self.searchBar) {
            self.tableView.tableHeaderView = self.searchBar;
        } else {
            self.tableView.tableHeaderView = [self deffaultSearchBar];
        }
    } else {
        if ([self.tableView.tableHeaderView isKindOfClass:[UISearchBar class]]) {
            self.tableView.tableHeaderView  = nil;
        }
    }
}
- (void) setSearchBar:(UISearchBar *)searchBar
{
    _searchBar = searchBar;
    [self setEnabledSearchBar:true];
}

- (void) showSearchBarWithAnimated:(BOOL)animated
{
    if (!self.isEnabledSearchBar) {
        return;
    }
    if (animated) {
        __weak MEFBaseTableViewController *weakSelf = self;
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionBeginFromCurrentState animations:^{
            [weakSelf.tableView setContentOffset:CGPointMake(0, 0)];
        } completion:^(BOOL finished) {
            
        }];
    } else {
        [self.tableView setContentOffset:CGPointMake(0, 0)];
    }
}

- (void) hideSearchBarWithAnimated:(BOOL)animated
{
    if (!self.isEnabledSearchBar) {
        return;
    }
    if (animated) {
        __weak MEFBaseTableViewController *weakSelf = self;
        [UIView animateWithDuration:0.45 delay:0.0 options:UIViewAnimationOptionLayoutSubviews animations:^{
            [weakSelf.tableView setContentOffset:CGPointMake(0, weakSelf.searchBar.frame.size.height)];
        } completion:^(BOOL finished) {
            
        }];
    } else {
        [self.tableView setContentOffset:CGPointMake(0, self.searchBar.frame.size.height)];
    }
}

- (UISearchBar*) deffaultSearchBar
{
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        _searchBar.searchBarStyle = UISearchBarStyleMinimal;
        [_searchBar setDelegate:self];
    }
    return _searchBar;
}

#pragma mark - - Override
- (void)viewDidLoad
{
	[super viewDidLoad];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    self.initHideSearchBar = false;
    self.alreadyReload = false;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    if (self.isEnabledBottomRefreshControl) {
        [self setEnabledBottomRefreshControl:true];
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if (self.searchBar) {
        [self.searchBar setFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    }
    if (self.refreshControl) {
        CGRect frame = self.refreshControl.frame;
        frame.size.width = self.view.frame.size.width;
        frame.size.height = 44;
        [self.refreshControl setFrame:frame];
    }
    if (self.bottomRefreshControl) {
        CGRect frame = self.bottomRefreshControl.frame;
        frame.size.width = self.view.frame.size.width;
        frame.size.height = MEFBTableBTCHeight;
        [self.bottomRefreshControl setFrame:frame];
        
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (UIEdgeInsets) appendEdgeInsets:(UIEdgeInsets)edgeInsets
{
    UIEdgeInsets edge = UIEdgeInsetsMake(self.contentInset.top + edgeInsets.top, self.contentInset.left + edgeInsets.left, self.contentInset.bottom + edgeInsets.bottom, self.contentInset.right + edgeInsets.right);
    return edge;
}

#pragma mark - - Scroll Refresh

- (void) releaseBottomRefreshControl
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:bottomRefreshContent:)]) {
        [self.delegate baseTableController:self bottomRefreshContent:self.bottomRefreshControl];
    }
}

- (void) willShowBottomRefreshControl
{
    self.saveEnabledRefreshControl = self.isEnabledRefreshControl;
    self.saveRefreshControl = self.refreshControl;
    [self setEnabledRefreshControl:false];
}

- (void) didHideBottomRefreshControl:(BOOL)release
{
    if (release) {
        return;
    }
    [self setRefreshControl:self.saveRefreshControl];
    [self setEnabledRefreshControl:self.saveEnabledRefreshControl];
    self.saveRefreshControl = nil;
    self.saveEnabledRefreshControl = false;
}
#pragma mark - - Refresh control



- (void) refreshControl:(UIRefreshControl*)refreshControl
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:refreshContent:)]) {
        [self.delegate baseTableController:self refreshContent:self.refreshControl];
    }
}

- (void) setRefreshControl:(UIRefreshControl *)refreshControl
{
    [super setRefreshControl:refreshControl];
    if (refreshControl) {
        [self.refreshControl addTarget:(self) action:@selector(refreshControl:) forControlEvents:UIControlEventValueChanged];
//        [self.tableView setContentOffset:CGPointMake(0, -44)];
    }
}

- (UIRefreshControl*) createRefreshControl
{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [refreshControl setTintColor:[UIColor whiteColor]];
    return refreshControl;
}

- (void) setEnabledBottomRefreshControl:(BOOL)enabledBottomRefreshControl
{
    _enabledBottomRefreshControl = enabledBottomRefreshControl;
    [self.refreshControlManager setEnabledBottomRefreshControl:enabledBottomRefreshControl];
}




- (void) setBottomRefreshControl:(UIView<MEFRefreshControlPropertyProtocol> *)bottomRefreshControl
{
    _bottomRefreshControl = bottomRefreshControl;
    [self.refreshControlManager setBottomRefreshControl:bottomRefreshControl];
}



- (void) setEnabledRefreshControl:(BOOL)enabledRefreshControl
{
    if (_enabledRefreshControl == enabledRefreshControl) {
        return;
    }
    _enabledRefreshControl = enabledRefreshControl;
    
    if (!self.refreshControl && _enabledRefreshControl) {
        self.refreshControl = [self createRefreshControl];
    } else if (!_enabledRefreshControl) {
        self.refreshControl = nil;
    }
}

- (void) endBottomRefreshingControl
{
    [self.refreshControlManager endBottomRefreshControl:self.tableView];
    [self setRefreshControl:self.saveRefreshControl];
    [self setEnabledRefreshControl:self.saveEnabledRefreshControl];
    self.saveRefreshControl = nil;
    self.saveEnabledRefreshControl = false;
}

- (void) beginRefreshingControl
{
    if (self.timeoutRefreshControl) {
        return;
    }
    self.timeoutRefreshControl = true;
    [self.refreshControlManager setEnabledBottomRefreshControl:false];
    [self performSelector:@selector(_beginRefreshingControl) withObject:nil afterDelay:0.5];
//    [self _beginRefreshingControl];
}

- (void) _beginRefreshingControl
{
    if (!self.timeoutRefreshControl) {
        return;
    }

    [self.tableView setContentOffset:CGPointMake(0, - (self.refreshControl.frame.size.height)) animated:YES];
    [self.refreshControl beginRefreshing];
}


- (void) endRefreshingControl
{
    self.timeoutRefreshControl = false;
    [self.refreshControlManager setEnabledBottomRefreshControl:self.enabledBottomRefreshControl];
//    [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    [self.refreshControl endRefreshing];
}




#pragma mark - - public methods

- (BOOL) isEmpty
{
    return self.unsortedSections.count == 0;
}

- (void) cancelEdited
{
    [self.editModeHashmap removeAllObjects];
    [self.editedObjects removeAllObjects];
    //!!!
    [self performSelector:@selector(reloadData) withObject:NULL afterDelay:0.3];
}

- (NSUInteger ) countObjects
{
    NSUInteger count = 0;
    for (MEFObject *object in self.sections) {
        count += object.items.count;
    }
    return count;
}


- (MEFObject *)dequeueReusableSectionWithKey:(NSString *)key
{
    if (!self.dequeueHashmap) {
        return nil;
    }
    return [self.dequeueHashmap objectForKey:key];
}


- (NSIndexPath *)previousIndexPath:(NSIndexPath *)oldIndexPath
{
    NSIndexPath *previousIndexPath = nil;
    NSInteger nextRow = oldIndexPath.row - 1;
    NSInteger currentSection = oldIndexPath.section;
    if (nextRow >= 0) {
        previousIndexPath = [NSIndexPath indexPathForRow:nextRow inSection:currentSection];
    } else {
        NSInteger nextSection = currentSection - 1;
        MEFObject *section = [self sectionAtIndex:nextSection];
        NSInteger rowObject = fmax(section.items.count - 1,0);
        if (nextSection >= 0) {
            previousIndexPath = [NSIndexPath indexPathForRow:rowObject inSection:nextSection];
        }
    }
    return previousIndexPath;
}

- (NSIndexPath *)nextIndexPath:(NSIndexPath *)oldIndexPath
{
    NSIndexPath *nextIndexPath = nil;
    MEFObject *section = [self sectionAtIndex:oldIndexPath.section];
    NSInteger rowCount = [section.items count];
    NSInteger nextRow = oldIndexPath.row + 1;
    NSInteger currentSection = oldIndexPath.section;
    if (nextRow < rowCount) {
        nextIndexPath = [NSIndexPath indexPathForRow:nextRow inSection:currentSection];
    } else {
        NSInteger sectionCount = [[self sections] count];
        NSInteger nextSection = currentSection + 1;
        if (nextSection < sectionCount)
        {
            nextIndexPath = [NSIndexPath indexPathForRow:0 inSection:nextSection];
        }
    }
    return nextIndexPath;
}


#pragma mark - - methods for get object/section by params

- (UITableViewCell*) cellAtObject:(MEFObject*)object
{
    MEFObject *section = [object parent];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[section.items indexOfObject:object] inSection:[self.sections indexOfObject:section]];
    return [self.tableView cellForRowAtIndexPath:indexPath];
}

- (NSIndexPath*) indexPathAtSection:(MEFObject*)section
{

    return [NSIndexPath indexPathForRow:0 inSection:[self.sections indexOfObject:section]];

}
- (NSIndexPath*) indexPathAtObject:(MEFObject*)object
{
    MEFObject *section = [object parent];
    return [NSIndexPath indexPathForRow:[section.items indexOfObject:object] inSection:[self.sections indexOfObject:section]];
}



- (MEFObject *)sectionAtKey:(NSString *)key
{
    if (self.dequeueHashmap.count) {
        return self.dequeueHashmap[key];
    }
    return [self.hashmap objectForKey:key];
}

- (MEFObject *)objectFromSectionsAtKey:(NSString *)key
{
	for (MEFObject *section in self.sections) {
        id obj = [section itemAtKey:key];
        if (obj) {
            return obj;
        }
    }
    return nil;
}

- (MEFObject *)sectionAtIndex:(NSInteger)index
{
	if (index >= self.sections.count) {
		return Nil;
	}
	return [self.sections objectAtIndex:index];
}

- (MEFObject *)objectAtIndex:(int)index row:(int)row
{
	MEFObject *section = [self sectionAtIndex:index];
	if (!section) {
		return Nil;
	}
	return [section itemAtIndex:row];
}

- (MEFObject *)objectAtIndexPath:(NSIndexPath *)indexPath
{
    if (!indexPath) {
        return nil;
    }
	MEFObject *section = [self sectionAtIndex:indexPath.section];
	if (!section) {
		return Nil;
	}
	return [section itemAtIndex:indexPath.row];
}



#pragma mark - -  add methods

- (void)addSection:(MEFObject *)section
{
	[section onEnter];
    [self.hashmap setObject:section forKey:section.key];
	[self.unsortedSections addObject:section];
}
- (void) addSections:(NSArray*)sections
{
    for (MEFObject *section in sections) {
        [self addSection:section];
    }
}






#pragma mark - - building methods
- (void)cleanup
{
    [self.editModeHashmap removeAllObjects];
    [self.editedObjects removeAllObjects];
    [self.sections removeAllObjects];
    [self.unsortedSections removeAllObjects];
    [self.hashmap removeAllObjects];
    [self.collaspeSections removeAllObjects];
    self.dequeueHashmap = nil;
}

- (void) reloadData
{
    NSAssert(self.datasource, @"datasource = nil");
    if (self.isEnabledSearchBar) {
        self.tableView.tableHeaderView = self.searchBar;
    } else {
        if ([self.tableView.tableHeaderView isKindOfClass:[UISearchBar class]]) {
            self.tableView.tableHeaderView  = nil;
        }
    }
    [self willReloadData];
    [self loadData];
    [self.tableView reloadData];
    [self didReloadData];
    if (self.isEnabledSearchBar && !_initHideSearchBar) {
        _initHideSearchBar = true;
        [self hideSearchBarWithAnimated:true];
    }
}

- (void) updates:(void (^)(MEFBaseTableViewController *controller, UITableView *tableView))updates
{
    [self.tableView beginUpdates];
    if (updates) {
        updates(self,self.tableView);
    }
    [self.tableView endUpdates];
}
- (void)loadData
{
	[self.sections removeAllObjects];
	NSMutableArray *sortedElements = [NSMutableArray arrayWithArray:self.unsortedSections];
	if (self.sectionGrouping) {
		self.dequeueHashmap = [NSMutableDictionary dictionary];

		for (MEFObject *section in sortedElements) {
			[section reloadObjects];
			for (MEFObject *object in section.items) {
				MEFObject *newSection = self.sectionGrouping(object, section);
                if (!newSection) {
                    continue;
                }
				[newSection addItem:object];
				if (![self.dequeueHashmap objectForKey:newSection.key]) {
					[self.dequeueHashmap setObject:newSection forKey:newSection.key];
				}
			}
		}
		[sortedElements removeAllObjects];
		[sortedElements addObjectsFromArray:self.dequeueHashmap.allValues];
		
	}

    if (self.isEnabledSearchBar && self.sectionSearch && self.searchBar.text && self.searchBar.text.length) {
        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(MEFObject *evaluatedObject, NSDictionary *bindings) {
            if (self.sectionSearch){
                BOOL result = self.sectionSearch(evaluatedObject, self.searchBar.text);
                
                return result;
            }
            return false;
        }];
        NSArray *findedElements = [sortedElements filteredArrayUsingPredicate:predicate];
        [sortedElements removeAllObjects];
        [sortedElements addObjectsFromArray:findedElements];
    }

    __weak MEFBaseTableViewController *weakSelf = self;
	if (self.sectionSort) {
		[sortedElements sortUsingComparator: ^NSComparisonResult (id obj1, id obj2) {
		    return weakSelf.sectionSort(obj1, obj2);
		}];
	}
    


    
	for (MEFObject *section in [NSArray arrayWithArray:sortedElements]) {
		[section sortObjects];
        //TODO: 
		if (!section.items.count) {
			[sortedElements removeObject:section];
            continue;
		}
        if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:sectionIsCollapseDuringReloadWithSection:)]) {
            if (![self.delegate baseTableController:self sectionIsCollapseDuringReloadWithSection:section]) {
                [self.collaspeSections addObject:section];
                [section restoreBeginState];
            }
        }
	}
    
    
    if (self.floatingHeaderView) {
        [self.floatingHeaderView removeFromSuperview];
        self.floatingHeaderView = nil;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(viewForFloatingHeaderWithBaseTableViewController:)] && [self.delegate respondsToSelector:@selector(heightForFloatingHeaderWithBaseTableViewController:)]) {
        self.floatingHeaderView = [self.delegate viewForFloatingHeaderWithBaseTableViewController:self];
        CGFloat height = [self.delegate heightForFloatingHeaderWithBaseTableViewController:self];
        [self.floatingHeaderView setFrame:CGRectMake(0, -height, self.view.frame.size.width, height)];
        [self.view addSubview:self.floatingHeaderView];
        [self.tableView setContentInset:UIEdgeInsetsMake(self.floatingHeaderView.frame.size.height, 0, 0, 0)];
        self.invalidFloatingHeader = true;
    }
    
    
    if (self.isAlreadyReload) {
        if (_placeholderView) {
            [_placeholderView removeFromSuperview];
            _placeholderView = nil;
        }
        if ([sortedElements count] <= 0) {
            if (self.datasource && [self.datasource respondsToSelector:@selector(createPlaceholderWithBaseTableViewController:)]) {
                _placeholderView = [self.datasource createPlaceholderWithBaseTableViewController:self];
                if (_placeholderView) {
                    [self.view addSubview:_placeholderView];
                }
            }
        }
    }
    self.alreadyReload = true;
    
    [self modifySections:sortedElements];
	[self.sections addObjectsFromArray:sortedElements];
   
}



#pragma mark - - Sort/group section
- (void)setGrouping:(MEFObjectGrouping)grouping
{
	self.sectionGrouping = grouping;
}





- (void)setSortSections:(MEFObjectSort)sort
{
	self.sectionSort = sort;
}

#pragma mark - SearchBar delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self searchSectionWithText: searchBar.text];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    [self.searchBar setShowsCancelButton: NO animated: YES];
    [self cancelSearchSection];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    [self.searchBar setShowsCancelButton: NO animated: YES];
}




#pragma mark - - search
- (void) searchSectionWithText:(NSString *)text
{
	[self.sections removeAllObjects];
    
    
    if (!self.sectionSearch) {
        NSMutableArray *findedElements = [NSMutableArray array];
        for (MEFObject *section in self.unsortedSections) {
            [section searchObjectsWithString:text];
            if (section.items.count) {
                [findedElements addObject:section];
            }
        }
        [self.sections addObjectsFromArray:findedElements];
    }

	[self reloadData];
}

- (void)cancelSearchSection
{
	[self.sections removeAllObjects];
	for (MEFObject *section in self.unsortedSections) {
		[section cancelSearch];
	}
	[self reloadData];
}







#pragma mark - - insert/delete row

- (BOOL) isCollapseSectionWithIndexPath:(NSIndexPath*)indexPath
{
    MEFObject *collapseSection = [self sectionAtIndex:indexPath.section];
    return [self.collaspeSections containsObject:collapseSection];
}

- (void) uncollapsedSectionWithIndexPath:(NSIndexPath*)indexPath
{
    MEFObject *collapseSection = [self sectionAtIndex:indexPath.section];
    [self.collaspeSections removeObject:collapseSection];
    [collapseSection reloadObjects];
     NSMutableArray *addIndexPaths = [NSMutableArray array];
    NSUInteger count = collapseSection.items.count;
    for (NSUInteger i = 0; i < count; ++i) {
        NSIndexPath *subIndexPath = [NSIndexPath indexPathForRow:i inSection:indexPath.section];
        [addIndexPaths addObject:subIndexPath];
    }
    [self.tableView insertRowsAtIndexPaths:addIndexPaths withRowAnimation:UITableViewRowAnimationFade];
}

- (void) collapseSectionWithIndexPath:(NSIndexPath*)indexPath;
{
    MEFObject *collapseSection = [self sectionAtIndex:indexPath.section];
    [self.collaspeSections addObject:collapseSection];
    NSMutableArray *removeIndexPaths = [NSMutableArray array];
    NSUInteger count = collapseSection.items.count;
    for (NSUInteger i = 0; i < count; ++i) {
        NSIndexPath *subIndexPath = [NSIndexPath indexPathForRow:i inSection:indexPath.section];
        [removeIndexPaths addObject:subIndexPath];
    }
    [collapseSection restoreBeginState];

    [self.tableView deleteRowsAtIndexPaths:removeIndexPaths withRowAnimation:UITableViewRowAnimationFade];
  
}

- (void) reloadSectionsWithAnimation:(UITableViewRowAnimation)rowAnimation
{
    [self loadData];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.sections.count)] withRowAnimation:rowAnimation];

}




//- (void) refreshConfingWithIndexPath:(NSIndexPath*)indexPath
//{
//    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//    MEFObject *object =  [self objectAtIndexPath:indexPath];
//    if ([self.datasource respondsToSelector:@selector(baseTableController:configTableViewCell:object:indexPath:)]) {
//        [self.datasource baseTableController:self configTableViewCell:cell object:object indexPath:indexPath];
//    }
//}

- (void) insertRowsWithIndexPath:(NSIndexPath*)indexPath objects:(NSArray *)objects withRowAnimation:(UITableViewRowAnimation)animation;
{
    MEFObject *section  = [self sectionAtIndex:indexPath.section];
    NSMutableArray *array = [NSMutableArray array];
    [section insertItems:objects index:indexPath.row];
    for (int i = 0; i < objects.count; i++) {
        NSIndexPath *ip = [NSIndexPath indexPathForRow:indexPath.row + i inSection:indexPath.section];
        [array addObject:ip];
    }
    [section reloadObjects];
    [self.tableView insertRowsAtIndexPaths:array withRowAnimation:animation];
}

- (void) insertRowWithIndexPath:(NSIndexPath*)indexPath byObject:(MEFObject *)object withRowAnimation:(UITableViewRowAnimation)animation;
{
//    MEFObject *section  = [self sectionAtIndex:indexPath.section];
//    if (section.items.count > indexPath.row) {
//        [section.items insertObject:object atIndex:indexPath.row];
//    } else {
//        [section.items addObject:object];
//    }
//    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:animation];
}

- (void) insertRowWithIndexPath:(NSIndexPath*)indexPath byObject:(MEFObject *)object
{
    [self insertRowWithIndexPath:indexPath byObject:object withRowAnimation:UITableViewRowAnimationAutomatic];
}


- (void) deleteRowWithIndexPaths:(NSArray<NSIndexPath *> *)indexPaths withRowAnimation:(UITableViewRowAnimation)rowAction;
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    for (NSIndexPath *indexPath in indexPaths) {
        MEFObject *section  = [self sectionAtIndex:indexPath.section];
        MEFObject *object = [self objectAtIndexPath:indexPath];
        
        NSMutableArray *sectionArray = [dictionary objectForKey:@(indexPath.section)];
        
        if (!sectionArray) {
            sectionArray = [NSMutableArray array];
            [dictionary setObject:sectionArray forKey:@(indexPath.section)];
        }
        [sectionArray addObject:object];
        
    }
    
    for (NSNumber* sectionNumber in [dictionary allKeys]) {
        NSMutableArray *sectionArray = [dictionary objectForKey:sectionNumber];
        MEFObject *section  = [self sectionAtIndex:[sectionNumber integerValue]];
        NSMutableArray *indexPaths = [NSMutableArray array];
        for (id object in sectionArray) {
            NSIndexPath *indexPath = [self indexPathAtObject:object];
            [indexPaths addObject:indexPath];
        }
        for (id object in sectionArray) {
            [section removeItem:object];
            if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:deleteSingleRowWithObject:)]) {
                [self.delegate baseTableController:self deleteSingleRowWithObject:object];
            } else {
                MEFObject *section_editing = [self.editModeHashmap objectForKey:[section indentifierObject]];
                if (!section_editing) {
                    section_editing = [section copyWithoutItems];
                    [self.editModeHashmap setObject:section_editing forKey:[section indentifierObject]];
                }
                
                [section_editing addItem:object];
                [self.editedObjects addObject:object];
            }
            
        }
        
        if (!section.items.count) {
            [self.sections removeObject:section];
            //Need test
            [self.hashmap removeObjectForKey:section.key];
            [self.unsortedSections removeObject:section];
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:[sectionNumber integerValue]] withRowAnimation:rowAction];
        } else {
            
            [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:rowAction];
        }
    }
}


- (void) deleteRowsInSection:(NSUInteger)section withRowAnimation:(UITableViewRowAnimation)rowAnimation
{
    [self.tableView beginUpdates];
    
    MEFObject *sectionObject  = [self sectionAtIndex:section];
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < sectionObject.items.count; i++) {
        [array addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    [sectionObject removeItems];
    [self.tableView deleteRowsAtIndexPaths:array withRowAnimation:rowAnimation];
    [self.tableView endUpdates];
}

- (void) deleteSectionsWithAnimation:(UITableViewRowAnimation)rowAnimation
{
    [self.tableView beginUpdates];
    for (int i = 0; i < self.sections.count; i++)  {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:i];
        MEFObject *section  = [self sectionAtIndex:indexPath.section];
        [section removeItems];
        if (!section.items.count) {
            [self.sections removeObject:section];
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:rowAnimation];
        } else {
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:rowAnimation];
        }

    }
    [self cleanup];
    [self.tableView endUpdates];
}

- (void) deleteRowWithIndexPath:(NSIndexPath*)indexPath
{
    [self deleteRowWithIndexPath:indexPath withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void) deleteRowWithIndexPath:(NSIndexPath*)indexPath withRowAnimation:(UITableViewRowAnimation)rowAction;
{
    MEFObject *section  = [self sectionAtIndex:indexPath.section];
    MEFObject *object = [self objectAtIndexPath:indexPath];
    [section removeItem:object];
    if (!section.items.count) {
        [self.sections removeObject:section];
        //Need test
        [self.hashmap removeObjectForKey:section.key];
        [self.unsortedSections removeObject:section];
        ///
        [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:rowAction];
    } else {
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:rowAction];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:deleteSingleRowWithObject:)]) {
        [self.delegate baseTableController:self deleteSingleRowWithObject:object];
    } else {
        MEFObject *section_editing = [self.editModeHashmap objectForKey:[section indentifierObject]];
        if (!section_editing) {
            section_editing = [section copyWithoutItems];
            [self.editModeHashmap setObject:section_editing forKey:[section indentifierObject]];
        }
        
        [section_editing addItem:object];
        [self.editedObjects addObject:object];
    }
    
}


#pragma mark - - Table View datasource
//Dont support
//- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
//
//}
//
//- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
//
//}
//
//- (void)tableView:(UITableView *)tableView didEndDisplayingHeaderView:(UIView *)view forSection:(NSInteger)section {
//
//}
//
//- (void)tableView:(UITableView *)tableView didEndDisplayingFooterView:(UIView *)view forSection:(NSInteger)section {
//
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    return 0;
//}
//
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:estimatedHeightForRowAtIndexPath:)]) {
        return [self.delegate baseTableController:self estimatedHeightForRowAtIndexPath:indexPath];
    }
    return [self tableView:tableView heightForRowAtIndexPath:indexPath];
}
//
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
//    return 0;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section {
//    return 0;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    return nil;
//}
//
//- (UITableViewCellAccessoryType)tableView:(UITableView *)tableView accessoryTypeForRowWithIndexPath:(NSIndexPath *)indexPath {
//    return UITableViewCellAccessoryDisclosureIndicator;
//}
//
//- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
//    return NO;
//}
//
//- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
//    return nil;
//}
//
//- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return nil;
//}
//
//- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return nil;
//}
//
//- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
//
//}
//
//- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath {
//
//}
//
//- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return NO;
//}
//
//- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
//    return NO;
//}
//
//- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
//
//}
//
//- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
//    return 0;
//}
//
//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
//    return nil;
//}
//
//- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
//    return nil;
//}
- (NSString*) identifierCellWithIndexPath:(NSIndexPath*)indexPath
{
    MEFObject *object = [self objectAtIndexPath:indexPath];
    return [self identifierCellWithIndexPath:indexPath object:object];
    
}
- (NSString*) identifierCellWithIndexPath:(NSIndexPath*)indexPath object:(MEFObject*)object
{
    NSString *reuseIdentifier = nil;
    if (self.datasource && [self.datasource respondsToSelector:@selector(baseTableController:reuseIdentifierWithObject:indexPath:)]) {
        reuseIdentifier = [self.datasource baseTableController:self reuseIdentifierWithObject:object indexPath:indexPath];
    }
    if (!reuseIdentifier || reuseIdentifier.length == 0) {
        reuseIdentifier = [NSString stringWithFormat:@"%d", object.type];
    }
    return reuseIdentifier;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
	MEFObject *object = [self objectAtIndexPath:indexPath];
    NSString *reuseIdentifier = [self identifierCellWithIndexPath:indexPath];
	UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
	if (!cell) {
        if (self.delegate && [self.datasource respondsToSelector:@selector(baseTableController:createTableViewCellWithObject:reuseIdentifier:indexPath:)]) {
            cell = [self.datasource baseTableController:self createTableViewCellWithObject:object reuseIdentifier:reuseIdentifier indexPath:indexPath];
        }
	}
    if ([self.datasource respondsToSelector:@selector(baseTableController:configTableViewCell:object:indexPath:)]) {
        [self.datasource baseTableController:self configTableViewCell:cell object:object indexPath:indexPath];
    }

	return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self sectionAtIndex:section].items.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

#pragma mark - - Table View delegate

- (void) tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(baseTableController:didHighlightRowAtIndexPath:)]) {
        [self.delegate baseTableController:self didHighlightRowAtIndexPath:indexPath];
    }
}
- (void) tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(baseTableController:didUnhighlightRowAtIndexPath:)]) {
        [self.delegate baseTableController:self didUnhighlightRowAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(baseTableController:accessoryButtonTappedForRowWithIndexPath:)]) {
        [self.delegate baseTableController:self accessoryButtonTappedForRowWithIndexPath:indexPath];
    }
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        if (self.delegate && [self.delegate respondsToSelector:@selector(didReloadWithBaseTableController:)]) {
            [self.delegate didReloadWithBaseTableController:self];
        }
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:willDisplayCell:forRowAtIndexPath:)]) {
        [self.delegate baseTableController:self willDisplayCell:cell forRowAtIndexPath:indexPath];
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(baseTableController:willSelectRowAtIndexPath:)]) {
        return [self.delegate baseTableController:self willSelectRowAtIndexPath:indexPath];
    }
    return indexPath;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(baseTableController:heightForRowAtIndexPath:)]) {
        return [self.delegate baseTableController:self heightForRowAtIndexPath:indexPath];
    }
    return self.tableView.rowHeight;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(baseTableController:didSelectRowAtIndexPath:)]) {
        [self.delegate baseTableController:self didSelectRowAtIndexPath:indexPath];
    }
}




- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([self.delegate respondsToSelector:@selector(baseTableController:viewForHeaderInSection:)]) {
        return [self.delegate baseTableController:self viewForHeaderInSection:section];
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([self.delegate respondsToSelector:@selector(baseTableController:heightForHeaderInSection:)]) {
        return [self.delegate baseTableController:self heightForHeaderInSection:section];
    }
    return 0;
}

- (BOOL) tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.delegate) {
        return self.isEnabledDragMode;
    }
    if (self.isEnabledDragMode && [self.delegate respondsToSelector:@selector(baseTableController:canMoveRowAtIndexPath:)]) {
        return [self.delegate baseTableController:self canMoveRowAtIndexPath:indexPath];
    }
    return false;
}


- (void) swapFromIndexPath:(NSIndexPath*)fromIndexPath toIndexPath:(NSIndexPath*)toIndexPath
{
    @try {

        
        MEFObject *objectFrom = [self objectAtIndexPath:fromIndexPath];
        
        MEFObject *sectionFrom = [self sectionAtIndex:fromIndexPath.section];
        
        MEFObject *objectTo  = [self objectAtIndexPath:toIndexPath];
        
        MEFObject *sectionTo = [self sectionAtIndex:toIndexPath.section];
        
        [sectionFrom.items removeObject:objectFrom];
        
        objectFrom.parent = objectTo.parent;
        if (toIndexPath.row < sectionTo.items.count) {
            [sectionTo.items insertObject:objectFrom atIndex:toIndexPath.row];
        } else {
            [sectionTo.items addObject:objectFrom];
        }
        
        BOOL result = false;
        if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:swapObjectAtIndexPath:toIndexPath:)]) {
            result = [self.delegate baseTableController:self swapObjectAtIndexPath:fromIndexPath toIndexPath:toIndexPath];
        }
        if (!result) {
            return;
        }
    } @catch (NSException *exception) {

    }

}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath
       toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:targetIndexPathForMoveFromRowAtIndexPath:toProposedIndexPath:)]) {
        return [self.delegate baseTableController:self targetIndexPathForMoveFromRowAtIndexPath:sourceIndexPath toProposedIndexPath:proposedDestinationIndexPath];
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willBeginReorderingRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.currentReorderingRow = indexPath;
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:willBeginReorderingRowAtIndexPath:)]) {
        [self.delegate baseTableController:self willBeginReorderingRowAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView didEndReorderingRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:didEndReorderingRowAtIndexPath:)]) {
        [self.delegate baseTableController:self didEndReorderingRowAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView didCancelReorderingRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:didCancelReorderingRowAtIndexPath:)]) {
        [self.delegate baseTableController:self didCancelReorderingRowAtIndexPath:indexPath];
    }
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:indentationLevelForRowAtIndexPath:)]) {
        return [self.delegate baseTableController:self indentationLevelForRowAtIndexPath:indexPath];
    }
    return 0;
}

- (void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:didDeselectRowAtIndexPath:)]) {
        [self.delegate baseTableController:self didDeselectRowAtIndexPath:indexPath];
    }
}



- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    //do whatever u want after row has been moved

    [self swapFromIndexPath:fromIndexPath toIndexPath:toIndexPath];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isEnabledEditMode) {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.isEnabledEditMode) {
        return;
    }
    MEFObject *section  = [self sectionAtIndex:indexPath.section];
    MEFObject *object = [self objectAtIndexPath:indexPath];
    [section removeItem:object];
    if (!section.items.count) {
        [self.sections removeObject:section];
        [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else {
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:deleteSingleRowWithObject:)]) {
        [self.delegate baseTableController:self deleteSingleRowWithObject:object];
    } else {
        MEFObject *section_editing = [self.editModeHashmap objectForKey:[section indentifierObject]];
        if (!section_editing) {
            section_editing = [section copyWithoutItems];
            [self.editModeHashmap setObject:section_editing forKey:[section indentifierObject]];
        }
        [section_editing addItem:object];
        [self.editedObjects addObject:object];
    }
}


- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.isEnabledDragMode || self.isEnabledEditMode;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:didEndDisplayingCell:forRowAtIndexPath:)]) {
        [self.delegate baseTableController:self didEndDisplayingCell:cell forRowAtIndexPath:indexPath];
    }
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:titleForHeaderInSection:)]) {
        [self.delegate baseTableController:self titleForHeaderInSection:section];
    }
    return nil;
}

- (void) baseTableController:(MEFBaseTableViewController *)controller didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (UITableViewCell *) baseTableController:(MEFBaseTableViewController*)controller createTableViewCellWithObject:(MEFObject *)object reuseIdentifier:(NSString *)reuseIdentifier indexPath:(NSIndexPath *)indexPath;
{
    return nil;
}
- (void) baseTableController:(MEFBaseTableViewController*)controller configTableViewCell:(UITableViewCell *)cell object:(MEFObject *)object indexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat) baseTableController:(MEFBaseTableViewController *)controller heightForHeaderInSection:(NSInteger)section
{
    return 0;
}


- (void) tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:willDisplayHeaderView:forSection:)]) {
        [self.delegate baseTableController:self willDisplayHeaderView:view forSection:section];
    }
}

- (void) tableView:(UITableView *)tableView didEndDisplayingHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:didEndDisplayingHeaderView:forSection:)]) {
        [self.delegate baseTableController:self didEndDisplayingHeaderView:view forSection:section];
    }
    
}
#pragma mark Footer
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if ([self.delegate respondsToSelector:@selector(baseTableController:heightForFooterInSection:)]) {
        return [self.delegate baseTableController:self heightForFooterInSection:section];
    }
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if ([self.delegate respondsToSelector:@selector(baseTableController:viewForFooterInSection:)]) {
        return [self.delegate baseTableController:self viewForFooterInSection:section];
    }
    return [UIView new];
    // If you are not using ARC:
    // return [[UIView new] autorelease];
}







#pragma mark - - scroll delegaete




- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:scrollViewDidScroll:)] ) {
        [self.delegate baseTableController:self scrollViewDidScroll:scrollView];
    }
    if (!self.isEnabledBottomRefreshControl) {
        return;
    }
    
    [self.refreshControlManager scrollViewDidScroll:scrollView];
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:scrollViewWillBeginDragging:)] ) {
        [self.delegate baseTableController:self scrollViewWillBeginDragging:scrollView];
    }
    
    if (!self.isEnabledBottomRefreshControl) {
        return;
    }
    
    [self.refreshControlManager scrollViewWillBeginDragging:scrollView];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:scrollViewWillEndDragging:withVelocity:targetContentOffset:)] ) {
        [self.delegate baseTableController:self scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
    }
    [self.refreshControlManager scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
}


- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView;
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:scrollViewWillBeginDecelerating:)] ) {
        [self.delegate baseTableController:self scrollViewWillBeginDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:scrollViewDidEndDecelerating:)] ) {
        [self.delegate baseTableController:self scrollViewDidEndDecelerating:scrollView];
    }
    
    [self.refreshControlManager scrollViewDidEndDecelerating:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:scrollViewDidEndDragging:willDecelerate:)] ) {
        [self.delegate baseTableController:self scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
    [self.refreshControlManager scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}




- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableController:scrollViewDidEndScrollingAnimation:)] ) {
        [self.delegate baseTableController:self scrollViewDidEndScrollingAnimation:scrollView];
    }
//    [self.refreshControlManager scrollViewDidEndScrollingAnimation:scrollView];
}

#pragma mark - - key board

- (void) keyboardWillShow:(NSNotification*)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height), 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width), 0.0);
    }
    

//    self.tableView.contentInset = contentInsets;
//    self.tableView.scrollIndicatorInsets = contentInsets;
//    self.tableView.contentOffset = CGPointMake(0, keyboardSize.height);
//    [self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void) keyboardWillHide:(NSNotification*)notification
{
    
}

#pragma mark - - Options


- (void) addOption:(MEFBTableOption)option
{
    self.options |= option;
}

- (void) removeOption:(MEFBTableOption)option
{
    self.options &= ~option;
}

- (BOOL) existOption:(MEFBTableOption)option
{
    return (self.options & option) != 0;
}
#pragma mark - - Properties
- (NSMutableArray *)unsortedSections
{
	if (!_unsortedSections) {
		_unsortedSections = [NSMutableArray new];
	}
	return _unsortedSections;
}

- (NSMutableArray *)sections
{
	if (!_sections) {
		_sections = [NSMutableArray new];
	}
	return _sections;
}

- (NSMutableDictionary *)hashmap
{
	if (!_hashmap) {
		_hashmap = [NSMutableDictionary dictionary];
	}
	return _hashmap;
}


- (NSMutableDictionary*) editModeHashmap
{
    if (!_editModeHashmap) {
        _editModeHashmap = [NSMutableDictionary dictionary];
    }
    return _editModeHashmap;
}

- (NSMutableArray*) collaspeSections
{
    if (!_collaspeSections) {
        _collaspeSections = [NSMutableArray array];
    }
    return _collaspeSections;
}

- (NSMutableArray*) editedObjects
{
    if (!_editedObjects) {
        _editedObjects = [NSMutableArray new];
    }
    return _editedObjects;
}

- (MEFRefreshControlManager*) refreshControlManager
{
    if (!_refreshControlManager) {
        _refreshControlManager = [MEFRefreshControlManager new];
        [_refreshControlManager setDelegate:self];
    }
    return _refreshControlManager;
}

#pragma mark - - dealloc
- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void) modifySections:(NSMutableArray *)sections
{
    
}
- (void) willReloadData
{
    
}
- (void) didReloadData
{
    
}
@end

