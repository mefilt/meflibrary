//
//  MEFMultiSelectTableViewController.m
//  ipad_project
//
//  Created by Прокофьев Руслан on 02.10.13.
//  Copyright (c) 2013 Прокофьев Руслан. All rights reserved.
//


#import "MEFMultiSelectTableViewController.h"
#import "UIImage+MEFAssistants.h"
@interface MEFMultiSelectTableViewController () <UIGestureRecognizerDelegate>

@property (strong, nonatomic, readwrite) NSMutableDictionary *selectedItemsDictionary;
@property (assign, nonatomic, readwrite) int currentCountSelected;
@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;
@end

@implementation MEFMultiSelectTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self initialize];
    }
    return self;
}
- (id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self initialize];
    }
    return self;
}

- (id) initWithStyle:(UITableViewStyle)style
{
    if (self = [super initWithStyle:style]) {
        [self initialize];
    }
    return self;
}

- (void) initialize
{
    [self setSelectCircleColor:[UIColor blueColor]];
}

- (void) setSelectCircleColor:(UIColor *)selectCircleColor
{
    _selectCircleColor = selectCircleColor;
    self.selectImage = [UIImage createSelectCircleWithColor:selectCircleColor rect:CGRectMake(0, 0, 30, 30) empty:false];
    self.emptySelectImage = [UIImage createSelectCircleWithColor:selectCircleColor rect:CGRectMake(0, 0, 30, 30) empty:true];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - - public methods


- (int) countItems
{
    int countItems = 0;
    for (MEFObject *sector in self.sections) {
        countItems += sector.items.count;
    }
    return countItems;
}

- (NSMutableArray *) selectedItems
{
    NSMutableArray *array = [NSMutableArray array];
    for (MEFSelectedItem *item in self.selectedItemsDictionary.allValues) {
        if (item.status == kMEFSIStatusAlreadyNotSelected || item.status == kMEFSIStatusNotSelected) {
            continue;
        }
        [array addObject:item];
    }
    return array;
}




- (void) setEnabledMultiSelect:(BOOL)enabledMultiSelect
{
    if (_enabledMultiSelect == enabledMultiSelect) {
        return;
    }
    _enabledMultiSelect = enabledMultiSelect;
    if (_enabledMultiSelect) {
        self.tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
        [self.tapGesture setDelegate:self];
        [self.view addGestureRecognizer:self.tapGesture];
    } else {
        if (self.tapGesture) {
            [self.view removeGestureRecognizer:self.tapGesture];
            self.tapGesture = nil;
        }
    }
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return true;
}


- (void) tapGesture:(UIGestureRecognizer*)gesture
{
    if (gesture.state != UIGestureRecognizerStateEnded) {
        return;
    }
    UIView *touchView = [gesture view];
    BOOL found = [self searchTouchView:gesture.view];
    if (!found) {
        return;
    }

    CGPoint location = [gesture locationInView:gesture.view];
    location = [touchView convertPoint:location  toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    [self selectedItemByIndexPath:indexPath updateDelegate:YES];
}



#pragma mark - - Window Delegate


- (BOOL)interceptEvent:(UIEvent *)event
{
    NSSet *touches = [event allTouches];
    UITouch *oneTouch = [touches anyObject];
    UIView *touchView = [oneTouch view];
    if (!touchView) {
        return NO;
    }
    CGPoint location = [oneTouch locationInView:oneTouch.view];
    location = [touchView convertPoint:location  toView:self.tableView];
    if (oneTouch.phase != UITouchPhaseEnded) {
        return NO;
    }

    BOOL found = [self searchTouchView:touchView.superview];
    if (!found) {
        return false;
    }
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    [self selectedItemByIndexPath:indexPath updateDelegate:YES];
    return false;
}

- (NSString*) uniqueIdentifierWithObject:(MEFObject*)object
{
    if (self.multiSelectDataSourse && [self.multiSelectDataSourse respondsToSelector:@selector(multiSelectUniqueIdentifierWithObject:)]) {
        return [self.multiSelectDataSourse multiSelectUniqueIdentifierWithObject:object];
    }
    return [object indentifierObject];
}


// оптимизовать код а то читаемость сложная
- (MEFSelectedItem*) selectedItemByIndexPath:(NSIndexPath*)indexPath
{
    return [self selectedItemByIndexPath:indexPath updateDelegate:false];
}

- (MEFSelectedItem*) selectedItemByIndexPath:(NSIndexPath*)indexPath updateDelegate:(BOOL)update
{
    MEFObject *object = [self objectAtIndexPath:indexPath];
    if (!object) {
        return Nil;
    }
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];

    
    NSString *key = [self uniqueIdentifierWithObject:object];
    MEFSelectedItem *selectedItem = [self.selectedItemsDictionary objectForKey:key];
    selectedItem.indexPath = indexPath;
    if (self.isEnabledOnlyOne && self.currentCountSelected > 0) {
        NSMutableArray *selectedItems = [self selectedItems];
        [selectedItems removeObject:selectedItem];
        for (MEFSelectedItem *selectedItem in selectedItems) {
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:selectedItem.indexPath];
            [self unselectItem:selectedItem cell:cell];
            if (update) {
                [self updateDelegateWithUnSelect:selectedItem];
            }
        }

    }
    
    
    if (selectedItem) {
        if ([selectedItem isSelected] && !self.isEnabledUnselect) {
            [self unselectItem:selectedItem cell:cell];
            if (update) {
                [self updateDelegateWithUnSelect:selectedItem];
            }
        } else if ((self.isEnabledOnlyOne && self.currentCountSelected == 0) || (!self.isEnabledOnlyOne)) {
                [self selectItem:selectedItem cell:cell];
        }
    } else if ((self.isEnabledOnlyOne && self.currentCountSelected == 0) || (!self.isEnabledOnlyOne)) {
        [cell setAccessoryView:[self accessoryViewWithStatus:selectedItem.status]];
        selectedItem = [self createSelectedItemByObject:object key:key status:kMEFSIStatusSelected];
        self.currentCountSelected++;
    }
    selectedItem.indexPath = indexPath;
    [self.selectedItemsDictionary setObject:selectedItem forKey:key];
    if (update) {
        [self updateDelegateWithSelect:selectedItem];
    }

    return selectedItem;
}

- (void) selectItem:(MEFSelectedItem*)selectedItem cell:(UITableViewCell*)cell
{
    switch (selectedItem.status) {
        case kMEFSIStatusAlreadyNotSelected:
        {
            selectedItem.status = kMEFSIStatusAlreadySelected;
            [cell setAccessoryView:[self accessoryViewWithStatus:selectedItem.status]];
            self.currentCountSelected++;
            
        } break;
        case kMEFSIStatusNotSelected:
        {
            selectedItem.status = kMEFSIStatusSelected;
            [cell setAccessoryView:[self accessoryViewWithStatus:selectedItem.status]];
            self.currentCountSelected++;
        } break;
        
        default:
            break;
    }
}

- (void) unselectItem:(MEFSelectedItem*)selectedItem cell:(UITableViewCell*)cell
{
    switch (selectedItem.status) {
        case kMEFSIStatusAlreadySelected:
        {
            selectedItem.status = kMEFSIStatusAlreadyNotSelected;
            [cell setAccessoryView:[self accessoryViewWithStatus:selectedItem.status]];
            self.currentCountSelected--;
        } break;
            
        case kMEFSIStatusSelected:
        {
            selectedItem.status = kMEFSIStatusNotSelected;
            [cell setAccessoryView:[self accessoryViewWithStatus:selectedItem.status]];
            self.currentCountSelected--;
        } break;
        default:
            break;
    }
}


- (void) updateDelegateWithUnSelect:(MEFSelectedItem*)item
{
    if ([self.multiSelectDelegate respondsToSelector:@selector(multiSelectDidUnSelected:controller:)]) {
        [self.multiSelectDelegate multiSelectDidUnSelected:item controller:self];
    }
}
- (void) updateDelegateWithSelect:(MEFSelectedItem*)item
{
    if ([self.multiSelectDelegate respondsToSelector:@selector(multiSelectDidSelected:controller:)]) {
        [self.multiSelectDelegate multiSelectDidSelected:item controller:self];
    }
}


#pragma mark - - Privates Methods
- (MEFSelectedItem*) createSelectedItemByObject:(MEFObject*)object key:(NSString*)key status:(MEFSelectedItemStatus)status
{
    MEFSelectedItem *selectedItem = [MEFSelectedItem new];
    selectedItem.data = object.data;
    selectedItem.status = status;
    selectedItem.type = object.type;
    selectedItem.key = object.key;
    return selectedItem;
}

- (MEFSelectedItem*) addSelectedItemByObject:(MEFObject*)object key:(NSString*)key status:(MEFSelectedItemStatus) status
{
    MEFSelectedItem *selectedItem = [MEFSelectedItem new];
    selectedItem.data = object.data;
    selectedItem.status = status;
    selectedItem.type = object.type;
    selectedItem.key = object.key;
    [self.selectedItemsDictionary setObject:selectedItem forKey:key];
    return selectedItem;
}



- (MEFSelectedItem*) createSelectedItemByObject:(MEFObject*)object key:(NSString*)key
{
    return [self createSelectedItemByObject:object key:key status:kMEFSIStatusNotSelected];
}

- (UIImageView*) accessoryViewWithStatus:(MEFSelectedItemStatus) status
{
    
    UIImageView *checkMark = nil;
    if (!self.isEnabledMultiSelect) {
        return nil;
    }
    if ((status == kMEFSIStatusSelected || status == kMEFSIStatusAlreadySelected)) {
        checkMark = [[UIImageView alloc] initWithImage:self.selectImage];
    } else {
        if (self.isEnabledOnlyOne) {
            checkMark = [[UIImageView alloc] initWithImage:self.emptySelectImage];
        } else {
            checkMark = [[UIImageView alloc] initWithImage:self.emptySelectImage];
        }
    }
    return checkMark;
}

- (BOOL) searchCellView:(UIView*)view
{
    BOOL result = false;
    if (view.superview) {
        result = [self searchTouchView:view.superview];
    }
    if ([view isKindOfClass:[UITableViewCell class]]) {
        result = true;
    }
    return result;
}
- (BOOL) searchTouchView:(UIView*)view
{
    BOOL result = false;
    if (view.superview) {
        result = [self searchTouchView:view.superview];
    }
    if (view == self.tableView) {
        result = true;
    }
    return result;
}

- (void) modifySections:(NSMutableArray *)sections
{
    if (!self.isEnabledMultiSelect) {
        return;
    }
    if (!self.isDisabledSelectionOnTop) {
        for (MEFObject *section in sections) {
            __block int index = 0;
            [section setModifyObjects:^(NSMutableArray *objects) {
                NSArray *temp = [NSArray arrayWithArray:objects];
        
                for (MEFObject *object in temp) {
                    if (![self.multiSelectDelegate respondsToSelector:@selector(multiSelectContainsByObject:controller:)]) {
                        continue;
                    }
                    if ([self.multiSelectDelegate multiSelectContainsByObject:object controller:self]) {
                        [objects removeObject:object];
                        [objects insertObject:object atIndex:index];
                        index++;
                    }
                }
           

            }];
        }
    }
}

- (void) cashItemsByTheSelected
{
    if (!self.isEnabledMultiSelect) {
        return;
    }
    for (MEFObject *sector in self.sections) {
        for (MEFObject *object in sector.items) {
            NSString *key = [self uniqueIdentifierWithObject:object];
            if ([self.multiSelectDelegate respondsToSelector:@selector(multiSelectContainsByObject:controller:)] && [self.multiSelectDelegate multiSelectContainsByObject:object controller:self]) {
                [self addSelectedItemByObject:object key:key status:kMEFSIStatusSelected];
                self.currentCountSelected++;
            } else {
                [self addSelectedItemByObject:object key:key status:kMEFSIStatusNotSelected];
            }
        }
    }
}


- (void) configMultiCell:(UITableViewCell *)cell  indexPath:(NSIndexPath *)indexPath
{
    if (!self.isEnabledMultiSelect) {
        return;
    }
    cell.userInteractionEnabled = false;
    MEFObject *object = [self objectAtIndexPath:indexPath];
    NSString *key = [self uniqueIdentifierWithObject:object];
    MEFSelectedItem *selectedItem = [self.selectedItemsDictionary objectForKey:key];
    selectedItem.indexPath = indexPath;
    if (selectedItem) {
        [cell setAccessoryView:[self accessoryViewWithStatus:selectedItem.status]];
        return;
    }

    if (![self.multiSelectDelegate respondsToSelector:@selector(multiSelectContainsByObject:controller:)]) {
        return;
    }
    
    if ([self.multiSelectDelegate multiSelectContainsByObject:object controller:self]) {
        selectedItem = [self createSelectedItemByObject:object key:key status:kMEFSIStatusAlreadySelected];
        NSString *key = [self uniqueIdentifierWithObject:object];
        [self.selectedItemsDictionary setObject:selectedItem forKey:key];
        [cell setAccessoryView:[self accessoryViewWithStatus:selectedItem.status]];
        self.currentCountSelected++;
    } else {
        selectedItem = [self createSelectedItemByObject:object key:key status:kMEFSIStatusNotSelected];
        NSString *key = [self uniqueIdentifierWithObject:object];
        [self.selectedItemsDictionary setObject:selectedItem forKey:key];
        [cell setAccessoryView:[self accessoryViewWithStatus:selectedItem.status]];
    }
//    selectedItem.indexPath = indexPath;
}


#pragma mark - - Delegate tableview
- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!self.hasCustomAccessory) {
        [cell setAccessoryView:[self accessoryViewWithStatus:kMEFSIStatusNotSelected]];
    }
    [self configMultiCell:cell indexPath:indexPath];
    return cell;
}


#pragma mark - - Base Table View Controller

- (void) didReloadData
{
    [self cashItemsByTheSelected];
}



#pragma mark - - Properties
- (NSMutableDictionary*) selectedItemsDictionary
{
    if (!_selectedItemsDictionary) {
        _selectedItemsDictionary = [NSMutableDictionary new];
    }
    return _selectedItemsDictionary;
}

- (UITapGestureRecognizer*) tapGesture
{
    if (!_tapGesture) {
        _tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    }
    return _tapGesture;
}


- (void) dealloc
{
    
}
@end


