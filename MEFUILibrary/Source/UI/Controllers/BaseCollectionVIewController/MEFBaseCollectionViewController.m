//
//  MEFBaseCollectionViewController.m
//  ipad_project
//
//  Created by Прокофьев Руслан on 06.11.13.
//  Copyright (c) 2013 Прокофьев Руслан. All rights reserved.
//

#import "MEFBaseCollectionViewController.h"

@interface MEFBaseCollectionViewController () <UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MEFRefreshControlManagerDelegate>
{
    BOOL _isBeginRefreshControl;
}
@property (strong, nonatomic, readwrite) NSMutableArray *sections;
@property (strong, nonatomic, readwrite) NSMutableDictionary *hashmap;
@property (strong, nonatomic, readwrite) NSMutableArray *assortedSections;
@property (strong, nonatomic, readwrite) UIRefreshControl *refreshControl;
@property (strong, nonatomic, readwrite) MEFRefreshControlManager *refreshControlManager;
@end

@implementation MEFBaseCollectionViewController






- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}


- (void) cleanup
{
    [self.sections removeAllObjects];
    [self.assortedSections removeAllObjects];
    [self.hashmap removeAllObjects];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];

}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}



- (void) setEnabledRefreshControl:(BOOL)isEnabledRefreshControl
{
    if (isEnabledRefreshControl && !self.refreshControl) {
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                            initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 25)];
        [refreshControl addTarget:self action:@selector(refreshContent:) forControlEvents:UIControlEventValueChanged];
        [self.collectionView addSubview:refreshControl];
        self.refreshControl = refreshControl;
    } else {
        [self.refreshControl removeFromSuperview];
        self.refreshControl = nil;
    }
    _enabledRefreshControl = isEnabledRefreshControl;
    
}

- (void) setBottomRefreshControl:(UIView<MEFRefreshControlPropertyProtocol> *)bottomRefreshControl
{
    [self.refreshControlManager setBottomRefreshControl:bottomRefreshControl];
}
- (UIView <MEFRefreshControlPropertyProtocol>*) bottomRefreshControl
{
    return [self.refreshControlManager bottomRefreshControl];
}
- (void) setEnabledRefreshControlBottom:(BOOL)isEnabledRefreshControlBottom
{
    [self.refreshControlManager setEnabledBottomRefreshControl:isEnabledRefreshControlBottom];
}

- (BOOL) isEnabledRefreshControlBottom
{
    return [self.refreshControlManager isEnabledBottomRefreshControl];
}


- (void) endBottomRefreshControl;
{
    [self.refreshControlManager endBottomRefreshControl:self.collectionView];
}

- (void) hideEndBottomRefreshControl:(void (^)(BOOL completed))completion
{
  
}

- (void) beginRefreshControl
{
    if (!_isBeginRefreshControl) {
        [self.collectionView setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:YES];
    }

    [self.refreshControl beginRefreshing];
}


- (void) endRefreshControl
{
    if (!_isBeginRefreshControl) {
        [self.collectionView setContentOffset:CGPointMake(0, 0) animated:YES];        
    }
    _isBeginRefreshControl = false;
    [self.refreshControl endRefreshing];
}

- (void) refreshContent:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(refreshCollectionContentWithBaseCollectionViewController:)]) {
        [self.delegate refreshCollectionContentWithBaseCollectionViewController:self];
    }
}







- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - - public Methods




- (BOOL) isEmpty
{
    return self.assortedSections.count == 0;
}
- (void) reloadData
{
    NSAssert(self.datasource != nil, @"DataSource - nil");
    
//    id o = self.collectionView.delegate;
//    id p = self.collectionView.dataSource;
    
    [self willReloadData];
    [self.collectionView reloadData];
}



- (void) addSectionByIndex:(NSInteger)row section:(MEFObject *)section
{
    NSInteger count = self.sections.count;
    if (row < count) {
        [self.sections insertObject:section atIndex:row];
    } else {
        [self.sections addObject:section];
    }
    
    if (section.key) {
        [self.hashmap setObject:section forKey:section.key];
    }
}

- (void) addSection:(MEFObject*)object
{
    if (object.key) {
        [self.hashmap setObject:object forKey:object.key];
    }
    [self.sections addObject:object];
}

- (MEFObject*) sectionAtIndex:(NSInteger) index
{
    if (index >= self.assortedSections.count) {
        return Nil;
    }
    return [self.assortedSections objectAtIndex:index];
}

- (MEFObject*) objectAtIndexPath:(NSIndexPath*)indexPath
{
    MEFObject *section = [self sectionAtIndex:indexPath.section];
    if (!section) {
        return Nil;
    }
    return [section itemAtIndex:indexPath.row];
}
- (MEFObject*) sectionAtKey:(NSString*) key
{
    return self.hashmap[key];
}

- (MEFObject *)objectFromSectionsAtKey:(NSString *)key
{
    for (MEFObject *section in self.hashmap.allValues) {
        id obj = [section itemAtKey:key];
        if (obj) {
            return obj;
        }
    }
    return nil;
}





- (void) reloadSection:(MEFObject*)section
{
    [self willLoadSection:section];
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:[self.assortedSections  indexOfObject:section]]];
}
- (void) reloadSections
{
    [self willReloadData];
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.assortedSections.count)]];
}



- (void) insertSections:(NSArray*)sections toSectionByIndex:(NSUInteger)index
{
    int lIndex = index;
    for (MEFObject *section in sections) {
        [self addSectionByIndex:lIndex section:section];
        lIndex++;
    }
    [self willReloadData];
    [self.collectionView insertSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(index, sections.count)]];
}
#pragma mark insert objects
- (void) insertObjects:(NSArray*)objects toSection:(MEFObject*)section
{
    [self insertObjects:objects toIndexPath:[NSIndexPath indexPathForRow:0 inSection:[self.assortedSections indexOfObject:section]]];
}

- (void) insertObjectsToLastSection:(NSArray*)objects
{
    [self insertObjects:objects toIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.assortedSections.count - 1]];
}
- (void) insertObjects:(NSArray*)objects toIndexPath:(NSIndexPath*)indexPath;
{
    MEFObject *section = [self sectionAtIndex:indexPath.section];
    NSUInteger countBegin = section.items.count;
    [section addItems:objects];
    [self willLoadSection:section];
    
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (NSUInteger i = countBegin; i < section.items.count; ++i) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
    }
    [self.collectionView insertItemsAtIndexPaths:indexPaths];
}

- (void) deleteItemsAtSection:(MEFObject *)section
{
    NSUInteger index = [self.assortedSections indexOfObject:section];
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (int i = 0;i < section.items.count; ++i) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:index]];
    }
    [section removeItems];
    [self.collectionView deleteItemsAtIndexPaths:indexPaths];
}



#pragma mark - - Collection data source


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    
    return self.assortedSections.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{

    MEFObject *object = [self sectionAtIndex:section];
    if (!object) {
        return 0;
    }
    NSUInteger count = object.items.count;
    return count;
}


- (UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MEFObject *object = [self objectAtIndexPath:indexPath];
    if (!object) {
        
        return nil;
        
    }
    NSString *indentifierObject = [self indentifierObject:object];

    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:indentifierObject forIndexPath:indexPath];
    
    if (!cell && self.datasource && [self.datasource respondsToSelector:@selector(baseCollectionViewController:createCollectionViewCellWithObject:cellForItemAtIndexPath:)]) {
        cell = [self.datasource baseCollectionViewController:self createCollectionViewCellWithObject:object cellForItemAtIndexPath:indexPath];
    }
    if (self.datasource && [self.datasource respondsToSelector:@selector(baseCollectionViewController:configCollectionViewCellWithObject:cellForItemAtIndexPath:cell:)]) {
        [self.datasource baseCollectionViewController:self configCollectionViewCellWithObject:object cellForItemAtIndexPath:indexPath cell:cell];
    }
    return cell;
}


#pragma mark -- delegate
- (BOOL) collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:shouldHighlightItemAtIndexPath:)]) {
        return [self.delegate baseCollectionViewController:self shouldHighlightItemAtIndexPath:indexPath];
    }
    return YES;
}



- (UICollectionReusableView*) collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:viewForSupplementaryElementOfKind:atIndexPath:)]) {
       return [self.delegate baseCollectionViewController:self viewForSupplementaryElementOfKind:kind atIndexPath:indexPath];
    }
    return nil;
}



- (UIEdgeInsets) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:layout:insetForSectionAtIndex:)]) {
        return [self.delegate baseCollectionViewController:self layout:collectionViewLayout insetForSectionAtIndex:section];
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
    return YES;
}


- (void) collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:didHighlightItemAtIndexPath:)]) {
        [self.delegate baseCollectionViewController:self didHighlightItemAtIndexPath:indexPath];
    }
}

- (void) collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:didUnhighlightItemAtIndexPath:)]) {
        [self.delegate baseCollectionViewController:self didUnhighlightItemAtIndexPath:indexPath];
    }
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:didSelectItemAtIndexPath:)]) {
        [self.delegate baseCollectionViewController:self didSelectItemAtIndexPath:indexPath];
    }
}


#pragma mark - -  Flow layout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:sizeForItemAtIndexPath:layout:)]) {
        return  [self.delegate baseCollectionViewController:self sizeForItemAtIndexPath:indexPath layout:collectionViewLayout];
    }
    return CGSizeMake(0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:layout:referenceSizeForHeaderInSection:)]) {
        return  [self.delegate baseCollectionViewController:self layout:collectionViewLayout referenceSizeForHeaderInSection:section];
    }
    return CGSizeMake(0, 0);
}



#pragma mark - - MEFRefreshControlManagerDelegate
- (CGFloat) heightTopRefreshControl
{
    return self.refreshControl.bounds.size.height;
}


- (void) willShowBottomRefreshControl
{
    [self.refreshControl setEnabled:false];
    if (self.delegate && [self.delegate respondsToSelector:@selector(willShowBottomRefreshControlWithBaseCollectionViewController:)]) {
        [self.delegate willShowBottomRefreshControlWithBaseCollectionViewController:self];
    }
}

- (void) didShowBottomRefreshControl
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didShowBottomRefreshControlWithBaseCollectionViewController:)]) {
        [self.delegate didShowBottomRefreshControlWithBaseCollectionViewController:self];
    }
}
- (void) releaseBottomRefreshControl
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(releaseBottomRefreshControlWithBaseCollectionViewController:)]) {
        [self.delegate releaseBottomRefreshControlWithBaseCollectionViewController:self];
    }
}
- (void) willHideBottomRefreshControl:(BOOL)release
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(willHideBottomRefreshControl:baseCollectionViewController:)]) {
        [self.delegate willHideBottomRefreshControl:release baseCollectionViewController:self];
    }
    
}
- (void) didHideBottomRefreshControl:(BOOL)release
{
    [self.refreshControl setEnabled:true];
    if (self.delegate && [self.delegate respondsToSelector:@selector(didHideBottomRefreshControl:baseCollectionViewController:)]) {
        [self.delegate didHideBottomRefreshControl:release baseCollectionViewController:self];
    }
}



- (NSMutableArray*) sortSections:(NSMutableArray*)sections
{
    
    return [NSMutableArray arrayWithArray:sections];
}

- (void) registerClasseForObject:(MEFObject*)section
{
    NSMutableDictionary *types = [NSMutableDictionary dictionary];
    for (MEFObject *object in section.items) {
        NSString *indentifierObject = [self indentifierObject:object];
        if ([types objectForKey:indentifierObject]) {
            continue;
        }
        [types setObject:object forKey:indentifierObject];
    }
    [self registerClassesWithDictionary:types];
}

- (void) registerClassesWithDictionary:(NSDictionary*)types
{
    for (NSString *key in types.allKeys) {
        MEFObject *object = types[key];
        Class class;
        UINib *nib;
        if (self.delegate && [self.datasource respondsToSelector:@selector(baseCollectionViewController:registerCollectionClass:)]) {
            class = [self.datasource baseCollectionViewController:self registerCollectionClass:object];
        }
        if (class != Nil) {
            [self.collectionView registerClass:class forCellWithReuseIdentifier:key];
        }
        if (self.datasource && [self.datasource respondsToSelector:@selector(baseCollectionViewController:registerCollectionNib:)]) {
            nib = [self.datasource baseCollectionViewController:self registerCollectionNib:object];
        }
        if (nib != nil) {
            [self.collectionView registerNib:nib forCellWithReuseIdentifier:key];
        }
        
    }
}
- (void) registerClasses:(NSArray*)objects
{
    NSMutableDictionary *types = [NSMutableDictionary dictionary];
    
    for (MEFObject *section in objects) {
        for (MEFObject *object in section.items) {
            NSString *indentifierObject = [self indentifierObject:object];
            if ([types objectForKey:indentifierObject]) {
                continue;
            }
            [types setObject:object forKey:indentifierObject];
        }
    }
    [self registerClassesWithDictionary:types];
}

- (NSString*) indentifierObject:(MEFObject*)object
{
    NSString *indentifierObject = nil;
    if (self.datasource && [self.datasource respondsToSelector:@selector(baseCollectionViewController:reuseIdentifierWithObject:)]) {
        indentifierObject = [self.datasource baseCollectionViewController:self reuseIdentifierWithObject:object];
    } else {
        indentifierObject = [NSString stringWithFormat:@"%d", object.type];
    }
    return indentifierObject;
}



#pragma mark - - Scroll view delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:scrollViewDidScroll:)]) {
        [self.delegate baseCollectionViewController:self scrollViewDidScroll:scrollView];
    }
    [self.refreshControlManager scrollViewDidScroll:scrollView];
}


#pragma mark - - scroll delegate
- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:scrollViewWillEndDragging:withVelocity:targetContentOffset:)]) {
        [self.delegate baseCollectionViewController:self scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
    }
    [self.refreshControlManager scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.refreshControlManager scrollViewWillBeginDragging:scrollView];
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:scrollViewDidEndDragging:willDecelerate:)]) {
        [self.delegate baseCollectionViewController:self scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
    [self.refreshControlManager scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

- (void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self.refreshControlManager scrollViewDidEndScrollingAnimation:scrollView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:scrollViewDidEndDecelerating:)]) {
        [self.delegate baseCollectionViewController:self scrollViewDidEndDecelerating:scrollView];
    }
    
    [self.refreshControlManager scrollViewDidEndDecelerating:scrollView];
}


#pragma mark - - load/ reload data

- (void) willLoadSection:(MEFObject*)section
{
    [section reloadObjects];
    [self registerClasseForObject:section];
    
}
- (void) willReloadData
{
    [self.assortedSections removeAllObjects];
    NSMutableArray *assorted_sections = [NSMutableArray arrayWithArray:self.sections];
    if (self.sectionSort) {
		__weak MEFBaseCollectionViewController *weakSelf = self;
		[assorted_sections sortUsingComparator: ^NSComparisonResult (id obj1, id obj2) {
		    return weakSelf.sectionSort(obj1, obj2);
		}];
	}
    [self.assortedSections addObjectsFromArray:assorted_sections];
    for (MEFObject *section in self.assortedSections) {
        [section reloadObjects];
    }
    [self registerClasses:self.assortedSections];
}

#pragma mark - - Properties
- (MEFRefreshControlManager*) refreshControlManager
{
    if (!_refreshControlManager) {
        _refreshControlManager = [[MEFRefreshControlManager alloc]init];
        [_refreshControlManager setDelegate:self];
    }
    return _refreshControlManager;
}
- (NSMutableArray*) sections
{
    if (!_sections) {
        _sections = [NSMutableArray new];
    }
    return _sections;
}
- (NSMutableArray*) assortedSections
{
    if (!_assortedSections) {
        _assortedSections = [NSMutableArray new];
    }
    return _assortedSections;
}

- (NSMutableDictionary*) hashmap
{
    if (!_hashmap) {
        _hashmap = [NSMutableDictionary dictionary];
    }
    return _hashmap;
}

@end
