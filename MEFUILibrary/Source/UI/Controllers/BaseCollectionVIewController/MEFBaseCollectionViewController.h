//
//  MEFBaseCollectionViewController.h
//  ipad_project
//
//  Created by Прокофьев Руслан on 06.11.13.
//  Copyright (c) 2013 Прокофьев Руслан. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEFObject.h"
#import "MEFRefreshControlManager.h"
@class MEFBaseCollectionViewController;



@protocol MEFBaseCollectionViewControllerDatasource <NSObject>


- (void) baseCollectionViewController:(MEFBaseCollectionViewController*)controller  configCollectionViewCellWithObject:(MEFObject*)object cellForItemAtIndexPath:(NSIndexPath *)indexPath cell:(UICollectionViewCell*)cell;

@optional
- (UICollectionViewCell*) baseCollectionViewController:(MEFBaseCollectionViewController*)controller createCollectionViewCellWithObject:(MEFObject*)object cellForItemAtIndexPath:(NSIndexPath *)indexPath;
- (Class) baseCollectionViewController:(MEFBaseCollectionViewController*)controller registerCollectionClass:(MEFObject*)object;
- (UINib*) baseCollectionViewController:(MEFBaseCollectionViewController*)controller registerCollectionNib:(MEFObject*)object;
- (NSString*) baseCollectionViewController:(MEFBaseCollectionViewController*)controller reuseIdentifierWithObject:(MEFObject*)object;

@end


@protocol MEFBaseCollectionViewControllerDelegate
@optional;

- (void) willShowBottomRefreshControlWithBaseCollectionViewController:(MEFBaseCollectionViewController*)controller;
- (void) didShowBottomRefreshControlWithBaseCollectionViewController:(MEFBaseCollectionViewController*)controller;
- (void) releaseBottomRefreshControlWithBaseCollectionViewController:(MEFBaseCollectionViewController*)controller;
- (void) willHideBottomRefreshControl:(BOOL)release baseCollectionViewController:(MEFBaseCollectionViewController*)controller;
- (void) didHideBottomRefreshControl:(BOOL)release baseCollectionViewController:(MEFBaseCollectionViewController*)controller;


- (void) refreshCollectionContentWithBaseCollectionViewController:(MEFBaseCollectionViewController*)controller;


- (void) bottomRefreshContentWithBaseCollectionViewController:(MEFBaseCollectionViewController*)controller;
- (void) showBeginRefreshControlWithBaseCollectionViewController:(MEFBaseCollectionViewController*)controller;
- (void) hideBeginRefreshControlWithBaseCollectionViewController:(MEFBaseCollectionViewController*)controller;

- (void) baseCollectionViewController:(MEFBaseCollectionViewController*)controller didHighlightItemAtIndexPath:(NSIndexPath *)indexPath;
- (void) baseCollectionViewController:(MEFBaseCollectionViewController*)controller didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath;
- (BOOL) baseCollectionViewController:(MEFBaseCollectionViewController*)controller shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath;

- (void) baseCollectionViewController:(MEFBaseCollectionViewController*)controller didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
- (void) baseCollectionViewController:(MEFBaseCollectionViewController*)controller didDeselectItemAtIndexPath:(NSIndexPath *)indexPath;
- (void) baseCollectionViewController:(MEFBaseCollectionViewController*)controller didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath;
- (void) baseCollectionViewController:(MEFBaseCollectionViewController*)controller didScrollWithPage:(NSUInteger)page;
- (void) baseCollectionViewController:(MEFBaseCollectionViewController*)controller willSelectItemAtIndexPath:(NSIndexPath *)indexPath;


- (CGSize) baseCollectionViewController:(MEFBaseCollectionViewController*)controller sizeForItemAtIndexPath:(NSIndexPath *)indexPath layout:(UICollectionViewLayout  *)collectionViewLayout;
- (UICollectionReusableView *) baseCollectionViewController:(MEFBaseCollectionViewController*)controller viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath;
- (CGSize)  baseCollectionViewController:(MEFBaseCollectionViewController*)controller  layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section;
- (UIEdgeInsets) baseCollectionViewController:(MEFBaseCollectionViewController*)controller layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section;



- (void) baseCollectionViewController:(MEFBaseCollectionViewController*)controller  scrollViewDidScroll:(UIScrollView *)scrollView;                                               // any offset changes
//- (void)scrollViewDidZoom:(UIScrollView *)scrollView NS_AVAILABLE_IOS(3_2); // any zoom scale changes
//
//// called on start of dragging (may require some time and or distance to move)
//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;
//// called on finger up if the user dragged. velocity is in points/millisecond. targetContentOffset may be changed to adjust where the scroll view comes to rest
//- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset NS_AVAILABLE_IOS(5_0);
//// called on finger up if the user dragged. decelerate is true if it will continue moving afterwards
- (void) baseCollectionViewController:(MEFBaseCollectionViewController*)controller scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
//
//- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView;   // called on finger up as we are moving
- (void) baseCollectionViewController:(MEFBaseCollectionViewController*)controller scrollViewDidEndDecelerating:(UIScrollView *)scrollView;      // called when scroll view grinds to a halt


- (void)  baseCollectionViewController:(MEFBaseCollectionViewController*)controller  scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset;

//- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView; // called when setContentOffset/scrollRectVisible:animated: finishes. not called if not animating
//
//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView;     // return a view that will be scaled. if delegate returns nil, nothing happens
//- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view NS_AVAILABLE_IOS(3_2); // called before the scroll view begins zooming its content
//- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale; // scale between minimum and maximum. called after any 'bounce' animations
//
//- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView;   // return a yes if you want to scroll to the top. if not defined, assumes YES
//- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView;


@end

@interface MEFBaseCollectionViewController : UICollectionViewController


#pragma mark - - method
- (MEFObject*) sectionAtIndex:(NSInteger) index;
- (MEFObject*) sectionAtKey:(NSString*) key;
- (MEFObject*) objectAtIndexPath:(NSIndexPath*)indexPath;
- (MEFObject *)objectFromSectionsAtKey:(NSString *)key;
- (void) addSection:(MEFObject*)object;

- (void) addSectionByIndex:(NSInteger)row section:(MEFObject*) section;
- (void) cleanup;
- (void) reloadData;



#pragma mark - - insery object

- (void) insertSections:(NSArray*)sections toSectionByIndex:(NSUInteger)section;
- (void) insertObjects:(NSArray*)objects toIndexPath:(NSIndexPath*)indexPath;
- (void) insertObjects:(NSArray*)objects toSection:(MEFObject*)section;
- (void) insertObjectsToLastSection:(NSArray*)objects;

- (void) reloadSection:(MEFObject*)section;
- (void) reloadSections;
- (void) deleteItemsAtSection:(MEFObject*)section;

- (void) willLoadSection:(MEFObject*)section;


#pragma mark - - top refresh control
- (void) beginRefreshControl;
- (void) endRefreshControl;


#pragma mark - - bottom refresh control
- (void) endBottomRefreshControl;



@property (assign, nonatomic, readwrite, getter=isEnabledRefreshControl) BOOL enabledRefreshControl;
@property (assign, nonatomic, readwrite, getter=isEnabledRefreshControlBottom) BOOL enabledRefreshControlBottom;

@property (copy, nonatomic, readwrite) MEFObjectSort sectionSort;
@property (weak, nonatomic, readwrite) NSObject <MEFBaseCollectionViewControllerDelegate> *delegate;
@property (weak, nonatomic, readwrite) NSObject <MEFBaseCollectionViewControllerDatasource> *datasource;
@property (nonatomic, readwrite, strong) UIView <MEFRefreshControlPropertyProtocol> *bottomRefreshControl;


@property (assign, nonatomic, readonly) BOOL isEmpty;
@property (strong, nonatomic, readonly) UIRefreshControl *refreshControl;
@property (strong, nonatomic, readonly) NSMutableArray *sections;
@property (strong, nonatomic, readonly) NSMutableArray *assortedSections;

@end
