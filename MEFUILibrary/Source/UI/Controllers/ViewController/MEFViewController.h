//
//  MEFViewController.h
//  Katusha
//
//  Created by Prokofev Ruslan on 19.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MEFNavigationItemEdge) {
    MEFNavigationItemEdgeLeft,
    MEFNavigationItemEdgeRigh,
};
typedef NS_ENUM(NSUInteger, MEFNavigationItemState) {
    MEFNavigationItemStateNormal,
    MEFNavigationItemStateEditing,
};


@protocol  MEFViewControllerStyle <NSObject>

- (void) loadingStyleWithParent:(UINavigationController*)navigationController;
@end

@interface MEFViewController : UIViewController
@property  (nonatomic, readwrite, weak) id <MEFViewControllerStyle> styleDelegate;
@property  (nonatomic, readwrite, assign, getter=isEnabledStyleTitle) BOOL enabledStyleTitle;
@end


@interface MEFViewController (Style) <MEFViewControllerStyle>

- (void) reloadStyle;


- (void) setExternalNavigationItems:(NSArray*)items edge:(MEFNavigationItemEdge)edge state:(MEFNavigationItemState)state;
- (void) reloadNavigationItems:(MEFNavigationItemState)state;
- (void) reloadNavigationItems;
//- (void) setEnabledNavigationItems:(BOOL)enabledNavigation;
@end

@interface MEFViewController (StyleAbstract)
- (BOOL) disableExternalNavigationItems:(MEFNavigationItemEdge)edge state:(MEFNavigationItemState)state;
- (NSArray*) createNavigationItems:(MEFNavigationItemEdge)edge state:(MEFNavigationItemState)state;
- (CGFloat) spacerNavigationItems:(MEFNavigationItemEdge)edge state:(MEFNavigationItemState)state;
- (NSString*) createNavigationTitleWithState:(MEFNavigationItemState)state;
@end