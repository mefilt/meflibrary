//
//  MEFStackCardPushSegue.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 22.09.15.
//  Copyright © 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFStackCardPushSegue.h"
#import "MEFStackCardViewController.h"
@implementation MEFStackCardPushSegue

- (void) perform
{
    id sourceViewController = self.sourceViewController;
    
    if (![sourceViewController isKindOfClass:[MEFStackCardViewController class]]) {
        @throw [NSException exceptionWithName:@"Source View controller not is king class MEFStackCardViewController" reason:nil userInfo:nil];
    }
    
    id destinationViewController = self.destinationViewController;;
    MEFStackCardViewController *stackCardViewController = sourceViewController;
    [stackCardViewController pushViewController:destinationViewController animated:self.animated];
    
}
@end
