//
//  MEFAdaptiveNavigationController.m
//  Katusha
//
//  Created by Prokofev Ruslan on 19.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFAdaptiveNavigationController.h"



@interface MEFAdaptiveNavigationController ()
{
}

@end

@implementation MEFAdaptiveNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)addViewController:(MEFViewController*)viewController
{
    NSAssert1([viewController isKindOfClass:[MEFViewController class]], @"viewController not extend MEFViewController", @"");
    [viewController moveToParentAdaptiveNavigationController:self];
}

- (void) addChildViewController:(UIViewController *)childController
{
    [super addChildViewController:childController];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
