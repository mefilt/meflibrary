//
//  MEFSVGImageSheet.h
//  MEFLibrary
//
//  Created by Mefilt on 26.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
@interface MEFSVGImageSheet : NSObject

+ (MEFSVGImageSheet*) SVGImageSheet;
- (UIImage*) imageWithLayer:(CALayer*)layer size:(CGSize)size;
- (CALayer*) layerWithIdentifier:(NSString*)identifier;
- (CALayer*) layerWithSVGFilename:(NSString*)filename;
- (UIImage*) imageWithSVGFilename:(NSString*)filename size:(CGSize)size;
- (UIImage*) imageWithIdentifier:(NSString*)identifier size:(CGSize)size;
@end
