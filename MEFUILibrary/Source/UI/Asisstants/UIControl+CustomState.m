//
//  UIControl+CustomState.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 10.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "UIControl+CustomState.h"

@implementation UIControl (CustomState)

+ (NSString*) stringState:(UIControlState)state
{
    switch (state) {
        case UIControlStateApplication:
            return @"UIControlStateApplication";
        case UIControlStateDisabled:
            return @"UIControlStateDisabled";
        case UIControlStateHighlighted:
            return @"UIControlStateHighlighted";
        case UIControlStateNormal:
            return @"UIControlStateNormal";
        case UIControlStateReserved:
            return @"UIControlStateReserved";
        case UIControlStateSelected:
            return @"UIControlStateSelected";
    }
    return @"";
}


@end
