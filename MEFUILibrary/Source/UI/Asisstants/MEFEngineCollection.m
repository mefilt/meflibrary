//
//  MEFEngineCollection.m
//  MEFLibrary
//
//  Created by mefilt on 09.06.16.
//  Copyright © 2016 Prokofev Ruslan. All rights reserved.
//

#import "MEFEngineCollection.h"
@interface MEFEngineCollection ()

@property (strong, nonatomic, readwrite) NSMutableArray *unsortedSections;
@property (strong, nonatomic, readwrite) NSMutableArray *sections;
@property (strong, nonatomic, readwrite) NSMutableDictionary *hashmap;
@property (strong, nonatomic, readwrite) NSMutableDictionary *dequeueHashmap;
@property (strong, nonatomic, readwrite) NSMutableDictionary *editModeHashmap;
@property (strong, nonatomic, readwrite) NSMutableArray *collaspeSections;
@property (strong, nonatomic, readwrite) NSMutableArray *editedObjects;

@end
@implementation MEFEngineCollection


- (void) reloadData
{
    
}
@end
