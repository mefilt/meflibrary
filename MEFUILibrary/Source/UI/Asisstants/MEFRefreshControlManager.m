//
//  MEFRefreshControlManager.m
//  MEFLibrary
//
//  Created by Mefilt on 23.06.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFRefreshControlManager.h"
#import "UIView+MEFAssistants.h"
@interface MEFRefreshControlManager ()

@property (nonatomic, readwrite, strong) UIView *contentView;
@property (nonatomic, readwrite, assign) MEFRCMOption options;
@property (nonatomic, readwrite, assign) UIEdgeInsets saveEdgeInsets;
@end
@implementation MEFRefreshControlManager


- (void) addOption:(MEFRCMOption)option
{
    self.options |= option;
    
    //    switch (option) {
    //        case MEFRCMOptionBottomRefreshControlBegin:
    //        {
    //            NSLog(@"BEGIN");
    //        }   break;
    //        case MEFRCMOptionBottomRefreshControlEnd:
    //        {
    //            NSLog(@"END");
    //        }   break;
    //        case MEFRCMOptionBottomRefreshControlVisible:
    //        {
    //            NSLog(@"VISIBLE");
    //        }   break;
    //        case MEFRCMOptionBeginUserScroll:
    //        {
    //            NSLog(@"BeginUserScroll");
    //        }   break;
    //        case MEFRCMOptionBottomRefreshControlRelease:
    //        {
    //            NSLog(@"Release");
    //        }   break;
    //        case MEFRCMOptionBottomRefreshControlLock:
    //        {
    //            NSLog(@"Lock");
    //        }   break;
    //        default:
    //            break;
    //    }
}

- (BOOL) existOption:(MEFRCMOption)option
{
    return (self.options & option) != 0;
}

- (void) removeOption:(MEFRCMOption)option
{
    self.options &= ~option;
}



#pragma mark - - Scroll view delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat scrollContentHeight = scrollView.contentSize.height < scrollView.frame.size.height ? scrollView.frame.size.height : scrollView.contentSize.height;
    CGFloat endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;// + scrollView.contentInset.bottom;
    CGFloat offSet = endScrolling - scrollContentHeight;
    if (![self existOption:MEFRCMOptionBeginUserScroll] || (endScrolling < scrollContentHeight) || (offSet <= 0)) {
        if ([self existOption:MEFRCMOptionBottomRefreshControlEnd] || [self existOption:MEFRCMOptionBottomRefreshControlRelease] || [self existOption:MEFRCMOptionBottomRefreshControlVisible]) {
            CGSize contentViewSize = [self calculateContentSize];
            if ([self existOption:MEFRCMOptionBottomRefreshControlAnimationHide]) {
                [self.contentView setPosition:CGPointMake(0, endScrolling)];
            } else {
                //                [self.contentView setPosition:CGPointMake(0, endScrolling - contentViewSize.height)];
            }
        }
        return;
    }
    
    
    
    CGSize contentViewSize = [self calculateContentSize];
    if ([self existOption:MEFRCMOptionBottomRefreshControlEnd] || [self existOption:MEFRCMOptionBottomRefreshControlRelease]) {
        [self.contentView setPosition:CGPointMake(0, endScrolling - contentViewSize.height)];
        return;
    }
    
    if (![self existOption:MEFRCMOptionBottomRefreshControlBegin]) {
        [self addOption:MEFRCMOptionBottomRefreshControlBegin];
        UIView *contentView = [self contentView];
        [contentView setSize:CGSizeMake(scrollView.frame.size.width, contentViewSize.height)];
        [contentView setPosition:CGPointMake(0, endScrolling)];
        [scrollView addSubview:contentView];
        [self.bottomRefreshControl setSize:CGSizeMake(scrollView.frame.size.width, contentViewSize.height)];
        [self.bottomRefreshControl setPosition:CGPointMake(0, 0)];
        [contentView setHidden:false];
        
        if (![self existOption:MEFRCMOptionBottomRefreshControlVisible]) {
            [self addOption:MEFRCMOptionBottomRefreshControlVisible];
            [self.bottomRefreshControl willShowRefreshControl];
            self.saveEdgeInsets = scrollView.contentInset;
            if (self.delegate && [self.delegate respondsToSelector:@selector(willShowBottomRefreshControl)]) {
                [self.delegate willShowBottomRefreshControl];
            }
        }
        
    }
    CGFloat heightValue =  [self heightValueBottomRefreshControl];
    CGFloat stretchValue = self.stretchValueBottomRefreshControl +  heightValue;
    CGFloat percent = 0;;
    
    if (offSet > [self heightValueBottomRefreshControl]) {
        percent = ((endScrolling - scrollContentHeight) - heightValue)  / (stretchValue - heightValue);
    }
    percent = fmin(percent, 1);
    percent = fmax(percent, 0);
    if (scrollView.isDecelerating && scrollView.isDragging) {
        [scrollView setContentInset:UIEdgeInsetsMake(0, 0, contentViewSize.height, 0)];
        [self.contentView setPositionY:endScrolling - contentViewSize.height];
    } else if (scrollView.isDragging && scrollView.isTracking) {
        CGFloat value = ceilf(contentViewSize.height * (offSet / heightValue));
        if (offSet <= heightValue) {
            CGFloat y = endScrolling - (contentViewSize.height * (offSet / contentViewSize.height));
            [self.contentView setPositionY:y];
            [scrollView setContentInset:UIEdgeInsetsMake(0, 0, contentViewSize.height, 0)];
        } else {
            [self.contentView setPositionY:endScrolling - contentViewSize.height];
            [scrollView setContentInset:UIEdgeInsetsMake(0, 0, contentViewSize.height, 0)];
        }
    }
    
    
    if ([self existOption:MEFRCMOptionBottomRefreshControlBegin]) {
        [self.bottomRefreshControl setPercent:percent];
    }
    if (percent == 1 && [self existOption:MEFRCMOptionBottomRefreshControlBegin]) {
        [self removeOption:MEFRCMOptionBottomRefreshControlBegin];
        [self addOption:MEFRCMOptionBottomRefreshControlEnd];
        [self.bottomRefreshControl didShowRefreshControl];
        if (self.delegate && [self.delegate respondsToSelector:@selector(didShowBottomRefreshControl)]) {
            [self.delegate didShowBottomRefreshControl];
        }
    }
}

- (CGFloat) stretchValueBottomRefreshControl
{
    if (self.bottomRefreshControl.stretchValue == 0) {
        return [self bottomRefreshControl].height * 2.0f;
    }
    return [[self bottomRefreshControl]stretchValue];
}

- (CGFloat) heightValueBottomRefreshControl
{
    if (self.bottomRefreshControl.height == 0) {
        return 44;
    }
    return [self bottomRefreshControl].height;
}
- (CGFloat) heightTopRefreshControl
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(heightTopRefreshControl)]) {
        return [self.delegate heightTopRefreshControl];
    }
    
    return 0;
}
- (CGSize) calculateContentSize
{
    return CGSizeMake(self.bottomRefreshControl.frame.size.width, self.bottomRefreshControl.height);
}

- (void) endBottomRefreshControl:(UIScrollView *)scrollView;
{
    if (![self existOption:MEFRCMOptionBottomRefreshControlRelease]) {
        return;
    }
    [self removeOption:MEFRCMOptionBottomRefreshControlRelease];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(willHideBottomRefreshControl:)]) {
        [self.delegate willHideBottomRefreshControl:true];
    }
    [self endBottomRefreshControl:^(BOOL completed) {
        [self removeOption:MEFRCMOptionBottomRefreshControlVisible];
        [self.contentView setHidden:true];
        [self.contentView removeFromSuperview];
        scrollView.contentInset = self.saveEdgeInsets;
        if (self.delegate && [self.delegate respondsToSelector:@selector(didHideBottomRefreshControl:)]) {
            [self.delegate didHideBottomRefreshControl:true];
        }
        
    } scrollView:scrollView shift:true];
    
}

- (void) endBottomRefreshControl:(void (^)(BOOL completed))completion scrollView:(UIScrollView*)scrollView shift:(BOOL)shift
{
    CGSize contentViewSize = [self calculateContentSize];
    CGFloat offset = scrollView.contentOffset.x - contentViewSize.height;
    offset = fmax(offset, 0);
    CGFloat contentOffset = scrollView.contentOffset.y;
    //    BOOL isMove = scrollView.contentSize.height > (contentOffset + scrollView.frame.size.height);
    [self addOption:MEFRCMOptionBottomRefreshControlAnimationHide];
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [scrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        [scrollView setContentOffset:CGPointMake(0, contentOffset - contentViewSize.height)];
    } completion:^(BOOL finished) {
        [self removeOption:MEFRCMOptionBottomRefreshControlAnimationHide];
        completion(finished);
    }];
}



- (void) hideBottomRefreshControl:(void (^)(BOOL completed))completion scrollView:(UIScrollView*)scrollView shift:(BOOL)shift
{
    CGSize contentViewSize = [self calculateContentSize];
    CGFloat offset = scrollView.contentOffset.x - contentViewSize.height;
    offset = fmax(offset, 0);
    CGFloat contentOffset = scrollView.contentOffset.y;
    [self addOption:MEFRCMOptionBottomRefreshControlAnimationHide];
    [scrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        [scrollView setContentOffset:CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height + contentViewSize.height)];
    } completion:^(BOOL finished) {
        [self removeOption:MEFRCMOptionBottomRefreshControlAnimationHide];
        completion(finished);
    }];
}






#pragma mark - - scroll delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (!self.enabledBottomRefreshControl) {
        return;
    }
    if (!self.bottomRefreshControl) {
        return;
    }
    //    NSLog(@"\n\n");
    if ([self existOption:MEFRCMOptionBottomRefreshControlVisible | MEFRCMOptionBottomRefreshControlBegin |
         MEFRCMOptionBottomRefreshControlEnd | MEFRCMOptionBottomRefreshControlRelease | MEFRCMOptionBottomRefreshControlLock | MEFRCMOptionBeginUserScroll]) {
        return;
    }
    [self addOption:MEFRCMOptionBeginUserScroll];
    
}



- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    //    NSLog(@"scrollViewDidEndDragging %d",decelerate);
    if (![self existOption:MEFRCMOptionBeginUserScroll]) {
        return;
    }
    if (decelerate) {
        if ([self existOption:MEFRCMOptionBottomRefreshControlEnd]) {
            //            CGFloat maxOffset = self.bottomRefreshControl.height;
            //            [scrollView setContentInset:UIEdgeInsetsMake(0, 0.0, maxOffset, 0.0)];
        }
        return;
    }
    [self scrollViewDidEnd:scrollView];
}

- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    
}





- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (![self existOption:MEFRCMOptionBeginUserScroll]) {
        return;
    }
    [self scrollViewDidEnd:scrollView];
}





- (void) scrollViewDidEnd:(UIScrollView *)scrollView
{
    //    NSLog(@"scrollViewDidEnd");
    //    if ([self existOption:MEFRCMOptionBottomRefreshControlLock]) {
    //        return;
    //    }
    [self removeOption:MEFRCMOptionBeginUserScroll];
    if ([self existOption:MEFRCMOptionBottomRefreshControlEnd]) {
        [self removeOption:MEFRCMOptionBottomRefreshControlEnd];
        [self addOption:MEFRCMOptionBottomRefreshControlRelease];
        [self.bottomRefreshControl releaseRefreshControl];
        if (self.delegate && [self.delegate respondsToSelector:@selector(releaseBottomRefreshControl)]) {
            [self.delegate releaseBottomRefreshControl];
        }
    } else if ([self existOption:MEFRCMOptionBottomRefreshControlBegin]) {
        [self removeOption:MEFRCMOptionBottomRefreshControlBegin];
        if (self.delegate && [self.delegate respondsToSelector:@selector(willHideBottomRefreshControl:)]) {
            [self.delegate willHideBottomRefreshControl:false];
        }
        //        [self addOption:MEFRCMOptionBottomRefreshControlLock];
        [self hideBottomRefreshControl:^(BOOL completed) {
            [self removeOption:MEFRCMOptionBottomRefreshControlVisible];
            //            [self removeOption:MEFRCMOptionBottomRefreshControlLock];
            if (self.delegate && [self.delegate respondsToSelector:@selector(didHideBottomRefreshControl:)]) {
                [self.delegate didHideBottomRefreshControl:false];
            }
            [self.contentView setHidden:true];
            [self.contentView removeFromSuperview];
            scrollView.contentInset = self.saveEdgeInsets;
        } scrollView:scrollView shift:false];
    }
}

- (void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
}

- (UIView*) contentView
{
    if (!_contentView) {
        _contentView = [[UIView alloc]initWithFrame:CGRectZero];
        [_contentView addSubview:self.bottomRefreshControl];
    }
    return _contentView;
}

@end


//- (void) showBottomRefreshControl:(void (^)(BOOL completed))completion scrollView:(UIScrollView*)scrollView
//{
//    [self.bottomRefreshControl setHidden:false];
//    CGFloat maxOffset = self.bottomRefreshControl.height;
//    CGFloat endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
//    //    [scrollView setContentInset:UIEdgeInsetsMake(0, 0.0, maxOffset, 0.0)];
//    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionLayoutSubviews animations:^{
//        [self.contentView setPosition:CGPointMake(0, scrollView.contentSize.height + [self calculateContentSize].height)];
//    } completion:^(BOOL finished) {
//        completion(finished);
//
//    }];
//}
