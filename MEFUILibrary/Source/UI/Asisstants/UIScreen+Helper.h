//
//  UIScreen+Helper.h
//  ipad_project
//
//  Created by Прокофьев Руслан on 04.10.13.
//  Copyright (c) 2013 Прокофьев Руслан. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScreen (Helper)
- (CGRect) currentScreenBoundsDependOnOrientation;
- (CGRect) currentScreenBoundsWithInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
@end
