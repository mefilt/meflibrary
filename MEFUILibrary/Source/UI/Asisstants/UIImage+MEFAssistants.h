//
//  UIImage+MEFAssistants.h
//  MEFLibrary
//
//  Created by Mefilt on 28.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (MEFAssistants)
+ (UIImage *)circleStyleImage:(UIImage *)image rect:(CGRect)rect distance:(CGFloat)distance color:(UIColor*)color lineWidth:(CGFloat)lineWidth;
+ (UIImage *) blurImage:(UIImage *)image;
- (UIImage *)crop:(CGRect)rect;
+ (UIImage *)circularImageByImage:(UIImage *)image rect:(CGRect)rect;
+ (void) asyncCircularImageByImage:(UIImage *)image rect:(CGRect)rect completed:(void (^)(UIImage*image))completed;
+ (UIImage*) createSelectCircleWithColor:(UIColor*)color rect:(CGRect)rect empty:(BOOL)empty;
@end
