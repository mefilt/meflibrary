//
//  MEFEngineCollection.h
//  MEFLibrary
//
//  Created by mefilt on 09.06.16.
//  Copyright © 2016 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MEFObject.h"
@interface MEFEngineCollection : NSObject


@property (strong, nonatomic, readonly) NSMutableArray *sections;


- (NSIndexPath *)previousIndexPath:(NSIndexPath *)oldIndexPath;
- (NSIndexPath *)nextIndexPath:(NSIndexPath *)oldIndexPath;

- (NSIndexPath*) indexPathAtObject:(MEFObject*)object;
- (MEFObject *)objectFromSectionsAtKey:(NSString *)key;
- (MEFObject*) sectionAtKey:(NSString*)key;
- (MEFObject*) sectionAtIndex:(NSInteger) index;
- (MEFObject*) objectAtIndexPath:(NSIndexPath*)indexPath;
- (MEFObject *)objectAtIndex:(int)index row:(int)row;
- (NSIndexPath*) indexPathAtSection:(MEFObject*)section;
- (NSString*) identifierCellWithIndexPath:(NSIndexPath*)indexPath;

#pragma mark - - dequeue reusable section
- (MEFObject*) dequeueReusableSectionWithKey:(NSString*)key;

- (void) reloadData;
- (void) cleanup;
@end
