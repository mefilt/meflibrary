//
//  UIView+Assistants.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 26.04.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MEFAssistants)

- (void) setSize:(CGSize)size;
- (void) setPosition:(CGPoint)position;
- (void) setPositionX:(CGFloat)positionX;
- (void) setPositionY:(CGFloat)positionY;
- (CGSize) systemLayoutSizeWithWidth:(CGFloat)width;
- (CGSize) systemLayoutSizeWithWidth:(CGFloat)width fittingSize:(CGSize)fittingSize;
@end
