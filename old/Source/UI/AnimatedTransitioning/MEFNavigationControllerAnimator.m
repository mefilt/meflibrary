//
//  BSNavigationSlitViewAnimatro.m
//  Katusha
//
//  Created by Prokofev Ruslan on 19.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFNavigationControllerAnimator.h"

@implementation MEFNavigationControllerAnimator
- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 0.25;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    [[transitionContext containerView] addSubview:fromViewController.view];
    [[transitionContext containerView] addSubview:toViewController.view];
//    toViewController.view.alpha = 0;
//    [transitionContext.containerView insertSubview:toViewController.view belowSubview:fromViewController.view];
    
    CGRect frame = toViewController.view.frame;
    
    
    frame.origin.y = -fromViewController.view.frame.size.height;
    toViewController.view.frame = [[transitionContext containerView] convertRect:frame fromView:fromViewController.view];
    
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
//        fromViewController.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
//        toViewController.view.alpha = 1;
        CGRect frame = toViewController.view.frame;
        frame.origin.y = 0;
        
        toViewController.view.frame = [[transitionContext containerView] convertRect:frame fromView:fromViewController.view];

        
    } completion:^(BOOL finished) {
        [fromViewController.view removeFromSuperview];
//        fromViewController.view.transform = CGAffineTransformIdentity;
//        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
         [transitionContext completeTransition:YES];
        
    }];
    
}
@end
