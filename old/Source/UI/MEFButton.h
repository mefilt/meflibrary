//
//  MEFButton.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 09.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEFButton : UIButton

@property (nonatomic, readwrite, copy) void (^didTouch)(MEFButton *button);
- (void)setBackgroundColor:(UIColor *)color forState:(UIControlState)state;
@end
