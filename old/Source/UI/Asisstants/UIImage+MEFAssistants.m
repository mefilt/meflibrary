//
//  UIImage+MEFAssistants.m
//  MEFLibrary
//
//  Created by Mefilt on 28.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "UIImage+MEFAssistants.h"

@implementation UIImage (MEFAssistants)

+ (void) asyncCircularImageByImage:(UIImage *)image rect:(CGRect)rect completed:(void (^)(UIImage*image))completed;
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        UIImage *imageCrop = [UIImage circularImageByImage:image rect:rect];
        if (completed) {
            completed(imageCrop);
        }
    });
}
+ (UIImage *)circularImageByImage:(UIImage *)image rect:(CGRect)rect
{
    @autoreleasepool {
        CGRect drawRect = rect;
        drawRect.origin = CGPointMake(0, 0);
        
        UIImage *newImage = image;
        // Begin a new image that will be the new image with the rounded corners
        // (here with the size of an UIImageView)
//        [UIImage UIGraphicsBeginImageContextWithOptions:rect.size];
        UIGraphicsBeginImageContextWithOptions(rect.size, false, [UIScreen mainScreen].scale);
        //        UIGraphicsBeginImageContextWithOptions(rect.size, NO, 2.0);
        // Add a clip before drawing anything, in the shape of an rounded rect
        [[UIBezierPath bezierPathWithRoundedRect:drawRect
                                    cornerRadius:rect.size.width / 2] addClip];
        CGContextRef ctx = UIGraphicsGetCurrentContext();

        // Draw your image
        
        [newImage drawInRect:drawRect];
        
        
       UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:drawRect
                                    cornerRadius:rect.size.width / 2];
//
        CGContextAddPath(ctx,path.CGPath);
        CGContextClip(ctx);
//        [[UIColor whiteColor] setStroke];
//        CGContextStrokePath(ctx);
        // Get the image
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        // Lets forget about that we were drawing
        UIGraphicsEndImageContext();
        
        return newImage;
    }
}

+ (UIImage *) circleByRect:(CGRect) rect  color:(UIColor *)color lineWidth:(CGFloat)lineWidth
{
    [UIImage UIGraphicsBeginImageContextWithOptions:rect.size];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, lineWidth);
    // Set stroke color
    CGContextSetStrokeColorWithColor(context, [color CGColor]);
    // Draw ellipse by (rect - 1)
    CGContextStrokeEllipseInRect(context, CGRectMake(rect.origin.x + lineWidth, rect.origin.y + lineWidth, rect.size.width - lineWidth * 2, rect.size.height - lineWidth * 2));
    // Get the image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage *)unionTwoImages:(CGRect) rect distance:(float)distance firstImage:(UIImage *)firstImage secondImage:(UIImage *)secondImage
{
    @autoreleasepool {
        [UIImage UIGraphicsBeginImageContextWithOptions:rect.size];
        CGPoint firstPoint = CGPointMake(0, 0);
        [firstImage drawAtPoint:firstPoint];
        CGPoint secondPoint = CGPointMake(distance, distance);
        [secondImage drawAtPoint:secondPoint];
        UIImage *imageC = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return imageC;
    }
}

+ (void) UIGraphicsBeginImageContextWithOptions:(CGSize)size
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        UIGraphicsBeginImageContextWithOptions(size, NO, 2.0);
    } else {
        if ([UIScreen instancesRespondToSelector:@selector(scale)]) {
            UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
        } else {
            UIGraphicsBeginImageContext(size);
        }
    }
}

+ (UIImage*) scaledImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    CGFloat w,h,w1,h1,scaleX,scaleY,scale,newW,newH,x,y;
    w = image.size.width;
    h = image.size.height;
    w1 = newSize.width;
    h1 = newSize.height;
    scaleX =  w1 / w;
    scaleY = h1 / h;
    scale  = fmax(scaleX, scaleY);
    newH = h * scale;
    newW = w * scale;
    x = fabs(newW - w1) * 0.5;
    y = fabs(newH - h1) * 0.5;
    [UIImage UIGraphicsBeginImageContextWithOptions:CGSizeMake(newW, newH)];
    [[UIColor whiteColor]setFill];
    UIRectFill(CGRectMake(0, 0, newW, newH));
    [image drawInRect:CGRectMake(0, 0, newW, newH)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [UIImage UIGraphicsBeginImageContextWithOptions:newSize];
    [newImage drawAtPoint:(CGPoint){-x, -y}];
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return croppedImage;
}

- (UIImage *)crop:(CGRect)rect {
    
    rect = CGRectMake(rect.origin.x*1,
                      rect.origin.y*1,
                      rect.size.width*1,
                      rect.size.height*1);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef
                                          scale:1
                                    orientation:self.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}

+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}



+ (UIImage *)circleStyleImage:(UIImage *)image rect:(CGRect)rect distance:(CGFloat)distance color:(UIColor*)color lineWidth:(CGFloat)lineWidth
{
    CGFloat w,h,x,y;
    w = rect.size.width - 2 * distance;
    h = rect.size.height - 2 * distance;
    x = (rect.size.width - w) * 0.5;
    y = (rect.size.height - h) * 0.5;
    UIImage *secondImage = [UIImage circularImageByImage:image rect:CGRectMake(x, y, w, h)];
    return [UIImage unionTwoImages:rect distance:distance firstImage:[UIImage circleByRect:rect color:color lineWidth:lineWidth] secondImage:secondImage];
}

+ (UIImage *) blurImage:(UIImage *)image
{
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [gaussianBlurFilter setDefaults];
    CIImage *inputImage = [CIImage imageWithCGImage:[image CGImage]];
    [gaussianBlurFilter setValue:inputImage forKey:kCIInputImageKey];
    [gaussianBlurFilter setValue:@100 forKey:kCIInputRadiusKey];
    
    CIImage *outputImage = [gaussianBlurFilter outputImage];
    CIContext *context   = [CIContext contextWithOptions:nil];
    CGImageRef cgimg     = [context createCGImage:outputImage fromRect:[inputImage extent]];  // note, use input image extent if you want it the same size, the output image extent is larger
    UIImage *imageblur       = [UIImage imageWithCGImage:cgimg];
    CGImageRelease(cgimg);
    return imageblur;
}

+ (UIImage*) createSelectCircleWithColor:(UIColor*)color rect:(CGRect)rect empty:(BOOL)empty
{
    [UIImage UIGraphicsBeginImageContextWithOptions:rect.size];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1);
    
    // Set stroke color
    CGContextSetStrokeColorWithColor(context, [color CGColor]);
    CGContextAddArc(context, rect.size.width * 0.5, rect.size.height * 0.5, rect.size.height * 0.5 - 1, 0, 360, false);
    CGContextStrokePath(context);
    
    if (!empty) {
        CGContextSetFillColorWithColor(context, [color CGColor]);
        CGContextAddArc(context, rect.size.width * 0.5, rect.size.height * 0.5, rect.size.height * 0.4 - 1, 0, 360, false);
        CGContextFillPath(context);
    }
    
//    CGContextStrokeEllipseInRect(context, CGRectMake(rect.origin.x + lineWidth, rect.origin.y + lineWidth, rect.size.width - lineWidth * 2, rect.size.height - lineWidth * 2));
    // Get the image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
@end
