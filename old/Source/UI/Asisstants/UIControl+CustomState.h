//
//  UIControl+CustomState.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 10.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIControl (CustomState)

+ (NSString*) stringState:(UIControlState)state;
@end
