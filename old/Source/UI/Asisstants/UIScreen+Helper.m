//
//  UIScreen+Helper.m
//  ipad_project
//
//  Created by Прокофьев Руслан on 04.10.13.
//  Copyright (c) 2013 Прокофьев Руслан. All rights reserved.
//

#import "UIScreen+Helper.h"
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define IOS_VERSION_LOWER_THAN_8 (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1)

@implementation UIScreen (Helper)

- (CGRect) currentScreenBoundsWithInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    CGRect screenBounds = [UIScreen mainScreen].bounds ;
 
    CGSize size = [UIScreen mainScreen].bounds.size;
        if (UIInterfaceOrientationIsLandscape(interfaceOrientation))
        {
            size = CGSizeMake(size.height, size.width);
        }
  

    screenBounds.size = size;
    return screenBounds;
}

- (CGRect) currentScreenBoundsDependOnOrientation
{
    CGRect screenBounds = [UIScreen mainScreen].bounds ;
    CGSize size = [UIScreen sizeInOrientation:[UIApplication sharedApplication].statusBarOrientation];
    screenBounds.size = size;
    return screenBounds;
}

+ (CGSize)sizeInOrientation:(UIInterfaceOrientation)orientation
{
    CGSize size = [UIScreen mainScreen].bounds.size;
    if (IOS_VERSION_LOWER_THAN_8) {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            size = CGSizeMake(size.height, size.width);
        }
    }
    return size;
}

@end
