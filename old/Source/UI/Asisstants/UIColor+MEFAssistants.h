//
//  UIColor+MEFAssistants.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 30.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MEFAssistants)
- (UIImage *) imageFromColorWithSize:(CGSize)size cornerRadius:(CGFloat) cornerRadius;
- (UIColor*) setAlpha:(CGFloat)alpha;
+ (UIColor *)colorFromHexCode:(NSString *)hexString;

+ (UIColor *) randomColor;
- (UIColor *) appendColorWithHue:(CGFloat)hue saturation:(CGFloat)saturation brightness:(CGFloat)brightness alpha:(CGFloat)alpha;
@end
