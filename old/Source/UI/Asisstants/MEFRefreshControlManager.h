//
//  MEFRefreshControlManager.h
//  MEFLibrary
//
//  Created by Mefilt on 23.06.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@protocol MEFRefreshControlPropertyProtocol <NSObject>

@property (nonatomic, readwrite, assign) CGFloat percent;

@required
/**
    Height
    Deffault value, equal height refresh control * 2
 **/

@property (nonatomic, readwrite, assign) CGFloat height;

- (void) willShowRefreshControl;
- (void) didShowRefreshControl;
- (void) releaseRefreshControl;


@optional
/**
 How much you need to pull up scrollview, to show refresh control
 Deffault value, equal height refresh control * 2
 **/
@property (nonatomic, readwrite, assign) CGFloat stretchValue;

@end

@protocol MEFRefreshControlManagerDelegate <NSObject>
@optional
- (void) willShowBottomRefreshControl;
- (void) didShowBottomRefreshControl;
- (void) releaseBottomRefreshControl;
- (void) willHideBottomRefreshControl:(BOOL)release;
- (void) didHideBottomRefreshControl:(BOOL)release;
@end


typedef NS_OPTIONS(NSUInteger, MEFRCMOption) {
    MEFRCMOptionNone = 1 << 0,
    MEFRCMOptionBeginUserScroll = 1 << 1,
    MEFRCMOptionBottomRefreshControlVisible = 1 << 2,
    MEFRCMOptionBottomRefreshControlLock = 1 << 3,
    MEFRCMOptionBottomRefreshControlBegin = 1 << 4,
    MEFRCMOptionBottomRefreshControlEnd = 1 << 5,
    MEFRCMOptionBottomRefreshControlRelease = 1 << 6,
    MEFRCMOptionBottomRefreshControlAnimationHide = 1 << 7,
    MEFRCMOptionBottomRefreshControlAnimationEnd = 1 << 8,
};


@interface MEFRefreshControlManager : NSObject <UIScrollViewDelegate>


@property (nonatomic, readwrite, assign) MEFRCMOption options;
@property (nonatomic, readwrite, assign, getter=isEnabledBottomRefreshControl) BOOL enabledBottomRefreshControl;
@property (nonatomic, readwrite, strong) UIView <MEFRefreshControlPropertyProtocol> *bottomRefreshControl;
@property (nonatomic, readwrite, weak) id <MEFRefreshControlManagerDelegate> delegate;


- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView;
- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void) scrollViewDidScroll:(UIScrollView *)scrollView;
- (void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;
- (void) endBottomRefreshControl:(UIScrollView *)scrollView;;

@end
