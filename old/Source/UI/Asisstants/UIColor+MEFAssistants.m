//
//  UIColor+MEFAssistants.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 30.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "UIColor+MEFAssistants.h"

@implementation UIColor (MEFAssistants)

- (UIColor*) setAlpha:(CGFloat)alpha
{
    CGFloat r,g,b,a;    
    [self getRed:&r green:&g blue:&b alpha:&a];
    return [UIColor colorWithRed:r green:g blue:b alpha:alpha];
}

- (UIColor *) appendColorWithHue:(CGFloat)hue saturation:(CGFloat)saturation brightness:(CGFloat)brightness alpha:(CGFloat)alpha
{
    CGFloat oldHue,oldSaturation,oldBrightness,oldAlpha;
    oldHue = oldSaturation = oldBrightness = oldAlpha = 0;
    [self getHue:&oldHue saturation:&oldSaturation brightness:&oldBrightness alpha:&oldAlpha];
    CGFloat newHue, newSaturation, newBrightness, newAlpha;
    newHue = oldHue + hue;
    newSaturation = oldSaturation + saturation;
    newBrightness = oldBrightness + brightness;
    newAlpha = oldAlpha + alpha;
    
    UIColor *color = [UIColor colorWithHue:newHue saturation:newSaturation brightness:newBrightness alpha:newAlpha];

    return color;
}

- (UIImage *) imageFromColorWithSize:(CGSize)size cornerRadius:(CGFloat) cornerRadius
{
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    
    CGContextSetFillColorWithColor(context, [self CGColor]);
    [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:cornerRadius]fill];
    //    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


+ (UIColor *)colorFromHexCode:(NSString *)hexString
{
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}


+ (UIColor *) randomColor
{
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    return color;
}


@end
