//
//  UIView+Assistants.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 26.04.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "UIView+MEFAssistants.h"

@implementation UIView (MEFAssistants)
- (void) setPosition:(CGPoint)position
{
    CGRect frame = self.frame;
    frame.origin = position;
    self.frame = frame;
}
- (void) setPositionX:(CGFloat)positionX
{
    [self setPosition:CGPointMake(positionX, self.frame.origin.y)];
}
- (void) setPositionY:(CGFloat)positionY
{
    [self setPosition:CGPointMake(self.frame.origin.x, positionY)];
}
- (void) setSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGSize) systemLayoutSizeWithWidth:(CGFloat)width
{
    return [self systemLayoutSizeWithWidth:width fittingSize:UILayoutFittingCompressedSize];
}

- (CGSize) systemLayoutSizeWithWidth:(CGFloat)width fittingSize:(CGSize)fittingSize
{
    BOOL translatesAutoresizingMaskIntoConstraints = self.translatesAutoresizingMaskIntoConstraints;
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
  
    NSLayoutConstraint *headerWidthConstraint = [NSLayoutConstraint
                                                 constraintWithItem:self attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual
                                                 toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:width
                                                 ];
    [self addConstraint:headerWidthConstraint];
    
//    [self setNeedsLayout];
//    [se layoutIfNeeded];
    
    CGFloat height = [self systemLayoutSizeFittingSize:fittingSize].height;
    [self removeConstraint:headerWidthConstraint];
//    [self setSize:CGSizeMake(width, height)];
    self.translatesAutoresizingMaskIntoConstraints = translatesAutoresizingMaskIntoConstraints;
    return CGSizeMake(width, height);
}



@end
