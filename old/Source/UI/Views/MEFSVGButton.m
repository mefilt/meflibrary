//
//  MEFSVGButton.m
//  MEFLibrary
//
//  Created by Mefilt on 04.06.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFSVGButton.h"
#import <Masonry/Masonry.h>
#import "MEFConstants.h"
#import "UIControl+CustomState.h"
@interface MEFSVGButtonState : NSObject

@property (nonatomic, readwrite, assign) UIControlState state;
@property (nonatomic, readwrite, strong) UIColor *strokeColor;
@property (nonatomic, readwrite, strong) UIColor *fillColor;
@property (nonatomic, readwrite, assign) CGSize size;
@property (nonatomic, readwrite, strong) NSString *SVGImageIdentifier;
@property (nonatomic, readwrite, strong) NSString *SVGFilename;
@property (nonatomic, readwrite, copy) void (^modifiLayer) (CALayer *renderLayer);

@end
@implementation MEFSVGButtonState

@end

@interface MEFSVGButton ()
@property (nonatomic, readwrite, strong) MEFSVGImageView *iconView;
@property (nonatomic, readwrite, strong) NSMutableDictionary *cash;
@end
@implementation MEFSVGButton


- (instancetype) init
{
    if (self = [super init]) {
        [self initialize];
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self initialize];
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initialize];
    }
    return self;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    
}
- (void)initialize
{
    [self addTarget:self action:@selector(action:) forControlEvents:UIControlEventTouchUpInside];
}

- (void) setImageIdentifier:(NSString *)imageIdentifier
{
    _imageIdentifier = imageIdentifier;
    [self setSVGImageIdentifier:imageIdentifier forState:UIControlStateNormal];
}

- (void) action:(id)sender
{
    if (self.didTouch) {
        self.didTouch();
    }
}
- (void)setSVGFilename:(NSString*) filename forState:(UIControlState)state modifyLayer:(void (^) (CALayer *renderLayer))modifiLayer
{
    MEFSVGButtonState *buttonState = [self buttonState:state];
    buttonState.SVGFilename = filename;
    buttonState.modifiLayer = modifiLayer;
    [self stateWasUpdated];
    
}
- (void)setSVGFilename:(NSString*)filename forState:(UIControlState)state
{
    MEFSVGButtonState *buttonState = [self buttonState:state];
    buttonState.SVGFilename = filename;
    [self stateWasUpdated];
}

- (void) setSVGImageIdentifier:(NSString *)imageIdentifier modifyLayer:(void (^)(CALayer *))modifiLayer forState:(UIControlState)state
{
    MEFSVGButtonState *buttonState = [self buttonState:state];
    buttonState.SVGImageIdentifier = imageIdentifier;
    buttonState.modifiLayer = modifiLayer;
    [self stateWasUpdated];
    
}
- (void) setSVGImageIdentifier:(NSString *)imageIdentifier forState:(UIControlState)state
{
    [self setSVGImageIdentifier:imageIdentifier modifyLayer:nil forState:state];
}


- (void)setSVGStrokeColor:(UIColor *)color forState:(UIControlState)state
{
    MEFSVGButtonState *buttonState = [self buttonState:state];
    buttonState.strokeColor = color;
    [self stateWasUpdated];
}
- (void)setSVGFillColor:(UIColor *)color forState:(UIControlState)state
{
    MEFSVGButtonState *buttonState = [self buttonState:state];
    buttonState.fillColor = color;
    [self stateWasUpdated];
}



- (void)setSVGSize:(CGSize)size forState:(UIControlState)state
{
    MEFSVGButtonState *buttonState = [self buttonState:state];
    buttonState.size = size;
    [self stateWasUpdated];
}
- (void) setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    [self stateWasUpdated];
}

- (void) setSelected:(BOOL)selected
{
    [super setSelected:selected];
    [self stateWasUpdated];
}
- (void) setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    [self stateWasUpdated];
}

- (void) stateWasUpdated
{
    MEFSVGButtonState *buttonState = [self buttonState:self.state];
    
    if (!buttonState) {
        return;
    }
    
    
    if (buttonState.SVGImageIdentifier) {
        [self.iconView setImageIdentifier:buttonState.SVGImageIdentifier modifyLayer:buttonState.modifiLayer];
    }
    if (buttonState.SVGFilename) {
        [self.iconView setImageSVGFile:buttonState.SVGFilename modifyLayer:buttonState.modifiLayer];
    }
    if (buttonState.strokeColor) {
        [self.iconView setSVGStrokeColor:buttonState.strokeColor];
    }
    if (buttonState.fillColor) {
        [self.iconView setSVGFillColor:buttonState.fillColor];
    }


    
    MEFBlockWeakSelf weakSelf = self;
    [self.iconView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(weakSelf);
        
        if (CGSizeEqualToSize(buttonState.size, CGSizeZero)) {
            make.left.equalTo(weakSelf.mas_left);
            make.right.equalTo(weakSelf.mas_right);
            make.top.equalTo(weakSelf.mas_top);
            make.bottom.equalTo(weakSelf.mas_bottom);
        } else {
            make.width.equalTo(@(buttonState.size.width));
            make.height.equalTo(@(buttonState.size.height));
        }

    }];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (MEFSVGButtonState*) buttonState:(UIControlState)state
{
    MEFSVGButtonState  *buttonState = self.cash[[UIControl stringState:state]] ;
    if (!buttonState) {
        buttonState = self.cash[[UIControl stringState:state]] = [MEFSVGButtonState new];
    }
    
    return buttonState;
    
}

- (NSMutableDictionary*) cash
{
    if (!_cash) {
        _cash = [NSMutableDictionary dictionary];
    }
    return _cash;
}

- (MEFSVGImageView*) iconView
{
    if (!_iconView) {
        _iconView = [[MEFSVGImageView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
        [_iconView setTranslatesAutoresizingMaskIntoConstraints:false];
        [_iconView setUserInteractionEnabled:false];
        [self addSubview:_iconView];
        MEFBlockWeakSelf weakSelf = self;
        [_iconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(weakSelf);
            make.left.equalTo(weakSelf.mas_left);
            make.right.equalTo(weakSelf.mas_right);
            make.top.equalTo(weakSelf.mas_top);
            make.bottom.equalTo(weakSelf.mas_bottom);
        }];

    }
    return _iconView;
}
@end
