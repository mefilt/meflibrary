//
//  MEFShadowView.h
//  MEFLibrary
//
//  Created by Mefilt on 15.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MEFShadowViewDirection) {
    MEFShadowViewDirectionA,
    MEFShadowViewDirectionB,
    MEFShadowViewDirectionC,
};



@interface MEFShadowView : UIView
{
}
@property (nonatomic, readwrite, strong) IBInspectable UIColor *shadowColor;
@property (nonatomic, readwrite, assign) IBInspectable CGFloat shadowBeginAlpha;
@property (nonatomic, readwrite, assign) IBInspectable CGPoint shadowDirection;

@end
