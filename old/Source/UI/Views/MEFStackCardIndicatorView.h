//
//  MEFStackCardIndicatorView.h
//  Katusha
//
//  Created by Prokofev Ruslan on 20.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEFStackCardIndicatorView : UIView

@property (nonatomic, readwrite, strong) UIImage *image;
@property (nonatomic, readwrite, assign) CGFloat percent;
@property (nonatomic, readwrite, strong) UIColor *colorStroke;
- (id) initWithImage:(UIImage*)image;
@end