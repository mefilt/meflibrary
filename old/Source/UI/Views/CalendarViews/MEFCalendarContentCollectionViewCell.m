//
//  MEFCalendarContentCollectionViewCell.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 16.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFCalendarContentCollectionViewCell.h"

@implementation MEFCalendarContentCollectionViewCell

- (id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        
        self.calendarContentView = [[MEFCalendarContentView alloc]initWithFrame:self.bounds];
        [self.calendarContentView setBackgroundColor:[UIColor clearColor]];
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        [self.contentView setClipsToBounds:false];
        [self setClipsToBounds:false];
        [self.calendarContentView setClipsToBounds:false];
        [self.contentView addSubview:self.calendarContentView];
//        [self.calendarContentView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.edges.equalTo(self.contentView);
//        }];
       
    }
    return self;
}
- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    [super applyLayoutAttributes:layoutAttributes];
    
}

- (void)awakeFromNib
{
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    [self.calendarContentView setFrame:self.bounds];

}


- (CGSize) sizeThatFits:(CGSize)size
{
    return [self.calendarContentView sizeThatFits:CGSizeMake(self.contentView.frame.size.width, 0)];
}
//- (CGSize) systemLayoutSizeFittingSize:(CGSize)targetSize
//{
//    return [self.calendarContentView systemLayoutSizeFittingSize:targetSize];
//}
@end
