//
//  MEFCalendarCollectionView.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 22.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MEFCalendarCollectionView;
@protocol  MEFCalendarCollectionViewDelegate <NSObject>
- (void) calendarCollectionViewWillLayoutSubviews:(MEFCalendarCollectionView*)calendarCollectionView;
- (void) calendarCollectionViewDidLayoutSubviews:(MEFCalendarCollectionView*)calendarCollectionView;
@end

@interface MEFCalendarCollectionView : UICollectionView

@property (nonatomic, readwrite, weak) id <MEFCalendarCollectionViewDelegate> calendarDelegate;
@end
