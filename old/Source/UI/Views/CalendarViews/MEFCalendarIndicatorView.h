//
//  MEFCalendarIndicatorView.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 14.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEFObjectPool.h"
@interface MEFCalendarIndicatorView : NSObject <MEFObjectPooling>

+ (instancetype) createInctance;
@property (nonatomic, readwrite, assign, getter=isEmpty) BOOL empty;
@property (nonatomic, readwrite, assign) CGRect frame;
@property (nonatomic, readwrite, strong) UIColor *dateColor;
@property (nonatomic, readwrite, strong) NSString *dateText;
@property (nonatomic, readwrite, strong) NSString *detailText;
@property (nonatomic, readwrite, strong) NSMutableDictionary *attributesDate;
@property (nonatomic, readwrite, strong) NSMutableDictionary *attributesDetail;
@property (nonatomic, readwrite, strong) NSDate *date;
@property (nonatomic, readwrite, assign, getter=isToday) BOOL today;
@property (nonatomic, readwrite, assign, getter=isLastDayOfMonth) BOOL lastDayOfMonth;
@property (nonatomic, readwrite, assign, getter=isDisable) BOOL disable;
@property (nonatomic, readwrite, assign) CGPoint customPosition;
@property (nonatomic, readwrite, assign, getter=isNeedCenteringCustomPosition) BOOL needCenteringCustomPosition;
@property (nonatomic, readwrite, strong) MEFCalendarIndicatorView *nextIndicatorView;
@property (nonatomic, readwrite, weak) MEFCalendarIndicatorView *prevIndicatorView;
- (CGSize) sizeThatFits:(CGSize)size;
- (CGSize) detailSizeThatFits:(CGSize)size;
@end
