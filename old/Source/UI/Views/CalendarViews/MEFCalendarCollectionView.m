//
//  MEFCalendarCollectionView.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 22.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFCalendarCollectionView.h"

@implementation MEFCalendarCollectionView


- (void) layoutSubviews {
    
    if (self.calendarDelegate && [self.calendarDelegate respondsToSelector:@selector(calendarCollectionViewWillLayoutSubviews:)]) {
        [self.calendarDelegate calendarCollectionViewWillLayoutSubviews:self];
    }
    [super layoutSubviews];
    if (self.calendarDelegate && [self.calendarDelegate respondsToSelector:@selector(calendarCollectionViewDidLayoutSubviews:)]) {
        [self.calendarDelegate calendarCollectionViewDidLayoutSubviews:self];
    }
    
    
}
@end
