//
//  MEFStackCardIndicatorView.m
//  Katusha
//
//  Created by Prokofev Ruslan on 20.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFStackCardIndicatorView.h"
#import "MEFConstants.h"

@interface MEFStackCardIndicatorView ()
{
    CGFloat _prevAngle;
}
@end
@implementation MEFStackCardIndicatorView

- (id) initWithImage:(UIImage*)image
{
    if (self = [super initWithFrame:CGRectMake(0, 0, image.size.height, image.size.width)]) {
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}
- (void) setPercent:(CGFloat)percent
{
    _percent = percent;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx= UIGraphicsGetCurrentContext();

    CGRect bounds = [self bounds];
    [self.image drawAtPoint:CGPointMake(0, 0)];
    CGPoint center;
    center.x = bounds.origin.x + bounds.size.width *  0.5;
    center.y = bounds.origin.y + bounds.size.height * 0.5;
    CGContextSaveGState(ctx);
    
    CGContextSetLineWidth(ctx,1.5);
    
    [self.colorStroke setStroke];
    CGFloat a = 360.0f * self.percent;
    CGFloat radian = DEGREES_TO_RADIANS(a);

    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddArc(path, NULL, center.x, center.y, self.image.size.width * 0.5 - 0.5, -radian, 0, false);
    CGContextAddPath(ctx, path);
    CGContextStrokePath(ctx);
    CGPathRelease(path);
    _prevAngle = a;
}


@end
