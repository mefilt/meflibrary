//
//  MEFLabel.m
//  MEFLibrary
//
//  Created by Mefilt on 23.06.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFAutoPreferredLabel.h"

@implementation MEFAutoPreferredLabel

- (void)layoutSubviews
{
    self.preferredMaxLayoutWidth = self.frame.size.width;
    [super layoutSubviews];
}
@end
