//
//  MEFSelectedItem.h
//  ipad_project
//
//  Created by Прокофьев Руслан on 02.10.13.
//  Copyright (c) 2013 Прокофьев Руслан. All rights reserved.
//

#import "MEFObject.h"

typedef NS_ENUM(NSInteger, MEFSelectedItemStatus) {
    kMEFSIStatusAlreadySelected,
    kMEFSIStatusAlreadyNotSelected,
    kMEFSIStatusNotSelected,
    kMEFSIStatusSelected,
};

@interface MEFSelectedItem : MEFObject
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (assign, nonatomic) MEFSelectedItemStatus status;

- (BOOL) isSelected;
@end
