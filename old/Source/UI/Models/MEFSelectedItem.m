//
//  MEFSelectedItem.m
//  ipad_project
//
//  Created by Прокофьев Руслан on 02.10.13.
//  Copyright (c) 2013 Прокофьев Руслан. All rights reserved.
//

#import "MEFSelectedItem.h"

@implementation MEFSelectedItem


- (BOOL) isSelected
{
    return (self.status == kMEFSIStatusSelected || self.status == kMEFSIStatusAlreadySelected);
}
@end
