//
//  MEFContainerSegue.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 04.06.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFContainerSegue.h"
#import "MEFContainerViewController.h"
@implementation MEFContainerSegue

- (void) perform
{
    id sourceViewController = self.sourceViewController;
    
    if (![sourceViewController isKindOfClass:[MEFContainerViewController class]]) {
        @throw [NSException exceptionWithName:@"Source View controller not is king class MEFContainerViewController" reason:nil userInfo:nil];
    }

    id destinationViewController = self.destinationViewController;;
    MEFContainerViewController *container = sourceViewController;
    [container setViewController:destinationViewController animateType:self.animateType];
    
}
@end
