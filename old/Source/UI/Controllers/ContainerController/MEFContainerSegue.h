//
//  MEFContainerSegue.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 04.06.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEFContainerSegue : UIStoryboardSegue
@property (nonatomic, readwrite, assign) NSUInteger animateType;
@end
