//
//  MEFAdaptiveNavigationController.h
//  Katusha
//
//  Created by Prokofev Ruslan on 19.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEFViewController.h"
@interface MEFAdaptiveNavigationController : MEFViewController

- (void) addViewController:(MEFViewController*)viewController;
@end


@interface MEFViewController (MEFAdaptiveNavigationController)
@property (nonatomic, readonly, strong) MEFAdaptiveNavigationController *adaptiveNavigationController;
- (void) moveToParentAdaptiveNavigationController:(MEFAdaptiveNavigationController *)parent;
@end
