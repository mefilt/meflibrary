//
//  MEFStackCardViewController.h
//  Katusha
//
//  Created by Prokofev Ruslan on 19.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFStack.h"
#import "MEFSplitViewViewConrollerProtocol.h"
#import "MEFStackCardIndicatorView.h"
#import "MEFAdaptiveNavigationController.h"
@interface MEFStackCardViewController : MEFAdaptiveNavigationController <MEFSplitViewViewConrollerProtocol>
{

}
@property (nonatomic, readwrite, assign) CGFloat leftPadding;
@property (nonatomic, readonly, strong) UIViewController *topViewController;
- (id) initWithRootViewController:(UIViewController*)rootViewController;


- (void) setRootViewController:(UIViewController*)controller animated:(BOOL)animated;
- (void) pushViewController:(UIViewController*)controller animated:(BOOL)animated;



- (id) setRootViewControllerWithSegueIdentifier:(NSString*)identifier animated:(BOOL)animated;;
- (id) pushViewControllerWithSegueIdentifier:(NSString*)identifier animated:(BOOL)animated;;


- (void) popViewControllerWithAnimated:(BOOL)animated;
- (void) popAllViewControllerWithAnimated:(BOOL)animated;
- (void) popViewController;

@end


@interface MEFStackCardViewController (Override)
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;
@end
