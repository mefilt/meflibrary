
//
//  MEFStackCardViewController.m
//  Katusha
//
//  Created by Prokofev Ruslan on 19.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFStackCardViewController.h"
#import "MEFPanGestureRecognizer.h"
#import "CGPointExtension.h"
#import "MEFStackCardPushSegue.h"
#import "MEFStackCardRootSegue.h"
const static CGFloat BSStackCardViewVisibleAmount = 2;
const static CGFloat BSStackCardViewPanPopPercent = 0.3f;
const static CGFloat BSStackCardViewPanPopAllPercent = 0.75f;

typedef NS_ENUM(NSUInteger, BBStackCardPanPhase) {
    BBStackCardPanPhaseNone,
    BBStackCardPanPhasePopCard,
    BBStackCardPanPhaseRootCard,
};

@interface MEFStackCardViewController () <UIGestureRecognizerDelegate>
{
 

}
@property (nonatomic, readwrite, strong) MEFStack *stack;
@property (nonatomic, readwrite, assign) int amountShowAnimation;
@property (nonatomic, readwrite, assign) BBStackCardPanPhase prevPhasePan;
@property (nonatomic, readwrite, assign) BBStackCardPanPhase phasePan;
@property (nonatomic, readwrite, strong) NSMutableArray *visibleCards;
@property (nonatomic, readwrite, strong) UIViewController *rootViewController;
@property (nonatomic, readwrite, strong) UIViewController *visibleTopController;
@property (nonatomic, readwrite, strong) MEFPanGestureRecognizer *panGesture;
@property (nonatomic, readwrite, assign, getter=isCurrentSegueAnimated) BOOL currentSegueAnimated;

@end

@implementation MEFStackCardViewController
@synthesize shiftViewControllerOffSet = _shiftViewControllerOffSet;
- (id) initWithRootViewController:(UIViewController*)rootViewController;
{
    if (self = [self init]) {
        [self setRootViewController:rootViewController];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.stack = [[MEFStack alloc]init];
        self.visibleCards = [NSMutableArray array];

    }
    return self;
}

- (id) init
{
    if (self = [super init]) {
        self.stack = [[MEFStack alloc]init];
        self.visibleCards = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.panGesture = [[MEFPanGestureRecognizer alloc]initWithTarget:self action:@selector(panGestureRecognizer:)];
    self.panGesture.maximumNumberOfTouches = 1;
    [self.panGesture setDelegate:self];
    [self.view addGestureRecognizer:self.panGesture];
    // Do any additional setup after loading the view.
}

- (id) setRootViewControllerWithSegueIdentifier:(NSString*)identifier animated:(BOOL)animated;
{
    self.currentSegueAnimated = animated;
    @try {
        [self performSegueWithIdentifier:identifier sender:nil];
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    return self.rootViewController;
}

- (id) pushViewControllerWithSegueIdentifier:(NSString*)identifier animated:(BOOL)animated
{
    self.currentSegueAnimated = animated;
    @try {
        [self performSegueWithIdentifier:identifier sender:nil];
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    return self.rootViewController;
}




- (UIViewController*) topViewController
{
    return [self.visibleCards lastObject];
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.visibleCards.count > 1 || self.stack.count) {
        return true;
    }
    return false;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    CGRect mainFrame = self.view.bounds;
    UIViewController *lastController = [self.stack lastObject];
    if (!lastController) {
        return;
    }
    CGRect frame = lastController.view.frame;
    frame.size = CGSizeMake(mainFrame.size.width, mainFrame.size.height);
    lastController.view.frame = frame;
}

- (CGRect) rootViewFrame
{
    CGRect mainFrame = self.view.bounds;
    CGRect frame = CGRectMake(0, 0, 0, 0);
    frame.size = CGSizeMake(mainFrame.size.width, mainFrame.size.height);
    return frame;
}

- (CGRect) hideTopViewFrame
{
    CGRect mainFrame = self.view.bounds;
    CGRect frame = CGRectMake(mainFrame.size.width, 0, 0, 0);
    frame.size = CGSizeMake(mainFrame.size.width, mainFrame.size.height);
    return frame;
}

- (CGRect) hideDownViewFrame
{
    CGRect mainFrame = self.view.bounds;
    CGRect frame = CGRectMake(mainFrame.size.width, 0, 0, 0);
    frame.size = CGSizeMake(mainFrame.size.width, mainFrame.size.height);
    return frame;
}


- (CGRect) showDownViewFrame
{
    CGRect mainFrame = self.view.bounds;
    CGRect frame = CGRectMake(0,0, 0, 0);
    frame.size = CGSizeMake(mainFrame.size.width, mainFrame.size.height);
    return frame;
}

- (CGRect) showTopViewFrame
{
    CGRect mainFrame = self.view.bounds;
    CGRect frame = CGRectMake(self.leftPadding, 0, 0, 0);
    frame.size = CGSizeMake(mainFrame.size.width, mainFrame.size.height);
    return frame;
}

- (void) setRootViewController:(UIViewController*)controller animated:(BOOL)animated
{
    NSArray *oldVisibleCards = [NSArray arrayWithArray:self.visibleCards];
    [self.visibleCards removeAllObjects];
    self.shiftViewControllerOffSet = 0;


    
    [self pushNavigationItemFromController:controller animated:animated];

    if (animated) {
        [self setRootViewController:controller];
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews animations:^{
            controller.view.frame = [self showDownViewFrame];
     
        } completion:^(BOOL finished) {
            for (UIViewController *oldController in oldVisibleCards) {
                [self removeViewController:oldController];
            }
            NSEnumerator *en = [self.stack objectEnumerator];
            UIViewController *nexController = nil;
            while (nexController = [en nextObject]) {
                [self removeViewController:nexController];
            }
            [self.stack removeAll];
        }];
    } else {
        [self setRootViewController:controller];
        for (UIViewController *oldController in oldVisibleCards) {
            [self removeViewController:oldController];
        }
        NSEnumerator *en = [self.stack objectEnumerator];
        UIViewController *nexController = nil;
        while (nexController = [en nextObject]) {
            [self removeViewController:nexController];
        }
        [self.stack removeAll];
    }

}
- (void) setRootViewController:(UIViewController*)controller
{
    if (controller) {
        [self addChildViewController:controller];
        controller.view.frame = [self rootViewFrame];
        [self.view addSubview:controller.view];
        [controller didMoveToParentViewController:self];
        [self.visibleCards addObject:controller];
    }
    _rootViewController = controller;
}


- (void) removeViewController:(UIViewController*) removeController;
{
    [removeController willMoveToParentViewController:nil];
    [removeController.view removeFromSuperview];
    [removeController removeFromParentViewController];
}

- (void) pushNavigationItemFromController:(UIViewController*)controller animated:(BOOL)animated
{
    self.navigationItem.rightBarButtonItems = controller.navigationItem.rightBarButtonItems;
    self.navigationItem.leftBarButtonItems = controller.navigationItem.leftBarButtonItems;
    self.navigationItem.title = controller.navigationItem.title;
}


#pragma mark - - push methods
- (void) pushViewController:(UIViewController*)controller animated:(BOOL)animated
{
    static BOOL pushLock = false;
    if (pushLock) {
        return;
    }
    pushLock = true;
    if (self.visibleCards.count == 0) {
        [self setRootViewController:controller animated:animated];
        return;
    }
    self.shiftViewControllerOffSet = self.leftPadding;
    
    controller.view.layer.masksToBounds = NO;
    controller.view.layer.cornerRadius = 35;
    controller.view.layer.shadowOffset = CGSizeMake(-self.leftPadding * 0.5, 0);
    controller.view.layer.shadowRadius = 5;
    controller.view.layer.shadowOpacity = 0.23;
    controller.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, 10, self.view.frame.size.height)].CGPath;
    
    
    UIViewController *lastController = [self.visibleCards lastObject];
    [self.visibleCards addObject:controller];
    [self addChildViewController:controller];
    controller.view.frame = [self hideTopViewFrame];
    [self.view addSubview:controller.view];
    [controller didMoveToParentViewController:self];
    
    

    NSArray *hideViewControllers = nil;
    if (self.visibleCards.count > BSStackCardViewVisibleAmount) {
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.visibleCards.count - BSStackCardViewVisibleAmount)];
        hideViewControllers = [self.visibleCards objectsAtIndexes:indexSet];
        [self.visibleCards removeObjectsAtIndexes:indexSet];
    }
    if (hideViewControllers && hideViewControllers.count) {
        [self.stack pushArray:hideViewControllers];
    }
    [self pushNavigationItemFromController:controller animated:animated];
    if (animated) {
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews animations:^{
            controller.view.frame = [self showTopViewFrame];
            lastController.view.frame = [self showDownViewFrame];
        } completion:^(BOOL finished) {
            for (UIViewController *vc in hideViewControllers) {
                [vc.view setHidden:true];
            }
            pushLock = false;
        }];
    } else {
        controller.view.frame = [self showTopViewFrame];
        lastController.view.frame = [self showDownViewFrame];
        pushLock = false;
    }
}



#pragma mark - - pop methods
- (void) popAllViewControllerWithAnimated:(BOOL)animated
{
    if (self.visibleCards.count <= 1) {
        return;
    }
 
    self.shiftViewControllerOffSet = 0;
    NSEnumerator *en = [self.stack objectEnumerator];
    UIViewController *rootViewController = [en nextObject];

    
    
    
    BOOL isFromStack = false;
    if ((isFromStack = (rootViewController != nil))) {
        [rootViewController.view setHidden:false];
        UIViewController *nexController = nil;
        while (nexController = [en nextObject]) {
            [self removeViewController:nexController];
        }
        [self.stack removeAll];
    } else {
        rootViewController = [self.visibleCards firstObject];
      
    }
    if (animated) {
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews animations:^{
            if (isFromStack) {
                for (UIViewController *v in self.visibleCards) {
                    v.view.frame = [self hideTopViewFrame];
                }
            } else {
                for (int i = 1; i < self.visibleCards.count;++i) {
                    UIViewController *v =self.visibleCards[i];
                    v.view.frame = [self hideTopViewFrame];
                }
            }
        } completion:^(BOOL finished) {
            if (isFromStack) {
                for (UIViewController *v in self.visibleCards) {
                    [self removeViewController:v];
                }
                [self.visibleCards removeAllObjects];
                [self.visibleCards addObject:rootViewController];
            } else {
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, self.visibleCards.count - 1)];
                NSArray *array = [self.visibleCards objectsAtIndexes:indexSet];
                [self.visibleCards removeObjectsAtIndexes:indexSet];
                for (UIViewController *v in array) {
                    [self removeViewController:v];
                }
            }
        }];
    } else {
        if (isFromStack) {
            for (UIViewController *v in self.visibleCards) {
                v.view.frame = [self hideTopViewFrame];
            }
            for (UIViewController *v in self.visibleCards) {
                [self removeViewController:v];
            }
            [self.visibleCards removeAllObjects];
            [self.visibleCards addObject:rootViewController];
        } else {
            for (int i = 1; i < self.visibleCards.count;++i) {
                UIViewController *v =self.visibleCards[i];
                v.view.frame = [self hideTopViewFrame];
            }
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, self.visibleCards.count - 1)];
            NSArray *array = [self.visibleCards objectsAtIndexes:indexSet];
            [self.visibleCards removeObjectsAtIndexes:indexSet];
            for (UIViewController *v in array) {
                [self removeViewController:v];
            }
        }
    }

    
}

- (void) popViewController
{
    [self popViewControllerWithAnimated:true];
}

- (void) popViewControllerWithAnimated:(BOOL)animated
{
    if (self.visibleCards.count <= 1) {
        return;
    }
    self.shiftViewControllerOffSet = 0;
    UIViewController *fromStackViewController = [self.stack pop];
    UIViewController *oldViewController = [self.visibleCards lastObject];
    [self.visibleCards removeLastObject];
    UIViewController *visibleViewController = [self.visibleCards lastObject];
    
    BOOL isFromStack = false;
    if ((isFromStack = (fromStackViewController != nil))) {
        [fromStackViewController.view setHidden:false];
        [self.visibleCards insertObject:fromStackViewController atIndex:self.visibleCards.count - 1];
    }


    [self pushNavigationItemFromController:visibleViewController animated:false];
    
    if (animated) {
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews animations:^{
            oldViewController.view.frame = [self hideTopViewFrame];
            CGRect visibleRect;
            if (isFromStack) {
                visibleRect = [self showTopViewFrame];
            } else {
                visibleRect = [self showDownViewFrame];
            }
            visibleViewController.view.frame = visibleRect;
      

        } completion:^(BOOL finished) {
            [self removeViewController:oldViewController];
        }];
    } else {
        oldViewController.view.frame = [self hideTopViewFrame];
        if (isFromStack) {
            visibleViewController.view.frame = [self showTopViewFrame];
        } else {
            visibleViewController.view.frame = [self showDownViewFrame];
        }
    }

}

#pragma mark - - Pan Gesture Recognizer
- (void) panGestureRecognizer:(MEFPanGestureRecognizer *)gesture
{
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
        {
            [self beganGestureRecognizer:(MEFPanGestureRecognizer *)gesture];
        } break;
            
        case UIGestureRecognizerStateChanged:
        {
            [self changedGestureRecognizer:(MEFPanGestureRecognizer *)gesture];
        } break;
            
        case UIGestureRecognizerStateEnded:
        {
            [self endedGestureRecognizer:(MEFPanGestureRecognizer *)gesture];
        } break;
            
        case UIGestureRecognizerStateCancelled:
        {
            [self endedGestureRecognizer:(MEFPanGestureRecognizer *)gesture];
        }
            
        default:
        {
        } break;
    }
}




- (void) beganGestureRecognizer:(MEFPanGestureRecognizer *)gesture
{
    CGPoint location = [gesture locationInView:self.view];
    gesture.prevLocation = location;
    
    if (self.visibleCards.count <= 1 || self.visibleTopController) {
        return;
    }
    self.prevPhasePan = BBStackCardPanPhaseNone;
    self.phasePan = BBStackCardPanPhaseNone;

    self.visibleTopController = [self.visibleCards lastObject];
    if (!CGRectContainsPoint(self.visibleTopController.view.frame, location)) {
        self.visibleTopController = nil;
    }
    
    
}

- (void) changedGestureRecognizer:(MEFPanGestureRecognizer *)gesture
{
    if (!self.visibleTopController) {
        return;
    }
    CGPoint location = [gesture locationInView:self.view];
    CGPoint prevLocation = [gesture prevLocation];
    CGPoint dif = ccpSub(location, prevLocation);
    dif.y = 0;
    CGRect topViewFrame = self.visibleTopController.view.frame;
    CGRect mainBounds = self.view.bounds;
 
    topViewFrame.origin = ccpAdd(topViewFrame.origin, dif);
    topViewFrame.origin.x = fmin(topViewFrame.origin.x, mainBounds.size.width);
    topViewFrame.origin.x = fmax(topViewFrame.origin.x, self.leftPadding);
    
    
    CGFloat percent = ((topViewFrame.origin.x - self.leftPadding) / mainBounds.size.width);

    if (percent >= BSStackCardViewPanPopPercent && percent <= BSStackCardViewPanPopAllPercent) {
        self.phasePan = BBStackCardPanPhasePopCard;
    } else if (percent >= BSStackCardViewPanPopAllPercent) {
        self.phasePan = BBStackCardPanPhaseRootCard;
    } else {
        self.phasePan = BBStackCardPanPhaseNone;
    }
    
    if (self.phasePan != self.prevPhasePan) {
        if (self.phasePan == BBStackCardPanPhaseRootCard) {
            if (self.prevPhasePan == BBStackCardPanPhasePopCard && self.stack.count) {
                UIViewController *downViewController = [self.visibleCards objectAtIndex:self.visibleCards.count - 2];
                UIViewController *rootViewController = [self.stack firstObject];
                self.amountShowAnimation++;
                [rootViewController.view setHidden:false];
                [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut animations:^{
                    downViewController.view.frame = [self hideDownViewFrame];
                } completion:^(BOOL finished) {
                    self.amountShowAnimation--;
                }];
            }
        } else if (self.phasePan == BBStackCardPanPhasePopCard) {
            if (self.prevPhasePan == BBStackCardPanPhaseRootCard && self.stack.count) {
                UIViewController *downViewController = [self.visibleCards objectAtIndex:self.visibleCards.count - 2];
                UIViewController *rootViewController = [self.stack firstObject];
                
                [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut animations:^{
                    downViewController.view.frame = [self showDownViewFrame];
                } completion:^(BOOL finished) {
                    if (self.amountShowAnimation == 0) {
                        [rootViewController.view setHidden:true];
                    } 
                }];
            }
        }
        
    }
    self.visibleTopController.view.frame = topViewFrame;
    gesture.prevLocation = location;
    self.prevPhasePan = self.phasePan;
}

- (void)endedGestureRecognizer:(MEFPanGestureRecognizer *)gesture
{
    if (self.phasePan == BBStackCardPanPhasePopCard) {
        [self popViewControllerWithAnimated:true];
    } else if (self.phasePan == BBStackCardPanPhaseRootCard) {
        [self popAllViewControllerWithAnimated:true];
    } else {
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionCurveEaseInOut animations:^{
            self.visibleTopController.view.frame = [self showTopViewFrame];
        } completion:^(BOOL finished) {
            
        }];
    }
    
    self.visibleTopController = nil;
    self.prevPhasePan = BBStackCardPanPhaseNone;
    self.phasePan = BBStackCardPanPhaseNone;
    self.amountShowAnimation = 0;
}


#pragma mark Override - UIViewCOntroller
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue isKindOfClass:[MEFStackCardPushSegue class]]) {
        [(MEFStackCardPushSegue*)segue setAnimated:self.isCurrentSegueAnimated];
    } else if ([segue isKindOfClass:[MEFStackCardRootSegue class]]) {
        [(MEFStackCardRootSegue*)segue setAnimated:self.isCurrentSegueAnimated];
        
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
