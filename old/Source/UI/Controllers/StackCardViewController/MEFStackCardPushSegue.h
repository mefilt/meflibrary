//
//  MEFStackCardPushSegue.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 22.09.15.
//  Copyright © 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEFStackCardPushSegue : UIStoryboardSegue
@property (nonatomic, readwrite, assign, getter=isAnimated) BOOL animated;
@end
