//
//  MEFCalendarViewController.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 10.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEFContainerViewController.h"
#import "MEFSegmentedControl.h"
typedef NS_OPTIONS(NSUInteger, MEFCalendarType) {
    MEFCalendarTypeNone = 0,
    MEFCalendarTypeWeek = 1 << 1,
    MEFCalendarTypeDay = 1 << 2,
    MEFCalendarTypeMonth = 1 << 3,
    MEFCalendarTypeInterval = 1 << 4
};



@interface MEFCalendarViewController : MEFContainerViewController
@property (nonatomic, readonly, assign) MEFCalendarType calendarType;
@property (nonatomic, readonly, strong) NSCalendar *calendarUTC;
@property (nonatomic, readonly, strong) NSCalendar *calendarLocal;
@property (nonatomic, readonly, strong) MEFSegmentedControl *segmentedControl;
//@property (nonatomic, readonly, copy) NSDate *selectedBeginDate;
//@property (nonatomic, readonly, copy) NSDate *selectedEndDate;

//not work visiblePastDates
@property (nonatomic, readwrite, assign, getter=isVisiblePastDates) BOOL visiblePastDates;

@property (nonatomic, readwrite, assign) MEFCalendarType availableCalendarTypes;
@property (nonatomic, readwrite, copy) NSDate *minDate;
@property (nonatomic, readwrite, copy) NSDate *maxDate;
@property (nonatomic, readwrite, assign, getter=isVisibleFutureDates) BOOL visibleFutureDates;
@property (nonatomic, readwrite, strong) UIColor *tintColor;
@property (nonatomic, readwrite, strong) UIColor *highLightTintColor;
@property (nonatomic, readwrite, strong) UIColor *selectedColor;
@property (nonatomic, readwrite, strong) UIColor *todayColor;
@property (nonatomic, readwrite, copy) void (^didTouchCalendar)(NSDate *beginDate, NSDate *endDate, MEFCalendarType type);
- (NSDate*) selectedBeginDate;
- (NSDate*) selectedEndDate;
- (id) initDeffaultCalendar;
- (void) settingsCalendarWithType:(MEFCalendarType)type;
- (void) setSelectedRange:(NSDate*)begin end:(NSDate*)end;
@end
