//
//  MEFCalendarViewController.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 10.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFCalendarViewController.h"
#import "NSBundle+Resource.h"
#import "MEFWeekBarView.h"
#import "MEFCalendarContentView.h"
#import "NSDate+MEFUtilities.h"
#import "MEFBaseCollectionViewController.h"
#import "MEFCalendarContentCollectionViewCell.h"
#import "UIColor+MEFAssistants.h"
#import "NSCalendar+MEFCurrent.h"
#import "NSDateFormatter+MEFAssistants.h"
#import "MEFCalendarTitleView.h"
#import "MEFCalendarTitleCollectionReusableView.h"
#import "MEFConstants.h"
#import "MEFCalendarCollectionView.h"

const static NSUInteger MEFCalendarFirstInitContentCount = 6;
//const static NSUInteger MEFCalendarHelpContentCount = 3;
@interface MEFCalendarData : NSObject
@property (nonatomic, readwrite, copy) NSDate *beginDate;
@property (nonatomic, readwrite, copy) NSDate *endDate;
@property (nonatomic, readwrite, copy) NSDate *month;
@property (nonatomic, readwrite, assign) MEFCalendarContentType contentType;

+ (MEFCalendarData*) calendarDataWithMonth:(NSDate*)month beginDate:(NSDate*)beginDate endDate:(NSDate*)endDate type:(MEFCalendarContentType)type;
@end

@implementation MEFCalendarData
+ (MEFCalendarData*) calendarDataWithMonth:(NSDate*)month beginDate:(NSDate*)beginDate endDate:(NSDate*)endDate type:(MEFCalendarContentType)type
{
    MEFCalendarData *calendarData = [MEFCalendarData new];
    calendarData.month = month;
    calendarData.beginDate = beginDate;
    calendarData.endDate = endDate;
    calendarData.contentType = type;
    return calendarData;
}
@end


@interface MEFCalendarViewController () <MEFCalendarContentViewDelegate, MEFBaseCollectionViewControllerDelegate, MEFBaseCollectionViewControllerDatasource, UIGestureRecognizerDelegate, MEFCalendarCollectionViewDelegate>
@property (nonatomic, readwrite, strong) MEFSegmentedControl *segmentedControl;
@property (nonatomic, readwrite, strong) MEFCalendarContentView *calendarContentView;
@property (nonatomic, readwrite, strong) MEFBaseCollectionViewController *baseCollectionViewController;
@property (nonatomic, readwrite, strong) NSCalendar *calendarUTC;
@property (nonatomic, readwrite, strong) NSCalendar *calendarLocal;
@property (nonatomic, readwrite, strong) UIPanGestureRecognizer *panGesture;

@property (nonatomic, readwrite, strong) NSIndexPath *scrollToIndexPath;
@property (nonatomic, readwrite, assign, getter=isNeedScrollToBottom) BOOL needScrollToBottom;
@property (nonatomic, readwrite, assign, getter=isNeedLockLoadingFutureDate) BOOL needLockLoadingFutureDate;
@property (nonatomic, readwrite, assign, getter=isNeedLockLoadingPastDate) BOOL needLockLoadingPastDate;

//for weak type
@property (nonatomic, readwrite, strong) MEFWeekBarView *weekBarView;
@property (nonatomic, readwrite, strong) MEFCalendarTitleView *calendarTitleView;


@property (nonatomic, readwrite, assign) MEFCalendarType calendarType;
@property (nonatomic, readwrite, copy) NSDate *originalSelectedBeginDate;
@property (nonatomic, readwrite, copy) NSDate *originalSelectedEndDate;
@property (nonatomic, readwrite, copy) NSDate *actualSelectedBeginDate;
@property (nonatomic, readwrite, copy) NSDate *actualSelectedEndDate;

@end

@implementation MEFCalendarViewController

- (id) initDeffaultCalendar
{
    if (self = [super initWithNibName:@"MEFCalendarViewController" bundle:[NSBundle libraryResourcesBundle]]) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.baseCollectionViewController = [[MEFBaseCollectionViewController alloc]initWithNibName:@"MEFCalendarCollectionViewController" bundle:[NSBundle libraryResourcesBundle]];
    [(MEFCalendarCollectionView*)self.baseCollectionViewController.collectionView setCalendarDelegate:self];
    [self.baseCollectionViewController.collectionView setShowsVerticalScrollIndicator:false];
    [self.baseCollectionViewController.collectionView setShowsHorizontalScrollIndicator:false];
    [self.baseCollectionViewController.collectionView registerNib:[UINib nibWithNibName:@"MEFCalendarTitleCollectionReusableView" bundle:[NSBundle libraryResourcesBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CalendarTitleCollectionReusableView"];
    [self.baseCollectionViewController setDelegate:self];
    [self.baseCollectionViewController setDatasource:self];
    [self setViewController:self.baseCollectionViewController animateType:MEFContainerViewAnimateTypeNone];
    
    [self addTopView:self.segmentedControl animated:false];

    [self.segmentedControl setTintColor:self.highLightTintColor];
    [self.segmentedControl setBackgroundColor:self.tintColor];
    
    MEFBlockWeakSelf weakSelf = self;
    [self.segmentedControl setChangedValue:^(NSUInteger index, NSString *title, MEFSegmentedControl *control) {
        if ([title isEqualToString:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.day", nil)]) {
            [weakSelf settingsCalendarWithType:MEFCalendarTypeDay];
        } else if ([title isEqualToString:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.week", nil)]) {
            [weakSelf settingsCalendarWithType:MEFCalendarTypeWeek];
        } else if ([title isEqualToString:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.month", nil)]) {
            [weakSelf settingsCalendarWithType:MEFCalendarTypeMonth];
        } else if ([title isEqualToString:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.interval", nil)]) {
            [weakSelf settingsCalendarWithType:MEFCalendarTypeInterval];
        }
        if (weakSelf.didTouchCalendar) {
            weakSelf.didTouchCalendar([weakSelf selectedBeginDate],[weakSelf selectedEndDate], weakSelf.calendarType);
        }

    }];
 

    [self settingsCalendarWithType:MEFCalendarTypeDay];
}

- (void) setAvailableCalendarTypes:(MEFCalendarType)availableCalendarTypes
{
    _availableCalendarTypes = availableCalendarTypes;
    [self reloadSegmentedControl];
}

- (void) reloadSegmentedControl
{
    [self.segmentedControl removeAll];
    if (self.availableCalendarTypes == MEFCalendarTypeNone) {
        [self.segmentedControl addTitle:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.day", nil)];
        [self.segmentedControl addTitle:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.week", nil)];
        [self.segmentedControl addTitle:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.month", nil)];
        [self.segmentedControl addTitle:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.interval", nil)];
    
    } else {
        if ((self.availableCalendarTypes & MEFCalendarTypeDay) != 0) {
            [self.segmentedControl addTitle:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.day", nil)];
        }
        if ((self.availableCalendarTypes & MEFCalendarTypeWeek) != 0) {
            [self.segmentedControl addTitle:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.week", nil)];
        }
        if ((self.availableCalendarTypes & MEFCalendarTypeMonth) != 0) {
            [self.segmentedControl addTitle:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.month", nil)];
        }
        if ((self.availableCalendarTypes & MEFCalendarTypeMonth) != 0) {
            [self.segmentedControl addTitle:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.interval", nil)];
        }
    }
    [self.segmentedControl selectSegmentAtTitle:[self titleSegmentAtCalendarType:self.calendarType]];
}

- (NSString*) titleSegmentAtCalendarType:(MEFCalendarType)calendarType
{
    switch (calendarType) {
        case MEFCalendarTypeDay:
            return MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.day", nil);
        case MEFCalendarTypeWeek:
            return MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.week", nil);
        case MEFCalendarTypeMonth:
            return MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.month", nil);
        case MEFCalendarTypeInterval:
            return MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.interval", nil);
        default:
            break;
    }
    return @"";
}

- (MEFCalendarType) calendarTypeAtTitle:(NSString*)title
{
    if ([title isEqualToString:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.day", nil)]) {
        return MEFCalendarTypeDay;
    } else if ([title isEqualToString:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.week", nil)]) {
        return MEFCalendarTypeWeek;
    } else if ([title isEqualToString:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.month", nil)]) {
        return MEFCalendarTypeMonth;
    } else if ([title isEqualToString:MEFLibraryLocalizedString(@"com.mefilt.controller.calendar.segmnetcontrol.button.interval", nil)]) {
        return MEFCalendarTypeInterval;
    }
    return MEFCalendarTypeNone;
}
- (void) setSelectedRange:(NSDate*)begin end:(NSDate*)end
{
    if (self.calendarType == MEFCalendarTypeMonth) {
        self.actualSelectedBeginDate = self.originalSelectedBeginDate = [NSDate firstDayOfMonth:begin calendar:self.calendarUTC];
        self.actualSelectedEndDate = self.originalSelectedEndDate = [NSDate firstDayOfMonth:end calendar:self.calendarUTC];
    } else {
        self.actualSelectedBeginDate = self.originalSelectedBeginDate = begin;
        self.actualSelectedEndDate = self.originalSelectedEndDate = end;
    }
    
    NSArray *visibleCells = [self.baseCollectionViewController.collectionView visibleCells];
    if (!visibleCells.count) {
        return;
    }
    for (MEFCalendarContentCollectionViewCell *cell in visibleCells) {
        MEFCalendarContentView *localCalendarContentView = cell.calendarContentView;
        [localCalendarContentView setSelectedRange:self.actualSelectedBeginDate end:self.actualSelectedEndDate];
    }
}

#pragma mark - - color
//[self.segmentedControl setBackgroundColor:[UIColor colorFromHexCode:@"3C376C"]];
//[self.segmentedControl setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
//[self.segmentedControl setTintColor:[UIColor colorFromHexCode:@"57528E"]];


- (void) setHighLightTintColor:(UIColor *)highLightTintColor
{
    _highLightTintColor = highLightTintColor;
}
- (void) setTintColor:(UIColor *)tintColor
{
    _tintColor = tintColor;

}

- (NSDate*) beginDateForWeekWithDate:(NSDate*)date
{
    NSInteger days = 6;
    if (self.minDate) {
        days = [self.calendarUTC daysBetweenDate:date andDate:self.minDate];
        days = fmin(days, 6);
    }
    
    return [date dateBySubtractingDays:days];
}


- (NSDate*) selectedBeginDate
{
    return self.actualSelectedBeginDate;
}

- (NSDate*) selectedEndDate
{
    if (self.calendarType == MEFCalendarTypeMonth) {
        return [NSDate lastDayOfMonth:self.actualSelectedBeginDate calendar:self.calendarUTC];
    }
    return self.actualSelectedEndDate;
}

- (void) settingsCalendarWithType:(MEFCalendarType)type
{
    [self settingsCalendarWithType:type contentCount:MEFCalendarFirstInitContentCount];
}



- (void) settingsCalendarWithType:(MEFCalendarType)type contentCount:(NSUInteger)count
{
    if (self.actualSelectedBeginDate && self.actualSelectedEndDate && self.calendarType != type) {
        switch (type) {
   
            case MEFCalendarTypeWeek:
            {
    
                self.actualSelectedBeginDate = [self beginDateForWeekWithDate:self.originalSelectedBeginDate];
                self.actualSelectedEndDate = self.originalSelectedBeginDate;

            }   break;
            case MEFCalendarTypeMonth:
            {
                self.actualSelectedBeginDate = [NSDate firstDayOfMonth:self.originalSelectedBeginDate calendar:self.calendarUTC];
                self.actualSelectedEndDate = [NSDate firstDayOfMonth:self.originalSelectedBeginDate calendar:self.calendarUTC];
                
            }   break;
            case MEFCalendarTypeDay:
            {
                self.actualSelectedBeginDate = self.originalSelectedBeginDate;
                self.actualSelectedEndDate = self.originalSelectedBeginDate;
            }   break;
            case MEFCalendarTypeInterval:
            case MEFCalendarTypeNone:
            {
                self.actualSelectedBeginDate = self.originalSelectedBeginDate;
                self.actualSelectedEndDate = self.originalSelectedEndDate;
            }   break;
            default:
                break;
        }
    }
    self.calendarType = type;
    [self reloadSegmentedControl];
    NSDate *beginDate = self.actualSelectedBeginDate;
    
    if (!beginDate) {
        beginDate = [NSDate date];
    }
 
    switch (type) {
        case MEFCalendarTypeMonth:
        {
            if (self.calendarTitleView) {
                [self removeTopView:self.calendarTitleView animated:true];
                self.calendarTitleView = nil;
            }
            if (self.weekBarView) {
                [self removeTopView:self.weekBarView animated:true];
                self.weekBarView = nil;
            }
            [[self baseCollectionViewController]cleanup];
            NSArray *sections = [self createSectionsForMonthPastDates:count dateYear:beginDate];
            for (MEFObject *section in sections) {
                [self.baseCollectionViewController addSection:section];
            }
            self.needScrollToBottom = true;
//            self.scrollToIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.baseCollectionViewController reloadData];
        }   break;
        case MEFCalendarTypeInterval:
        case MEFCalendarTypeDay:
        case MEFCalendarTypeWeek:
        {
            if (!self.calendarTitleView) {
                self.calendarTitleView = [[[NSBundle libraryResourcesBundle]loadNibNamed:@"MEFCalendarTitleView" owner:nil options:nil]lastObject];
                [self.calendarTitleView setBackgroundColor:[UIColor colorFromHexCode:@"F7F7F7"]];
                [self.calendarTitleView setFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
                [self.calendarTitleView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
                [self addTopView:self.calendarTitleView animated:true];
            }
            if (!self.weekBarView) {
                self.weekBarView = [[MEFWeekBarView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
                [self.weekBarView setBackgroundColor:[UIColor colorFromHexCode:@"F7F7F7"]];
                [self.weekBarView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
                [self.weekBarView setLocalCalendar:self.calendarLocal];
                [self addTopView:self.weekBarView animated:true];
            }

            [[self baseCollectionViewController]cleanup];
            NSDate *month = beginDate;
            [self.calendarTitleView.titleLabel setText:[NSString stringWithFormat:@"%d",(int)month.year]];
            NSDate *lastDayOfMonth = [NSDate lastDayOfMonth:month calendar:self.calendarUTC];
            NSDate *lastDayOfWeek = [NSDate getLastDayOfWeek:lastDayOfMonth calendar:self.calendarUTC];
            NSArray *sections = [self createSectionsForDayPastDates:count month:month endDate:lastDayOfWeek];
            for (MEFObject *section in sections) {
                [self.baseCollectionViewController addSection:section];
            }
            self.needScrollToBottom = true;
            [self.baseCollectionViewController reloadData];
        }   break;
        default:
            break;
    }


}

- (NSArray*) createSectionsForMonthPastDates:(NSUInteger)count dateYear:(NSDate*)dateYear
{
    NSMutableArray *sections = [NSMutableArray array];
    
    NSDateComponents *dateComponents = [self.calendarUTC components:(NSCalendarUnitYear | NSCalendarUnitMonth) fromDate:dateYear];
    [dateComponents setMonth:1];
    NSDate *beginMonth = [self.calendarUTC dateFromComponents:dateComponents];
    [dateComponents setMonth:12];
    NSDate *endMonth = [self.calendarUTC dateFromComponents:dateComponents];
    for (int i = 0; i < count; i++) {
        MEFObject *section = [MEFObject objectByType:0];
        MEFObject *obejct = [MEFObject objectByType:0];
        MEFCalendarData *data = [MEFCalendarData calendarDataWithMonth:nil beginDate:beginMonth endDate:endMonth type:MEFCalendarContentTypeMonths];
        section.data = data;
        obejct.data = data;
        [section addItem:obejct];
        [sections insertObject:section atIndex:0];
        NSDateComponents *dateComponents = [self.calendarUTC components:(NSCalendarUnitYear | NSCalendarUnitMonth) fromDate:beginMonth];
        [dateComponents setYear:[dateComponents year] - 1];
        [dateComponents setMonth:1];
        beginMonth = [self.calendarUTC dateFromComponents:dateComponents];
        [dateComponents setMonth:12];
        endMonth =  [self.calendarUTC dateFromComponents:dateComponents];
    }
    return sections;
}


- (NSArray*) createSectionsForMonthFutureDates:(NSUInteger)count dateYear:(NSDate*)dateYear
{
    NSMutableArray *sections = [NSMutableArray array];
    
    NSDateComponents *dateComponents = [self.calendarUTC components:(NSCalendarUnitYear | NSCalendarUnitMonth) fromDate:dateYear];
    [dateComponents setMonth:1];
    NSDate *beginMonth = [self.calendarUTC dateFromComponents:dateComponents];
    [dateComponents setMonth:12];
    NSDate *endMonth = [self.calendarUTC dateFromComponents:dateComponents];
    for (int i = 0; i < count; i++) {
        MEFObject *section = [MEFObject objectByType:0];
        MEFObject *obejct = [MEFObject objectByType:0];
        MEFCalendarData *data = [MEFCalendarData calendarDataWithMonth:nil beginDate:beginMonth endDate:endMonth type:MEFCalendarContentTypeMonths];
        section.data = data;
        obejct.data = data;
        [section addItem:obejct];
        [sections addObject:section];
        
        NSDateComponents *dateComponents = [self.calendarUTC components:(NSCalendarUnitYear | NSCalendarUnitMonth) fromDate:beginMonth];
        [dateComponents setYear:[dateComponents year] + 1];
        [dateComponents setMonth:1];
        beginMonth = [self.calendarUTC dateFromComponents:dateComponents];
        [dateComponents setMonth:12];
        endMonth =  [self.calendarUTC dateFromComponents:dateComponents];
    }
    return sections;
}


- (NSArray*) createSectionsForDayPastDates:(NSUInteger)count month:(NSDate*)month endDate:(NSDate *)endDate
{
    NSMutableArray *sections = [NSMutableArray array];
    NSDate *firstDayOfMonth = [NSDate firstDayOfMonth:month calendar:self.calendarUTC];
    NSDate *firstDayOfWeek = [NSDate getFirstDayOfWeek:firstDayOfMonth calendar:self.calendarUTC];
    month = firstDayOfMonth;
    NSDate *beginDate = firstDayOfWeek;
    for (int i = 0; i < count; i++) {
        MEFObject *section = [MEFObject objectByType:0];
        MEFObject *obejct = [MEFObject objectByType:0];
        MEFCalendarData *data = [MEFCalendarData calendarDataWithMonth:month beginDate:beginDate endDate:endDate type:MEFCalendarContentTypeDays];
        section.data = data;
        obejct.data = data;
        [section addItem:obejct];
        [sections insertObject:section atIndex:0];
        endDate = [beginDate dateBySubtractingDays:1];
        month = [month dateBySubtractingMonths:1 calendar:self.calendarUTC];
        NSDate *firstDayOfMonth = [NSDate firstDayOfMonth:month calendar:self.calendarUTC];
        NSDate *firstDayOfWeek = [NSDate getFirstDayOfWeek:firstDayOfMonth calendar:self.calendarUTC];
        month = firstDayOfMonth;
        beginDate =  firstDayOfWeek;
    }
    return sections;
}


- (NSArray*) createSectionsForDayFutureDates:(NSUInteger)count month:(NSDate*)month beginDate:(NSDate *)beginDate
{
    NSMutableArray *sections = [NSMutableArray array];
    NSDate *lastDayOfMonth = [NSDate lastDayOfMonth:month calendar:self.calendarUTC];
    NSDate *lastDayOfWeek = [NSDate getLastDayOfWeek:lastDayOfMonth calendar:self.calendarUTC];
    NSDate *endDate = lastDayOfWeek;
    for (int i = 0; i < count; i++) {
        MEFObject *section = [MEFObject objectByType:0];
        MEFObject *obejct = [MEFObject objectByType:0];
        MEFCalendarData *data = [MEFCalendarData calendarDataWithMonth:month beginDate:beginDate endDate:endDate type:MEFCalendarContentTypeDays];
        section.data = data;
        obejct.data = data;
        [section addItem:obejct];
        [sections addObject:section];
        
        beginDate = [endDate dateByAddingDays:1];
        month = [month dateByAddingMonths:1 calendar:self.calendarUTC];
        NSDate *firstDayOfMonth = [NSDate firstDayOfMonth:month calendar:self.calendarUTC];
        NSDate *lastDayOfMonth = [NSDate lastDayOfMonth:month calendar:self.calendarUTC];
        NSDate *lastDayOfWeek = [NSDate getLastDayOfWeek:lastDayOfMonth calendar:self.calendarUTC];
        month = firstDayOfMonth;
        endDate =  lastDayOfWeek;
    }
    return sections;
}



- (void) settingsCalendarForInterval
{
    
}


- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
//    if (self.isNeedScrollToBottom) {
//        self.needScrollToBottom = false;
//        [self.baseCollectionViewController.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.baseCollectionViewController.sections.count - 1] atScrollPosition:UICollectionViewScrollPositionBottom animated:false];
//        
//    }
}
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    if (self.isNeedScrollToBottom) {
//        self.needScrollToBottom = false;
//        [self.baseCollectionViewController.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.baseCollectionViewController.sections.count - 1] atScrollPosition:UICollectionViewScrollPositionBottom animated:false];
//        
//    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark MEFCalendarContentView delegate

- (CGPoint) centerPositionDayOfWeek:(NSDate *)date calendarContentView:(MEFCalendarContentView *)calendarContentView
{
    return [self.weekBarView centerPositionDayOfWeek:date];
}


- (void) didTouchDate:(NSDate *)date calendarContentView:(MEFCalendarContentView *)calendarContentView
{
    switch (self.calendarType) {
        case MEFCalendarTypeInterval:
        {
            if (!self.actualSelectedBeginDate) {
                [calendarContentView setTintColor:[UIColor greenColor]];
                self.actualSelectedBeginDate = date;
                self.actualSelectedEndDate = date;
            } else if ([self.actualSelectedBeginDate isEqual:self.actualSelectedEndDate]) {
                self.actualSelectedEndDate = date;
                if ([self.actualSelectedEndDate isEarlierThanDate:self.actualSelectedBeginDate]) {
                    self.actualSelectedEndDate = self.actualSelectedBeginDate;
                    self.actualSelectedBeginDate = date;
                }
            } else if (self.actualSelectedBeginDate && self.actualSelectedEndDate) {
                self.actualSelectedBeginDate = self.actualSelectedEndDate = nil;
            }
        }   break;
        case MEFCalendarTypeDay:
        {
            self.actualSelectedBeginDate = date;
            self.actualSelectedEndDate = date;
        }   break;
        case MEFCalendarTypeMonth:
        {
            self.actualSelectedBeginDate = date;
            self.actualSelectedEndDate = date;
        }   break;
        case MEFCalendarTypeNone:
        {
            
        }   break;
        case MEFCalendarTypeWeek:
        {
            self.actualSelectedBeginDate = [self beginDateForWeekWithDate:date];
            self.actualSelectedEndDate = date;
        }   break;
        default:
            break;
    }
    if (self.calendarType == MEFCalendarTypeWeek) {
        self.originalSelectedBeginDate = date;
        self.originalSelectedEndDate = date;
    } else {
        self.originalSelectedBeginDate = self.actualSelectedBeginDate;
        self.originalSelectedEndDate = self.actualSelectedEndDate;
    }

    if (self.didTouchCalendar) {
        self.didTouchCalendar([self selectedBeginDate],[self selectedEndDate], self.calendarType);
    }
    for (MEFCalendarContentCollectionViewCell *cell in [self.baseCollectionViewController.collectionView visibleCells]) {
        MEFCalendarContentView *localCalendarContentView = cell.calendarContentView;
        if (calendarContentView != localCalendarContentView) {
            [localCalendarContentView setTintColor:[UIColor colorFromHexCode:@"F7F1D0"]];
        }

        [localCalendarContentView setSelectedRange:self.actualSelectedBeginDate end:self.actualSelectedEndDate];
    }
}
#pragma mark -- methods for creation dates 
- (void) appendPastDates
{
    NSLog(@"appendPastDates");
    UICollectionView *cv = self.baseCollectionViewController.collectionView;
    UICollectionViewFlowLayout *cvLayout = (UICollectionViewFlowLayout *)cv.collectionViewLayout;
    
    NSArray *visibleCells = [cv visibleCells];
    if (![visibleCells count]) {
        return;
    }
    NSIndexPath *fromIndexPath = [cv indexPathForCell:((UICollectionViewCell *)visibleCells[0]) ];
    NSInteger fromSection = fromIndexPath.section;
    
    UICollectionViewLayoutAttributes *fromAttrs = [cvLayout layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:fromSection]];
    CGPoint fromSectionOrigin = [cv convertPoint:fromAttrs.frame.origin fromView:cv];
    
    MEFObject *firstSection = [self.baseCollectionViewController sectionAtIndex:0];
    MEFCalendarData *firstCalendarData = firstSection.data;
    
    NSUInteger saveMonth = visibleCells.count;
    
    NSArray *newSections = nil;
    
    if (self.calendarType == MEFCalendarTypeMonth) {
        NSDateComponents *dateComponents = [self.calendarUTC components:(NSCalendarUnitYear | NSCalendarUnitMonth) fromDate:firstCalendarData.beginDate];
        [dateComponents setYear:[dateComponents year] + saveMonth];
        NSDate *dateYear = [self.calendarUTC dateFromComponents:dateComponents];
        newSections = [self createSectionsForMonthPastDates:saveMonth * 3  dateYear:dateYear];
    } else {
        NSDate *month = [firstCalendarData.month dateByAddingMonths:saveMonth calendar:self.calendarUTC];
        NSDate *lastDayOfMonth = [NSDate lastDayOfMonth:month calendar:self.calendarUTC];
        NSDate *lastDayOfWeek = [NSDate getLastDayOfWeek:lastDayOfMonth calendar:self.calendarUTC];
        newSections = [self createSectionsForDayPastDates:saveMonth * 3 month:month endDate:lastDayOfWeek];
    }
    
    
    [self.baseCollectionViewController cleanup];
    for (MEFObject *section in newSections) {
        [self.baseCollectionViewController addSection:section];
    }
    [self.baseCollectionViewController reloadData];
    [cvLayout invalidateLayout];
    [cvLayout prepareLayout];
    
    
    NSInteger toSection;
    if (self.calendarType == MEFCalendarTypeMonth) {
        toSection = [self.calendarUTC components:NSCalendarUnitYear fromDate:[[newSections.firstObject data] beginDate] toDate:firstCalendarData.beginDate options:0].year;
    } else {
        toSection = [self.calendarUTC components:NSCalendarUnitMonth fromDate:[[newSections.firstObject data] beginDate] toDate:firstCalendarData.beginDate options:0].month;
    }
    
    UICollectionViewLayoutAttributes *toAttrs = [cvLayout layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:toSection]];
    CGPoint toSectionOrigin = [cv convertPoint:toAttrs.frame.origin fromView:cv];
    
    
    CGFloat y = (cv.contentOffset.y + (toSectionOrigin.y - fromSectionOrigin.y));
    if (y <= 0) {
        self.needLockLoadingPastDate = true;
    }
    
    [cv setContentOffset:CGPointMake(cv.contentOffset.x, (cv.contentOffset.y + (toSectionOrigin.y - fromSectionOrigin.y)))];
    
    
}

- (void) appendFutureDates
{
    UICollectionView *cv = self.baseCollectionViewController.collectionView;
    UICollectionViewFlowLayout *cvLayout = (UICollectionViewFlowLayout *)cv.collectionViewLayout;
    
    NSArray *visibleCells = [cv visibleCells];
    if (![visibleCells count]) {
        return;
    }
    NSArray *pathsForVisibleItems = cv.indexPathsForVisibleItems;
    
    UICollectionViewLayoutAttributes *fromAttrs = [cvLayout layoutAttributesForItemAtIndexPath:[pathsForVisibleItems lastObject]];
    CGPoint fromSectionOrigin = [cv convertPoint:fromAttrs.frame.origin fromView:cv];
    
    
    
    MEFObject *lastVisibleSection = [self.baseCollectionViewController sectionAtIndex:[[pathsForVisibleItems lastObject] section]];
    MEFCalendarData *lastVisibleCalendarData = lastVisibleSection.data;
    NSUInteger addingDate = visibleCells.count;
    NSArray *newSections = nil;
    
    
    NSInteger maxCountAddingDate = 0;
    if (!self.isVisibleFutureDates) {
        if (self.calendarType == MEFCalendarTypeMonth) {
            NSDateComponents *now =  [self.calendarUTC components:NSCalendarUnitYear fromDate:[NSDate new]];
            NSDateComponents *last = [self.calendarUTC components:NSCalendarUnitYear fromDate:lastVisibleCalendarData.beginDate];
            if (now.year == last.year) {
                return;
            }
            NSDateComponents *dateComponents = [self.calendarUTC components:NSCalendarUnitYear fromDate:lastVisibleCalendarData.endDate];
            NSDate *year = [self.calendarUTC dateFromComponents:dateComponents];
            maxCountAddingDate = (NSInteger)labs([self.calendarUTC yearsBetweenDate:[NSDate date] andDate:year]) + 1;
        } else {
            NSDateComponents *now =  [self.calendarUTC components:NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate new]];
            NSDateComponents *last = [self.calendarUTC components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:lastVisibleCalendarData.month];
            if (now.month == last.month && now.year == last.year) {
                return;
            }
            maxCountAddingDate = (NSInteger)[self.calendarUTC monthsBetweenDate:[NSDate date] andDate:lastVisibleCalendarData.month];
        }
        maxCountAddingDate = fmin(addingDate * 3, addingDate + maxCountAddingDate);
    } else {
        maxCountAddingDate = addingDate  * 3;
    }
    
    
    if (self.calendarType == MEFCalendarTypeMonth) {
        NSDateComponents *dateComponents = [self.calendarUTC components:(NSCalendarUnitYear | NSCalendarUnitMonth) fromDate:lastVisibleCalendarData.endDate];
        [dateComponents setYear:[dateComponents year] - addingDate];
        newSections = [self createSectionsForMonthFutureDates:maxCountAddingDate dateYear:[self.calendarUTC dateFromComponents:dateComponents]];
    } else {
        NSDate *month = [lastVisibleCalendarData.month dateBySubtractingMonths:addingDate calendar:self.calendarUTC];
        NSDate *firstDayOfMonth = [NSDate firstDayOfMonth:month calendar:self.calendarUTC];
        NSDate *firstDayOfWeek = [NSDate getFirstDayOfWeek:firstDayOfMonth calendar:self.calendarUTC];
        newSections = [self createSectionsForDayFutureDates:maxCountAddingDate month:month beginDate:firstDayOfWeek];
    }
    if (!newSections.count) {
        return;
    }
    
    [self.baseCollectionViewController cleanup];
    for (MEFObject *section in newSections) {
        [self.baseCollectionViewController addSection:section];
    }
    [self.baseCollectionViewController reloadData];
    [cvLayout invalidateLayout];
    [cvLayout prepareLayout];
    
    NSInteger toSection;
    if (self.calendarType == MEFCalendarTypeMonth) {
        toSection = [self.calendarUTC components:NSCalendarUnitYear fromDate:[[newSections.firstObject data] beginDate] toDate:lastVisibleCalendarData.beginDate options:0].year;
    } else {
        toSection = [self.calendarUTC components:NSCalendarUnitMonth fromDate:[(MEFCalendarData*)[newSections.firstObject data] month] toDate:lastVisibleCalendarData.month options:0].month;
    }
    
    UICollectionViewLayoutAttributes *toAttrs = [cvLayout layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:toSection]];
    CGPoint toSectionOrigin = [cv convertPoint:toAttrs.frame.origin fromView:cv];
    CGFloat y = (cv.contentOffset.y + (toSectionOrigin.y - fromSectionOrigin.y));
    if (y > (cv.contentSize.height - CGRectGetHeight(cv.bounds))) {
        self.needLockLoadingFutureDate = true;
    }
    [cv setContentOffset:CGPointMake(cv.contentOffset.x, y)];
}



- (void) settingCalendarContentCollectionViewCell:(MEFCalendarContentCollectionViewCell*)contentCollectionViewCell inexPath:(NSIndexPath*)indexPath pseudo:(BOOL)pseudo
{
    
    MEFCalendarContentView *calendarContentView = contentCollectionViewCell.calendarContentView;
    [calendarContentView setDelegate:self];
    MEFCalendarData *calendarData = [self.baseCollectionViewController objectAtIndexPath:indexPath].data;
    calendarContentView.visibledToday = true;
    calendarContentView.beginDate = calendarData.beginDate;
    calendarContentView.endDate =  calendarData.endDate;
    [calendarContentView setSelectedColor:self.selectedColor];
    [calendarContentView setTintTodayColor:self.todayColor];
    calendarContentView.minDate = self.minDate;
    calendarContentView.maxDate = self.maxDate;
//    NSLog(@"calendar begin %@ - %@, selected %@ - %@",calendarData.beginDate,calendarData.endDate,self.selectedBeginDate,self.selectedEndDate);
    [calendarContentView updateCalendarContentWithCalendarUTC:self.calendarUTC localCalendar:self.calendarLocal type:calendarData.contentType];
    [calendarContentView setSelectedRange:self.actualSelectedBeginDate end:self.actualSelectedEndDate];

}


#pragma mark - - base delegate


- (UICollectionViewCell*) baseCollectionViewController:(MEFBaseCollectionViewController *)controller createCollectionViewCellWithObject:(MEFObject *)object cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = nil;
    return cell;
}

- (void) baseCollectionViewController:(MEFBaseCollectionViewController *)controller configCollectionViewCellWithObject:(MEFObject *)object cellForItemAtIndexPath:(NSIndexPath *)indexPath cell:(UICollectionViewCell *)cell
{
    MEFCalendarContentCollectionViewCell *contentCollectionViewCell = (MEFCalendarContentCollectionViewCell*) cell;
    [self settingCalendarContentCollectionViewCell:contentCollectionViewCell inexPath:indexPath pseudo:false];
}



- (UINib*) baseCollectionViewController:(MEFBaseCollectionViewController *)controller registerCollectionNib:(MEFObject *)object
{
    return  [UINib nibWithNibName:@"MEFCalendarContentCollectionViewCell" bundle:[NSBundle libraryResourcesBundle]];
}

- (NSString*) baseCollectionViewController:(MEFBaseCollectionViewController *)controller reuseIdentifierWithObject:(MEFObject *)object
{
    return @"CalendarContentCollectionViewCell";
}

- (UICollectionReusableView*) baseCollectionViewController:(MEFBaseCollectionViewController *)controller viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{

    MEFObject *sec = [controller sectionAtIndex:indexPath.section];
    MEFCalendarData *calendarData = sec.data;
    
    if (calendarData.contentType != MEFCalendarContentTypeMonths) {
        return nil;
    }
   
    MEFCalendarTitleCollectionReusableView *calendarTitleCollectionReusableView = [controller.collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"CalendarTitleCollectionReusableView" forIndexPath:indexPath];
    
    [calendarTitleCollectionReusableView setBackgroundColor:[UIColor colorFromHexCode:@"F7F7F7"]];
    MEFObject *object = [controller objectAtIndexPath:indexPath];
    MEFCalendarData *data = object.data;
    [calendarTitleCollectionReusableView.titleLabel setText:[NSDateFormatter yearFromDate :data.beginDate]];
    return calendarTitleCollectionReusableView;
}


- (CGSize) baseCollectionViewController:(MEFBaseCollectionViewController *)controller layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    MEFObject *sec = [controller sectionAtIndex:section];
    MEFCalendarData *calendarData = sec.data;
    if (calendarData.contentType != MEFCalendarContentTypeMonths) {
        return CGSizeMake(self.view.frame.size.width, 0);
    }
   
    return CGSizeMake(self.view.frame.size.width, 30);
}

- (CGSize) baseCollectionViewController:(MEFBaseCollectionViewController *)controller sizeForItemAtIndexPath:(NSIndexPath *)indexPath layout:(UICollectionViewLayout *)collectionViewLayout
{
    static MEFCalendarContentCollectionViewCell *contentCollectionViewCell = nil;
    if (!contentCollectionViewCell) {
        contentCollectionViewCell = [[[NSBundle libraryResourcesBundle] loadNibNamed:@"MEFCalendarContentCollectionViewCell" owner:nil options:nil]lastObject];
    }
    [self settingCalendarContentCollectionViewCell:contentCollectionViewCell inexPath:indexPath pseudo:true];
    [contentCollectionViewCell setFrame:CGRectMake(0, 0, self.view.frame.size.width, 0)];
    CGSize size = [contentCollectionViewCell sizeThatFits:CGSizeMake(self.view.frame.size.width, 0)];
    return size;
}


- (void) baseCollectionViewController:(MEFBaseCollectionViewController *)controller scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.needLockLoadingFutureDate = false;
    self.needScrollToBottom = false;
    self.needLockLoadingPastDate = false;
}

- (void) baseCollectionViewController:(MEFBaseCollectionViewController *)controller scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    self.needScrollToBottom = false;
    self.needLockLoadingFutureDate = false;
    self.needLockLoadingPastDate = false;
}


- (void) baseCollectionViewController:(MEFBaseCollectionViewController *)controller scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSArray *visibleCells = [controller.collectionView visibleCells];
    if (visibleCells.count == 0) {
        return;
    }
    if (self.calendarType == MEFCalendarTypeMonth) {
        return;
    }
    NSArray *visibleIndexPaths = [controller.collectionView indexPathsForVisibleItems];
    for (NSIndexPath *indexPath in visibleIndexPaths) {
        UICollectionViewCell *cell = [controller.collectionView cellForItemAtIndexPath:indexPath];
        CGRect cellFrame = cell.frame;
        cellFrame.origin.y -= controller.collectionView.contentOffset.y;
        if (CGRectContainsRect(controller.collectionView.frame, cellFrame)) {
            MEFObject *section = [controller sectionAtIndex:indexPath.section];
            MEFCalendarData *calendarData = section.data;
            [self.calendarTitleView.titleLabel setText:[NSString stringWithFormat:@"%d",(int)calendarData.month.year]];
            break;
        }
    }
}



#pragma mark - - calendar collection delegate

- (void) calendarCollectionViewWillLayoutSubviews:(MEFCalendarCollectionView *)calendarCollectionView
{
    if ([self baseCollectionViewController].sections.count && calendarCollectionView.frame.size.height > calendarCollectionView.contentSize.height) {
//        need loading data to contains full size display
        //
        [self settingsCalendarWithType:self.calendarType contentCount:[self baseCollectionViewController].sections.count * 3];
        return;
    }
    if (calendarCollectionView.contentOffset.y < 0.0f && !self.needLockLoadingPastDate) {
        [self appendPastDates];
    }
    
    if (calendarCollectionView.contentOffset.y > fmax(0,(calendarCollectionView.contentSize.height - CGRectGetHeight(calendarCollectionView.bounds))) && !self.isNeedLockLoadingFutureDate && !self.needScrollToBottom) {
        [self appendFutureDates];
    }
}

- (void) calendarCollectionViewDidLayoutSubviews:(MEFCalendarCollectionView *)calendarCollectionView
{
    if (self.isNeedScrollToBottom) {
        self.needScrollToBottom = false;
        [self.baseCollectionViewController.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.baseCollectionViewController.sections.count - 1] atScrollPosition:UICollectionViewScrollPositionBottom animated:false];
        
    }

}

#pragma mark - - navitation


#pragma  mark - - gesture delegate

- (void) panGesture:(UIPanGestureRecognizer*)panGesture
{
    switch (panGesture.state) {
        case UIGestureRecognizerStateBegan:
        {
        }   break;
        case UIGestureRecognizerStateChanged:
        {
            
        }   break;
        case UIGestureRecognizerStateEnded:
        {
            
        }   break;
        case UIGestureRecognizerStateCancelled:
        {
            
        }   break;
        default:
            break;
    }
}
- (MEFSegmentedControl*) segmentedControl
{
    if (!_segmentedControl) {
        _segmentedControl = [[MEFSegmentedControl alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 54)];
        [_segmentedControl setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    }
    return _segmentedControl;
}


- (NSCalendar*) calendarUTC
{
    if (!_calendarUTC) {
        _calendarUTC = [NSCalendar calendarUTC];
    }
    return _calendarUTC;
}
- (NSCalendar*) calendarLocal
{
    if (!_calendarLocal) {
        _calendarLocal = [NSCalendar myCurrentCalendar];
    }
    return _calendarLocal;
}
@end
