//
//  MEFSplitViewRightSegue.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 24.09.15.
//  Copyright © 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFSplitViewRightSegue.h"
#import "MEFSplitViewController.h"
@implementation MEFSplitViewRightSegue
- (void) perform
{
    id sourceViewController = self.sourceViewController;
    
    if (![sourceViewController isKindOfClass:[MEFSplitViewController class]]) {
        @throw [NSException exceptionWithName:@"Source View controller not is king class MEFSplitViewController" reason:nil userInfo:nil];
    }
    
    id destinationViewController = self.destinationViewController;;
    MEFSplitViewController *splitViewController = sourceViewController;
    [splitViewController setRightViewController:destinationViewController animated:self.animated];
    
}
@end
