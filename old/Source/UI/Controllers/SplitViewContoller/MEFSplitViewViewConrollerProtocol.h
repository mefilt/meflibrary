//
//  MEFSplitViewViewConrollerProtocol.h
//  Katusha
//
//  Created by Prokofev Ruslan on 31.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol MEFSplitViewViewConrollerProtocol <NSObject>


@optional;
@property (nonatomic, readwrite, assign) CGFloat shiftViewControllerOffSet;
@end
