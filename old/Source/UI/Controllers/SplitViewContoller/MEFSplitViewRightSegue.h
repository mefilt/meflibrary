//
//  MEFSplitViewRightSegue.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 24.09.15.
//  Copyright © 2015 Prokofev Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEFSplitViewRightSegue : UIStoryboardSegue
@property (nonatomic, readwrite, assign, getter=isAnimated) BOOL animated;
@end
