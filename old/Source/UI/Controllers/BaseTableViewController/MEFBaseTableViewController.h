//
//  MEFBaseViewController.h
//  ipad_project
//
//  Created by Прокофьев Руслан on 24.09.13.
//  Copyright (c) 2013 Прокофьев Руслан. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MEFObject.h"
#import "MEFRefreshControlManager.h"

@class MEFBaseTableViewController;

@protocol MEFBaseTableViewControllerDatasource <NSObject>

@required
- (void) baseTableController:(MEFBaseTableViewController*)controller configTableViewCell:(UITableViewCell *)cell object:(MEFObject *)object indexPath:(NSIndexPath *)indexPath;
@optional
- (UITableViewCell *) baseTableController:(MEFBaseTableViewController*)controller createTableViewCellWithObject:(MEFObject *)object reuseIdentifier:(NSString *)reuseIdentifier indexPath:(NSIndexPath *)indexPath;
- (NSString*) baseTableController:(MEFBaseTableViewController*)controller reuseIdentifierWithObject:(MEFObject *)object  indexPath:(NSIndexPath *)indexPath;
- (UIView *) createPlaceholderWithBaseTableViewController:(MEFBaseTableViewController*)controller;
@end



@protocol MEFBaseTableViewControllerScrollView <NSObject>
@optional
- (void) baseTableController:(MEFBaseTableViewController*)controller scrollViewDidScroll:(UIScrollView *)scrollView;
- (void) baseTableController:(MEFBaseTableViewController*)controller scrollViewWillBeginDragging:(UIScrollView *)scrollView;
- (void) baseTableController:(MEFBaseTableViewController*)controller scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset;
- (void) baseTableController:(MEFBaseTableViewController*)controller scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void) baseTableController:(MEFBaseTableViewController*)controller scrollViewWillBeginDecelerating:(UIScrollView *)scrollView;;
- (void) baseTableController:(MEFBaseTableViewController*)controller scrollViewDidEndDecelerating:(UIScrollView *)scrollView;
- (void) baseTableController:(MEFBaseTableViewController*)controller scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;
@end


@protocol MEFBaseTableViewControllerFloatingView <NSObject>
@optional
- (UIView*) viewForFloatingHeaderWithBaseTableViewController:(MEFBaseTableViewController*)controller;
- (CGFloat) heightForFloatingHeaderWithBaseTableViewController:(MEFBaseTableViewController*)controller;

@end




@protocol MEFBaseTableViewControllerDelegate <NSObject, MEFBaseTableViewControllerScrollView, MEFBaseTableViewControllerFloatingView>
@optional
- (void) didReloadWithBaseTableController:(MEFBaseTableViewController*)controller;

- (void) baseTableController:(MEFBaseTableViewController*)controller didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (void) baseTableController:(MEFBaseTableViewController*)controller didDeselectRowAtIndexPath:(NSIndexPath *)indexPath;

- (NSIndexPath *) baseTableController:(MEFBaseTableViewController*)controller willSelectRowAtIndexPath:(NSIndexPath *)indexPath;

- (void) baseTableController:(MEFBaseTableViewController*)controller didHighlightRowAtIndexPath:(NSIndexPath *)indexPath;
- (void) baseTableController:(MEFBaseTableViewController*)controller didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath;

- (CGFloat) baseTableController:(MEFBaseTableViewController*)controller  heightForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSString*) baseTableController:(MEFBaseTableViewController *)controller titleForHeaderInSection:(NSInteger)section;

- (CGFloat) baseTableController:(MEFBaseTableViewController*)controller estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath;

#pragma mark - - refresh content
- (void) baseTableController:(MEFBaseTableViewController*)controller refreshContent:(UIRefreshControl *)refreshControl;
- (void) baseTableController:(MEFBaseTableViewController*)controller bottomRefreshContent:(UIView <MEFRefreshControlPropertyProtocol>*)refreshControl;

#pragma mark - - Header
- (CGFloat) baseTableController:(MEFBaseTableViewController*)controller heightForHeaderInSection:(NSInteger)section;
- (UIView *) baseTableController:(MEFBaseTableViewController*)controller viewForHeaderInSection:(NSInteger)section;

#pragma mark - - Footer
- (CGFloat) baseTableController:(MEFBaseTableViewController*)controller heightForFooterInSection:(NSInteger)section;
- (UIView *) baseTableController:(MEFBaseTableViewController*)controller viewForFooterInSection:(NSInteger)section;

#pragma mark - - display cell
- (void) baseTableController:(MEFBaseTableViewController*)controller willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
- (void) baseTableController:(MEFBaseTableViewController*)controller didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath;



- (BOOL) baseTableController:(MEFBaseTableViewController*)controller swapObjectAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath;

- (BOOL) baseTableController:(MEFBaseTableViewController*)controller canMoveRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSIndexPath *) baseTableController:(MEFBaseTableViewController*)controller targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath
                                      toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath;


- (void) baseTableController:(MEFBaseTableViewController*)controller willBeginReorderingRowAtIndexPath:(NSIndexPath *)indexPath;
- (void) baseTableController:(MEFBaseTableViewController*)controller didEndReorderingRowAtIndexPath:(NSIndexPath *)indexPath;
- (void) baseTableController:(MEFBaseTableViewController*)controller didCancelReorderingRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger) baseTableController:(MEFBaseTableViewController*)controller indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void) baseTableController:(MEFBaseTableViewController*)controller accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath;

- (void) baseTableController:(MEFBaseTableViewController*)controller deleteSingleRowWithObject:(MEFObject *) deletedObject;


#pragma mark - - section collapse
- (BOOL) baseTableController:(MEFBaseTableViewController*)controller sectionIsCollapseDuringReloadWithSection:(MEFObject*)section;

@end




@interface MEFBaseTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, MEFBaseTableViewControllerDelegate>

@property (weak, nonatomic) id <MEFBaseTableViewControllerDelegate> delegate;
@property (weak, nonatomic) id <MEFBaseTableViewControllerDatasource> datasource;


@property (strong, nonatomic, readwrite) UISearchBar *searchBar;

@property (strong, nonatomic, readonly) NSMutableArray *sections;
@property (strong, nonatomic, readonly) NSMutableArray *editedObjects;

@property (assign, nonatomic, readwrite, getter=isEnabledDragMode) BOOL enabledDragMode;
@property (assign, nonatomic, readwrite, getter=isEnabledEditMode) BOOL enabledEditMode;
@property (assign, nonatomic, readwrite, getter=isEnabledSearchBar) BOOL enabledSearchBar;
@property (assign, nonatomic, readwrite, getter=isEnabledRefreshControl) BOOL enabledRefreshControl;
@property (assign, nonatomic, readwrite, getter=isEnabledBottomRefreshControl) BOOL enabledBottomRefreshControl;
@property (assign, nonatomic, readwrite, getter=isEnabledStickHeaderView) BOOL enabledStickHeaderView;

@property (assign, nonatomic, readonly, getter=isEmpty) BOOL empty;
@property (strong, nonatomic, readonly) NSIndexPath *currentReorderingRow;

@property (strong, nonatomic, readwrite) UIView <MEFRefreshControlPropertyProtocol> *bottomRefreshControl;

@property (copy, nonatomic, readwrite) MEFObjectSearch sectionSearch;
@property (copy, nonatomic, readwrite) MEFObjectSort sectionSort;
@property (copy, nonatomic, readwrite) MEFObjectGrouping sectionGrouping;

@property (nonatomic, readwrite, assign) UIView *topheaderStickView;
@property (nonatomic, readwrite, assign) UIEdgeInsets contentInset;

#pragma mark - - other methods
- (NSIndexPath *)previousIndexPath:(NSIndexPath *)oldIndexPath;
- (NSIndexPath *)nextIndexPath:(NSIndexPath *)oldIndexPath;
- (void) cancelEdited;
- (NSUInteger) countObjects;


#pragma mark - - load/clean data
- (void) reloadData;
- (void) cleanup;
- (void) updates:(void (^)(MEFBaseTableViewController *controller, UITableView *tableView))updates;


#pragma add methods
- (void) addSection:(MEFObject*)section;
- (void) addSections:(NSArray*)sections;

#pragma mark - - methods for get object/section by params
- (UITableViewCell*) cellAtObject:(MEFObject*)object;
- (NSIndexPath*) indexPathAtObject:(MEFObject*)object;
- (MEFObject *)objectFromSectionsAtKey:(NSString *)key;
- (MEFObject*) sectionAtKey:(NSString*)key;
- (MEFObject*) sectionAtIndex:(NSInteger) index;
- (MEFObject*) objectAtIndexPath:(NSIndexPath*)indexPath;
- (MEFObject *)objectAtIndex:(int)index row:(int)row;
- (NSIndexPath*) indexPathAtSection:(MEFObject*)section;
- (NSString*) identifierCellWithIndexPath:(NSIndexPath*)indexPath;
#pragma mark - - dequeue reusable section
- (MEFObject*) dequeueReusableSectionWithKey:(NSString*)key;


#pragma mark - - setContent inset

#pragma mark - - control refresh
- (void) beginRefreshingControl;
- (void) endRefreshingControl;

#pragma mark - - bottom control refresh
- (void) endBottomRefreshingControl;



#pragma mark - - search methods
- (void) searchSectionWithText:(NSString*)text;
- (void) cancelSearchSection;
- (void) showSearchBarWithAnimated:(BOOL)animated;
- (void) hideSearchBarWithAnimated:(BOOL)animated;

#pragma mark - - insert/delete/refresh methods


- (void) deleteRowWithIndexPath:(NSIndexPath*)indexPath;
- (void) deleteRowWithIndexPath:(NSIndexPath*)indexPath withRowAnimation:(UITableViewRowAnimation)rowAction;


- (void) deleteSectionsWithAnimation:(UITableViewRowAnimation)rowAnimation;
- (void) insertRowWithIndexPath:(NSIndexPath*)indexPath byObject:(MEFObject *)object;
- (void) insertRowWithIndexPath:(NSIndexPath*)indexPath byObject:(MEFObject *)object withRowAnimation:(UITableViewRowAnimation)animation;
- (void) insertRowsWithIndexPath:(NSIndexPath*)indexPath objects:(NSArray *)objects withRowAnimation:(UITableViewRowAnimation)animation;
- (void) deleteRowsInSection:(NSUInteger)section withRowAnimation:(UITableViewRowAnimation)rowAnimation;


#pragma mark - - collapse methods
- (void) collapseSectionWithIndexPath:(NSIndexPath*)indexPath;
- (void) uncollapsedSectionWithIndexPath:(NSIndexPath*)indexPath;
- (BOOL) isCollapseSectionWithIndexPath:(NSIndexPath*)indexPath;

- (void) reloadSectionsWithAnimation:(UITableViewRowAnimation)rowAnimation;


@end


@interface MEFBaseTableViewController (Modify)
- (void) willReloadData;
- (void) modifySections:(NSMutableArray*)sections;
- (void) didReloadData;
@end