//
//  MEFViewController.m
//  Katusha
//
//  Created by Prokofev Ruslan on 19.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFViewController.h"
#import "MEFAdaptiveNavigationController.h"

@implementation UINavigationItem (Spacer)

+ (UIBarButtonItem*)negativeSpacerWithWidth:(CGFloat) width
{
    UIBarButtonItem *item = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                             target:nil
                             action:nil];
    item.width = width;//(width >= 0 ? -width : width);
    return item;
}

@end

@implementation NSArray (Union)

+ (NSArray*) unionWithArray:(NSArray*)array secondArray:(NSArray*)secondArray
{
    NSMutableArray *unionArray = [NSMutableArray array];
    [unionArray addObjectsFromArray:array];
    if (secondArray) {
        [unionArray addObjectsFromArray:secondArray];
    }
    
    return unionArray;
}
@end


@interface MEFNavigationItemsModel : NSObject
@property (nonatomic, readwrite, strong) NSArray *items;
@property (nonatomic, readwrite, assign) MEFNavigationItemState state;
@property (nonatomic, readwrite, assign) MEFNavigationItemEdge edge;
@end
@interface UINavigationItem (Spacer)

@end

@interface NSArray (Union)

@end

@implementation MEFNavigationItemsModel
@end

@interface MEFViewController ()
@property (nonatomic, strong) NSMutableDictionary *navigationItemsModelsCash;
@property (nonatomic, assign) MEFNavigationItemState currentState;
@end

@implementation MEFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];



}



- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self reloadStyle];
}



- (void) willMoveToParentViewController:(UIViewController *)parent
{
    if ([parent isKindOfClass:[UINavigationController class]]) {
        [self reloadStyle];
        [self reloadNavigationItems];
    }
    [super willMoveToParentViewController:parent];
}

- (void) didMoveToParentViewController:(UIViewController *)parent
{
    [super didMoveToParentViewController:parent];
}


- (MEFAdaptiveNavigationController*) adaptiveNavigationController
{
    return [self findAdaptiveNavigationControllerFromParent:self];
}

- (MEFAdaptiveNavigationController*) findAdaptiveNavigationControllerFromParent:(UIViewController *)parentViewController
{
    UIViewController *lastParent = parentViewController.parentViewController;
    while (lastParent) {
        if ([lastParent isKindOfClass:[MEFAdaptiveNavigationController class]]) {
            return (MEFAdaptiveNavigationController*)lastParent;
        }
        lastParent = lastParent.parentViewController;
    }
    return nil;
}

- (void)moveToParentAdaptiveNavigationController:(MEFAdaptiveNavigationController *)parent
{

}


#pragma mark  style
- (void) loadingStyleWithParent:(UINavigationController*)navigationController
{
    
}

#pragma GCC diagnostic ignored "-Wwarning-flag"
- (void)setStyleDelegate:(id <MEFViewControllerStyle>)styleDelegate
{
    _styleDelegate = styleDelegate;
    [self reloadStyle];
}

- (void) reloadStyle
{
    
    if ([self.parentViewController isKindOfClass:[UINavigationController class]]) {
        if (self.styleDelegate && [self.styleDelegate respondsToSelector:@selector(loadingStyleWithParent:)]) {
            [self.styleDelegate loadingStyleWithParent:(UINavigationController *) self.parentViewController];
        } else {
            [self loadingStyleWithParent:(UINavigationController*)self.parentViewController];
        }
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void) setExternalNavigationItems:(NSArray*)items edge:(MEFNavigationItemEdge)edge state:(MEFNavigationItemState)state
{
    NSString *hash = [NSString stringWithFormat:@"%lu_%lu",(unsigned long)state,(unsigned long)edge];
    MEFNavigationItemsModel *model = [self navigationItemsModelsCash][hash];
    if (!model) {
        model  = [MEFNavigationItemsModel new];
        [self navigationItemsModelsCash][hash] = model;
    }
    model.items = items;
    model.edge = edge;
    model.state = state;
    if ([self isViewLoaded]) {
        [self reloadNavigationItems];
    }
}

- (NSArray*) createExternalNavigationItems:(MEFNavigationItemEdge)edge state:(MEFNavigationItemState)state
{
    NSString *hash = [NSString stringWithFormat:@"%lu_%lu",(unsigned long)state,(unsigned long)edge];
    MEFNavigationItemsModel *model = [self navigationItemsModelsCash][hash];
    if (!model) {
        return nil;
    }
    return model.items;
}

- (void) reloadNavigationItems
{
    [self reloadNavigationItems:self.currentState];
}

- (void) setEnabledNavigationItems:(BOOL)enabledNavigation
{
    
}
- (void) reloadNavigationItems:(MEFNavigationItemState)state
{
    self.currentState = state;
    
    if (!self.navigationController) {
        return;
    }
    
    CGFloat leftSpacer = [self spacerNavigationItems:MEFNavigationItemEdgeLeft state:state];
    CGFloat rightSpacer = [self spacerNavigationItems:MEFNavigationItemEdgeRigh state:state];
    
    NSArray *leftButtons = [self createNavigationItems:MEFNavigationItemEdgeLeft state:state];
    NSArray *rightButtons = [self createNavigationItems:MEFNavigationItemEdgeRigh state:state];
    
    NSArray *leftExternalButtons = nil;//[self createExternalNavigationItems:MEFNavigationItemEdgeLeft state:state];
    NSArray *rightExternalButtons = nil;//[self createExternalNavigationItems:MEFNavigationItemEdgeRigh state:state];
    
    if (![self disableExternalNavigationItems:MEFNavigationItemEdgeLeft state:state]) {
        leftExternalButtons = [self createExternalNavigationItems:MEFNavigationItemEdgeLeft state:state];
    }
    if (![self disableExternalNavigationItems:MEFNavigationItemEdgeRigh state:state]) {
        rightExternalButtons = [self createExternalNavigationItems:MEFNavigationItemEdgeRigh state:state];
    }
    
    
    NSArray *unionLeft = nil;
    if (leftExternalButtons) {
        unionLeft = [NSArray unionWithArray:leftExternalButtons secondArray:leftButtons];
    } else {
        unionLeft = leftButtons;
    }
    
    NSArray *unionRight = nil;
    if (rightExternalButtons) {
        unionRight = [NSArray unionWithArray:rightExternalButtons secondArray:rightButtons];
    } else {
        unionRight = rightButtons;
    }
    
    if (leftSpacer != 0) {
        unionLeft = [self createNavigationItems:unionLeft withSpacer:leftSpacer];
    }
    if (rightSpacer != 0) {
        unionRight = [self createNavigationItems:unionRight withSpacer:rightSpacer];
    }
    
    [self.navigationItem setLeftBarButtonItems:unionLeft animated:true];
    [self.navigationItem setRightBarButtonItems:unionRight animated:true];
}



- (NSArray*) createNavigationItems:(NSArray*)items withSpacer:(CGFloat)spacer
{
    if (!items) {
        return nil;
    }
    NSMutableArray *new = [NSMutableArray array];
    UIBarButtonItem *spacerItem = [UINavigationItem negativeSpacerWithWidth:spacer];
    [new addObject:spacerItem];
    [new addObjectsFromArray:items];
    return new;
}

- (BOOL) disableExternalNavigationItems:(MEFNavigationItemEdge)edge state:(MEFNavigationItemState)state
{
    return false;
}
- (CGFloat) spacerNavigationItems:(MEFNavigationItemEdge)edge state:(MEFNavigationItemState)state
{
    return 0;
}
- (NSArray*) createNavigationItems:(MEFNavigationItemEdge)edge state:(MEFNavigationItemState)state
{
    return @[];
}

- (NSMutableDictionary*) navigationItemsModelsCash
{
    if (!_navigationItemsModelsCash) {
        _navigationItemsModelsCash = [NSMutableDictionary dictionary];
    }
    return _navigationItemsModelsCash;
}

@end


