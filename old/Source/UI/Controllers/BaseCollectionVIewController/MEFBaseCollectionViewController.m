//
//  MEFBaseCollectionViewController.m
//  ipad_project
//
//  Created by Прокофьев Руслан on 06.11.13.
//  Copyright (c) 2013 Прокофьев Руслан. All rights reserved.
//

#import "MEFBaseCollectionViewController.h"

typedef NS_OPTIONS(NSUInteger, MEFBaseCollectionScrollFlag) {
    MEFBaseCollectionScrollFlagNone = 1 << 0,
    MEFBaseCollectionScrollFlagBeginUserScroll = 1 << 1,
    MEFBaseCollectionScrollFlagVisibleBottomRefreshControlBegin = 1 << 2,
    MEFBaseCollectionScrollFlagVisibleBottomRefreshControlEnd = 1 << 3,
    MEFBaseCollectionScrollFlagIsCompletedBottomRefreshControl = 1 << 4,
    MEFBaseCollectionScrollFlagEndBottomRefreshControl = 1 << 5,
};
@interface MEFBaseCollectionViewController () <UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{

    MEFBaseCollectionScrollFlag _flag;
    BOOL _isBeginRefreshControl;
}
@property (strong, nonatomic, readwrite) NSMutableArray *sections;
@property (strong, nonatomic, readwrite) NSMutableDictionary *hashmap;
@property (strong, nonatomic, readwrite) NSMutableArray *assortedSections;
@property (strong, nonatomic, readwrite) UIRefreshControl *refreshControl;
@end

@implementation MEFBaseCollectionViewController






- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}


- (void) cleanup
{
    [self.sections removeAllObjects];
    [self.assortedSections removeAllObjects];
    [self.hashmap removeAllObjects];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];

}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

#pragma mark - refresh control top/bottom
- (void) setBottomRefreshControl:(UIView<MEFBaseRefreshControlProtocol> *)bottomRefreshControl
{
    _bottomRefreshControl = bottomRefreshControl;
    
    if (!_bottomRefreshControl) {
        return;
    }
    
    CGRect frame = _bottomRefreshControl.frame;
    frame.origin = CGPointMake(0, self.view.frame.size.height);
    frame.size = CGSizeMake(self.view.frame.size.width, 44);
    _bottomRefreshControl.frame = frame;
    [_bottomRefreshControl setAlpha:1];
    [_bottomRefreshControl start];
    [self.collectionView addSubview:_bottomRefreshControl];
}

- (void) setEnabledRefreshControl:(BOOL)isEnabledRefreshControl
{
    if (isEnabledRefreshControl && !self.refreshControl) {
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                            initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 25)];
        [refreshControl addTarget:self action:@selector(refreshContent:) forControlEvents:UIControlEventValueChanged];
        [self.collectionView addSubview:refreshControl];
        self.refreshControl = refreshControl;
    } else {
        [self.refreshControl removeFromSuperview];
        self.refreshControl = nil;
    }
    _enabledRefreshControl = isEnabledRefreshControl;
    
}

- (void) setEnabledRefreshControlBottom:(BOOL)isEnabledRefreshControlBottom
{
    if (isEnabledRefreshControlBottom) {
//        NSAssert(self.bottomRefreshControl != nil, @"UIView <MEFBaseRefreshControlProtocol>*bottomRefreshControl is nil");
    }
    _enabledRefreshControlBottom = isEnabledRefreshControlBottom;
    
}


- (void) endBottomRefreshControlWithCompletion:(void (^)(BOOL completed))completion
{
    if ([self existScrollFlag:MEFBaseCollectionScrollFlagBeginUserScroll]) {
        [self addScrollFlag:MEFBaseCollectionScrollFlagEndBottomRefreshControl];
        completion(false);
        return;
    }
    [self hideEndBottomRefreshControl:^(BOOL completed) {
        completion(true);
    }];
}

- (void) hideEndBottomRefreshControl:(void (^)(BOOL completed))completion
{
    _flag = MEFBaseCollectionScrollFlagNone;
    if (self.delegate && [self.delegate respondsToSelector:@selector(hideBeginRefreshControlWithBaseCollectionViewController:)]) {
        [self.delegate hideBeginRefreshControlWithBaseCollectionViewController:self];
    }
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionLayoutSubviews animations:^{
        [self.collectionView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    } completion:^(BOOL finished) {
        [self.bottomRefreshControl setHidden:true];
        [self.bottomRefreshControl end];
        completion(finished);
    }];
}

- (void) beginRefreshControl
{
    if (!_isBeginRefreshControl) {
        [self.collectionView setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:YES];
    }

    [self.refreshControl beginRefreshing];
}


- (void) endRefreshControl
{
    if (!_isBeginRefreshControl) {
        [self.collectionView setContentOffset:CGPointMake(0, 0) animated:YES];        
    }
    _isBeginRefreshControl = false;
    [self.refreshControl endRefreshing];
}

- (void) refreshContent:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(refreshCollectionContentWithBaseCollectionViewController:)]) {
        [self.delegate refreshCollectionContentWithBaseCollectionViewController:self];
    }
}







- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - - public Methods




- (BOOL) isEmpty
{
    return self.assortedSections.count == 0;
}
- (void) reloadData
{
    NSAssert(self.datasource != nil, @"DataSource - nil");
    
//    id o = self.collectionView.delegate;
//    id p = self.collectionView.dataSource;
    
    [self willReloadData];
    [self.collectionView reloadData];
}



- (void) addSectionByIndex:(NSInteger)row section:(MEFObject *)section
{
    NSInteger count = self.sections.count;
    if (row < count) {
        [self.sections insertObject:section atIndex:row];
    } else {
        [self.sections addObject:section];
    }
    
    if (section.key) {
        [self.hashmap setObject:section forKey:section.key];
    }
}

- (void) addSection:(MEFObject*)object
{
    if (object.key) {
        [self.hashmap setObject:object forKey:object.key];
    }
    [self.sections addObject:object];
}

- (MEFObject*) sectionAtIndex:(NSInteger) index
{
    if (index >= self.assortedSections.count) {
        return Nil;
    }
    return [self.assortedSections objectAtIndex:index];
}

- (MEFObject*) objectAtIndexPath:(NSIndexPath*)indexPath
{
    MEFObject *section = [self sectionAtIndex:indexPath.section];
    if (!section) {
        return Nil;
    }
    return [section itemAtIndex:indexPath.row];
}
- (MEFObject*) sectionAtKey:(NSString*) key
{
    return self.hashmap[key];
}




- (void) reloadSection:(MEFObject*)section
{
    [self willLoadSection:section];
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:[self.assortedSections  indexOfObject:section]]];
}
- (void) reloadSections
{
    [self willReloadData];
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.assortedSections.count)]];
}

- (void) insertSections:(NSArray*)sections toSectionByIndex:(NSUInteger)index
{
    for (MEFObject *section in sections) {
        [self addSectionByIndex:index section:section];
        [self willLoadSection:section];
    }
    [self.collectionView insertSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(index, sections.count)]];
}
#pragma mark insert objects
- (void) insertObjects:(NSArray*)objects toSection:(MEFObject*)section
{
    [self insertObjects:objects toIndexPath:[NSIndexPath indexPathForRow:0 inSection:[self.assortedSections indexOfObject:section]]];
}

- (void) insertObjectsToLastSection:(NSArray*)objects
{
    [self insertObjects:objects toIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.assortedSections.count - 1]];
}
- (void) insertObjects:(NSArray*)objects toIndexPath:(NSIndexPath*)indexPath;
{
    MEFObject *section = [self sectionAtIndex:indexPath.section];
    NSUInteger countBegin = section.items.count;
    [section addItems:objects];
    [self willLoadSection:section];
    
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (NSUInteger i = countBegin; i < section.items.count; ++i) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
    }
    [self.collectionView insertItemsAtIndexPaths:indexPaths];
}

- (void) deleteItemsAtSection:(MEFObject *)section
{
    NSUInteger index = [self.assortedSections indexOfObject:section];
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (int i = 0;i < section.items.count; ++i) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:index]];
    }
    [section removeItems];
    [self.collectionView deleteItemsAtIndexPaths:indexPaths];
}



#pragma mark - - Collection data source


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    
    return self.assortedSections.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{

    MEFObject *object = [self sectionAtIndex:section];
    if (!object) {
        return 0;
    }
    NSUInteger count = object.items.count;
    return count;
}


- (UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MEFObject *object = [self objectAtIndexPath:indexPath];
    if (!object) {
        
        return nil;
        
    }
    NSString *indentifierObject = [self indentifierObject:object];

    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:indentifierObject forIndexPath:indexPath];
    
    if (!cell && self.datasource && [self.datasource respondsToSelector:@selector(baseCollectionViewController:createCollectionViewCellWithObject:cellForItemAtIndexPath:)]) {
        cell = [self.datasource baseCollectionViewController:self createCollectionViewCellWithObject:object cellForItemAtIndexPath:indexPath];
    }
    if (self.datasource && [self.datasource respondsToSelector:@selector(baseCollectionViewController:configCollectionViewCellWithObject:cellForItemAtIndexPath:cell:)]) {
        [self.datasource baseCollectionViewController:self configCollectionViewCellWithObject:object cellForItemAtIndexPath:indexPath cell:cell];
    }
    return cell;
}


#pragma mark -- delegate
- (BOOL) collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:shouldHighlightItemAtIndexPath:)]) {
        return [self.delegate baseCollectionViewController:self shouldHighlightItemAtIndexPath:indexPath];
    }
    return YES;
}



- (UICollectionReusableView*) collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:viewForSupplementaryElementOfKind:atIndexPath:)]) {
       return [self.delegate baseCollectionViewController:self viewForSupplementaryElementOfKind:kind atIndexPath:indexPath];
    }
    return nil;
}



- (UIEdgeInsets) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:layout:insetForSectionAtIndex:)]) {
        return [self.delegate baseCollectionViewController:self layout:collectionViewLayout insetForSectionAtIndex:section];
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void) collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:didHighlightItemAtIndexPath:)]) {
        [self.delegate baseCollectionViewController:self didHighlightItemAtIndexPath:indexPath];
    }
}

- (void) collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:didUnhighlightItemAtIndexPath:)]) {
        [self.delegate baseCollectionViewController:self didUnhighlightItemAtIndexPath:indexPath];
    }
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:didSelectItemAtIndexPath:)]) {
        [self.delegate baseCollectionViewController:self didSelectItemAtIndexPath:indexPath];
    }
}


#pragma mark - -  Flow layout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:sizeForItemAtIndexPath:layout:)]) {
        return  [self.delegate baseCollectionViewController:self sizeForItemAtIndexPath:indexPath layout:collectionViewLayout];
    }
    return CGSizeMake(0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:layout:referenceSizeForHeaderInSection:)]) {
        return  [self.delegate baseCollectionViewController:self layout:collectionViewLayout referenceSizeForHeaderInSection:section];
    }
    return CGSizeMake(0, 0);
}



//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
//{
//    return 0;
//}
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//{
//    return 0;
//}
//
//- (void) collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:didDeselectItemAtIndexPath:)]) {
//        return  [self.delegate baseCollectionViewController:self didDeselectItemAtIndexPath:indexPath];
//    }
//}
//- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:didSelectItemAtIndexPath:)]) {
//        return  [self.delegate baseCollectionViewController:self didSelectItemAtIndexPath:indexPath];
//    }
//}
//
//- (void) collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:didEndDisplayingCell:forItemAtIndexPath:)]) {
//        [self.delegate baseCollectionViewController:self didEndDisplayingCell:cell forItemAtIndexPath:indexPath];
//    }
//
//}

- (NSMutableArray*) sortSections:(NSMutableArray*)sections
{
    
    return [NSMutableArray arrayWithArray:sections];
}

- (void) registerClasseForObject:(MEFObject*)section
{
    NSMutableDictionary *types = [NSMutableDictionary dictionary];
    for (MEFObject *object in section.items) {
        NSString *indentifierObject = [self indentifierObject:object];
        if ([types objectForKey:indentifierObject]) {
            continue;
        }
        [types setObject:object forKey:indentifierObject];
    }
    [self registerClassesWithDictionary:types];
}

- (void) registerClassesWithDictionary:(NSDictionary*)types
{
    for (NSString *key in types.allKeys) {
        MEFObject *object = types[key];
        Class class;
        UINib *nib;
        if (self.delegate && [self.datasource respondsToSelector:@selector(baseCollectionViewController:registerCollectionClass:)]) {
            class = [self.datasource baseCollectionViewController:self registerCollectionClass:object];
        }
        if (class != Nil) {
            [self.collectionView registerClass:class forCellWithReuseIdentifier:key];
        }
        if (self.datasource && [self.datasource respondsToSelector:@selector(baseCollectionViewController:registerCollectionNib:)]) {
            nib = [self.datasource baseCollectionViewController:self registerCollectionNib:object];
        }
        if (nib != nil) {
            [self.collectionView registerNib:nib forCellWithReuseIdentifier:key];
        }
        
    }
}
- (void) registerClasses:(NSArray*)objects
{
    NSMutableDictionary *types = [NSMutableDictionary dictionary];
    
    for (MEFObject *section in objects) {
        for (MEFObject *object in section.items) {
            NSString *indentifierObject = [self indentifierObject:object];
            if ([types objectForKey:indentifierObject]) {
                continue;
            }
            [types setObject:object forKey:indentifierObject];
        }
    }
    [self registerClassesWithDictionary:types];
}

- (NSString*) indentifierObject:(MEFObject*)object
{
    NSString *indentifierObject = nil;
    if (self.datasource && [self.datasource respondsToSelector:@selector(baseCollectionViewController:reuseIdentifierWithObject:)]) {
        indentifierObject = [self.datasource baseCollectionViewController:self reuseIdentifierWithObject:object];
    } else {
        indentifierObject = [NSString stringWithFormat:@"%d", object.type];
    }
    return indentifierObject;
}

#pragma mark - - scroll view flag
- (void) addScrollFlag:(MEFBaseCollectionScrollFlag)flag
{
    _flag = _flag | flag;
}

- (BOOL) existScrollFlag:(MEFBaseCollectionScrollFlag)flag
{
    return (_flag & flag) != 0;
}

- (void) removeScrollFlag:(MEFBaseCollectionScrollFlag)flag
{
    _flag = _flag & ~flag;
}



#pragma mark - - Scroll view delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:scrollViewDidScroll:)]) {
        [self.delegate baseCollectionViewController:self scrollViewDidScroll:scrollView];
    }
    static NSInteger previousPage = 0;
    CGFloat pageWidth = scrollView.frame.size.width;
    CGFloat fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    if (previousPage != page) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:didScrollWithPage:)]) {
            [self.delegate baseCollectionViewController:self didScrollWithPage:page];
        }
        previousPage = page;
    }
    
    CGFloat contentSize = scrollView.contentSize.height < scrollView.frame.size.height ? scrollView.frame.size.height : scrollView.contentSize.height;
    CGFloat endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    CGFloat offSet = endScrolling - contentSize;
    
    if (![self existScrollFlag:MEFBaseCollectionScrollFlagBeginUserScroll] || (endScrolling < contentSize) || (offSet <= 0)) {
        if ([self existScrollFlag:MEFBaseCollectionScrollFlagVisibleBottomRefreshControlBegin]) {
            CGRect frame = self.bottomRefreshControl.frame;
            frame.origin.y = endScrolling - frame.size.height * 1.5;
            self.bottomRefreshControl.frame = frame;
        }
        return;
    }
    
    if (![self existScrollFlag:MEFBaseCollectionScrollFlagVisibleBottomRefreshControlBegin]) {
        [self addScrollFlag:MEFBaseCollectionScrollFlagVisibleBottomRefreshControlBegin];
        [self.bottomRefreshControl setHidden:false];
        CGRect frame = self.bottomRefreshControl.frame;
        frame.size = CGSizeMake(self.view.frame.size.width, self.bottomRefreshControl.height);
        frame.origin.y = endScrolling + frame.size.height;
        self.bottomRefreshControl.frame = frame;
        [self.bottomRefreshControl setAlpha:1];
        [self.bottomRefreshControl setPercent:0];
        [self.bottomRefreshControl start];
    }
 
    
    CGFloat maxOffset = self.bottomRefreshControl.frame.size.height + self.bottomRefreshControl.frame.size.height;
    if (offSet <= self.bottomRefreshControl.frame.size.height) {
        CGRect frame = self.bottomRefreshControl.frame;
        frame.origin.y = endScrolling - frame.size.height * (offSet / (self.bottomRefreshControl.frame.size.height * 1.5));
        self.bottomRefreshControl.frame = frame;
    } else {
        CGRect frame = self.bottomRefreshControl.frame;
        frame.origin.y = endScrolling - frame.size.height * 1.5;
        self.bottomRefreshControl.frame = frame;
    }
    
    if (![self existScrollFlag:MEFBaseCollectionScrollFlagVisibleBottomRefreshControlEnd]) {
        [self addScrollFlag:MEFBaseCollectionScrollFlagVisibleBottomRefreshControlEnd];
        if (self.delegate && [self.delegate respondsToSelector:@selector(showBeginRefreshControlWithBaseCollectionViewController:)]) {
            [self.delegate showBeginRefreshControlWithBaseCollectionViewController:self];
        }
    }

    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut animations:^{
        [self.collectionView setContentInset:UIEdgeInsetsMake(0, 0.0, offSet , 0.0)];
    } completion:^(BOOL finished) {
        
    }];
    
    if ([self existScrollFlag:MEFBaseCollectionScrollFlagIsCompletedBottomRefreshControl]) {
        return;
    }
    CGFloat percent = (endScrolling - scrollView.contentSize.height - maxOffset) /  (self.bottomRefreshControl.frame.size.height + self.bottomRefreshControl.height);
    percent = fmin(percent, 1);
    percent = fmax(percent, 0);
   
    [self.bottomRefreshControl setPercent:percent];
    if (percent == 1) {
        [self addScrollFlag:MEFBaseCollectionScrollFlagIsCompletedBottomRefreshControl];
        [self.bottomRefreshControl start];
        if (self.delegate && [self.delegate respondsToSelector:@selector(bottomRefreshContentWithBaseCollectionViewController:)]) {
            [self.delegate bottomRefreshContentWithBaseCollectionViewController:self];

        }
    }
    

}


#pragma mark - - scroll delegate
- (void) scrollViewDidScrollToTop:(UIScrollView *)scrollView
{
    
}



- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (_enabledRefreshControlBottom && ![self isEmpty]) {
        _flag = MEFBaseCollectionScrollFlagNone | MEFBaseCollectionScrollFlagBeginUserScroll;
    }
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:scrollViewDidEndDragging:willDecelerate:)]) {
        [self.delegate baseCollectionViewController:self scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
    if (![self existScrollFlag:MEFBaseCollectionScrollFlagBeginUserScroll]) {
        return;
    }


    if ([self existScrollFlag:MEFBaseCollectionScrollFlagIsCompletedBottomRefreshControl]) {
        CGFloat maxOffset = self.bottomRefreshControl.frame.size.height + self.bottomRefreshControl.frame.size.height;
        [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut animations:^{
            [self.collectionView setContentInset:UIEdgeInsetsMake(0, 0.0, maxOffset, 0.0)];
        } completion:^(BOOL finished) {
            if ([self existScrollFlag:MEFBaseCollectionScrollFlagEndBottomRefreshControl]) {
                [self hideEndBottomRefreshControl:^(BOOL completed) {
                    
                }];
            }
        }];
    } else if ([self existScrollFlag:MEFBaseCollectionScrollFlagVisibleBottomRefreshControlBegin] && !decelerate) {
        [self removeScrollFlag:MEFBaseCollectionScrollFlagVisibleBottomRefreshControlBegin];
        [self hideEndBottomRefreshControl:^(BOOL completed) {
            
        }];
    }
    if (!decelerate) {
        [self removeScrollFlag:MEFBaseCollectionScrollFlagBeginUserScroll];
    }
}

- (void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseCollectionViewController:scrollViewDidEndDecelerating:)]) {
        [self.delegate baseCollectionViewController:self scrollViewDidEndDecelerating:scrollView];
    }
    
    if (![self existScrollFlag:MEFBaseCollectionScrollFlagBeginUserScroll]) {
        return;
    }
    if ([self existScrollFlag:MEFBaseCollectionScrollFlagIsCompletedBottomRefreshControl]) {
        CGFloat maxOffset = self.bottomRefreshControl.frame.size.height + self.bottomRefreshControl.frame.size.height;
        [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut animations:^{
            [self.collectionView setContentInset:UIEdgeInsetsMake(0, 0.0, maxOffset, 0.0)];
        } completion:^(BOOL finished) {
            if ([self existScrollFlag:MEFBaseCollectionScrollFlagEndBottomRefreshControl]) {
                [self hideEndBottomRefreshControl:^(BOOL completed) {
                    
                }];
            }
        }];

    } else if ([self existScrollFlag:MEFBaseCollectionScrollFlagVisibleBottomRefreshControlBegin]) {
        [self removeScrollFlag:MEFBaseCollectionScrollFlagVisibleBottomRefreshControlBegin];
        [self hideEndBottomRefreshControl:^(BOOL completed) {
            
        }];
    }
    [self removeScrollFlag:MEFBaseCollectionScrollFlagBeginUserScroll];
}


#pragma mark - - load/ reload data

- (void) willLoadSection:(MEFObject*)section
{
    [section reloadObjects];
    [self registerClasseForObject:section];
    
}
- (void) willReloadData
{
    [self.assortedSections removeAllObjects];
    NSMutableArray *assorted_sections = [NSMutableArray arrayWithArray:self.sections];
    if (self.sectionSort) {
		__weak MEFBaseCollectionViewController *weakSelf = self;
		[assorted_sections sortUsingComparator: ^NSComparisonResult (id obj1, id obj2) {
		    return weakSelf.sectionSort(obj1, obj2);
		}];
	}
    [self.assortedSections addObjectsFromArray:assorted_sections];
    for (MEFObject *section in self.assortedSections) {
        [section reloadObjects];
    }
    [self registerClasses:self.assortedSections];
}

#pragma mark - - Properties
- (NSMutableArray*) sections
{
    if (!_sections) {
        _sections = [NSMutableArray new];
    }
    return _sections;
}
- (NSMutableArray*) assortedSections
{
    if (!_assortedSections) {
        _assortedSections = [NSMutableArray new];
    }
    return _assortedSections;
}

- (NSMutableDictionary*) hashmap
{
    if (!_hashmap) {
        _hashmap = [NSMutableDictionary dictionary];
    }
    return _hashmap;
}

@end
