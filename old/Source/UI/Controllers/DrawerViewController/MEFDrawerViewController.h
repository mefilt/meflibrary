//
//  BSNavigationCardViewController.h
//  ipad_project
//
//  Created by Прокофьев Руслан on 16.01.14.
//  Copyright (c) 2014 Прокофьев Руслан. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MEFDrawerViewControllerStyle) {
    MEFDrawerViewControllerStyleOverSlide,
    MEFDrawerViewControllerStyleShiftSlide,
};

typedef NS_ENUM(NSUInteger, MEFDrawerViewControllerDrawerState) {
    MEFDrawerViewControllerDrawerStateOpen,
    MEFDrawerViewControllerDrawerStateClose
};

@protocol MEFDrawerViewControllerDataSource <NSObject>

- (UIViewController *)drawerViewController;
- (UIViewController *)drawerMainViewController;
@end

@protocol MEFDrawerCardViewControllerDelegate <NSObject>
- (CGFloat)widthForDrawerViewController;
@end

/**
 MEFDrawerViewController support segue and storyboard
panel segue and main segue set indificator "embed"
 **/


static NSString * const MEFDrawerPanelSegueIndificator = @"drawePanelSegue";
static NSString * const MEFDrawerMainSegueIndificator = @"draweMainSegue";


static NSString * const MEFDrawerViewControllerWillDraggingDrawerNotification = @"MEFDrawerViewControllerWillDragDrawerNotification";
static NSString * const MEFDrawerViewControllerShowDrawerNotification = @"MEFDrawerViewControllerShowDrawerNotification";
static NSString * const MEFDrawerViewControllerHideDrawerNotification = @"MEFDrawerViewControllerHideDrawerNotification";

@interface MEFDrawerViewController : UIViewController

@property (strong, nonatomic, readwrite) UIViewController *mainViewController ;
@property (strong, nonatomic, readwrite) UIViewController *drawerViewController;

@property (weak, nonatomic) NSObject <MEFDrawerViewControllerDataSource> *dataSource;
@property (weak, nonatomic) NSObject <MEFDrawerCardViewControllerDelegate> *delegate;
@property (assign, nonatomic, readwrite) MEFDrawerViewControllerStyle styleDrawer;
@property (nonatomic, readwrite, assign, getter=isEnabledDrawer) BOOL enabledDrawer;
@property (assign, nonatomic, readwrite) IBInspectable CGFloat widthDrawerPanel;

- (id)initWithDrawerViewController:(UIViewController *)drawerViewController mainController:(UIViewController*)mainController;
- (void) showDrawerViewControllerWithAnimated:(BOOL)animated;
- (void) hideDrawerViewControllerWithAnimated:(BOOL)animated;
- (id) setMainViewControllerWithSegueIdentifier:(NSString*)identifier animated:(BOOL)animated;



@end
