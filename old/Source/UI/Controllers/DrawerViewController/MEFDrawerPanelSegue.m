//
//  MEFDrawerPanelSegue.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 07.06.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFDrawerPanelSegue.h"
#import "MEFDrawerViewController.h"
@implementation MEFDrawerPanelSegue


- (void) perform
{
    id sourceViewController = self.sourceViewController;
    
    if (![sourceViewController isKindOfClass:[MEFDrawerViewController class]]) {
        @throw [NSException exceptionWithName:@"Source View controller not is king class MEFDrawerViewController" reason:nil userInfo:nil];
    }
    
    id destinationViewController = self.destinationViewController;;
    
    MEFDrawerViewController *drawer = sourceViewController;
    [drawer setDrawerViewController:destinationViewController];
}
@end
