//
//  BSNavigationCardViewController.m
//  ipad_project
//
//  Created by Прокофьев Руслан on 16.01.14.
//  Copyright (c) 2014 Прокофьев Руслан. All rights reserved.
//
#import "MEFDrawerViewController.h"
#import "MEFPanGestureRecognizer.h"
#import "MEFConstants.h"
#import "CGPointExtension.h"
#import <Masonry/Masonry.h>
typedef NS_OPTIONS(NSUInteger, MEFDrawerViewControllerOption) {
    MEFDrawerViewControllerDrawerStateOptionNone = 1 << 0,
    MEFDrawerViewControllerDrawerStateOptionOpenClose = 1 << 1,
    MEFDrawerViewControllerDrawerStateOptionGesturePan = 1 << 2,
};

const static CGFloat MEFNavigationCardViewControllerXSwipe = 70.0f;
const static CGFloat MEFNavigationCardViewControllerPercentForHide = 0.7f;
const static CGFloat MEFNavigationCardViewControllerPercentForShow = 0.3f;
const static CGFloat MEFNavigationCardViewControllerShadowAlpha = 0.4f;

const static CGFloat MEFDrawerVisibleWidth = 50.0f;

typedef enum : NSInteger {
    MEFDrawerViewControllerDrawerStateGestureTypeNone,
    MEFDrawerViewControllerDrawerStateGestureTypeDrag,
    MEFDrawerViewControllerDrawerStateGestureTypeLongSwipe,
} MEFDrawerViewControllerDrawerStateGestureType;

typedef NS_OPTIONS(NSUInteger, MEFDrawerDatasourceState) {
    MEFDrawerDatasourceStateNone = 1 << 0,
    MEFDrawerDatasourceStateInit = 1 << 1,
    MEFDrawerDatasourceStateFromDecoder = 1 << 2,
};

const NSString static * MEFDrawerCollisionIndetifier = @"MEFDrawerCollisionIndetifier";
@interface MEFDrawerViewController () <UIGestureRecognizerDelegate, UICollisionBehaviorDelegate, UIDynamicAnimatorDelegate>
{

}
@property (assign, nonatomic, readwrite) MEFDrawerViewControllerDrawerState currentState;
@property (assign, nonatomic, readwrite) MEFDrawerViewControllerDrawerStateGestureType gestureType;

@property (nonatomic, strong) MASConstraint *leftDraweViewConstraint;
@property (nonatomic, strong) MASConstraint *leftMainViewConstraint;
@property (strong, nonatomic) UIView *shadowView;
@property (strong, nonatomic) MEFPanGestureRecognizer *panGestureRecognizer;
@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;
@property (nonatomic, readwrite, assign, getter=isNeedAnimationSegue) BOOL needAnimationSegue;


@end


@implementation MEFDrawerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.styleDrawer = MEFDrawerViewControllerStyleShiftSlide;;
        [self setEnabledDrawer:true];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.styleDrawer = MEFDrawerViewControllerStyleShiftSlide;
        [self setEnabledDrawer:true];
    }
    return self;
}

- (id)initWithDrawerViewController:(UIViewController *)navController mainController:(UIViewController*)mainController
{
    if (self = [self init]) {
        self.styleDrawer = MEFDrawerViewControllerStyleShiftSlide;;
        [self setEnabledDrawer:true];
        self.drawerViewController = navController;
        self.mainViewController = mainController;

    }
    return self;
}

- (id) init
{
    if (self = [super init]) {
    }
    return self;
}

#pragma mark - - override
- (void) loadView
{
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [view setBackgroundColor:[UIColor clearColor]];
    self.view = view;
}




- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self loadControllers];
    self.panGestureRecognizer = [[MEFPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture:)];
    [self.panGestureRecognizer setDelegate:self];
    [self.view addGestureRecognizer:self.panGestureRecognizer];
    self.tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    [self.tapGesture setDelegate:self];
    [self.shadowView addGestureRecognizer:self.tapGesture];
}


#pragma mark - - load methods


- (void) loadControllers
{
    if (!self.drawerViewController) {
        @try {
            [self performSegueWithIdentifier:MEFDrawerPanelSegueIndificator sender:nil];
        } @catch (NSException *e) {}
    }
    if (!self.mainViewController) {
        @try {
            [self performSegueWithIdentifier:MEFDrawerMainSegueIndificator sender:nil];
        } @catch (NSException *e) {
        
        }
        
    }

    if (!self.drawerViewController && self.dataSource && [self.dataSource respondsToSelector:@selector(drawerViewController)]) {
        self.drawerViewController = [self.dataSource drawerViewController];
    }
    if (!self.mainViewController && self.dataSource && [self.dataSource respondsToSelector:@selector(drawerMainViewController)]) {
        self.mainViewController = [self.dataSource drawerMainViewController];
    }
    
    NSAssert(self.drawerViewController != nil, @"drawerViewController == nil");


    
    if (self.currentState == MEFDrawerViewControllerDrawerStateClose) {
        [self.shadowView setAlpha:0];
    } else {
        [self.shadowView setAlpha:MEFNavigationCardViewControllerShadowAlpha];
    }
    [self setDrawerViewController:self.drawerViewController];
    [self setMainViewController:self.mainViewController];
    


   
 }

- (id) setMainViewControllerWithSegueIdentifier:(NSString*)identifier animated:(BOOL)animated
{
    self.needAnimationSegue = animated;
    @try {
        [self performSegueWithIdentifier:identifier sender:nil];
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    self.needAnimationSegue = false;
    return self.mainViewController;
}


- (void) setMainViewController:(UIViewController *)mainViewController
{
    if (![self isViewLoaded]) {
        _mainViewController = mainViewController;
        return;
    }
    if (_mainViewController) {
        [_mainViewController willMoveToParentViewController:nil];
        [_mainViewController.view removeFromSuperview];
        [_mainViewController removeFromParentViewController];
        _mainViewController = nil;
    }
    if (!mainViewController) {
        return;
    }
    _mainViewController = mainViewController;
    [self addChildViewController:_mainViewController];
    [_mainViewController.view setNeedsDisplay];
    [self.view addSubview:_mainViewController.view];
    [_mainViewController didMoveToParentViewController:self];
     
    [_mainViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        if (self.styleDrawer == MEFDrawerViewControllerStyleOverSlide) {
            make.edges.equalTo(self.view);
        } else {
            make.top.equalTo(self.view);
            make.bottom.equalTo(self.view);
            make.width.equalTo(self.view);
            make.left.equalTo(self.drawerViewController.view.mas_right);
        }

    }];
    if (self.drawerViewController.view.superview) {
        [self.view bringSubviewToFront:self.drawerViewController.view];
    }
    if (self.shadowView.superview) {
        [self.view bringSubviewToFront:self.shadowView];
    }
}

- (CGFloat) actualDrawerWidth
{
    CGFloat width = self.widthDrawerPanel;
    if (self.delegate && [self.delegate respondsToSelector:@selector(widthForDrawerViewController)]) {
        width = [self.delegate widthForDrawerViewController];
    }
    return width;
}
- (void) setDrawerViewController:(UIViewController *)drawerViewController
{
    if (![self isViewLoaded]) {
        _drawerViewController = drawerViewController;
        return;
    }
    if (_drawerViewController) {
        [_drawerViewController willMoveToParentViewController:nil];
        [_drawerViewController.view removeFromSuperview];
        [_drawerViewController removeFromParentViewController];
        _drawerViewController = nil;
    }
    _drawerViewController = drawerViewController;
    [self addChildViewController:_drawerViewController];
    [_drawerViewController.view setNeedsDisplay];
    [self.view addSubview:_drawerViewController.view];
    [_drawerViewController didMoveToParentViewController:self];
    
    CGFloat width = [self actualDrawerWidth];

    MEFBlockWeakSelf weakSelf = self;
    [self.drawerViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.view);
        make.bottom.equalTo(weakSelf.view);
        if (weakSelf.currentState == MEFDrawerViewControllerDrawerStateClose) {
            weakSelf.leftDraweViewConstraint = make.left.equalTo(weakSelf.view).offset(-width);
        } else {
            weakSelf.leftDraweViewConstraint = make.left.equalTo(weakSelf.view);
        }

        make.width.equalTo(@(width));
    }];
    

    [self.shadowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.left.equalTo(self.drawerViewController.view.mas_right);
        make.right.equalTo(self.view);
    }];
    
    if (self.shadowView.superview) {
        [self.view bringSubviewToFront:self.shadowView];
    }
    
}


#pragma mark - - show/hide
- (void) showDrawerViewControllerWithAnimated:(BOOL)animated
{
    if (!self.isEnabledDrawer) {
        return;
    }
    
    self.currentState = MEFDrawerViewControllerDrawerStateOpen;
    if (!self.isViewLoaded) {
        return;
    }
    [self.drawerViewController.view setNeedsLayout];
    [self.drawerViewController.view updateConstraintsIfNeeded];
    [self.shadowView setNeedsLayout];
    [self.shadowView updateConstraintsIfNeeded];
    [self.mainViewController.view setNeedsLayout];
    [self.mainViewController.view updateConstraintsIfNeeded];
    void (^animations) (void) = ^ (void) {
     self.leftDraweViewConstraint.offset(0);
        [self.shadowView setAlpha:MEFNavigationCardViewControllerShadowAlpha];
        [self.drawerViewController.view layoutIfNeeded];
        [self.shadowView layoutIfNeeded];
        [self.mainViewController.view layoutIfNeeded];
    };
    
    if (animated) {
        [UIView animateWithDuration:0.3 delay:0.0 options:0 animations:animations completion:^(BOOL finished) {
            [[NSNotificationCenter defaultCenter] postNotificationName:MEFDrawerViewControllerShowDrawerNotification object:nil];
        }];
    } else {
        animations();
        [[NSNotificationCenter defaultCenter] postNotificationName:MEFDrawerViewControllerShowDrawerNotification object:nil];
    }
}

- (void) hideDrawerViewControllerWithAnimated:(BOOL)animated
{

    self.currentState = MEFDrawerViewControllerDrawerStateClose;
    if (!self.isViewLoaded ) {
        return;
    }
    self.leftDraweViewConstraint.offset(0);
    [self.drawerViewController.view setNeedsLayout];
    [self.drawerViewController.view updateConstraintsIfNeeded];
    [self.shadowView setNeedsLayout];
    [self.shadowView updateConstraintsIfNeeded];
    [self.mainViewController.view setNeedsLayout];
    [self.mainViewController.view updateConstraintsIfNeeded];
    CGFloat width = [self actualDrawerWidth];
    void (^animations) (void) = ^ (void) {
        self.leftDraweViewConstraint.offset(-width);
        [self.shadowView setAlpha:0];
        [self.drawerViewController.view layoutIfNeeded];
//        [self.drawerViewController.view layoutSubviews];
        [self.shadowView layoutIfNeeded];
        [self.mainViewController.view layoutIfNeeded];
//        [self.mainViewController.view layoutSubviews];
    };
    if (animated) {
        [UIView animateWithDuration:0.3 delay:0.0 options:0 animations:animations completion:^(BOOL finished) {
            [[NSNotificationCenter defaultCenter] postNotificationName:MEFDrawerViewControllerHideDrawerNotification object:nil];
        }];
    } else {
        animations();
        [[NSNotificationCenter defaultCenter] postNotificationName:MEFDrawerViewControllerHideDrawerNotification object:nil];
    }
}

- (BOOL) gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (!self.isEnabledDrawer) {
        return false;
    }

    
    CGPoint location = [gestureRecognizer locationInView:self.view];
    if ([self.tapGesture isEqual:gestureRecognizer] && self.currentState == MEFDrawerViewControllerDrawerStateOpen && CGRectContainsPoint(self.shadowView.frame, location)) {
        return true;
    }
    if ([self.panGestureRecognizer isEqual:gestureRecognizer]) {
        if (self.currentState == MEFDrawerViewControllerDrawerStateOpen) {
            return CGRectContainsPoint(self.view.bounds, location);
        } else {
            return CGRectContainsPoint(self.view.bounds, location);
        }
        return true;
    }
    return false;
}

#pragma mark - - action
- (void) tapGesture:(UIGestureRecognizer *)gesture
{
    if (self.currentState == MEFDrawerViewControllerDrawerStateClose) {
        return;
    }
    [self hideDrawerViewControllerWithAnimated:true];
}

- (void) swipeGesture:(UISwipeGestureRecognizer *)gesture
{
    if (gesture.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self hideDrawerViewControllerWithAnimated:true];
    } else {
        [self showDrawerViewControllerWithAnimated:true];
    }
}

- (void) panGesture:(UIGestureRecognizer *)gesture
{
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
        {
            [self beganGestureRecognizer:(MEFPanGestureRecognizer *)gesture];
        } break;
            
        case UIGestureRecognizerStateChanged:
        {
            [self changedGestureRecognizer:(MEFPanGestureRecognizer *)gesture];
        } break;
            
        case UIGestureRecognizerStateEnded:
        {
            [self endedGestureRecognizer:(MEFPanGestureRecognizer *)gesture];
        } break;
            
        case UIGestureRecognizerStateCancelled:
        {
            [self endedGestureRecognizer:(MEFPanGestureRecognizer *)gesture];
        }
            
        default:
        {
        } break;
    }
}

#pragma mark - - pan gesture move
- (void) beganGestureRecognizer:(MEFPanGestureRecognizer *)gesture
{
    CGPoint location = [gesture locationInView:self.view];
    gesture.prevLocation = location;
    CGRect drawerRect;
    if (self.currentState == MEFDrawerViewControllerDrawerStateClose) {
        drawerRect = CGRectMake(0, 0, MEFDrawerVisibleWidth, self.view.frame.size.height);
    } else {
        drawerRect = self.drawerViewController.view.bounds;
    }

    if (CGRectContainsPoint(drawerRect, location)) {
        self.gestureType = MEFDrawerViewControllerDrawerStateGestureTypeDrag;
        [[NSNotificationCenter defaultCenter]postNotificationName:MEFDrawerViewControllerWillDraggingDrawerNotification object:nil];
    }  else {
        self.gestureType = MEFDrawerViewControllerDrawerStateGestureTypeLongSwipe;
    }
}

- (void) changedGestureRecognizer:(MEFPanGestureRecognizer *)gesture
{
    CGPoint location = [gesture locationInView:self.view];
    CGPoint prevLocation = [gesture prevLocation];
    CGPoint dif = ccpSub(location, prevLocation);
    dif.y = 0;
    if (self.gestureType == MEFDrawerViewControllerDrawerStateGestureTypeDrag) {
        CGRect rectNavPoint = self.drawerViewController.view.frame;
        rectNavPoint.origin = ccpAdd(rectNavPoint.origin, dif);
        rectNavPoint.origin.x = fmaxf(rectNavPoint.origin.x, -rectNavPoint.size.width);
        rectNavPoint.origin.x = fminf(rectNavPoint.origin.x, 0);
        self.leftDraweViewConstraint.offset(rectNavPoint.origin.x);
        CGFloat percent = CGRectGetMaxX(self.drawerViewController.view.frame) / CGRectGetWidth(self.drawerViewController.view.frame);
        [self.shadowView setAlpha:MEFNavigationCardViewControllerShadowAlpha * percent];
    }
    gesture.prevLocation = location;
}

- (void) endedGestureRecognizer:(MEFPanGestureRecognizer *)gesture
{
    if (self.gestureType == MEFDrawerViewControllerDrawerStateGestureTypeDrag) {
        CGFloat percent = CGRectGetMaxX(self.drawerViewController.view.frame) / CGRectGetWidth(self.drawerViewController.view.frame);
        if (self.currentState == MEFDrawerViewControllerDrawerStateOpen) {
            if (percent <= MEFNavigationCardViewControllerPercentForHide) {
                [self hideDrawerViewControllerWithAnimated:true];
            } else {
                [self showDrawerViewControllerWithAnimated:true];
            }
        } else {
            if (percent >= MEFNavigationCardViewControllerPercentForShow) {
                [self showDrawerViewControllerWithAnimated:true];
            } else {
                [self hideDrawerViewControllerWithAnimated:true];
            }
        }
    } else if (self.gestureType == MEFDrawerViewControllerDrawerStateGestureTypeLongSwipe) {
        if ([gesture velocityInView:self.view].x < MEFNavigationCardViewControllerXSwipe) {
            [self hideDrawerViewControllerWithAnimated:true];
        } else if ([gesture velocityInView:self.view].x > MEFNavigationCardViewControllerXSwipe) {
            [self showDrawerViewControllerWithAnimated:true];
        }
    }
    self.gestureType = MEFDrawerViewControllerDrawerStateGestureTypeNone;
}

#pragma mark - - property


- (UIView *)shadowView
{
    if (!_shadowView) {
        _shadowView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
        [_shadowView setTranslatesAutoresizingMaskIntoConstraints:false];
        [_shadowView setBackgroundColor:[UIColor blackColor]];
        [_shadowView setAlpha:MEFNavigationCardViewControllerShadowAlpha];
        [self.view addSubview:_shadowView];
    }
    return _shadowView;
}

@end
