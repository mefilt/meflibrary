//
//  MEFButton.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 09.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFButton.h"
#import "UIControl+CustomState.h"
@interface MEFButtonState : NSObject
@property (nonatomic, readwrite, strong) UIColor *color;
@end

@implementation MEFButtonState
@end

@interface MEFButton ()
@property (nonatomic, readwrite, strong) NSMutableDictionary *cash;
@end

@implementation MEFButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype) init
{
    if (self = [super init]) {
        [self initialize];
    }
    return self;
}
- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self initialize];
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    [self addTarget:self action:@selector(_didTouch:) forControlEvents:UIControlEventTouchUpInside];
}


- (void)setBackgroundColor:(UIColor *)color forState:(UIControlState)state;
{
    MEFButtonState *buttonState = [self buttonState:state];
    buttonState.color = color;
    [self stateWasUpdated];
}



- (void) setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    [self stateWasUpdated];
}

- (void) setSelected:(BOOL)selected
{
    [super setSelected:selected];
    [self stateWasUpdated];
}
- (void) setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    [self stateWasUpdated];
}

- (void) stateWasUpdated
{
    MEFButtonState *buttonState = [self buttonState:self.state];
    if (!buttonState) {
        buttonState = [self buttonState:UIControlStateNormal];
    }
    if (!buttonState) {
        return;
    }
    
    if (buttonState.color) {
        [self setBackgroundColor:buttonState.color];
    } else {
         MEFButtonState *buttonState = [self buttonState:UIControlStateNormal];
        [self setBackgroundColor:buttonState.color];
    }

}

- (void) _didTouch:(id)sender
{
    if (self.didTouch) {
        self.didTouch(self);
    }
}

- (MEFButtonState*) buttonState:(UIControlState)state
{
    MEFButtonState *buttonState = self.cash[[UIControl stringState:state]] ;
    if (!buttonState) {
        buttonState = self.cash[[UIControl stringState:state]] = [MEFButtonState new];
    }
    
    return buttonState;
    
}



- (NSMutableDictionary*) cash
{
    if (!_cash) {
        _cash = [NSMutableDictionary dictionary];
    }
    return _cash;
}
@end
