//
//  MEFConstants.h
//  MEFLibrary
//
//  Created by Mefilt on 15.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//
#import <CocoaLumberjack/CocoaLumberjack.h>
#import <CocoaLumberjack/DDLogMacros.h>
#ifndef MEFLibrary_MEFConstants_h
#define MEFLibrary_MEFConstants_h

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)
#define DEGREE(angle) ((angle) / 180.0 * M_PI)

#define MEFString(int) [[NSString alloc]initWithFormat:@"%d",int]
#define MEFBlockWeakObjectWithName(name,o) __typeof__(o) __weak name = o;
#define MEFBlockWeakObject(o) __typeof__(o) __weak
#define MEFBlockStrongObject(o) __typeof__(o) __strong
#define MEFBlockWeakSelf MEFBlockWeakObject(self)

const static CGFloat MEFAnimationDurationFast = 0.25f;
const static CGFloat MEFAnimationDurationNormal = 0.3f;
const static CGFloat MEFAnimationDurationSlow = 0.5f;

#define MEF_NETWORK_LOG_ENABLE 1


typedef NS_OPTIONS(NSUInteger, MEFLogContext) {
    MEFLogContextNone = 0,
    MEFLogContextNetwork = 800000,
    MEFLogContextGeneral,
};




#define MEFNetworkLogError(frmt, ...)   LOG_MAYBE(NO,                DDLogLevelError, DDLogFlagError,   MEFLogContextNetwork, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFNetworkLogWarn(frmt, ...)    LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelWarning, DDLogFlagWarning, MEFLogContextNetwork, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFNetworkLogInfo(frmt, ...)    LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelInfo, DDLogFlagInfo,    MEFLogContextNetwork, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFNetworkLogDebug(frmt, ...)   LOG_MAYBE(NO, DDLogLevelDebug, DDLogFlagDebug,   MEFLogContextNetwork, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFNetworkLogVerbose(frmt, ...) LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelVerbose, DDLogFlagVerbose, MEFLogContextNetwork, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)




#define MEFGeneralLogError(frmt, ...)   LOG_MAYBE(NO,                DDLogLevelError, DDLogFlagError,   MEFLogContextGeneral, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFGeneralLogWarn(frmt, ...)    LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelWarning, DDLogFlagWarning, MEFLogContextGeneral, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFGeneralLogInfo(frmt, ...)    LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelInfo, DDLogFlagInfo,    MEFLogContextGeneral, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFGeneralLogDebug(frmt, ...)   LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelDebug, DDLogFlagDebug,   MEFLogContextGeneral, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFGeneralLogVerbose(frmt, ...) LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelVerbose, DDLogFlagVerbose, MEFLogContextGeneral, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)




#endif



