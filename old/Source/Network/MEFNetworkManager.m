    //
//  MEFNetworkManager.m
//  Katusha
//
//  Created by Prokofev Ruslan on 12.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFNetworkManager.h"
#import <AFDownloadRequestOperation/AFDownloadRequestOperation.h>
#import <Haneke/Haneke.h>
#import <Haneke/HNKDiskCache.h>
#import "MEFNetworkModel.h"
#import "MEFConstants.h"
#import "NSString+MEFUtilities.h"

    @interface HNKDiskCache ()

- (NSString*)pathForKey:(NSString*)key;
@end
@interface MyCustomTextSerializer : NSObject
@end
@implementation MyCustomTextSerializer
+ (id)objectFromData:(NSData *)data error:(NSError **)error
{
    NSString* latin = [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    return latin;
}


@end



@interface MyCustomJSONSerializer : RKNSJSONSerialization

@end
@implementation MyCustomJSONSerializer
+ (id)objectFromData:(NSData *)data error:(NSError **)error
{
    NSString* latin = [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    latin = [MyCustomJSONSerializer stringByRemovingControlCharacters:latin];
    NSData* utf8 = [latin dataUsingEncoding:NSUTF8StringEncoding];
    
    return [NSJSONSerialization JSONObjectWithData:utf8 options:0 error:error];
}

+ (NSString *)stringByRemovingControlCharacters: (NSString *)inputString
{
    NSCharacterSet *controlChars = [NSCharacterSet controlCharacterSet];
    NSRange range = [inputString rangeOfCharacterFromSet:controlChars];
    if (range.location != NSNotFound) {
        NSMutableString *mutable = [NSMutableString stringWithString:inputString];
        while (range.location != NSNotFound) {
            [mutable deleteCharactersInRange:range];
            range = [mutable rangeOfCharacterFromSet:controlChars];
        }
        return mutable;
    }
    return inputString;
}

@end

@interface MyCustomImageSerializer : NSObject <RKSerialization>

@end
@implementation MyCustomImageSerializer
+ (id)objectFromData:(NSData *)data error:(NSError **)error
{
    
    return data;
}
+ (NSData *)dataFromObject:(id)object error:(NSError **)error
{
    
    return object;
}


@end
@interface MEFNetworkManager ()
{

}
@property (nonatomic, readwrite, strong) NSMutableDictionary *operationCache;
@property (nonatomic, readwrite, strong) NSMutableDictionary *modelCache;
@property (nonatomic, readwrite, strong) NSOperationQueue *operationQueue;
@property (nonatomic, readwrite, assign) NetworkStatus networkStatus;
@end

const static CGFloat MEFNetworkManagerTimeout = 60.0f;

@implementation MEFNetworkManager

static NSString * const cachePathComponent = @"mefilt.com";
static HNKDiskCache *_diskCache;
static NSString *_rootDirectory;
static dispatch_queue_t _cashQueue;
static Reachability *_reachability;
- (id) init
{
    if (self = [super init]) {
        self.operationCache = [NSMutableDictionary dictionary];
        self.modelCache = [NSMutableDictionary dictionary];
        self.operationQueue = [[NSOperationQueue alloc]init];
        RKLogConfigureByName("RestKit", RKLogLevelOff);
        RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelOff);
        RKLogConfigureByName("RestKit/Network", RKLogLevelOff);
        [RKMIMETypeSerialization registerClass:[MyCustomJSONSerializer class] forMIMEType:RKMIMETypeJSON];
        [RKMIMETypeSerialization registerClass:[MyCustomImageSerializer class] forMIMEType:@"image/jpeg"];
        [RKMIMETypeSerialization registerClass:[MyCustomJSONSerializer class] forMIMEType:@"text/json"];
        [RKMIMETypeSerialization registerClass:[MyCustomTextSerializer class] forMIMEType:@"text/plain"];
        
        
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            _reachability = [Reachability reachabilityWithHostName:MEFNetworkManagerReachabilityHostName];
            [_reachability startNotifier];
            _cashQueue  = dispatch_queue_create([[NSString stringWithFormat:@"%@.cash",cachePathComponent]UTF8String], NULL);
            NSString *cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
            _rootDirectory = [cachesDirectory stringByAppendingPathComponent:cachePathComponent];
            NSFileManager *fm = [[NSFileManager alloc]init];
            [fm createDirectoryAtPath:_rootDirectory withIntermediateDirectories:true attributes:nil error:nil];
            _diskCache = [[HNKDiskCache alloc]initWithDirectory:_rootDirectory capacity:MEFNetworkManagerDataDiskCapacity];
            HNKCacheFormat *format = [[HNKCacheFormat alloc] initWithName:@"thumbnail"];
            format.compressionQuality = 1;
            format.preloadPolicy  = HNKPreloadPolicyLastSession;
            format.diskCapacity = MEFNetworkManagerImageDiskCapacity;
            [[HNKCache sharedCache] registerFormat:format];            
        });
        self.networkStatus = self.reachability.currentReachabilityStatus;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChangedNotification:) name:kReachabilityChangedNotification object:nil];
        
    }
    return self;
}


#pragma mark - - rReachability Changed Notification

- (void) reachabilityChangedNotification:(NSNotification*) notification
{
    NetworkStatus currentStatus = self.reachability.currentReachabilityStatus;
    if (self.networkStatus != currentStatus) {
        self.networkStatus = currentStatus;
        if (self.delegate && [self.delegate respondsToSelector:@selector(reachabilityInternetConnection:)]) {
            [self.delegate reachabilityInternetConnection:currentStatus];
        }
    }
}

#pragma mark static varible
- (Reachability*) reachability
{
    return _reachability;
}
+ (HNKDiskCache*) diskCache
{
    return _diskCache;
}
+ (NSString*) rootDirectory
{
    return _rootDirectory;
}
+ (dispatch_queue_t) cashQueue
{
    return _cashQueue;
}


#pragma mark - - delegate
- (BOOL) catchRestError:(MEFError*) error query:(MEFRestQuery*)qury
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(catchRestError:query:)]) {
        return [self.delegate catchRestError:error query:qury];
    }
    return false;
}
- (void) handleRestError:(MEFError*) error query:(MEFRestQuery*)qury callback:(void (^)(MEFNMHandleRestErrorCallbackAction))callback
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(handleRestError:query:callback:)]) {
        [self.delegate handleRestError:error query:qury callback:callback];
    }
}




- (BOOL) isNotReachable
{
    return self.reachability.currentReachabilityStatus == NotReachable;
}
- (NSError*) NSURLErrorNotConnectedToInternet
{
    return [NSError errorWithDomain:@"Error Not Connected To Internet" code:NSURLErrorNotConnectedToInternet userInfo:nil];
}

#pragma mark - - generate
- (MEFRestNetworkModel *) generateSingleNetworkModelWithRestQuery:(MEFRestQuery *)query URL:(NSString*)URL success:(void (^)(MEFRestResponse *))succes error:(void (^)(MEFError *))error
{
    NSString *hash = [[NSProcessInfo processInfo]globallyUniqueString];
    return [self generateSingleNetworkModelWithRestQuery:query hash:hash URL:URL success:succes error:error];
}



- (MEFRestNetworkModel *) generateSingleNetworkModelWithRestQuery:(MEFRestQuery *)query hash:(NSString*)hash URL:(NSString*)URL success:(void (^)(MEFRestResponse *))succes error:(void (^)(MEFError *))error
{

    MEFRestNetworkModel *model = [[MEFRestNetworkModel alloc]init];
    model.query = query;
    model.hashOperation = hash;
    model.succes = succes;
    model.url = URL;
    model.error = error;
    return model;
}



#pragma mark - - run rest query
- (NSString*) runRestQueries:(NSArray *)queries success:(void (^)(NSArray *))succes error:(void (^)(MEFError *error, MEFQuery *query))error
{
    NSAssert(self.serverURL != nil, @"Url = nil");
    NSString *hash = [[NSProcessInfo processInfo]globallyUniqueString];
    MEFGroupNetworkModel *model = [[MEFGroupNetworkModel alloc]init];
    model.url = self.serverURL;
    model.hashOperation = hash;
    model.queries = queries;
    model.succes = succes;
    model.error = error;
    [self runGroupNetworkModel:model];
    return hash;
}

- (void) runRestQueries:(NSArray *)queries hash:(NSString*)hash success:(void (^)(NSArray *))succes error:(void (^)(MEFError *error, MEFQuery *query))error
{
    NSAssert(self.serverURL != nil, @"Url = nil");
    MEFGroupNetworkModel *model = [[MEFGroupNetworkModel alloc]init];
    model.url = self.serverURL;
    model.hashOperation = hash;
    model.queries = queries;
    model.succes = succes;
    model.error = error;
    [self runGroupNetworkModel:model];
}



- (NSString*) runRestQueryWithURL:(NSString*)URL query:(MEFRestQuery*)query success:(void (^) (MEFRestResponse *restResponse))succes error:(void (^) (MEFError *error))error;
{
    NSString *hash = [[NSProcessInfo processInfo]globallyUniqueString];
    [self runRestQuery:query hash:hash URL:URL success:succes error:error];
    return hash;
}



- (NSString*) runRestQuery:(MEFRestQuery *)query success:(void (^)(MEFRestResponse *))succes error:(void (^)(MEFError *))error
{
 
    NSString *hash = [[NSProcessInfo processInfo]globallyUniqueString];
    [self runRestQuery:query hash:hash URL:self.serverURL success:succes error:error];
    return hash;
}

- (void) runRestQuery:(MEFRestQuery *)query hash:(NSString*)hash success:(void (^)(MEFRestResponse *))succes error:(void (^)(MEFError *))error
{
    [self runRestQuery:query hash:hash URL:self.serverURL success:succes error:error];
}

- (void) runRestQuery:(MEFRestQuery *)query hash:(NSString*)hash URL:(NSString*)URL success:(void (^)(MEFRestResponse *))succes error:(void (^)(MEFError *))error
{
    MEFRestNetworkModel *singleModel = [self generateSingleNetworkModelWithRestQuery:query hash:hash URL:URL success:succes error:error];
    [self runSingleModel:singleModel];
}

- (void) runRestDataQuery:(MEFRestDataQuery *)query hashOperation:(NSString*)hashOperation URL:(NSString*)URL succes:(void (^) (NSData *data, NSString *pathFile)) succes error:(void (^)(MEFError *))error progress:(void (^) (CGFloat progress))progress;
{
    MEFRestDataNetworkModel *model = [[MEFRestDataNetworkModel alloc]init];
    model.query = query;
    model.hashOperation = hashOperation;
    model.succes = succes;
    model.progress = progress;
    model.filename = query.filename;
    model.hashData = query.hashData;
    model.url = URL;
    model.error = error;

    
    MEFBlockWeakSelf weakSelf = self;

    [[MEFNetworkManager diskCache] fetchDataForKey:model.hashData success:^(NSData *data) {
        NSString *filePath = [[MEFNetworkManager diskCache] pathForKey:model.hashData];
        
        if ([query checkInvalidData:[NSData dataWithContentsOfFile:filePath]]) {
            [[MEFNetworkManager diskCache]  removeDataForKey:filePath];
            [weakSelf runRestDataQuery:model];
            return;
        }
        succes(data, filePath);
    } failure:^(NSError *error) {
        [weakSelf runRestDataQuery:model];
    }];
    
}

- (NSString*) runRestDataQuery:(MEFRestDataQuery *)query URL:(NSString*)URL succes:(void (^) (NSData *data, NSString *pathFile)) succes error:(void (^)(MEFError *))error progress:(void (^) (CGFloat progress))progress
{
//    if ([self isNotReachable]) {
//        error([self NSURLErrorNotConnectedToInternet]);
//        return nil;
//    }
    NSString *hash = [[NSProcessInfo processInfo]globallyUniqueString];
    [self runRestDataQuery:query  hashOperation:hash URL:URL succes:succes error:error progress:progress];
    return hash;
}

- (NSString*) runRestDataQuery:(MEFRestDataQuery *)query succes:(void (^) (NSData *data, NSString *pathFile)) succes error:(void (^)(MEFError *))error progress:(void (^) (CGFloat progress))progress
{
    return [self runRestDataQuery:query URL:self.serverURL succes:succes error:error progress:progress];
}

#pragma mark - - run model

- (void) runSingleModel:(MEFRestNetworkModel *)model
{
    NSOperation *operation = [self createOperationWithSingleNetworkModel:model];
    [self addNetworkModelToCashe:model andOperationToCashe:operation];
    [self.operationQueue addOperation:operation];
}

- (void) runRestDataQuery:(MEFRestDataNetworkModel*)model
{
    NSURLRequest *request = [self createRestURLRequestWithQuery:model.query model:model];
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingString:cachePathComponent];
    filePath = [filePath stringByAppendingString:model.filename];
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    MEFBlockWeakSelf weakSelf = self;
    AFDownloadRequestOperation *operation = [self createDataOperationWithRequest:request filePath:filePath hashData:model.hashData success:^(NSData *data, NSString *pathFile) {
        [weakSelf removeNetworkModelAndOperationWithHash:model.hashOperation];
        model.succes(data, pathFile);
    } error:^(NSError *error) {
        [weakSelf removeNetworkModelAndOperationWithHash:model.hashOperation];
        model.error([weakSelf createErrorWithNativeError:error statusCode:operation.response.statusCode]);
    } progress:^(CGFloat progress) {
        model.progress(progress);
    }];
    
    [self addNetworkModelToCashe:model andOperationToCashe:operation];
    [self.operationQueue addOperation:operation];
}

- (void) runGroupNetworkModel:(MEFGroupNetworkModel*)groupModel
{
    dispatch_group_t serviceGroup = dispatch_group_create();
    NSMutableArray *responses = [NSMutableArray arrayWithCapacity:groupModel.queries.count];
    NSMutableArray *models = [NSMutableArray arrayWithCapacity:groupModel.queries.count];
    for (int i = 0; i < groupModel.queries.count; i++) {
        [responses addObject:[NSNull null]];
    }
    groupModel.responses = responses;
    groupModel.models = models;
    MEFBlockWeakSelf weakSelf = self;
    MEFBlockWeakObject(groupModel) weakGroupModel = groupModel;
    NSUInteger index = 0;
    for (MEFRestQuery *query in groupModel.queries) {
        dispatch_group_enter(serviceGroup);
        MEFRestNetworkModel *singleModel = [self generateSingleNetworkModelWithRestQuery:query URL:groupModel.url success:^(MEFRestResponse *restResponse) {
            weakGroupModel.responses[index] = restResponse;
            dispatch_group_leave(serviceGroup);
        } error:^(MEFError *error) {
            MEFBlockStrongObject(weakGroupModel) strongModel = weakGroupModel;
            if (!strongModel) {
                return ;
            }
            if (weakGroupModel.models) {
                for (MEFRestNetworkModel *model in weakGroupModel.models) {
                    [weakSelf cancelQuery:model.hashOperation];
                }
            }
            [weakSelf removeNetworkModelAndOperationWithHash:strongModel.hashOperation];
            strongModel.error(error, query);
        }];
        index++;
        [models addObject:singleModel];
    }
    [self addNetworkModelToCashe:groupModel];
    for (MEFRestNetworkModel *model in models) {
        [self runSingleModel:model];
    }
    
    dispatch_group_notify(serviceGroup,dispatch_get_main_queue(),^{
        MEFBlockStrongObject(weakGroupModel) strongModel = weakGroupModel;
        if (!strongModel) {
            return ;
        }
        [weakSelf removeNetworkModelAndOperationWithHash:strongModel.hashOperation];
        strongModel.succes(weakGroupModel.responses);
    });
}



- (void) runDownloadFileWithURL:(NSString*)url hashOperation:(NSString*)hashOperation hashData:(NSString*)hashData success:(void (^) (NSData *data,  NSString *pathFile))succes error:(void (^) (NSError *error))error progress:(void (^) (CGFloat progress))progress;
{
    MEFFileNetworkModel *model = [[MEFFileNetworkModel alloc]init];
    model.hashOperation = hashOperation;
    model.url = url;
    model.hashData = hashData;
    model.error = error;
    model.filename = [url fileExtension];
    model.progress = progress;
    model.succes = succes;
    NSOperation *operation = [self createDataOperationWithNetworkModel:model];
    [self addNetworkModelToCashe:model andOperationToCashe:operation];
    [self.operationQueue addOperation:operation];
}



- (void) runDownloadImageWithURL:(NSString *)url key:(NSString *)key hash:(NSString*) hash success:(void (^)(UIImage *))succes error:(void (^)(NSError *))error
{
    
    MEFImageNetworkModel *model = [[MEFImageNetworkModel alloc]init];
    model.hashOperation = hash;
    model.succes = succes;
    model.key = key;
    model.error = error;
    model.url = url;
    NSOperation *operation = [self createOperationWithImageNetworkModel:model];
    [self addNetworkModelToCashe:model andOperationToCashe:operation];
    [self.operationQueue addOperation:operation];
}



#pragma mark - - remove//add model
- (void) removeNetworkModelAndOperationWithHash:(NSString*) hash
{
    [self.operationCache removeObjectForKey:hash];
    [self.modelCache removeObjectForKey:hash];
}



- (void) addNetworkModelToCashe:(MEFNetworkModel*)model
{
    [self.modelCache setObject:model forKey:model.hashOperation];
}

- (void) addNetworkModelToCashe:(MEFNetworkModel*)model andOperationToCashe:(NSOperation*)operation
{
    [self.operationCache setObject:operation forKey:model.hashOperation];
    [self.modelCache setObject:model forKey:model.hashOperation];
}


#pragma mark - - download

- (NSString*) downloadFileWithURL:(NSString*)url hashData:(NSString*)hashData success:(void (^) (NSData *data, NSString *pathFile))succes error:(void (^) (NSError *error))errorBlock progress:(void (^) (CGFloat progress))progress;
{
    NSString *hash = [[NSProcessInfo processInfo]globallyUniqueString];
    MEFBlockWeakSelf weakSelf = self;
    [[MEFNetworkManager diskCache] fetchDataForKey:hashData success:^(NSData *data) {
        MEFNetworkLogDebug(@"Loaded data from cashe %@",hashData);
        NSString *filePath = [[MEFNetworkManager diskCache] pathForKey:hashData];
        return succes(data, filePath);
    } failure:^(NSError *error) {
        [weakSelf runDownloadFileWithURL:url hashOperation:hash hashData:hashData success:succes error:errorBlock progress:progress];
    }];
    return hash;
}



- (NSString*) downloadImageWithURL:(NSString *)url key:(NSString *)key success:(void (^)(UIImage *))succes error:(void (^)(NSError *))errorBlock
{
    MEFBlockWeakSelf weakSelf = self;
    NSString *hash = [[NSProcessInfo processInfo]globallyUniqueString];
    BOOL result = false;
    
    result = [[HNKCache sharedCache] fetchImageForKey:key formatName:@"thumbnail" success:^(UIImage *image) {
        succes(image);
    } failure:^(NSError *error) {
        [weakSelf runDownloadImageWithURL:url key:key hash:hash success:succes error:errorBlock];
    }];
    if (!result) {
        [self runDownloadImageWithURL:url key:key hash:hash success:succes error:errorBlock];
    }
    
    
    
    return hash;
}


#pragma mark - - other

- (void) a {
    
//    MEFError *myError = [MEFError new];
//    myError.HTTPStatusCode = operation.HTTPRequestOperation.response.statusCode;
//    myError.netwrokError = error;
//
//    if ([self catchRestError:myError query:query]) {
//        [self handleRestError:myError query:query callback:^(MEFNMHandleRestErrorCallbackAction state) {
//            if (state == MEFNMHandleRestErrorCallbackActionRepeting) {
////                [weakSelf runRestQuery:query hash:model.hashOperation URL:model.url success:model.succes error:model.error];
//            } else if (state == MEFNMHandleRestErrorCallbackActionError) {
////                model.error(error);
//            }
//        }];
//    } else {
////        model.error(error);
//    }

}

- (MEFRestResponse*) createRestResponseWithQuery:(MEFRestQuery*)query mappingResult:(RKMappingResult*)mappingResult
{
    MEFRestResponse *res = [MEFRestResponse new];
    res.query = query;
    res.mappingResult = mappingResult;
    res.response = mappingResult.firstObject;
    return res;
}

- (MEFError*) createErrorWithNativeError:(NSError*)error statusCode:(NSInteger)statusCode
{
    MEFError *myError = [MEFError new];
    myError.HTTPStatusCode = statusCode;
    myError.error = error;
    return myError;
}



- (NSOperation*) createOperationWithSingleNetworkModel:(MEFRestNetworkModel *)model
{
    MEFRestQuery *query = model.query;
    NSURLRequest *request = [self createRestURLRequestWithQuery:query model:model];
    NSArray *responseDescriptors = [query responseDescriptors];
    MEFNetworkLogDebug(@"URL Server %@",[request.URL description]);
    
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:responseDescriptors];
    MEFBlockWeakSelf weakSelf = self;
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        #if (MEF_NETWORK_LOG_ENABLE == 1)
            NSString *string = [[NSString alloc]initWithData:operation.HTTPRequestOperation.responseData encoding:NSUTF8StringEncoding];
            MEFNetworkLogDebug(@"Result from server module %@ json - %@",model.query.module,string);
        #endif
        [weakSelf removeNetworkModelAndOperationWithHash:model.hashOperation];
        MEFRestResponse *response = [weakSelf createRestResponseWithQuery:query mappingResult:mappingResult];
        if ([query catchResponseError:response.response]) {
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(handleResponseError:query:callback:)]) {
                [weakSelf.delegate handleResponseError:response query:query callback:^(MEFNMHandleResponceErrorCallbackAction state) {
                    if (state == MEFNMHandleResponceErrorCallbackActionNone) {
                        model.succes(response);
                    } else if (state == MEFNMHandleResponceErrorCallbackActionRepeting) {
                        [weakSelf runRestQuery:query hash:model.hashOperation URL:model.url success:model.succes error:model.error];
                    }
                }];
            } else {
                model.succes(response);
            }
        } else {
            model.succes(response);
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if ([operation isCancelled]) {
            MEFNetworkLogDebug(@"Operation cancelled");
            return;
        }
        MEFNetworkLogDebug(@"Error from server %@",[error description]);
        
        NSString *response = [[NSString alloc]initWithData:operation.HTTPRequestOperation.responseData encoding:NSUTF8StringEncoding];
        MEFNetworkLogDebug(@"E Response from server %@",response);
        
        [weakSelf removeNetworkModelAndOperationWithHash:model.hashOperation];
        MEFError *myError = [weakSelf createErrorWithNativeError:error statusCode:operation.HTTPRequestOperation.response.statusCode];

        if ([weakSelf catchRestError:myError query:query]) {
            [weakSelf handleRestError:myError query:query callback:^(MEFNMHandleRestErrorCallbackAction state) {
                if (state == MEFNMHandleRestErrorCallbackActionRepeting) {
                    [weakSelf runRestQuery:query hash:model.hashOperation URL:model.url success:model.succes error:model.error];
                } else if (state == MEFNMHandleRestErrorCallbackActionError) {
                    model.error(myError);
                }
            }];
        } else {
            model.error(myError);
        }
    }];
    return operation;
}

- (NSURLRequest*) createRestURLRequestWithQuery:(MEFQuery*)query model:(MEFNetworkModel*)model
{
    NSMutableString *url = [[NSMutableString alloc]initWithFormat:@"%@/%@",[model url],[query module]];
    
    RKRequestDescriptor *requestDescriptor = [query requestDescriptor];
  
    NSData *HTTPBody = nil;
    if (requestDescriptor) {
        NSError *error = nil;;
        NSDictionary *parameters = [RKObjectParameterization parametersWithObject:query requestDescriptor:requestDescriptor error:&error];
        if (requestDescriptor.method != RKRequestMethodGET) {
            NSData *JSON = [RKMIMETypeSerialization dataFromObject:parameters MIMEType:RKMIMETypeJSON error:&error];
            NSAssert(error == nil, @"request invalid");
#if (MEF_NETWORK_LOG_ENABLE == 1)
            NSString *js = [[NSString alloc]initWithData:JSON encoding:NSUTF8StringEncoding];
            MEFNetworkLogDebug(@"JSON to server %@ %@",query.module,js);
#endif
            
            HTTPBody = JSON;
        } else {
            [url appendString:[NSString stringWithFormat:[url rangeOfString:@"?"].location == NSNotFound ? @"?%@" : @"&%@", AFQueryStringFromParametersWithEncoding(parameters, NSUTF8StringEncoding)]];
        }
    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[[NSURL alloc]initWithString:url] cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:MEFNetworkManagerTimeout];
    if (HTTPBody) {
        [request setHTTPBody:HTTPBody];
    }

    
    
    [request setHTTPMethod:RKStringFromRequestMethod(requestDescriptor.method)];
    if (self.HTTPHeaderFields) {
        MEFNetworkLogDebug(@"HTTPHeaderFields to server %@ %@",query.module,self.HTTPHeaderFields);
        for (NSString *key in self.HTTPHeaderFields.allKeys) {
            [request setValue:self.HTTPHeaderFields[key] forHTTPHeaderField:key];
        }
    }
    if (query.HTTPHeaderFields) {
        MEFNetworkLogDebug(@"HTTPHeaderFields to server %@ %@",query.module,query.HTTPHeaderFields);
        for (NSString *key in query.HTTPHeaderFields.allKeys) {
            [request setValue:query.HTTPHeaderFields[key] forHTTPHeaderField:key];
        }
    }
  
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
//    if (requestDescriptor.method != RKRequestMethodPUT) {
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    }


   
    return request;
}



- (NSOperation*) createOperationWithImageNetworkModel:(MEFImageNetworkModel*)model
{

    MEFBlockWeakSelf weakSelf = self;
    MEFNetworkLogDebug(@"Download image %@",model.url);
    AFImageRequestOperation *requestOperation = [AFImageRequestOperation imageRequestOperationWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:model.url] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:MEFNetworkManagerTimeout] imageProcessingBlock:^UIImage *(UIImage *image) {
        
        return image;
    } success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        if (model.key) {
            dispatch_async(_cashQueue, ^{
                [[HNKCache sharedCache] setImage:image forKey:model.key formatName:@"thumbnail"];
            });
        }
        [weakSelf removeNetworkModelAndOperationWithHash:model.hashOperation];
        model.succes(image);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        [weakSelf removeNetworkModelAndOperationWithHash:model.hashOperation];
        model.error(error);
    }];
    

    return requestOperation;
}


- (AFDownloadRequestOperation*) createDataOperationWithRequest:(NSURLRequest*) request filePath:(NSString*)filePath hashData:(NSString*)hashData  success:(void (^) (NSData *data, NSString *pathFile)) successCB error:(void (^) (NSError *error))errorCB progress:(void (^) (CGFloat progress))progresCB
{
    AFDownloadRequestOperation *downloadOperation = [[AFDownloadRequestOperation alloc]initWithRequest:request targetPath:filePath shouldResume:false];
    [downloadOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        float downloadProgress = totalBytesRead / (float)totalBytesExpectedToRead;
        progresCB(downloadProgress);
    }];
    [downloadOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSData *data = [NSData dataWithContentsOfFile:responseObject];
        [[MEFNetworkManager diskCache] setData:data forKey:hashData];
        MEFNetworkLogDebug(@"File path %@",responseObject);
        successCB(data, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorCB(error);
    }];
    
    return downloadOperation;
    
}




- (AFDownloadRequestOperation*) createDataOperationWithNetworkModel:(MEFFileNetworkModel*)networkModel
{
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingString:cachePathComponent];
    filePath = [filePath stringByAppendingString:[networkModel filename]];
    
    //experemental
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:filePath error:&error];
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:networkModel.url]];
    MEFBlockWeakSelf weakSelf = self;
    AFDownloadRequestOperation *downloadOperation = [self createDataOperationWithRequest:request filePath:filePath hashData:networkModel.hashData success:^(NSData *data, NSString *pathFile) {
        [weakSelf removeNetworkModelAndOperationWithHash:networkModel.hashOperation];
        networkModel.succes(data, filePath);
    } error:^(NSError *error) {
        [weakSelf removeNetworkModelAndOperationWithHash:networkModel.hashOperation];
        networkModel.error(error);
    } progress:^(CGFloat progress) {
        networkModel.progress(progress);
    }];
    return downloadOperation;
}







#pragma mark - - control operation methods
- (void) pauseQuery:(NSString*)key
{
    MEFNetworkModel *model = [self.modelCache objectForKey:key];
    if (![model isKindOfClass:[MEFDataNetworkModel class]]) {
        return;
    }
    AFDownloadRequestOperation *operaion = [self.operationCache objectForKey:key];
    [operaion pause];
}

- (void) resumeQuery:(NSString*)key
{
    MEFNetworkModel *model = [self.operationCache objectForKey:key];
    if (![model isKindOfClass:[MEFDataNetworkModel class]]) {
        return;
    }
    AFDownloadRequestOperation *operaion = [self.operationCache objectForKey:key];
    [operaion resume];
}

- (void) cancelQueries
{
    #if (MEF_NETWORK_LOG_ENABLE == 1)
        for (MEFNetworkModel *networkModel in self.modelCache.allValues) {
            NSString *string = [[NSString alloc]initWithFormat:@"Cancel query %@",networkModel.url];
            MEFNetworkLogDebug(@"Result from server %@",string);
        }
    #endif
    [self.operationQueue cancelAllOperations];
    [self.modelCache removeAllObjects];
    [self.operationCache removeAllObjects];
}

- (void) cancelQuery:(NSString*)key
{
    MEFNetworkModel *model = [self.modelCache objectForKey:key];
    if ([model isKindOfClass:[MEFGroupNetworkModel class]]) {
        for (MEFNetworkModel *submodel in [(MEFGroupNetworkModel*)model models]) {
            [self cancelQuery:submodel.hashOperation];
        }
    } else {
        NSOperation *operation =  [self.operationCache objectForKey:key];
        [operation cancel];
    }
    #if (MEF_NETWORK_LOG_ENABLE == 1)
        NSString *string = [[NSString alloc]initWithFormat:@"Cancel query %@",model.url];
        MEFNetworkLogDebug(@"Result from server %@",string);
    #endif

    [self.modelCache removeObjectForKey:key];
    [self.operationCache removeObjectForKey:key];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    [self cancelQueries];
    
}
@end
