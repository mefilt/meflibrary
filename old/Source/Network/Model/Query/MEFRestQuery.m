//
//  MEFQuery.m
//  Katusha
//
//  Created by Prokofev Ruslan on 12.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFRestQuery.h"
@implementation MEFQuery
- (RKRequestDescriptor*) requestDescriptor;
{
    RKObjectMapping *mapping = [self requestMapping];
    if (!mapping) {
        return nil;
    }
    RKRequestDescriptor *requestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:mapping objectClass:[self class] rootKeyPath:nil method:self.requestMethod];
    return requestDescriptor;
}
- (RKRequestMethod) requestMethod
{
    return RKRequestMethodPOST;
}

- (RKObjectMapping*) responseMapping
{
    return nil;
}

- (RKObjectMapping*) requestMapping
{
    return nil;
}
- (NSString*) module
{
    return @"";
}
@end

@implementation MEFRestQuery
- (id) init
{
    if (self = [super init]) {

    }
    return self;
}
- (NSArray*) responseDescriptors;
{
    RKObjectMapping *mapping = [self responseMapping];
    if (!mapping) {
        return nil;
    }
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:self.requestMethod pathPattern:nil keyPath:nil statusCodes:statusCodes];
    return @[responseDescriptor];
}

- (RKRequestDescriptor*) requestDescriptor;
{
    RKObjectMapping *mapping = [self requestMapping];
    if (!mapping) {
        return nil;
    }
    RKRequestDescriptor *requestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:mapping objectClass:[self class] rootKeyPath:nil method:self.requestMethod];
    return requestDescriptor;
}

- (RKRequestMethod) requestMethod
{
    return RKRequestMethodPOST;
}


- (RKObjectMapping*) requestMapping
{
    return nil;
}

- (BOOL) catchResponseError:(MEFRestResponse*) response
{
    return false;
}
@end


@implementation MEFRestDataQuery

- (NSString*) hashData
{
    return @"";
}
- (NSString*) filename
{
    return @"";
}
- (BOOL) checkInvalidData:(NSData*)data
{
    return false;
}
@end