//
//  MEFNetworkModel.h
//  Katusha
//
//  Created by Prokofev Ruslan on 12.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MEFRestQuery.h"
#import "MEFRestResponse.h"
#import "MEFError.h"

@interface MEFNetworkModel : NSObject
{
    NSString *_hashOperation;
}
@property (nonatomic, readwrite, strong) NSString *hashOperation;
@property (nonatomic, readwrite, strong) NSString *url;
@end




@interface MEFImageNetworkModel : MEFNetworkModel
@property (nonatomic, readwrite, copy) void (^succes) (UIImage *image);
@property (nonatomic, readwrite, copy) void (^error) (NSError *error);
@property (nonatomic, readwrite, strong) NSString *path;
@property (nonatomic, readwrite, strong) NSString *key;
@end



@interface MEFRestNetworkModel : MEFNetworkModel
@property (nonatomic, readwrite, strong) MEFRestQuery *query;
@property (nonatomic, readwrite, copy) void (^succes) (MEFRestResponse *response);
@property (nonatomic, readwrite, copy) void (^error) (MEFError *error);
@end


@interface MEFDataNetworkModel : MEFNetworkModel
@property (nonatomic, readwrite, strong) NSString *filename;
@property (nonatomic, readwrite, strong) NSString *hashData;
@property (nonatomic, readwrite, copy) void (^succes) (NSData *data, NSString *pathFile);
@property (nonatomic, readwrite, copy) void (^progress) (CGFloat progress);
@end

@interface MEFFileNetworkModel : MEFDataNetworkModel
@property (nonatomic, readwrite, copy) void (^error) (NSError *error);
@end


@interface MEFRestDataNetworkModel : MEFDataNetworkModel
@property (nonatomic, readwrite, strong) MEFRestDataQuery *query;
@property (nonatomic, readwrite, copy) void (^error) (MEFError *error);
@end







@interface MEFGroupNetworkModel : MEFNetworkModel
{
    
}
@property (nonatomic, readwrite, strong) NSMutableArray *responses;
@property (nonatomic, readwrite, strong) NSMutableArray *models;
@property (nonatomic, readwrite, strong) NSArray *queries;
@property (nonatomic, readwrite, copy) void (^succes) (NSArray *responses);
@property (nonatomic, readwrite, copy) void (^error) (MEFError *error, MEFQuery *query);
@end
