//
//  MEFNetworkManager.h
//  Katusha
//
//  Created by Prokofev Ruslan on 12.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MEFRestResponse.h"
#import "MEFRestQuery.h"
#import "MEFError.h"

#import <Reachability/Reachability.h>


static const CGFloat MEFNetworkManagerImageDiskCapacity =  200 * 1024 * 1024;
static const CGFloat MEFNetworkManagerDataDiskCapacity =  200 * 1024 * 1024;
static NSString * const MEFNetworkManagerReachabilityHostName = @"www.google.com";

typedef NS_ENUM(NSUInteger, MEFNMHandleRestErrorCallbackAction) {
    MEFNMHandleRestErrorCallbackActionNone,
    MEFNMHandleRestErrorCallbackActionRepeting,
    MEFNMHandleRestErrorCallbackActionError,
};

typedef NS_ENUM(NSUInteger, MEFNMHandleResponceErrorCallbackAction) {
    MEFNMHandleResponceErrorCallbackActionNone,
    MEFNMHandleResponceErrorCallbackActionRepeting,
    MEFNMHandleResponceErrorCallbackActionHook
};

@protocol MEFNetworkManagerProtocol <NSObject>


@optional
- (void) reachabilityInternetConnection:(NetworkStatus)networkStatus;
/**
 whether you want to handle the error
 
 @param MEFError error -  rest error
 @param MEFRestQuery query
 @return Method return BOOL if YES then handle error and call method handleRestError
 
 **/

- (BOOL) catchRestError:(MEFError*) error query:(MEFRestQuery*)qury;

/**
 
 @param MEFError error -  rest error
 @param MEFRestQuery query
 @param callback  - need call (callback) block and input flag isRepeatingQuery, When isRepeatingQuery is true, then query send repeat
 **/

- (void) handleRestError:(MEFError*) error query:(MEFRestQuery*)qury callback:(void (^)(MEFNMHandleRestErrorCallbackAction state)) callback;

- (void) handleResponseError:(MEFRestResponse*) response query:(MEFRestQuery*)qury callback:(void (^)(MEFNMHandleResponceErrorCallbackAction state)) callback;


@end


@interface MEFNetworkManager : NSObject
{

}
@property (nonatomic, readwrite, copy) NSString *serverURL;
@property (nonatomic, readwrite, copy) NSDictionary *HTTPHeaderFields;
@property (nonatomic, readwrite, weak) id <MEFNetworkManagerProtocol> delegate;
@property (nonatomic, readonly, strong) Reachability *reachability;

/**
 Send to server queries, To receive a response from the server is used block callback
 if one query returns an error, then the rest of the queries stop and call error block
 Method support MEFNetworkManagerProtocol
 
 @param Queries - array MEFRestQuery
 @param success - block have 1 argument responces it - array's MEFRestResponse
 @param error - block have 1 argumnet Error
 @return Method return hash key - queries, key is needed for the removal of  queries
 
 **/
- (NSString*) runRestQueries:(NSArray*)queries success:(void (^) (NSArray *responses))succes error:(void (^) (MEFError *error, MEFQuery *query))error;



- (void) runRestQueries:(NSArray*)queries hash:(NSString*)hash success:(void (^)(NSArray *))succes error:(void (^)(MEFError *error, MEFQuery *query))error;


/**
 Send to server query, To receive a response from the server is used block callback
 
 @param Query - MEFRestQuery
 @param success - block have 1 argument responces it - MEFRestResponse
 @param error - block have 1 argumnet Error
 @return Method return hash key - queries, key is needed for the removal of  query
 **/

- (NSString*) runRestQuery:(MEFRestQuery*)query success:(void (^) (MEFRestResponse *restResponse))succes error:(void (^) (MEFError *error))error;

/**
 Send to server query, To receive a response from the server is used block callback
 
 @param Query - MEFRestQuery
 @param success - block have 1 argument responces it - MEFRestResponse
 @param error - block have 1 argumnet Error
 @param URL - URL server
 @return Method return hash key - queries, key is needed for the removal of  query
 **/

- (NSString*) runRestQueryWithURL:(NSString*)URL query:(MEFRestQuery*)query success:(void (^) (MEFRestResponse *restResponse))succes error:(void (^) (MEFError *error))error;


/**
 Send to server query, To receive a response from the server is used block callback
 
 @param Query - MEFRestQuery
 @param success - block have 1 argument responces it - MEFRestResponse
 @param error - block have 1 argumnet Error
 @param URL - URL server
 @param HASH - Hash operation
 @return Method return hash key - queries, key is needed for the removal of  query
 **/

- (void) runRestQuery:(MEFRestQuery *)query hash:(NSString*)hash URL:(NSString*)URL success:(void (^)(MEFRestResponse *))succes error:(void (^)(MEFError *))error;
- (void) runRestQuery:(MEFRestQuery *)query hash:(NSString*)hash success:(void (^)(MEFRestResponse *))succes error:(void (^)(MEFError *))error;


- (void) runRestDataQuery:(MEFRestDataQuery *)query hashOperation:(NSString*)hashOperation URL:(NSString*)URL succes:(void (^) (NSData *data, NSString *pathFile)) succes error:(void (^)(MEFError *))error progress:(void (^) (CGFloat progress))progress;
- (NSString*) runRestDataQuery:(MEFRestDataQuery *)query URL:(NSString*)URL succes:(void (^) (NSData *data, NSString *pathFile)) succes error:(void (^)(MEFError *))error progress:(void (^) (CGFloat progress))progress;
- (NSString*) runRestDataQuery:(MEFRestDataQuery *)query succes:(void (^) (NSData *data, NSString *pathFile)) succes error:(void (^)(MEFError *))error progress:(void (^) (CGFloat progress))progress;


- (NSString*) downloadImageWithURL:(NSString*)url key:(NSString*)key success:(void (^) (UIImage *image))succes error:(void (^) (NSError *error))error;

- (NSString*) downloadFileWithURL:(NSString*)url hashData:(NSString*)hashData success:(void (^) (NSData *data, NSString *pathFile))succes error:(void (^) (NSError *error))errorBlock progress:(void (^) (CGFloat progress))progress;




- (void) pauseQuery:(NSString*)hash;
- (void) resumeQuery:(NSString*)hash;

- (void) cancelQueries;
- (void) cancelQuery:(NSString*)hash;
@end
