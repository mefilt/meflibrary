//
//  MEFStack.m
//  Katusha
//
//  Created by Prokofev Ruslan on 19.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFStack.h"

@implementation MEFStack

- (id) init
{
    if (self = [super init]) {
        _container = [NSMutableArray array];
    }
    return self;
}
- (void) removeAll
{
    [_container removeAllObjects];
}
- (id) push:(id) object
{
    NSAssert(object != nil, @"object == nil");
    [_container addObject:object];
    return object;
}

- (id) pop;
{
    id object = nil;
    if (!(object = _container.lastObject)) {
        return nil;
    }
    [_container removeLastObject];
    return object;
}

- (id) lastObject
{
    return [_container lastObject];
}

- (id) firstObject
{
    return [_container firstObject];
}
- (void) pushArray:(NSArray*) array
{
    [_container addObjectsFromArray:array];
}

- (NSEnumerator*) reverseObjectEnumerator
{
    return [_container reverseObjectEnumerator];
}

- (NSEnumerator*) objectEnumerator
{
    return [_container objectEnumerator];
}
- (NSUInteger) count
{
    return [_container count];
}
@end
