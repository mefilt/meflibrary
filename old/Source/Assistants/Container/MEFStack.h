//
//  MEFStack.h
//  Katusha
//
//  Created by Prokofev Ruslan on 19.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MEFStack : NSObject
{
    NSMutableArray *_container;
}

- (NSEnumerator*) objectEnumerator;
- (NSEnumerator*) reverseObjectEnumerator;
- (void) removeAll;
- (void) pushArray:(NSArray*) array;
- (id) push:(id) object;
- (id) pop;
- (id) lastObject;
- (id) firstObject;
- (NSUInteger) count;
@end
