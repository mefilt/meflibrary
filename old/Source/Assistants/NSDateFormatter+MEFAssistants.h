//
//  NSDateFormatter+MEFAssistants.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 17.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (MEFAssistants)

+ (NSDateFormatter*) currentDateFormatter;
+ (void) settingCurrentDateFormatter:(NSLocale*)locale timeZone:(NSTimeZone*)timeZone;
+ (NSString*) monthFromDate:(NSDate*)date;
+ (NSString*) yearFromDate:(NSDate*)date;
@end
