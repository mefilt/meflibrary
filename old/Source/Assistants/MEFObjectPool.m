//
//  MEFObjectPool.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 22.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFObjectPool.h"

@interface MEFObjectPoolContainer : NSObject
@property (nonatomic, readwrite, strong) NSMutableSet *objects;
@property (nonatomic, readwrite, strong) NSMutableSet *empty;
@property (nonatomic, readwrite, strong) Class class;
@property (nonatomic, readwrite, weak) MEFObjectPool *pool;
@end
@implementation MEFObjectPoolContainer
- (NSMutableSet*) objects
{
    if (!_objects) {
        _objects = [NSMutableSet set];
    }
    return _objects;
}
- (NSMutableSet*) empty
{
    if (!_empty) {
        _empty = [NSMutableSet set];
    }
    return _empty;
}

- (void) registerObject:(id) object
{
    [self.objects addObject:object];
    [self.empty addObject:object];
    [object addObserver:self forKeyPath:@"empty" options:NSKeyValueObservingOptionNew context:nil];
}

- (void) dealloc
{
    for (id object in self.objects) {
        [object removeObserver:self forKeyPath:@"empty"];
    }
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (![keyPath isEqualToString:@"empty"]) {
        return;
    }
    if ([object isEmpty]) {
        if ([object respondsToSelector:@selector(willReturnToPool:)]) {
            [object willReturnToPool:self.pool];
        }
        [self.empty addObject:object];
        if ([object respondsToSelector:@selector(didReturnToPool:)]) {
            [object didReturnToPool:self.pool];
        }
    } else {
        if ([object respondsToSelector:@selector(willPoolKickedOut:)]) {
            [object willPoolKickedOut:self.pool];
        }
        [self.empty removeObject:object];
        if ([object respondsToSelector:@selector(didPoolKickedOut:)]) {
            [object didPoolKickedOut:self.pool];
        }
    }
}
@end



@interface MEFObjectPool ()

@property (nonatomic, readwrite, strong) NSMutableDictionary *pool;
@end

@implementation MEFObjectPool


- (MEFObjectPoolContainer*) objectPoolContainerWithClass:(Class)class
{
    NSString *className = NSStringFromClass(class);
    MEFObjectPoolContainer *poolContainer = self.pool[className];
    if (!poolContainer) {
        poolContainer = [MEFObjectPoolContainer new];
        poolContainer.pool = self;
        poolContainer.class = class;
        self.pool[className] = poolContainer;
    }
    return poolContainer;
}

- (id) createObjectWithClass:(Class)myClass
{
    id <MEFObjectPooling> object = [myClass createInctance];
    [object setEmpty:true];
    return object;
}

- (void) logoutClass:(Class) myClass
{
    NSString *className = NSStringFromClass(myClass);
    [self.pool removeObjectForKey:className];
}


- (void) registerClass:(Class)myClass
{
    [self registerClass:myClass count:100];
}

- (void) registerClass:(Class)myClass count:(NSUInteger)count
{
    if (![myClass conformsToProtocol:@protocol(MEFObjectPooling)]) {
        @throw [NSException exceptionWithName:@"class not protocoling MEFObjectPooling" reason:@"Need implement MEFObjectPooling" userInfo:nil];
    }
    
    MEFObjectPoolContainer *objectPoolContainer = [self objectPoolContainerWithClass:myClass];
    for (int i = 0; i < count; i++) {
        id object = [self createObjectWithClass:myClass];
        [objectPoolContainer registerObject:object];
    }
}

- (id) emptyObjectWithClass:(Class) myClass
{
    MEFObjectPoolContainer *objectPoolContainer = [self objectPoolContainerWithClass:myClass];
    
    id <MEFObjectPooling> object = [objectPoolContainer.empty anyObject];
    if (!object) {
        object = [self createObjectWithClass:myClass];
        [objectPoolContainer registerObject:object];
    }
    [object setEmpty:false];
    
    
    
    return object;
}

- (NSMutableDictionary*) pool
{
    if (!_pool) {
        _pool = [NSMutableDictionary dictionary];
    }
    return _pool;
}
@end
