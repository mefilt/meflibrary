//
//  NSString+Network.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 10.09.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Network)
/**
 
 @param parameters - NSArray (NSDictionary) or Dictionary
**/
+ (NSString*) createURLWithParameters:(id)parameters url:(NSString*)url;
@end
