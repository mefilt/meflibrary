//
//  NSString+MEFUtilities.h
//  iphone_project
//
//  Created by Прокофьев Руслан on 01.08.13.
//  Copyright (c) 2013 Gevorg Petrosian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (MEFUtilities)
- (NSString *) URLEncodedString;
- (BOOL)isEmpty;
- (BOOL) isEmptyTrimmingWhiteSpace;
- (BOOL) isEqualToArray:(NSArray*)array;
- (NSString*) fileExtension;


- (NSNumber*) hashToNumber;
- (CGSize) sizeWithMyFont:(UIFont *)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode;
- (BOOL) containsString:(NSString*)string;


+ (UIFont*) fontForString:(NSString*)string toFitInRect:(CGRect)rect seedFont:(UIFont*)seedFont;
+ (NSString *)replaceInString:(NSString *)chaine pattern:(NSString *)pattern
                     template:(NSString *)template range:(NSRange*)range;
- (NSString *) stringByStrippingHTML;

- (NSArray *)rangesOfUppercaseLettersInString:(NSString *)str;


- (NSString*) shiftAllWordsOnNewLine;
@end

