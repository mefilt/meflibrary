//
//  MEFSVGImageSheet.m
//  MEFLibrary
//
//  Created by Mefilt on 26.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "MEFSVGImageSheet.h"
#import <SVGKit/SVGKit.h>

@interface MEFSVGImageSheet ()

@property (nonatomic, readwrite, strong) NSMutableDictionary *imageSheetsHashMap;
@property (nonatomic, readwrite, strong) NSMutableArray *imageSheets;
@end
@implementation MEFSVGImageSheet
static MEFSVGImageSheet *_SVGImageSheet = nil;
+ (MEFSVGImageSheet*) SVGImageSheet
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!_SVGImageSheet) {
            _SVGImageSheet = [[MEFSVGImageSheet alloc]init];
        }
    });

    return _SVGImageSheet;
}

- (id) init
{
    if (self = [super init]) {
        self.imageSheetsHashMap = [NSMutableDictionary dictionary];
        self.imageSheets = [NSMutableArray array];
        [self load];
    }
    return self;
}

- (void) load
{
    NSString *bundleRoot = [[NSBundle mainBundle] bundlePath];
    NSFileManager *manager = [NSFileManager defaultManager];
    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:bundleRoot];
    NSString *filename;
    @autoreleasepool {
    while ((filename = [direnum nextObject] )) {
        if ([filename hasSuffix:@".svg"]) {
            [self addSVGFileWithContentsOfFile:[[NSBundle mainBundle]pathForResource:filename ofType:@""]];
        }       
    }
    }
}

- (void) addSVGFileWithContentsOfFile:(NSString *)contentsOfFile
{
    SVGKImage *image = [[SVGKImage alloc]initWithContentsOfFile:contentsOfFile];
    [self.imageSheets addObject:image];
    [self.imageSheetsHashMap setObject:image forKey:[[contentsOfFile pathComponents]lastObject]];
}

- (SVGElement <ConverterSVGToCALayer> *) findElementFromSVGImage:(Element *)root identifier:(NSString*)identifier
{
    for (Element *element in root.childNodes) {
        if ([element isKindOfClass:[SVGElement class]] && [element conformsToProtocol:@protocol(ConverterSVGToCALayer)]) {
            SVGElement <ConverterSVGToCALayer> *elementSVG = (SVGElement <ConverterSVGToCALayer>*) element;
            if (elementSVG.identifier && [elementSVG.identifier isEqualToString:identifier]) {
                return elementSVG;
            }
        }
        SVGElement <ConverterSVGToCALayer> *child = [self findElementFromSVGImage:element identifier:identifier];
        if (child) {
            return child;
        }
    }
    return nil;
}

- (void) findChildAndCalculateSize:(SVGElement*)root parentLayer:(CALayer*)parentLayer
{
    CGSize rootSize = parentLayer.frame.size;
    for (Element *element in root.childNodes) {
        if ([element isKindOfClass:[SVGElement class]] && [element conformsToProtocol:@protocol(ConverterSVGToCALayer)]) {
            CALayer *layer = [(SVGElement <ConverterSVGToCALayer>*)element newLayer];
            [self findChildAndCalculateSize:(SVGElement*)element parentLayer:layer];
            [parentLayer addSublayer:layer];
            if (rootSize.width < CGRectGetMaxX(layer.frame)) {
                rootSize.width = CGRectGetMaxX(layer.frame);
            }
            if (rootSize.height < CGRectGetMaxY(layer.frame)) {
                rootSize.height = CGRectGetMaxY(layer.frame);
            }
        }
        
    }
    parentLayer.frame = CGRectMake(parentLayer.frame.origin.x, parentLayer.frame.origin.y, rootSize.width, rootSize.height);
    
}

- (UIImage*) imageWithLayer:(CALayer*)layer size:(CGSize)size
{
    
    UIGraphicsBeginImageContextWithOptions(size, FALSE, [UIScreen mainScreen].scale );
    CGContextRef context = UIGraphicsGetCurrentContext();
//    [self renderToContext:context layer:layer size:size antiAliased:true curveFlatnessFactor:1.0 interpolationQuality:kCGInterpolationDefault flipYaxis:FALSE];
    

    
//    [image renderToContext:context antiAliased:shouldAntialias curveFlatnessFactor:multiplyFlatness interpolationQuality:interpolationQuality flipYaxis:FALSE];
    
//    UIImage* result = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
    

    

    CGRect imageBounds = CGRectMake(0, 0, layer.frame.size.width, layer.frame.size.height);
    
//    NSLog(@"size %@, image %@",NSStringFromCGSize(size),NSStringFromCGSize(imageBounds.size));
    CGSize scaleConvertImageToView = CGSizeMake(size.width / imageBounds.size.width, size.height / imageBounds.size.height );
    CGFloat minScale = fminf(scaleConvertImageToView.width, scaleConvertImageToView.height);
//    NSString* formattedNumber = [NSString stringWithFormat:@"%.2f", minScale];
//    NSLog(@"bef size %f",minScale);
//    minScale = [formattedNumber floatValue];
    
//    NSLog(@"size %f",minScale);
    scaleConvertImageToView.width = scaleConvertImageToView.height = minScale;
    CGContextSaveGState(context);

    CGSize sizeCTM = CGSizeMake((size.width - imageBounds.size.width * scaleConvertImageToView.width) * 0.5, (size.height - imageBounds.size.height * scaleConvertImageToView.height) * 0.5);
//    NSLog(@"sizeCTM %@",NSStringFromCGSize(sizeCTM));
    CGContextTranslateCTM(context, sizeCTM.width, sizeCTM.height);
    CGContextScaleCTM( context, scaleConvertImageToView.width, scaleConvertImageToView.height);

    CGContextSetInterpolationQuality( context, 0);
    [layer renderInContext:context];
    CGContextRestoreGState(context);
    
    
    
    UIImage* result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}


- (UIImage*) imageWithSVGFilename:(NSString *)filename size:(CGSize)size
{
    CALayer *layer = [self layerWithSVGFilename:filename];
    if (!layer) {
        return nil;
    }
    return [self imageWithLayer:layer size:size];
}
    
- (UIImage*) imageWithIdentifier:(NSString*)identifier size:(CGSize)size
{
    CALayer *layer = [self layerWithIdentifier:identifier];
    if (!layer) {
        return nil;
    }
    return [self imageWithLayer:layer size:size];
}

- (CALayer*) layerWithSVGFilename:(NSString*)filename
{
    if ([filename rangeOfString:@"svg"].location == NSNotFound) {
        filename = [NSString stringWithFormat:@"%@.svg",filename];
    }
    SVGKImage *image = self.imageSheetsHashMap[filename];
    //    SVGElement <ConverterSVGToCALayer> *element = [self findElementFromSVGImage:image.DOMTree identifier:identifier];
    CALayer *layer = [image.DOMTree newLayer];
    [self findChildAndCalculateSize:image.DOMTree parentLayer:layer];
    return layer;
}

- (CALayer*) layerWithIdentifier:(NSString*)identifier
{
    for (SVGKImage *image in self.imageSheets) {
        
        SVGElement <ConverterSVGToCALayer> *element = [self findElementFromSVGImage:image.DOMTree identifier:identifier];
        if (!element) {
            continue;
        }
        CALayer *layer = [element newLayer];
        [self findChildAndCalculateSize:element parentLayer:layer];
        return layer;
    }
    return nil;
}



//-(CGContextRef) newCGContextAutosizedToFit
//{
//    NSAssert( [self hasSize], @"Cannot export this image because the SVG file has infinite size. Either fix the SVG file, or set an explicit size you want it to be exported at (by calling .size = something on this SVGKImage instance");
//    if( ! [self hasSize] )
//        return NULL;
//    
//    DDLogVerbose(@"[%@] DEBUG: Generating a CGContextRef using the current root-object's viewport (may have been overridden by user code): {0,0,%2.3f,%2.3f}", [self class], self.size.width, self.size.height);
//    
//    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//    CGContextRef context = CGBitmapContextCreate( NULL/*malloc( self.size.width * self.size.height * 4 )*/, self.size.width, self.size.height, 8, 4 * self.size.width, colorSpace, (CGBitmapInfo)kCGImageAlphaNoneSkipLast );
//    CGColorSpaceRelease( colorSpace );
//    
//    return context;
//}



- (void) renderToContext:(CGContextRef) context layer:(CALayer*)layer size:(CGSize)size antiAliased:(BOOL) shouldAntialias curveFlatnessFactor:(CGFloat) multiplyFlatness interpolationQuality:(CGInterpolationQuality) interpolationQuality flipYaxis:(BOOL) flipYaxis
{
    
    /** Typically a 10% performance improvement right here */
    if( !shouldAntialias )
        CGContextSetShouldAntialias( context, FALSE );
    
    /** Apple refuses to let you reset this, because they are selfish */
    CGContextSetFlatness( context, multiplyFlatness );
    
    /** Apple's own performance hints system */
    CGContextSetInterpolationQuality( context, interpolationQuality );
    
    /** Quartz, CoreGraphics, and CoreAnimation all use an "upside-down" co-ordinate system.
     This means that images rendered are upside down.
     
     Apple's UIImage class automatically "un-flips" this - but if you are rendering raw NSData (which is 5x-10x faster than creating UIImages!) then the flipping is "lost"
     by Apple's API's.
     
     The only way to fix it is to pre-transform by y = -y
     
     This is VERY useful if you want to render SVG's into OpenGL textures!
     */
    if (flipYaxis) {
        CGContextTranslateCTM(context, 0, size.height );
        CGContextScaleCTM(context, 1.0, -1.0);
    }
    

    [layer renderInContext:context];
    
    NSMutableString* perfImprovements = [NSMutableString string];
    if( shouldAntialias )
        [perfImprovements appendString:@" NO-ANTI-ALIAS"];
    if( perfImprovements.length < 1 )
        [perfImprovements appendString:@"NONE"];
    

}



@end
