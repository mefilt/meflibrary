//
//  NSCalendar+Current.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 13.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCalendar (MEFCurrent)

+ (void) settingCurrentCalendar:(NSLocale*)locale timeZone:(NSTimeZone*)timeZone;


+ (NSCalendar*) myCurrentCalendar;
+ (NSString*) monthFromDate:(NSDate*)date;

+ (NSUInteger) numberOfDaysInMonthCount:(NSDate*)date;

- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
- (NSInteger)monthsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
- (NSDate*) nowDateWithoutTime;
- (NSInteger)yearsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
@end
@interface NSCalendar (UTC)

+ (NSCalendar*) calendarUTC;
@end
