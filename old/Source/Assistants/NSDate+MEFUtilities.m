//
//  NSDate+Utilities.m
//  Additions
//
//  Created by Erica Sadun, http://ericasadun.com
//  iPhone Developer's Cookbook, 3.x and beyond
//  BSD License, Use at your own risk
/*
 #import <humor.h> : Not planning to implement: dateByAskingBoyOut and dateByGettingBabysitter
 ----
 General Thanks: sstreza, Scott Lawrence, Kevin Ballard, NoOneButMe, Avi`, August Joki. Emanuele Vulcano, jcromartiej
*/

#import "NSDate+MEFUtilities.h"

@implementation NSDate (MEFUtilities)

+ (NSDate*) dateFromString:(NSString *)string
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    NSDate *date = [dateFormatter dateFromString:string];

    return date;
}

- (NSComparisonResult) compareWithoutTime:(NSDate*)date2;
{
    NSDate *date1 = self;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger comps = (NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear);
    
    NSDateComponents *date1Components = [calendar components:comps
                                                    fromDate: date1];
    NSDateComponents *date2Components = [calendar components:comps
                                                    fromDate: date2];
    
    date1 = [calendar dateFromComponents:date1Components];
    date2 = [calendar dateFromComponents:date2Components];

    return [date1 compare:date2];
}


+ (NSDate *)dateWithOutTime:(NSDate *)datDate
{
//    if(datDate == nil) {
//        datDate = [NSDate date];
//    }
    NSDateComponents* comps = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:datDate];
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

#pragma mark Relative Dates

///////////////////////////////////////////////////////////////////////////////////////////////////
+ (NSDate *) dateWithDaysFromNow: (NSUInteger) days
{
	NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] + D_DAY * days;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return newDate;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
+ (NSDate *) dateWithDaysBeforeNow: (NSUInteger) days
{
	NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] - D_DAY * days;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return newDate;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
+ (NSDate *) dateTomorrow
{
	return [NSDate dateWithDaysFromNow:1];
}

- (NSDate *) dateTomorrow
{
    NSTimeInterval aTimeInterval = self.timeIntervalSinceReferenceDate + D_DAY;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return newDate;
}

- (NSDate *)dateYesterday
{
    NSTimeInterval aTimeInterval = self.timeIntervalSinceReferenceDate - D_DAY;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return newDate;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
+ (NSDate *) dateYesterday
{
	return [NSDate dateWithDaysBeforeNow:1];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
+ (NSDate *) dateWithHoursFromNow: (NSUInteger) dHours
{
	NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] + D_HOUR * dHours;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return newDate;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
+ (NSDate *) dateWithHoursBeforeNow: (NSUInteger) dHours
{
	NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] - D_HOUR * dHours;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return newDate;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
+ (NSDate *) dateWithMinutesFromNow: (NSUInteger) dMinutes
{
	NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] + D_MINUTE * dMinutes;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return newDate;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
+ (NSDate *) dateWithMinutesBeforeNow: (NSUInteger) dMinutes
{
	NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] - D_MINUTE * dMinutes;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return newDate;
}

+ (NSDate*) firstDayOfMonth:(NSDate *)date calendar:(NSCalendar *)calendar
{
    NSDateComponents *dateComponents = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitTimeZone | NSCalendarUnitWeekOfMonth  | NSCalendarUnitDay) fromDate:date];
    [dateComponents setDay:1];
    return [calendar dateFromComponents:dateComponents];
}

+ (NSDate *) lastDayOfMonth: (NSDate*) date calendar:(NSCalendar*)calendar
{
    NSDateComponents *dateComponents = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitTimeZone | NSCalendarUnitWeekOfMonth  | NSCalendarUnitDay) fromDate:date];
    
    [dateComponents setDay:[self numberOfDaysInMonthCount:date calendar:calendar]];
    return [calendar dateFromComponents:dateComponents];
}

+ (NSUInteger) numberOfDaysInMonthCount:(NSDate*)date calendar:(NSCalendar*)calendar;

{
    NSRange dayRange = [calendar rangeOfUnit:NSCalendarUnitDay
                                                            inUnit:NSCalendarUnitMonth
                                                           forDate:date];
    return dayRange.length;
}






+ (NSDate *) getFirstDayOfWeek: (NSDate *) value calendar:(NSCalendar*)calendar
{
    // Calculate first day in week

    NSDateComponents *weekdayComponents     = [calendar components:NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:value];
    
    //calcuate number of days to substract from today, in order to get the first day of the week. In this case, the first day of the week is monday. This is represented by adding 2 to the setDay.
    //Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6 and Saturday = 7. By adding more to this integers, you will go into the next week.
    NSDateComponents *componentsToSubtract  = [[NSDateComponents alloc] init];
    [componentsToSubtract setDay: (0 - ((weekdayComponents.weekday + 5) % 7))];
    [componentsToSubtract setHour: 0 - [weekdayComponents hour]];
    [componentsToSubtract setMinute: 0 - [weekdayComponents minute]];
    [componentsToSubtract setSecond: 0 - [weekdayComponents second]];
    
    //Create date for first day in week
    return [calendar dateByAddingComponents:componentsToSubtract toDate:value options:0];
}


+ (NSDate *) getFirstDayOfWeek: (NSDate *) value {
    // Calculate first day in week
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    return [self getFirstDayOfWeek:value calendar:gregorian];
}

+ (NSDate *) getLastDayOfWeek: (NSDate *) value
{
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    return [self getLastDayOfWeek:value calendar:gregorian];
}
+ (NSDate *) getLastDayOfWeek: (NSDate *) value calendar:(NSCalendar*)calendar
{
    // Calculate first day in week
    
    NSDateComponents *weekdayComponents = [calendar components:NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:value];
    
    //calcuate number of days to substract from today, in order to get the first day of the week. In this case, the first day of the week is monday. This is represented by adding 2 to the setDay.
    //Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6 and Saturday = 7. By adding more to this integers, you will go into the next week.
    NSDateComponents *componentsToSubtract  = [[NSDateComponents alloc] init];
    [componentsToSubtract setDay: (0 - ((weekdayComponents.weekday + 5) % 7))];
    [componentsToSubtract setHour: 0 - [weekdayComponents hour]];
    [componentsToSubtract setMinute: 0 - [weekdayComponents minute]];
    [componentsToSubtract setSecond: 0 - [weekdayComponents second]];
    
    //Create date for first day in week
    NSDate *beginningOfWeek = [calendar dateByAddingComponents:componentsToSubtract toDate:value options:0];
    //By adding 6 to the date of the first day, we can get the last day, in our example Sunday.
    NSDateComponents *componentsToAdd = [calendar components:NSCalendarUnitDay fromDate:beginningOfWeek];
    [componentsToAdd setDay:6];
    
    return [calendar dateByAddingComponents:componentsToAdd toDate:beginningOfWeek options:0];
}


- (NSDate *) dateByAddingMonths: (NSInteger) months calendar:(NSCalendar*)calendar
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setMonth:months];
    NSDate *newDate = [calendar dateByAddingComponents:components toDate:self options:DATE_COMPONENTS];
    return newDate;
}

- (NSDate *) dateBySubtractingMonths: (NSInteger) months calendar:(NSCalendar*)calendar
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setMonth:-months];
    NSDate *newDate = [calendar dateByAddingComponents:components toDate:self options:DATE_COMPONENTS];
    return newDate;
}

- (NSDate*) dateFewMonthEarlier:(int)count calendar:(NSCalendar*)calendar
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setMonth:count];
    NSDate *newDate = [calendar dateByAddingComponents:components toDate:self options:DATE_COMPONENTS];
    return newDate;
}


- (NSDate*) dateFewMonthEarlier:(int)count
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setMonth:count];
    NSDate *newDate = [CURRENT_CALENDAR dateByAddingComponents:components toDate:self options:DATE_COMPONENTS];
    return newDate;
}

- (NSDate*) dateFewMonthLater:(int)count
{
    return [self dateFewMonthEarlier:-count];
}

- (NSDate*) dateFewWeeksEarlier:(int)count
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setWeekOfYear: count];
    NSDate *newDate = [CURRENT_CALENDAR dateByAddingComponents:components toDate:self options:DATE_COMPONENTS];
    return newDate;
}

- (NSDate*) dateFewWeeksLater:(int)count
{
    return [self dateFewWeeksEarlier:-count];
}
#pragma mark Comparing Dates

- (BOOL) isFirstDayOfMonth
{
    return self.day == 1;
}

- (BOOL) isWeakday
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSRange weekdayRange = [calendar maximumRangeOfUnit:NSCalendarUnitWeekday];
    NSDateComponents *components = [calendar components:NSCalendarUnitWeekday fromDate:self];
    NSUInteger weekdayOfDate = [components weekday];
    if (weekdayOfDate == weekdayRange.location || weekdayOfDate == weekdayRange.length) {
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isEqualToDateIgnoringTime: (NSDate *) aDate
{
	NSDateComponents *components1 = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	NSDateComponents *components2 = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:aDate];
	return (([components1 year] == [components2 year]) &&
			([components1 month] == [components2 month]) &&
			([components1 day] == [components2 day]));
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isToday
{
	return [self isEqualToDateIgnoringTime:[NSDate date]];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isTomorrow
{
	return [self isEqualToDateIgnoringTime:[NSDate dateTomorrow]];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isYesterday
{
	return [self isEqualToDateIgnoringTime:[NSDate dateYesterday]];
}

// This hard codes the assumption that a week is 7 days
///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isSameWeekAsDate: (NSDate *) aDate
{
	NSDateComponents *components1 = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	NSDateComponents *components2 = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:aDate];

	// Must be same week. 12/31 and 1/1 will both be week "1" if they are in the same week
	if ([components1 weekOfYear] != [components2 weekOfYear]) return NO;

	// Must have a time interval under 1 week. Thanks @aclark
	return (fabs([self timeIntervalSinceDate:aDate]) < D_WEEK);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isThisWeek {
	return [self isSameWeekAsDate:[NSDate date]];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isNextWeek
{
	NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] + D_WEEK;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return [self isSameYearAsDate:newDate];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isLastWeek {
    NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] - D_WEEK;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return [self isSameYearAsDate:newDate];
}

+ (BOOL) isDateBelogsToThisWeek: (NSDate *) value {
    NSDate *thisWeek = [NSDate date];
    return [value isSameWeekAsDate:thisWeek];
}

+ (BOOL) isDateBelogsToLastWeek: (NSDate *) value {
    NSDate *lastweek = [NSDate dateWithDaysBeforeNow:7];
    return [value isSameWeekAsDate:lastweek];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isSameYearAsDate: (NSDate *) aDate
{
	NSDateComponents *components1 = [CURRENT_CALENDAR components:NSCalendarUnitYear fromDate:self];
	NSDateComponents *components2 = [CURRENT_CALENDAR components:NSCalendarUnitYear fromDate:aDate];
	return ([components1 year] == [components2 year]);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isThisYear
{
	return [self isSameYearAsDate:[NSDate date]];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isNextYear
{
	NSDateComponents *components1 = [CURRENT_CALENDAR components:NSCalendarUnitYear fromDate:self];
	NSDateComponents *components2 = [CURRENT_CALENDAR components:NSCalendarUnitYear fromDate:[NSDate date]];

	return ([components1 year] == ([components2 year] + 1));
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isLastYear
{
	NSDateComponents *components1 = [CURRENT_CALENDAR components:NSCalendarUnitYear fromDate:self];
	NSDateComponents *components2 = [CURRENT_CALENDAR components:NSCalendarUnitYear fromDate:[NSDate date]];

	return ([components1 year] == ([components2 year] - 1));
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isEarlierThanDate: (NSDate *) aDate
{
	return ([self earlierDate:aDate] == self);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) isLaterThanDate: (NSDate *) aDate
{
	return ([self laterDate:aDate] == self);
}

- (BOOL)isBetweenDate:(NSDate *)earlierDate andDate:(NSDate *)laterDate;
{
    // first check that we are later than the earlierDate.
    if ([self compare:earlierDate] == NSOrderedDescending) {
        
        // next check that we are earlier than the laterData
        if ( [self compare:laterDate] == NSOrderedAscending ) {
            return YES;
        }
    }
    
    // otherwise we are not
    return NO;
}



#pragma mark Adjusting Dates



///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSDate *) dateByAddingDays: (NSInteger) dDays
{
	NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + D_DAY * dDays;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return newDate;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSDate *) dateBySubtractingDays: (NSInteger) dDays
{
	return [self dateByAddingDays: (dDays * -1)];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSDate *) dateByAddingHours: (NSInteger) dHours
{
	NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + D_HOUR * dHours;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return newDate;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSDate *) dateBySubtractingHours: (NSInteger) dHours
{
	return [self dateByAddingHours: (dHours * -1)];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSDate *) dateByAddingMinutes: (NSInteger) dMinutes
{
	NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + D_MINUTE * dMinutes;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	return newDate;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSDate *) dateBySubtractingMinutes: (NSInteger) dMinutes
{
	return [self dateByAddingMinutes: (dMinutes * -1)];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSDate *) dateAtStartOfDay
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	[components setHour:0];
	[components setMinute:0];
	[components setSecond:0];
	return [CURRENT_CALENDAR dateFromComponents:components];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSDateComponents *) componentsWithOffsetFromDate: (NSDate *) aDate
{
	NSDateComponents *dTime = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:aDate toDate:self options:0];
	return dTime;
}

#pragma mark Retrieving Intervals

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) minutesAfterDate: (NSDate *) aDate
{
	NSTimeInterval ti = [self timeIntervalSinceDate:aDate];
	return (NSInteger) (ti / D_MINUTE);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) minutesBeforeDate: (NSDate *) aDate
{
	NSTimeInterval ti = [aDate timeIntervalSinceDate:self];
	return (NSInteger) (ti / D_MINUTE);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) hoursAfterDate: (NSDate *) aDate
{
	NSTimeInterval ti = [self timeIntervalSinceDate:aDate];
	return (NSInteger) (ti / D_HOUR);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) hoursBeforeDate: (NSDate *) aDate
{
	NSTimeInterval ti = [aDate timeIntervalSinceDate:self];
	return (NSInteger) (ti / D_HOUR);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) daysAfterDate: (NSDate *) aDate
{
	NSTimeInterval ti = [self timeIntervalSinceDate:aDate];
	return (NSInteger) (ti / D_DAY);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) daysBeforeDate: (NSDate *) aDate
{
	NSTimeInterval ti = [aDate timeIntervalSinceDate:self];
	return (NSInteger) (ti / D_DAY);
}

#pragma mark Decomposing Dates

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) nearestHour
{
	NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] + D_MINUTE * 30;
	NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
	NSDateComponents *components = [CURRENT_CALENDAR components:NSCalendarUnitHour fromDate:newDate];
	return [components hour];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) hour
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components hour];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) minute
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components minute];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) seconds
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components second];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) day
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components day];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) month
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components month];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) week
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components weekOfYear];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) weekday
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components weekday];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) nthWeekday // e.g. 2nd Tuesday of the month is 2
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components weekdayOrdinal];
}
///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger) year
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components year];
}

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [self daysBetweenDate:fromDateTime andDate:toDateTime calendar:calendar];
}
+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime calendar:(NSCalendar*)calendar;
{
    NSDate *fromDate;
    NSDate *toDate;
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day] + 1;
}
@end
