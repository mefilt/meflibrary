//
//  MEFObjectPool.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 22.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MEFObjectPool;
@protocol MEFObjectPooling <NSObject>


@required
+ (instancetype) createInctance;
@property (nonatomic, readwrite, assign, getter=isEmpty) BOOL empty;
@optional

- (void) willPoolKickedOut:(MEFObjectPool*)pool;
- (void) didPoolKickedOut:(MEFObjectPool*)pool;

- (void) willReturnToPool:(MEFObjectPool*)pool;
- (void) didReturnToPool:(MEFObjectPool*)pool;


@end
@interface MEFObjectPool : NSObject
- (void) registerClass:(Class) myClass count:(NSUInteger)count;
   - (void) logoutClass:(Class) myClass;
- (id) emptyObjectWithClass:(Class) myClass;
@end
