//
//  MEFAssistantsLibConstants.h
//  Assistants
//
//  Created by Prokofev Ruslan on 27.10.15.
//  Copyright © 2015 Prokofev Ruslan. All rights reserved.
//
#import <CocoaLumberjack/CocoaLumberjack.h>
#import <CocoaLumberjack/DDLogMacros.h>
#ifndef MEFAssistantsLibConstants_h
#define MEFAssistantsLibConstants_h

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


#define MEFString(int) [[NSString alloc]initWithFormat:@"%d",int]
#define MEFBlockWeakObjectWithName(name,o) __typeof__(o) __weak name = o;
#define MEFBlockWeakObject(o) __typeof__(o) __weak
#define MEFBlockStrongObject(o) __typeof__(o) __strong
#define MEFBlockWeakSelf MEFBlockWeakObject(self)

#define MEFAssistantsLogContext 10000



#define MEFAssistantsLogErrorError(frmt, ...)   LOG_MAYBE(NO,                DDLogLevelError, DDLogFlagError,   MEFAssistantsLogContext, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFAssistantsLogErrorWarn(frmt, ...)    LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelWarning, DDLogFlagWarning, MEFAssistantsLogContext, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFAssistantsLogErrorInfo(frmt, ...)    LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelInfo, DDLogFlagInfo,    MEFAssistantsLogContext, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFAssistantsLogErrorDebug(frmt, ...)   LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelDebug, DDLogFlagDebug,   MEFAssistantsLogContext, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFAssistantsLogErrorVerbose(frmt, ...) LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelVerbose, DDLogFlagVerbose, MEFAssistantsLogContext, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)

#endif /* MEFAssistantsLibConstants_h */
