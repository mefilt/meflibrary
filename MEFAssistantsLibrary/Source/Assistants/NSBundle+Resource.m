
//
//  NSBundle+Resource.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 09.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "NSBundle+Resource.h"
#import <CoreGraphics/CoreGraphics.h>
#import <CoreText/CoreText.h>
#import "MEFAssistantsConstants.h"
@implementation NSBundle (Resource)

+ (NSBundle*)libraryResourcesBundle
{
    static dispatch_once_t onceToken;
    static NSBundle *myLibraryResourcesBundle = nil;
    dispatch_once(&onceToken, ^{
         NSString* mainBundlePath = [[NSBundle mainBundle] resourcePath];
        MEFAssistantsLogErrorDebug(@"Library bundle path %@",mainBundlePath);
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"LibraryResource" withExtension:@"bundle"];
        myLibraryResourcesBundle = [NSBundle bundleWithURL:url];
        [NSBundle loadMyTTFFontsWithBundle:myLibraryResourcesBundle inDirectory:@""];
        
    });
    return myLibraryResourcesBundle;
}

+ (void) loadMyTTFFontsWithBundle:(NSBundle*)bundle inDirectory:(NSString*)directory
{
    NSArray *fonts = [bundle pathsForResourcesOfType:@"ttf" inDirectory:directory];    
    for (NSString *fontPath in fonts) {
        NSData *inData = [NSData dataWithContentsOfFile:fontPath];
        CFErrorRef error;
        CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)inData);
        CGFontRef font = CGFontCreateWithDataProvider(provider);
        if (! CTFontManagerRegisterGraphicsFont(font, &error)) {
            CFStringRef errorDescription = CFErrorCopyDescription(error);
            MEFAssistantsLogErrorError(@"Failed to load font: %@", errorDescription);
            CFRelease(errorDescription);
        }
        CFRelease(font);
        CFRelease(provider);
    }
}

+ (NSBundle*) myPreferredLanguageResourcesBundle
{
    static dispatch_once_t onceToken;
    static NSBundle *myLanguageResourcesBundle = nil;
    dispatch_once(&onceToken, ^ {
        NSBundle *libraryBundle = [NSBundle libraryResourcesBundle];
        NSString *langID = [[NSLocale currentLocale] localeIdentifier];
        NSString *path = [libraryBundle pathForResource:langID ofType:@"lproj"];
        myLanguageResourcesBundle = [NSBundle bundleWithPath:path];
        if (!myLanguageResourcesBundle) {
            NSString *path = [libraryBundle pathForResource:@"Base" ofType:@"lproj"];
            myLanguageResourcesBundle = [NSBundle bundleWithPath:path];
        }
      });
    
    return myLanguageResourcesBundle;
}



+ (NSString*) libraryLocalizedString:(NSString *)key comment:(NSString *)comment
{
    NSString *value = [[NSBundle myPreferredLanguageResourcesBundle] localizedStringForKey:key value:key table:nil];
//    NSLog(@"value %@",value)
    return value;
}

@end