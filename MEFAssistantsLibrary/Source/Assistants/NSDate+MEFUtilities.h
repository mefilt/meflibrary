  //
//  NSDate+Utilities.h
//  Additions
//
//  Created by Erica Sadun, http://ericasadun.com
//  iPhone Developer's Cookbook, 3.x and beyond
//  BSD License, Use at your own risk

#import <Foundation/Foundation.h>
//kCFCalendarUnitWeek
#define DATE_COMPONENTS (NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekOfYear  |  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal)
#define CURRENT_CALENDAR [NSCalendar currentCalendar]

#define D_MINUTE	60
#define D_HOUR		3600
#define D_DAY		86400
#define D_WEEK		604800
#define D_MONTH		2678400
#define D_YEAR		31556926

@interface NSDate (MEFUtilities)

+ (NSDate *)dateWithOutTime:(NSDate *)datDate;
// Relative dates from the current date
+ (NSDate *) dateTomorrow;
- (NSDate *) dateTomorrow;
+ (NSDate *) dateYesterday;
- (NSDate *) dateYesterday;
+ (NSDate *) dateWithDaysFromNow: (NSUInteger) days;
+ (NSDate *) dateWithDaysBeforeNow: (NSUInteger) days;
+ (NSDate *) dateWithHoursFromNow: (NSUInteger) dHours;
+ (NSDate *) dateWithHoursBeforeNow: (NSUInteger) dHours;
+ (NSDate *) dateWithMinutesFromNow: (NSUInteger) dMinutes;
+ (NSDate *) dateWithMinutesBeforeNow: (NSUInteger) dMinutes;

- (NSComparisonResult) compareWithoutTime:(NSDate*)date2;;
+ (NSDate *) getFirstDayOfWeek: (NSDate *) value calendar:(NSCalendar*)calendar;
+ (NSDate *) getFirstDayOfWeek: (NSDate *) value;
+ (NSDate *) getLastDayOfWeek: (NSDate *) value;
+ (NSDate *) getLastDayOfWeek: (NSDate *) value calendar:(NSCalendar*)calendar;

+ (NSDate *) firstDayOfMonth: (NSDate*) date calendar:(NSCalendar*)calendar;

+ (NSDate *) lastDayOfMonth: (NSDate*) date calendar:(NSCalendar*)calendar;
+ (NSUInteger) numberOfDaysInMonthCount:(NSDate*)date calendar:(NSCalendar*)calendar;

+ (BOOL) isDateBelogsToLastWeek: (NSDate *) value;
+ (BOOL) isDateBelogsToThisWeek: (NSDate *) value;

- (NSDate*) dateFewMonthEarlier:(int)count calendar:(NSCalendar*)calendar;
- (NSDate*) dateFewWeeksEarlier:(int)count;
- (NSDate*) dateFewWeeksLater:(int)count;
- (NSDate*) dateFewMonthEarlier:(int)count;
- (NSDate*) dateFewMonthLater:(int)count;

- (BOOL) isFirstDayOfMonth;
- (BOOL) isWeakday;
// Comparing dates
- (BOOL) isEqualToDateIgnoringTime: (NSDate *) aDate;
- (BOOL) isToday;
- (BOOL) isTomorrow;
- (BOOL) isYesterday;
- (BOOL) isSameWeekAsDate: (NSDate *) aDate;
- (BOOL) isThisWeek;
- (BOOL) isNextWeek;
- (BOOL) isLastWeek;
- (BOOL) isSameYearAsDate: (NSDate *) aDate;
- (BOOL) isThisYear;
- (BOOL) isNextYear;
- (BOOL) isLastYear;
- (BOOL) isEarlierThanDate: (NSDate *) aDate;
- (BOOL) isLaterThanDate: (NSDate *) aDate;

- (BOOL)isBetweenDate:(NSDate *)earlierDate andDate:(NSDate *)laterDate;

// Adjusting dates
- (NSDate *) dateByAddingMonths: (NSInteger) months calendar:(NSCalendar*)calendar;
- (NSDate *) dateBySubtractingMonths: (NSInteger) months calendar:(NSCalendar*)calendar;


- (NSDate *) dateByAddingDays: (NSInteger) dDays;
- (NSDate *) dateBySubtractingDays: (NSInteger) dDays;
- (NSDate *) dateByAddingHours: (NSInteger) dHours;
- (NSDate *) dateBySubtractingHours: (NSInteger) dHours;
- (NSDate *) dateByAddingMinutes: (NSInteger) dMinutes;
- (NSDate *) dateBySubtractingMinutes: (NSInteger) dMinutes;
- (NSDate *) dateAtStartOfDay;

// Retrieving intervals
- (NSInteger) minutesAfterDate: (NSDate *) aDate;
- (NSInteger) minutesBeforeDate: (NSDate *) aDate;
- (NSInteger) hoursAfterDate: (NSDate *) aDate;
- (NSInteger) hoursBeforeDate: (NSDate *) aDate;
- (NSInteger) daysAfterDate: (NSDate *) aDate;
- (NSInteger) daysBeforeDate: (NSDate *) aDate;

// converted @"yyyy-MM-dd'T'HH:mm:ssZ"
+ (NSDate*) dateFromString:(NSString *)string;
// Decomposing dates
+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime calendar:(NSCalendar*)calendar;


@property (readonly) NSInteger nearestHour;
@property (readonly) NSInteger hour;
@property (readonly) NSInteger minute;
@property (readonly) NSInteger seconds;
@property (readonly) NSInteger day;
@property (readonly) NSInteger month;
@property (readonly) NSInteger week;
@property (readonly) NSInteger weekday;
@property (readonly) NSInteger nthWeekday; // e.g. 2nd Tuesday of the month == 2
@property (readonly) NSInteger year;
@end
