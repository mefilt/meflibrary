//
//  NSCalendar+Current.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 13.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "NSCalendar+MEFCurrent.h"

@implementation NSCalendar (MEFCurrent)
static NSLocale *_currentLocale;
static NSTimeZone *_currentTimeZone;
+ (void) settingCurrentCalendar:(NSLocale*)locale timeZone:(NSTimeZone*)timeZone
{
    _currentTimeZone = timeZone;
    _currentLocale = locale;
}
+ (NSCalendar*) myCurrentCalendar
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    if (_currentLocale) {
        [calendar setLocale:_currentLocale];
    }
    if (_currentTimeZone) {
        [calendar setTimeZone:_currentTimeZone];
    }

    return calendar;
}
+ (NSString*) monthFromDate:(NSDate*)date
{
    NSDateComponents *components = [[NSCalendar myCurrentCalendar]  components:NSCalendarUnitMonth fromDate:date];
    
    return [[self myCurrentCalendar]monthSymbols][[components month] - 1];
    
}
+ (NSUInteger) numberOfDaysInMonthCount:(NSDate*)date
{
    NSRange dayRange = [[NSCalendar myCurrentCalendar] rangeOfUnit:NSCalendarUnitDay
                              inUnit:NSCalendarUnitMonth
                             forDate:date];
    return dayRange.length;
}

- (NSDate*) nowDateWithoutTime
{
    NSDateComponents *components = [self components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    NSDate *now = [self dateFromComponents:components];
    return now;
}

- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    [self rangeOfUnit: NSCalendarUnitDay startDate:&fromDate
             interval:NULL forDate:fromDateTime];
    [self rangeOfUnit: NSCalendarUnitDay startDate:&toDate
             interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [self components: NSCalendarUnitDay
                                           fromDate:fromDate toDate:toDate options:0];
    
    NSInteger days = labs([difference day]);
    return days;
    
}
- (NSInteger)monthsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    [self rangeOfUnit: NSCalendarUnitMonth startDate:&fromDate
                         interval:NULL forDate:fromDateTime];
    [self rangeOfUnit: NSCalendarUnitMonth startDate:&toDate
                         interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [self components: NSCalendarUnitMonth
                                                       fromDate:fromDate toDate:toDate options:0];
    
    NSInteger months = labs([difference month]) + 1;
    return months;
    
}

- (NSInteger)yearsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    [self rangeOfUnit:NSCalendarUnitYear startDate:&fromDate
             interval:NULL forDate:fromDateTime];
    [self rangeOfUnit:NSCalendarUnitYear startDate:&toDate
             interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [self components:NSCalendarUnitYear
                                           fromDate:fromDate toDate:toDate options:0];
    
    NSInteger months = [difference year];
    return months;
    
}
@end


@implementation NSCalendar (UTC)
//static NSCalendar *_currentUTCCalendar;
+ (NSCalendar*) calendarUTC
{
    NSCalendar *calendarUTC = [NSCalendar currentCalendar];
    calendarUTC.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    return calendarUTC;
}
@end
