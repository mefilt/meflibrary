//
//  NSString+MEFUtilities.m
//  iphone_project
//
//  Created by Прокофьев Руслан on 01.08.13.
//  Copyright (c) 2013 Gevorg Petrosian. All rights reserved.
//
#import "NSString+MEFUtilities.h"
@implementation NSString (MEFUtilities)

- (NSString *) URLEncodedString
{
    NSMutableString * output = [NSMutableString string];
    const char * source = [self UTF8String];
    NSInteger sourceLen = strlen(source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = (const unsigned char)source[i];
        if (false && thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}
- (BOOL) isEqualToArray:(NSArray *)array
{
    for (NSString* str in array) {
        if ([str isEqualToString:self]) {
            return true;
        }
    }
    return false;
}

- (BOOL)containsString:(NSString*)string
{
    if (!string) {
        return false;
    }
    return [[self lowercaseString] rangeOfString:[string lowercaseString]].location != NSNotFound;
}

- (BOOL) isEmptyTrimmingWhiteSpace
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[self stringByTrimmingCharactersInSet: set] length] == 0) {
        return true;
    }
    return false;
}


- (BOOL) isEmpty
{
    if (!self) {
        return true;
    }
    if ([self isKindOfClass:[NSNull class]]) {
        return true;
    }
    if ([self isMemberOfClass:[NSNull class]]) {
        return true;
    }
    if ([self class] == [NSNull class]) {
        return true;
    }
    if ([self isEqualToString:@"<null>"]) {
        return true;
    }
    if ([self isEqualToString:@"(null)"]) {
        return true;
    }
    if ([self isEqual:[NSNull null]]) {
        return true;
    }
    if (self.length == 0) {
        return true;
    }
    return false;
}




- (NSString*) fileExtension
{
    NSString *fileName = [self copy];
    NSString *file = [fileName stringByDeletingPathExtension];
    NSString *fileExtension = [fileName stringByReplacingCharactersInRange:NSMakeRange(0, file.length) withString:@""];
    return fileExtension;
}



- (NSNumber *)hashToNumber
{
    NSUInteger hash = [self hash];
    NSNumber *hashNumber = [NSNumber numberWithUnsignedInteger:hash];
    return hashNumber;
}

- (CGSize) sizeWithMyFont:(UIFont *)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode
{
    CGSize result = CGSizeZero;
        if ([self respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)]) {
            NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.lineBreakMode = lineBreakMode;
            paragraphStyle.alignment = NSTextAlignmentLeft;
            
            NSDictionary * attributes = @{NSFontAttributeName : font,
                                          NSParagraphStyleAttributeName : paragraphStyle};
            result = [self boundingRectWithSize:size options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin
                                           attributes:attributes
                                              context:nil].size;
            result.width = ceilf(result.width);
            result.height = ceilf(result.height);
        } else {
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Wdeprecated-declarations"
            result = [self sizeWithFont:font
                            constrainedToSize:size
                                lineBreakMode:lineBreakMode];
        #pragma clang diagnostic pop
        }
    
    return result;
}


+ (UIFont*) fontForString:(NSString*)string toFitInRect:(CGRect)rect seedFont:(UIFont*)seedFont
{
    UIFont* returnFont = seedFont;
    CGSize stringSize = [string sizeWithMyFont:returnFont constrainedToSize:rect.size lineBreakMode:NSLineBreakByWordWrapping];
    
    while(stringSize.width > rect.size.width){
        returnFont = [UIFont systemFontOfSize:returnFont.pointSize -1];
        stringSize = [string sizeWithMyFont:returnFont constrainedToSize:rect.size lineBreakMode:NSLineBreakByWordWrapping];
    }
    
    return returnFont;
}
+ (NSString *)replaceInString:(NSString *)chaine pattern:(NSString *)pattern
                     template:(NSString *)template range:(NSRange*)range
{
    NSMutableString *chaineMutable = [[NSMutableString alloc] initWithString:chaine];
    NSMutableString *chaineMutableForRange = [[NSMutableString alloc] initWithString:chaine];
    NSRegularExpression *regex = [[NSRegularExpression alloc] init];
    
    regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                      options:NSRegularExpressionCaseInsensitive error:NULL];

    
    *range = [regex rangeOfFirstMatchInString:chaineMutableForRange options:NSMatchingReportProgress range:NSMakeRange(0, [chaine length])];
    
    [regex replaceMatchesInString:(NSMutableString *)chaineMutable
                          options:NSMatchingReportProgress range:NSMakeRange(0, [chaine length])
                     withTemplate:template];
    NSString *returnedString = [[NSString alloc] initWithString:chaineMutable];
    
    return returnedString;
}


- (NSString *) stringByStrippingHTML
{
    NSRange r;
    NSString *s = [self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}



- (NSArray *)rangesOfUppercaseLettersInString:(NSString *)str
{
    NSCharacterSet *cs = [NSCharacterSet uppercaseLetterCharacterSet];
    NSMutableArray *results = [NSMutableArray array];
    NSScanner *scanner = [NSScanner scannerWithString:str];
    while (![scanner isAtEnd]) {
        [scanner scanUpToCharactersFromSet:cs intoString:NULL]; // skip non-uppercase characters
        NSString *temp;
        NSUInteger location = [scanner scanLocation];
        if ([scanner scanCharactersFromSet:cs intoString:&temp]) {
            // found one (or more) uppercase characters
            NSRange range = NSMakeRange(location, [temp length]);
            [results addObject:[NSValue valueWithRange:range]];
        }
    }
    return results;
}

- (NSString*) shiftAllWordsOnNewLine
{
    NSMutableString *mutalbleStr = [NSMutableString new];
    NSArray *array = [self componentsSeparatedByString:@" "];
    for (NSString *st in array) {
        [mutalbleStr appendString:st];
        if ([array lastObject] != st) {
            [mutalbleStr appendString:@"\n"];
        }
    }
    return mutalbleStr;
}
@end
