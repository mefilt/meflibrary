//
//  NSString+Network.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 10.09.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "NSString+Network.h"
#import "NSString+MEFUtilities.h"
@implementation NSString (Network)

+ (NSString*) createURLWithParameter:(NSDictionary*)parameter
{
    NSMutableString *keyParam = [NSMutableString string];
    for (NSString *key in parameter) {
        NSString *value = parameter[key];
        if ([[value description] isEmpty] ) {
            [keyParam appendString:[NSString stringWithFormat:@"%@&",key]];
        } else {
            value = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)value, NULL, (__bridge CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ", CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)));
            
            [keyParam appendString:[NSString stringWithFormat:@"%@=%@&",key,value]];
        }
    }
    return keyParam;
}

+ (NSString*) createURLWithParameters:(id)parameters url:(NSString*)url
{
    if (!parameters) {
        return url;
    }
    if ([parameters isKindOfClass:[NSArray class]] && parameters && [(NSArray*)parameters count]) {
        NSMutableString *keyParam = [NSMutableString string];
        for (NSDictionary *parameter in parameters) {
            [keyParam appendString:[self createURLWithParameter:parameter]];
        }
        [keyParam deleteCharactersInRange:NSMakeRange(fmax(keyParam.length - 1,0), 1)];
        return [NSString stringWithFormat:@"%@?%@",url,keyParam];
    } else if ([parameters isKindOfClass:[NSDictionary class]]  && [(NSDictionary*)parameters count]) {
        NSMutableString *keyParam = [NSMutableString string];
        [keyParam appendString:[self createURLWithParameter:parameters]];
        [keyParam deleteCharactersInRange:NSMakeRange(fmax(keyParam.length - 1,0), 1)];
        return [NSString stringWithFormat:@"%@?%@",url,keyParam];
    }
    return url;
}

@end
