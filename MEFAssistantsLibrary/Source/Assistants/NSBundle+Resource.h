//
//  NSBundle+Resource.h
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 09.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
#define MEFLibraryLocalizedString(__key__,__comment__) [NSBundle libraryLocalizedString:__key__ comment:__comment__]
@interface NSBundle (Resource)

+ (NSBundle*)libraryResourcesBundle;
+ (void) loadMyTTFFontsWithBundle:(NSBundle*)bundle inDirectory:(NSString*)directory;
+ (NSString*) libraryLocalizedString:(NSString*)key comment:(NSString*)comment;
@end


//
//"com.mefilt.controller.calendar.segmnetcontrol.button.day" = "Day";
//"com.mefilt.controller.calendar.segmnetcontrol.button.week" = "Week";
//"com.mefilt.controller.calendar.segmnetcontrol.button.month" = "Month";
//"com.mefilt.controller.calendar.segmnetcontrol.button.interval" = "Interval";
