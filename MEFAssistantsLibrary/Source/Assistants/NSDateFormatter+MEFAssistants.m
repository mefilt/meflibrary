//
//  NSDateFormatter+MEFAssistants.m
//  MEFLibrary
//
//  Created by Prokofev Ruslan on 17.07.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import "NSDateFormatter+MEFAssistants.h"

@implementation NSDateFormatter (MEFAssistants)

static NSDateFormatter *_mefstaticDateFormatter;
+ (NSDateFormatter*) currentDateFormatter
{
    if (!_mefstaticDateFormatter) {
        _mefstaticDateFormatter = [NSDateFormatter new];
    }
    return _mefstaticDateFormatter;
}


+ (void) settingCurrentDateFormatter:(NSLocale*)locale timeZone:(NSTimeZone*)timeZone
{
    [[self currentDateFormatter]setLocale:locale];
    [[self currentDateFormatter]setTimeZone:timeZone];
}

+ (NSString*) monthFromDate:(NSDate*)date
{
    [[self currentDateFormatter] setDateFormat:@"MMMM"];
    return [[self currentDateFormatter]stringFromDate:date];
}
+ (NSString*) yearFromDate:(NSDate*)date
{
    [[self currentDateFormatter] setDateFormat:@"YYYY"];
    return [[self currentDateFormatter]stringFromDate:date];
}

@end
