
#  Be sure to run `pod spec lint MEFAssistants.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "MEFLibrary"
  s.version      = "0.1.3"
  s.summary      = "A short description of MEFAssistants."
  s.requires_arc = true
  s.description  = <<-DESC
                   A longer description of MEFAssistants in Markdown format.

                   * Think: Why did you write this? What is the focus? What does it do?
                   * CocoaPods will be using this to generate tags, and improve search results.
                   * Try to keep it short, snappy and to the point.
                   * Finally, don't worry about the indent, CocoaPods strips it!
                   DESC

  s.homepage     = "http://mefilt.com"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See http://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  #s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  s.author             = { "Mefilt" => "Mefilt@gmail.com" }
  # Or just: s.author    = "Mefilt"
  # s.authors            = { "Mefilt" => "Mefilt@gmail.com" }
  # s.social_media_url   = "http://twitter.com/Mefilt"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

  # s.platform     = :ios
  s.platform     = :ios, "7.0"

  #  When using multiple platforms
  # s.ios.deployment_target = "5.0"
  # s.osx.deployment_target = "10.7"
  # s.watchos.deployment_target = "2.0"


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  s.source       = { :git => "https://mefilt@bitbucket.org/MEFLibrary/assistants.git"}


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  # s.source_files  = "Assistants/Assistants/Source/**/*.{h,m}"
  # s.exclude_files = "Classes/Exclude"

  # s.public_header_files = "Classes/**/*.h"

  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # s.resource  = "icon.png"
  # s.resources = "Resources/*.png"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # s.framework  = "SomeFramework"
  # s.frameworks = "SomeFramework", "AnotherFramework"

  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }

 	s.subspec 'UI' do |ui|
	 	ui.platform     = :ios, "7.0"
		ui.source_files  = "MEFUILibrary/Source/**/*.{h,m}", "MEFUILibrary/MEFUILibrary.h"
    	ui.dependency 'CocoaLumberjack'
    	ui.dependency 'TTTAttributedLabel'
    	ui.dependency 'Masonry'
        ui.resource_bundles = {
			'LibraryResource' => ['LibraryResource/Resource/**/*.{xib,png,jpeg,jpg,ttf,lproj}', 'LibraryResource/Resource/*.{lproj}'],         
			'Test' => ['LibraryResource/Resource/XIB/*.xib']         		
       	}

		ui.subspec 'Base' do |base|
	 		base.platform     = :ios, "7.0"
			base.source_files  = "MEFUILibrary/Source/UI/Asisstants/*.{h,m}", "MEFUILibrary/Source/UI/Controllers/BaseCollectionVIewController/*.{h,m}", "MEFUILibrary/MEFUIConstants.h", "MEFUILibrary/Source/UI/Controllers/BaseTableViewController/*.{h,m}", "MEFUILibrary/Source/UI/Models/*.{h,m}", "MEFUILibrary/Source/UI/Controllers/ViewController/*.{h,m}","MEFUILibrary/Source/UI/Controllers/AdaptiveNavigationController/*.{h,m}"
     	    base.resource_bundles = {
				'LibraryResource' => ['LibraryResource/Resource/**/*.{xib,png,jpeg,jpg,ttf,lproj}', 'LibraryResource/Resource/*.{lproj}'],         
					'Test' => ['LibraryResource/Resource/XIB/*.xib']         		
			}
  		end
  	end


	


  	s.subspec 'Assistants' do |assis|
		assis.source_files  = "MEFAssistantsLibrary/Source/**/*.{h,m}", "MEFAssistantsLibrary/MEFAssistantsLibrary.h"
    	assis.dependency 'CocoaLumberjack'    	
  	end
  	s.subspec 'Network' do |net|
		net.source_files  = "MEFNetworkLibrary/Source/**/*.{h,m}", "MEFNetworkLibrary/MEFNetworkLibrary.h"
    	net.dependency 'CocoaLumberjack'
    	net.dependency 'AFDownloadRequestOperation', '~> 1.1.0'
		net.dependency 'Reachability'
		net.dependency 'Haneke'
		net.dependency 'RestKit'
		net.dependency 'MEFLibrary/Assistants'
  	end
  
	

end
