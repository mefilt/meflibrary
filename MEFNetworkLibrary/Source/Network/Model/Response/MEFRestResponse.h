//
//  MEFRestResponse.h
//  Katusha
//
//  Created by Prokofev Ruslan on 17.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
@class MEFRestQuery;
@interface MEFRestResponse : NSObject
{

}
@property (nonatomic, readwrite, strong) RKMappingResult *mappingResult;
@property (nonatomic, readwrite, strong) id response;
@property (nonatomic, readwrite, strong) MEFRestQuery *query;
@end
