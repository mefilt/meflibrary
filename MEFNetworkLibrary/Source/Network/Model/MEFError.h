//
//  BSError.h
//  Katusha
//
//  Created by Prokofev Ruslan on 12.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MEFError : NSObject
{

}
@property (nonatomic, readwrite, assign) NSInteger HTTPStatusCode;
@property (nonatomic, readwrite, strong) NSError *error;
@end
