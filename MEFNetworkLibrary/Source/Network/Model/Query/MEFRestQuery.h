//
//  MEFQuery.h
//  Katusha
//
//  Created by Prokofev Ruslan on 12.03.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "MEFRestResponse.h"

@interface MEFQuery : NSObject

- (RKRequestDescriptor*) requestDescriptor;
- (RKRequestMethod) requestMethod;
- (NSString*) module;
@property (nonatomic, readwrite, strong) NSDictionary *HTTPHeaderFields;
@end


@interface MEFRestQuery : MEFQuery
- (NSArray*) responseDescriptors;
@end



@interface MEFRestQuery (Abstract)
- (BOOL) catchResponseError:(MEFRestResponse*) response;
- (RKObjectMapping*) responseMapping;
- (RKObjectMapping*) requestMapping;
@end


@interface MEFRestDataQuery : MEFQuery
@end

@interface MEFRestDataQuery (Abstract)
- (BOOL) checkInvalidData:(NSData*)data;
- (NSString*) hashData;
- (NSString*) filename;
@end