//
//  MEFConstants.h
//  MEFLibrary
//
//  Created by Mefilt on 15.05.15.
//  Copyright (c) 2015 Prokofev Ruslan. All rights reserved.
//
#import <CocoaLumberjack/CocoaLumberjack.h>
#import <CocoaLumberjack/DDLogMacros.h>
#ifndef MEFLibrary_MEFConstants_h
#define MEFLibrary_MEFConstants_h


const static CGFloat MEFAnimationDurationSlow = 0.5f;

#define MEF_NETWORK_LOG_ENABLE 1


typedef NS_OPTIONS(NSUInteger, MEFLogContext) {
    MEFLogContextNone = 0,
    MEFLogContextNetwork = 800000,
    MEFLogContextGeneral,
};




#define MEFNetworkLogError(frmt, ...)   LOG_MAYBE(NO,                DDLogLevelError, DDLogFlagError,   MEFLogContextNetwork, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFNetworkLogWarn(frmt, ...)    LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelWarning, DDLogFlagWarning, MEFLogContextNetwork, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFNetworkLogInfo(frmt, ...)    LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelInfo, DDLogFlagInfo,    MEFLogContextNetwork, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFNetworkLogDebug(frmt, ...)   LOG_MAYBE(NO, DDLogLevelDebug, DDLogFlagDebug,   MEFLogContextNetwork, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define MEFNetworkLogVerbose(frmt, ...) LOG_MAYBE(LOG_ASYNC_ENABLED, DDLogLevelVerbose, DDLogFlagVerbose, MEFLogContextNetwork, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)







#endif



